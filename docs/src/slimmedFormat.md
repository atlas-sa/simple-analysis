# Slimmed Ntuple Format

The slimmed ntuples contain a simple ROOT tree containing the full information used by SimpleAnalysis.
This is fairly easy to create as for example done in the `Delphes2SA.py` script. The variables needed are listed
and explained in the table below. The most complicated variable is "`id`" variable which is a 32-bit pattern used to
defined which particle identification criteria an object passes. The definition varies by object type and for the detailed list,
one needs to consult the [AnalysisObject.h](https://gitlab.cern.ch/atlas-sa/framework/-/blob/master/SimpleAnalysisFramework/AnalysisObject.h)
header file.

|  Group   | Variables | Description |
| -------- | --------- | ----------- |
| Electrons ("el_") | pt, eta, phi, charge, id, motherID | Momentum components, charge, id bit patttern, pdgId of parent particle |
| Muons     ("mu_") | pt, eta, phi, charge, id, motherID | as above |
| Taus     ("tau_") | pt, eta, phi, charge, id, motherID | as above |
| Photons  ("ph_")  | pt, eta, phi, charge, id, motherID | as above |
| B-hadrons ("bhadron_") | pt, eta, phi, charge, id, motherID | as above |
| Jets     ("jet_") | pt, eta, phi, m, id    | anti-kt r=0.4 jet four momenta, id bit pattern. Charge and parent pdgID is not used |
| Track jets  "trackjet_") | pt, eta, phi, m, id, motherID      | variable radius jet from charge particles only |
| Large-R jets ("fatjet_") | pt, eta, phi, m, charge, id, motherID      | Trimmed, anti-kt r=1.0 jets. Charge is used for D2*1e5 |
| Etmiss ("met_")   | pt, phi | Missing transverse momentum and its angle |
| Hard-scatter truth ("HSTruth_") | pt, eta, phi, charge, m, id, motherID| Four momentum, charge and pdgId of SUSY particles, b/t quarks, W/Z/h bosons |
|                   | prodVx, prodVy, prodVx, decayVx, decayVy, decayVz | Production and decay vertex location |
| pdf               | id1, x1, pdf1, id1, x1, pdf1, scale | pdgId of colliding partons, their momentum fraction, Q-scale and PDF value |
| other             | sumet, genHT, genMET          | Scalar sum of transverse momenta of all visible particles, jets, and invisible particles |
|                   | Event, eventWeight, mcWeights | Event number, default event weight and optionally alternative events weights from PDF variations |
|                   | mcChannel, mcVetoCode, susyChannel | Internal ATLAS numbering |
