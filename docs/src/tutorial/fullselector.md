---
title: The full analysis selector
---

Because copy-pasting a set of lines sometimes goes wrong, instead of trying to debug which line you forgot to copy, or which line was not correctly pasted, here is the full analysis selector that you will need in the following:

```cpp
#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(MyAnalysisName)

void MyAnalysisName::Init() {

    // Define signal regions
    addRegions({"SR_h_Low_bin1", "SR_h_Low_bin2", "SR_h_Low_bin3"});
    addRegions({"SR_h_Med_bin1", "SR_h_Med_bin2", "SR_h_Med_bin3"});
    addRegions({"SR_h_High_bin1", "SR_h_High_bin2", "SR_h_High_bin3"});

    addRegion("preselection");

    // Init some histograms
    addHistogram("hist_met",100,0,1000); //1D
    addHistogram("hist_mt",100,0,1000); //1D
    addHistogram("hist_metvsmt",100,0,1000,100,0,1000); //2D
}

void MyAnalysisName::ProcessEvent(AnalysisEvent *event) {

    auto baselineElectrons = event->getElectrons(7.0, 2.47, ELooseBLLH);
    auto baselineMuons = event->getMuons(6.0, 2.70, MuMedium | MuNotCosmic | MuZ05mm | MuQoPSignificance);

    auto baselineJets = event->getJets(20.0, 4.5);

    auto met_Vect  = event->getMET(); //actually returns the 4-vector
    float met = met_Vect.Pt();
    auto weights = event->getMCWeights();

    auto radiusCalcLep = [] (const AnalysisObject& lep,const AnalysisObject&) {
        return (0.04 + 10/lep.Pt()) > 0.4 ? 0.4 : 0.04 + 10/lep.Pt();
      };

    baselineElectrons = overlapRemoval(baselineElectrons, baselineMuons, 0.01);

    baselineJets = overlapRemoval(baselineJets, baselineElectrons, 0.2);
    baselineElectrons = overlapRemoval(baselineElectrons, baselineJets, radiusCalcLep);

    baselineJets = overlapRemoval(baselineJets, baselineMuons, 0.2, LessThan3Tracks);
    baselineMuons = overlapRemoval(baselineMuons, baselineJets, radiusCalcLep);

    auto signalElectrons = filterObjects(baselineElectrons, 7.0, 2.47, ETightLH | ED0Sigma5  | EZ05mm  | EIsoFixedCutLoose);
    auto signalMuons = filterObjects(baselineMuons, 6.0, 2.7, MuD0Sigma3 | MuIsoFixedCutLoose);

    auto signalLeptons = signalElectrons + signalMuons;
    auto baselineLeptons = baselineElectrons + baselineMuons;

    auto signalJets = filterObjects(baselineJets, 30.0, 2.80, JVT120Jet);
    auto signalBJets = filterObjects(signalJets, 30.0, 2.8, BTag77MV2c10);

    auto largeRJets = reclusterJets(signalJets, 1.0, 30, 0.2, 0.05);

    float mt = 0., mct = 0., mbb = 0., mlb1 = 0.;
    if (signalLeptons.size() == 1 && signalBJets.size() == 2 ) {
        mt   = calcMT(signalLeptons[0], met_Vect);
        mct = calcMCT(signalBJets[0],signalBJets[1],met_Vect);
        mbb = (signalBJets[0]+signalBJets[1]).M();
        mlb1 = (signalBJets[0]+signalLeptons[0]).M();
    }

    // Preselection
    if(baselineLeptons.size() != 1) return;
    if(signalLeptons.size() != 1) return;
    if(signalBJets.size() != 2) return;
    if(signalJets.size() > 3 || signalJets.size() < 2) return;
    if(mt < 50.) return;
    if(met < 220.) return;

    fill("hist_met", met); // fill 1D histogram
    fill("hist_mt", mt); // fill 1D histogram
    fill("hist_metvsmt", met, mt); // fill 2D histogram

    // Can fill integers, floats, doubles, booleans
    ntupVar("met", met);
    ntupVar("mbb", mbb);

    // Can even fill entire analysis objects
    ntupVar("signalJets",signalJets);

    accept("preselection");

    // Avoiding too many curly braces for the sake of readability. I know, try not to snap.
    // Exclusion signal regions
    if(met > 240 && mbb > 100 && mbb <= 140 && mct > 180) {
        if(mt > 100 && mt < 160) {
            if(mct > 180 && mct <= 230) accept("SR_h_Low_bin1");
            else if(mct > 230 && mct <= 280) accept("SR_h_Low_bin2");
            else accept("SR_h_Low_bin3");
        }
        else if(mt > 160 && mt < 240) {
            if(mct > 180 && mct <= 230) accept("SR_h_Med_bin1");
            else if(mct > 230 && mct <= 280) accept("SR_h_Med_bin2");
            else accept("SR_h_Med_bin3");
        }
        else if(mt > 240 && mlb1 > 120) {
            if(mct > 180 && mct <= 230) accept("SR_h_High_bin1");
            else if(mct > 230 && mct <= 280) accept("SR_h_High_bin2");
            else accept("SR_h_High_bin3");
        }
    }

    return;

}
```
