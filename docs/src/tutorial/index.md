---
title: SimpleAnalysis Tutorial
---

The following pages will guide you through a tutorial and hands-on session of SimpleAnalysis, a general-purpose truth framework that implements ATLAS searches. SimpleAnalaysis has been written and maintained by Brian Petersen.

Some previous tutorials (:material-lock: ATLAS internal) with introduction slides:

* :material-lock: [First Uncovered signatures + Run 3 opportunities workshop, May 13, 2020](https://indico.cern.ch/event/914886/sessions/349637/attachments/2037343/3465329/simpleanalysis_tutorial_200513.pdf)
* :material-lock: [Second Uncovered signatures + Run 3 opportunities workshop, July 23, 2020](https://indico.cern.ch/event/938818/sessions/357927/attachments/2078603/3490947/simpleanalysis_tutorial_200723.pdf)
