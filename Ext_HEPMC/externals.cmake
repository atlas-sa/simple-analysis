#
# Package building HepMC3 locally - hack'ish since HepMC3 installs in lib64/, not lib/
#
ExternalProject_Add( proj_HEPMC
  CMAKE_ARGS -DHEPMC3_ENABLE_ROOTIO=OFF -DHEPMC3_ENABLE_SEARCH=OFF -DHEPMC3_ENABLE_PYTHON=OFF -DHEPMC3_INSTALL_EXAMPLES=OFF -DHEPMC3_BUILD_STATIC_LIBS=ON -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  GIT_REPOSITORY https://gitlab.cern.ch/hepmc/HepMC3.git
  GIT_TAG 3.2.6
  PREFIX  ${CMAKE_CURRENT_BINARY_DIR}/myHepMC
  )
ExternalProject_Get_Property(proj_HEPMC install_dir)

ExternalProject_Add_Step(proj_HEPMC
    libcopy DEPENDEES install
    COMMAND ${CMAKE_COMMAND} -E copy ${install_dir}/lib64/libHepMC3.so.3 ${install_dir}/lib/libHepMC3.so.3
    COMMAND ${CMAKE_COMMAND} -E copy ${install_dir}/lib64/libHepMC3.so ${install_dir}/lib/libHepMC3.so
)

include_directories(${install_dir}/include)

set( HEPMC_LIBRARIES ${install_dir}/lib/libHepMC3.so )

add_library(HepMC SHARED IMPORTED)
add_dependencies(HepMC proj_HEPMC)
