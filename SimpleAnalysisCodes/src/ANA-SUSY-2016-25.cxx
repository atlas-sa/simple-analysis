#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(EwkCompressed2016)

void EwkCompressed2016::Init()
{
  addRegions( {"SRSF_iMLLa","SRSF_iMLLb","SRSF_iMLLc","SRSF_iMLLd","SRSF_iMLLe","SRSF_iMLLf","SRSF_iMLLg",
               "SRSF_eMLLa","SRSF_eMLLb","SRSF_eMLLc","SRSF_eMLLd","SRSF_eMLLe","SRSF_eMLLf","SRSF_eMLLg",
               "SRSF_iMT2a","SRSF_iMT2b","SRSF_iMT2c","SRSF_iMT2d","SRSF_iMT2e","SRSF_iMT2f" } );
}

//---------------------------------------------------------

//---------------------------------------------------------  

void EwkCompressed2016::ProcessEvent(AnalysisEvent *event)
{
  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, EVeryLooseLH);
  auto baseMuons      = event->getMuons(4, 2.5, MuMedium);
  auto preJets        = event->getJets(20., 2.8); 
  auto metVec         = event->getMET();
  float met = metVec.Pt();

  // Reject events with bad jets
  if (countObjects(preJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // hard lepton overlap removal and signal lepton/jet definitions
  auto baseJets        = overlapRemoval(preJets,       baseElectrons, 0.2, NOT(BTag85MV2c10));
       baseElectrons   = overlapRemoval(baseElectrons, baseJets,      0.4);
       baseElectrons   = overlapRemoval(baseElectrons, baseMuons,     0.01);
       baseJets        = overlapRemoval(baseJets,      baseMuons,     0.4, LessThan3Tracks);
       baseMuons       = overlapRemoval(baseMuons,     baseJets, 0.4);
  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT50Jet);
  auto signalBJets     = filterObjects( signalJets,    30,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects( baseMuons,     4,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoFixedCutTightTrackOnly);

  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;
  
  //---------------------------------------------------------
  //
  //                Calculate variables
  //
  //---------------------------------------------------------
  
  float jet1Pt    = signalJets.size() >= 1 ? signalJets[0].Pt() : -1.;
  float dPhiJ1Met = signalJets.size() >= 1 ? fabs( signalJets[0].DeltaPhi( metVec ) ) : -1.; 

  // Minimum of the delta phi between all jets with MET
  float minDPhiAllJetsMet = minDphi(metVec,signalJets);

  // Preselect exactly 2 baseline leptons (as derivation does)
  // Final selection requires exactly 2 signal leptons; signal leptons are subset of baseline
  if ( !(baseLeptons.size() == 2) ) return;

  float lep1Pt = baseLeptons[0].Pt();

  // SFOS decision
  bool isOS = ( baseLeptons[0].charge() != baseLeptons[1].charge() ); // opposite charge
  bool isSF = ( baseLeptons[0].type() == baseLeptons[1].type() ) ; // same flavour
  
  // Higher level leptonic variables
  float mll          = (baseLeptons[0]+baseLeptons[1]).M();
  float drll         = baseLeptons[0].DeltaR(baseLeptons[1]);
  float metOverHtLep = met / sumObjectsPt(baseLeptons,999,4); 
  float MTauTau      = calcMTauTau(baseLeptons[0], baseLeptons[1], metVec);
  float mt           = calcMT( baseLeptons[0],metVec );
  
  // mT2 with trial invisible particle mass at 100 GeV
  float mn = 100.; 
  //  float mt2 = calc_massive_MT2(baseLeptons[0], baseLeptons[1], metVec, mn);   
  float mt2=calcAMT2(baseLeptons[0],baseLeptons[1],metVec,mn,mn);

  // SUSYTools final state (fs) code
  int fs = event->getSUSYChannel();
  // stau veto for slepton signal - note this does not affect the denominator in the acceptance numbers
  bool isNotStau = ( fs != 206 && fs != 207 );

  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------
  
  // Common cuts
  bool pass_common_cuts = false;
  if (    ( isNotStau )
       && ( signalBJets.size()   == 0 ) 
       && ( met                  >  200. ) 
       && ( jet1Pt               >  100. )
       && ( dPhiJ1Met            >  2.0 )
       && ( minDPhiAllJetsMet    >  0.4 )
       && ( signalLeptons.size() == 2 )  
       && ( isOS && isSF )
       && ( lep1Pt > 5. )
       && ( MTauTau < 0. || MTauTau > 160. )
       && ( mll > 1. && mll < 60. )
       && ( mll < 3. || mll > 3.2 )
       && ( drll > 0.05 )
      ) pass_common_cuts = true;

  bool keep_SRMLL = pass_common_cuts && ( metOverHtLep > std::max( 5., 15. - 2. * mll ) ) && drll < 2.0 && mt < 70.;
  bool keep_SRMT2 = pass_common_cuts &&   metOverHtLep > std::max( 3., 15. - 2. * (mt2 - mn) );

  // Inclusive mll regions
  if (keep_SRMLL && mll < 3)  accept("SRSF_iMLLa");
  if (keep_SRMLL && mll < 5)  accept("SRSF_iMLLb");
  if (keep_SRMLL && mll < 10) accept("SRSF_iMLLc");
  if (keep_SRMLL && mll < 20) accept("SRSF_iMLLd");
  if (keep_SRMLL && mll < 30) accept("SRSF_iMLLe");
  if (keep_SRMLL && mll < 40) accept("SRSF_iMLLf");
  if (keep_SRMLL && mll < 60) accept("SRSF_iMLLg");

  // Exclusive mll region
  if (keep_SRMLL && mll < 3)  accept("SRSF_eMLLa");
  else if (keep_SRMLL && mll < 5)  accept("SRSF_eMLLb");
  else if (keep_SRMLL && mll < 10) accept("SRSF_eMLLc");
  else if (keep_SRMLL && mll < 20) accept("SRSF_eMLLd");
  else if (keep_SRMLL && mll < 30) accept("SRSF_eMLLe");
  else if (keep_SRMLL && mll < 40) accept("SRSF_eMLLf");
  else if (keep_SRMLL && mll < 60) accept("SRSF_eMLLg");
  
  // Inclusive mT2 regions
  if (keep_SRMT2 && mt2 < 102)  accept("SRSF_iMT2a");
  if (keep_SRMT2 && mt2 < 105)  accept("SRSF_iMT2b");
  if (keep_SRMT2 && mt2 < 110)  accept("SRSF_iMT2c");
  if (keep_SRMT2 && mt2 < 120)  accept("SRSF_iMT2d");
  if (keep_SRMT2 && mt2 < 130)  accept("SRSF_iMT2e");
  if (keep_SRMT2 && mt2 >= 100) accept("SRSF_iMT2f"); 
   
}
