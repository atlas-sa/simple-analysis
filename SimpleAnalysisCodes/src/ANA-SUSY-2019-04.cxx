#include "SimpleAnalysisFramework/AnalysisClass.h"

#include <algorithm>

DefineAnalysis(OneLeptonMultiJets2018)

static double calc_mlj_pair(const AnalysisObject& l1,
                            const AnalysisObject& l2,
                            const AnalysisObjects& jets, size_t n_jets)
{
	float minmax = 9e9;
	float tmpmass;
	for(size_t i=0; i<n_jets;i++){
		if(i >= jets.size()) continue;
		for(size_t j=0; j<n_jets;j++){
			if(i==j) continue;
			if(j >= jets.size()) continue;
			TLorentzVector tmp1(l1);
			TLorentzVector tmp2(l2);
			tmp1 += jets[i];
			tmp2 += jets[j];
			tmpmass = std::max(tmp1.M(),tmp2.M());
			if(tmpmass < minmax) minmax = tmpmass;
		}
	}
	return minmax;
}

static int bjet_category(size_t num_bjets)
{
	switch (num_bjets) {
	case 0: return -1;
	case 1: return 0;
	case 2: return 1;
	case 3: return 2;
	case 4: return 3;
	default: return 3; // cat 3: >= 4 b-jets
	}
}

static int jet_btag_category(const AnalysisObject& jet)
{
	// no b-tagging weight in truth information, just
	// use the extremes
	if (jet.pass(BTag70DL1r)) {
		return 5;
	}

	return 1;
}

static float calc_ht(const AnalysisObjects& jets)
{
	float ht = 0;
	for (const auto& jet: jets) {
		ht += jet.Pt();
	}
	return ht;
}

static float min_dr_lep_jet(const AnalysisObjects& leptons, const AnalysisObjects& jets)
{
	float min_dr = 99;

	for (const auto& lep: leptons) {
		for (const auto& jet: jets) {
			const float dr = lep.DeltaR(jet);
			if (dr < min_dr)
				min_dr = dr;
		}
	}

	return min_dr;
}

static float calc_three_jet_max_pt_mass(const AnalysisObjects& jets,
	                                    const AnalysisObject* lep=nullptr,
	                                    const AnalysisObject* met=nullptr)
{
	// either only jets, or include both lepton and MET
	if (lep || met) {
		assert(lep != nullptr);
		assert(met != nullptr);
	}

	TLorentzVector max_pt_system;

	for (size_t i=0; i < jets.size(); ++i) {
		for (size_t j=i+1; j < jets.size(); ++j) {
			for (size_t k=j+1; k < jets.size(); ++k) {

				TLorentzVector system = jets[i] + jets[j] + jets[k];
				if (lep && met) {
					system += *lep;
					system += *met;
				}

				if (system.Pt() > max_pt_system.Pt()) {
					max_pt_system = system;
				}
			}
		}
	}

	return max_pt_system.M();
}

/* Function to get no of set bits in binary
 * representation of positive integer n */
static unsigned int countSetBits(unsigned int n)
{
	unsigned int count = 0;
	while (n)
	{
	    count += n & 1;
	    n >>= 1;
	}
	return count;
}


static float calc_minmax_mass(const AnalysisObjects& jets, int jetdiff=10)
{
    const int nJets = jets.size();

    //bitwise representation of all jets and for which half they are selected
    // One bit for every jet, marking into which set they are grouped
    const unsigned int bitmax = 1 << nJets;
    float minmass = 999999999;

    for(unsigned int bit=0; bit < bitmax; bit++){
        const int bitcount = countSetBits(bit);
        if (abs(nJets - 2*bitcount) > jetdiff) {
        	continue;
        }

        TLorentzVector sum1, sum2;
        // loop through jets and assign to either sum, depending on bit
        for(int i=0; i<nJets; i++) {
            if (bit & (1<<i)) 
            	sum1 += jets[i];
            else
            	sum2 += jets[i];
        }
        if (sum1.M() > sum2.M() && sum1.M() < minmass) 
        	minmass = sum1.M();
        else if (sum2.M() > sum1.M() && sum2.M() < minmass)
        	minmass = sum2.M();
    }

    return minmass;
}

void OneLeptonMultiJets2018::Init()
{
        addONNX("4jets","OneLeptonMultiJets2018_4jets.onnx");
	addONNX("5jets","OneLeptonMultiJets2018_5jets.onnx");
	addONNX("6jets","OneLeptonMultiJets2018_6jets.onnx");
	addONNX("7jets","OneLeptonMultiJets2018_7jets.onnx");
	addONNX("8jets","OneLeptonMultiJets2018_8jets.onnx");


	addRegions({
		"preSelection", "oneLeptonChannel", "sameSignChannel",

		"1l_15j20_0b", "1l_15j20_3b",
		"ss_10j20_0b", "ss_10j20_3b",

		"1l_12j40_0b", "1l_12j40_3b",
		"ss_8j40_0b", "ss_8j40_3b",

		"1l_12j60_0b", "1l_12j60_3b",
		"ss_7j60_0b", "ss_7j60_3b",

		"1l_10j80_0b", "1l_10j80_3b",
		"ss_6j80_0b", "ss_6j80_3b",

		"1l_8j100_0b", "1l_8j100_3b",
		"ss_6j100_0b", "ss_6j100_3b",

		// discovery regions for shape analysis
		"ss_shape_6j_3b",

		"1l_shape_4j_4b",
		"1l_shape_5j_4b",
		"1l_shape_6j_4b",
		"1l_shape_7j_4b",
		"1l_shape_8j_4b",
	});

	addHistogram("1l_nn_4j_4b", 4, 0, 1);
	addHistogram("1l_nn_5j_4b", 4, 0, 1);
	addHistogram("1l_nn_6j_4b", 4, 0, 1);
	addHistogram("1l_nn_7j_4b", 4, 0, 1);
	addHistogram("1l_nn_8j_4b", 4, 0, 1);
}

void OneLeptonMultiJets2018::ProcessEvent(AnalysisEvent *event)
{
	auto baseElectrons = event->getElectrons(10,2.47, EMediumLH | EZ05mm);
	auto baseMuons     = event->getMuons(10, 2.5, MuMedium | MuZ05mm);
	auto baseJets      = event->getJets(20.,4.5);
	auto baseLeptons   = baseElectrons + baseMuons;

	// overlap removal
	baseElectrons  	= overlapRemoval(baseElectrons, baseMuons, 0.01);
	auto Jets       = overlapRemoval(baseJets, baseElectrons, 0.2, NOT(BTag85MV2c10));
	auto muJetSpecial = [] (const AnalysisObject& jet, const AnalysisObject& muon) { 
	  if (jet.pass(NOT(BTag85MV2c10)) && (jet.pass(LessThan3Tracks) || muon.Pt()/jet.Pt()>0.5)) return 0.4;
	  else return 0.;
	};
	Jets            = overlapRemoval(Jets, baseMuons, muJetSpecial, NOT(BTag85MV2c10));
	auto Electrons  = overlapRemoval(baseElectrons, Jets, 0.4);
	auto Muons      = overlapRemoval(baseMuons, Jets, 0.4);

	// signal objects
	auto signalElectrons = filterObjects(Electrons, 15, 2.47, ETightLH | EIsoPLVTight | ED0Sigma5);
	auto signalMuons     = filterObjects(Muons, 15, 2.4, MuMedium | MuIsoPLVTight | MuD0Sigma3);
	auto signalLeptons   = signalElectrons + signalMuons;

	auto signalJets20    = filterObjects(Jets, 20., 2.5, JVTTight);
	auto bjets20		 = filterObjects(signalJets20, 20, 2.5, BTag70DL1r);

	auto signalJets40    = filterObjects(signalJets20, 40., 2.5);
	auto bjets40		 = filterObjects(signalJets40, 40, 2.5, BTag70DL1r);

	auto signalJets60    = filterObjects(signalJets20, 60., 2.5);
	auto bjets60		 = filterObjects(signalJets60, 60, 2.5, BTag70DL1r);

	auto signalJets80    = filterObjects(signalJets20, 80., 2.5);
	auto bjets80		 = filterObjects(signalJets80, 80, 2.5, BTag70DL1r);

	auto signalJets100   = filterObjects(signalJets20, 100., 2.5);
	auto bjets100		 = filterObjects(signalJets100, 100, 2.5, BTag70DL1r);


	////////////////////////////
	// Preselection
	// at least one lepton, leading lepton with at least 27 GeV
	if (signalLeptons.size() < 1 || signalLeptons[0].Pt() < 27)
		return;

	// at least 4 jets with pT > 20 GeV
	if (signalJets20.size() < 4)
		return;

	accept("preSelection");

	// split into same-sign or 1-lepton channel

	const bool isSameSignChannel =    (signalLeptons.size() == 2)
	                               && (signalLeptons[0].charge() == signalLeptons[1].charge())
	                               && (baseLeptons.size() == 2);

	const bool isOneLeptonChannel = !isSameSignChannel;

	////////////////////////////
	// Jet counting analysis

	if (isOneLeptonChannel) {
		accept("oneLeptonChannel");

		if (signalJets20.size() >= 15) {
			if (bjets20.size() == 0) {
				accept("1l_15j20_0b");
			}
			else if (bjets20.size() >= 3) {
				 accept("1l_15j20_3b");
			}
		}

		if (signalJets40.size() >= 12) {
			if (bjets40.size() == 0) {
				accept("1l_12j40_0b");
			}
			else if (bjets40.size() >= 3) {
				 accept("1l_12j40_3b");
			}
		}

		if (signalJets60.size() >= 12) {
			if (bjets60.size() == 0) {
				accept("1l_12j60_0b");
			}
			else if (bjets60.size() >= 3) {
				 accept("1l_12j60_3b");
			}
		}

		if (signalJets80.size() >= 10) {
			if (bjets80.size() == 0) {
				accept("1l_10j80_0b");
			}
			else if (bjets80.size() >= 3) {
				 accept("1l_10j80_3b");
			}
		}

		if (signalJets100.size() >= 8) {
			if (bjets100.size() == 0) {
				accept("1l_8j100_0b");
			}
			else if (bjets100.size() >= 3) {
				 accept("1l_8j100_3b");
			}
		}
	}

	if (isSameSignChannel) {
		accept("sameSignChannel");

		if (signalJets20.size() >= 10) {
			if (bjets20.size() == 0) {
				accept("ss_10j20_0b");
			}
			else if (bjets20.size() >= 3) {
				 accept("ss_10j20_3b");
			}
		}

		if (signalJets40.size() >= 8) {
			if (bjets40.size() == 0) {
				accept("ss_8j40_0b");
			}
			else if (bjets40.size() >= 3) {
				 accept("ss_8j40_3b");
			}
		}

		if (signalJets60.size() >= 7) {
			if (bjets60.size() == 0) {
				accept("ss_7j60_0b");
			}
			else if (bjets60.size() >= 3) {
				 accept("ss_7j60_3b");
			}
		}

		if (signalJets80.size() >= 6) {
			if (bjets80.size() == 0) {
				accept("ss_6j80_0b");
			}
			else if (bjets80.size() >= 3) {
				 accept("ss_6j80_3b");
			}
		}

		if (signalJets100.size() >= 6) {
			if (bjets100.size() == 0) {
				accept("ss_6j100_0b");
			}
			else if (bjets100.size() >= 3) {
				 accept("ss_6j100_3b");
			}
		}

	}

	////////////////////////////
	// Shape analysis


	if (isSameSignChannel) {

		if (signalJets20.size() == 6) {
			const double minmax_mlj = calc_mlj_pair(signalLeptons[0], signalLeptons[1], signalJets20, 4);
			
			if (minmax_mlj < 155 && bjets20.size() >= 3) {
				accept("ss_shape_6j_3b");
			}	
		}
	}

	if (isOneLeptonChannel && signalJets20.size() >= 4 && signalJets20.size() <= 8 && bjets20.size() >= 1) { 
		// prepare NN inputs
		const float Escale = 100; // GeV


		auto met = event->getMET();

		std::vector<double> nn_inputs;
		nn_inputs.reserve(65);
		nn_inputs.push_back(signalJets20.size()); // n_jet
		nn_inputs.push_back(bjet_category(bjets20.size())); // TODO: n_bcat
		nn_inputs.push_back(bjets20.size()); // n_bjet
		nn_inputs.push_back(calc_three_jet_max_pt_mass(signalJets20) / Escale); // three_jet_maxpt_mass
		nn_inputs.push_back(calc_ht(signalJets20) / Escale); // ht_jet
		nn_inputs.push_back(calc_ht(bjets20) / Escale); // ht_bjet
		nn_inputs.push_back(calc_three_jet_max_pt_mass(signalJets20, &signalLeptons[0], &met) / Escale); // three_jet_lep_met_mass_max_pt
		nn_inputs.push_back(min_dr_lep_jet(signalLeptons, signalJets20)); // minDeltaRlj
		nn_inputs.push_back(calc_minmax_mass(signalJets20) / Escale); // minmax_mass
		nn_inputs.push_back(met.Pt() / Escale); // met
		nn_inputs.push_back(met.Phi()); // met_phi

		// jet_pt 0-9
		nn_inputs.push_back(signalJets20.size() > 0 ? signalJets20[0].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 1 ? signalJets20[1].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 2 ? signalJets20[2].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 3 ? signalJets20[3].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 4 ? signalJets20[4].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 5 ? signalJets20[5].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 6 ? signalJets20[6].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 7 ? signalJets20[7].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 8 ? signalJets20[8].Pt() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 9 ? signalJets20[9].Pt() / Escale : 1e-7);

		// jet_eta 0-9
		nn_inputs.push_back(signalJets20.size() > 0 ? signalJets20[0].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 1 ? signalJets20[1].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 2 ? signalJets20[2].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 3 ? signalJets20[3].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 4 ? signalJets20[4].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 5 ? signalJets20[5].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 6 ? signalJets20[6].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 7 ? signalJets20[7].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 8 ? signalJets20[8].Eta() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 9 ? signalJets20[9].Eta() : -5.0);

		// jet_phi 0-9
		nn_inputs.push_back(signalJets20.size() > 0 ? signalJets20[0].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 1 ? signalJets20[1].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 2 ? signalJets20[2].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 3 ? signalJets20[3].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 4 ? signalJets20[4].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 5 ? signalJets20[5].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 6 ? signalJets20[6].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 7 ? signalJets20[7].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 8 ? signalJets20[8].Phi() : -5.0);
		nn_inputs.push_back(signalJets20.size() > 9 ? signalJets20[9].Phi() : -5.0);

		// jet_e 0-9
		nn_inputs.push_back(signalJets20.size() > 0 ? signalJets20[0].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 1 ? signalJets20[1].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 2 ? signalJets20[2].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 3 ? signalJets20[3].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 4 ? signalJets20[4].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 5 ? signalJets20[5].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 6 ? signalJets20[6].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 7 ? signalJets20[7].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 8 ? signalJets20[8].E() / Escale : 1e-7);
		nn_inputs.push_back(signalJets20.size() > 9 ? signalJets20[9].E() / Escale : 1e-7);

		// jet_btag_bin 0-9
		nn_inputs.push_back(signalJets20.size() > 0 ? jet_btag_category(signalJets20[0]) : 0);
		nn_inputs.push_back(signalJets20.size() > 1 ? jet_btag_category(signalJets20[1]) : 0);
		nn_inputs.push_back(signalJets20.size() > 2 ? jet_btag_category(signalJets20[2]) : 0);
		nn_inputs.push_back(signalJets20.size() > 3 ? jet_btag_category(signalJets20[3]) : 0);
		nn_inputs.push_back(signalJets20.size() > 4 ? jet_btag_category(signalJets20[4]) : 0);
		nn_inputs.push_back(signalJets20.size() > 5 ? jet_btag_category(signalJets20[5]) : 0);
		nn_inputs.push_back(signalJets20.size() > 6 ? jet_btag_category(signalJets20[6]) : 0);
		nn_inputs.push_back(signalJets20.size() > 7 ? jet_btag_category(signalJets20[7]) : 0);
		nn_inputs.push_back(signalJets20.size() > 8 ? jet_btag_category(signalJets20[8]) : 0);
		nn_inputs.push_back(signalJets20.size() > 9 ? jet_btag_category(signalJets20[9]) : 0);

		nn_inputs.push_back(signalLeptons[0].Pt() / Escale);
		nn_inputs.push_back(signalLeptons[0].Eta());
		nn_inputs.push_back(signalLeptons[0].Phi());
		nn_inputs.push_back(signalLeptons[0].E() / Escale);

		// now select which NN to use, according to jet multiplicity

		MVA *nn = nullptr;
		
		switch (signalJets20.size()) {
		case 4:
		  nn = getMVA("4jets");
		  break;
		case 5:
		  nn = getMVA("5jets");
		  break;
		case 6:
		  nn = getMVA("6jets");
		  break;
		case 7:
		  nn = getMVA("7jets");
		  break;
		case 8:
		  nn = getMVA("8jets");
		  break;
		default:
			assert(false);
		}

		// evaluate neural network
		const float nn_value = nn->evaluate(nn_inputs);

		// and fill the output distribution
		// Note: the threshold for the NNs is tuned to achieve
		// a flat signal efficiency
		switch (signalJets20.size()) {
		case 4:
			fill("1l_nn_4j_4b", nn_value);
			if (bjets20.size() >= 4 && nn_value > 0.73)
				accept("1l_shape_4j_4b");
			break;

		case 5:
			fill("1l_nn_5j_4b", nn_value);
			if (bjets20.size() >= 4 && nn_value > 0.76)
				accept("1l_shape_5j_4b");
			break;

		case 6:
			fill("1l_nn_6j_4b", nn_value);
			if (bjets20.size() >= 4 && nn_value > 0.77)
				accept("1l_shape_6j_4b");
			break;

		case 7:
			fill("1l_nn_7j_4b", nn_value);
			if (bjets20.size() >= 4 && nn_value > 0.72)
				accept("1l_shape_7j_4b");
			break;

		case 8:
			fill("1l_nn_8j_4b", nn_value);
			if (bjets20.size() >= 4 && nn_value > 0.73)
				accept("1l_shape_8j_4b");
			break;

		default:
			assert(false);
		}
	}
}
