#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <TFile.h>
#include <TH2.h>
#include <TF1.h>

DefineAnalysis(DisappearingTrack2018_EventAcc)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This analysis is to compute the event acceptance quoted in Method 2 in the auxillary material.                            //
// This is the geometrical and kinematic acceptance at generator-level.                                                      //
// Results are stored in the produced histograms, rather than the typical accept(region) method.                             //
// More explanation is in Section 1.3 of https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/SUSY-2018-19/hepdata_info.pdf //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void DisappearingTrack2018_EventAcc::Init() {
    addHistogram("EWKCutflow",5,0,5);// All Events, MET, Jet Pt, minDPhi, lepton veto
    addHistogram("StrongCutflow",5,0,5);// All Events, MET, Jet Pt, minDPhi, lepton veto
    addHistogram("EWKAccept",2,0,2);
    addHistogram("StrongAccept",2,0,2);
    addRegion("EWKAccept");
    addRegion("StrongAccept");
}

void DisappearingTrack2018_EventAcc::ProcessEvent(AnalysisEvent *event) {
    
    // Object Definition - see INT Note Sections 4.2 -> 4.5
    auto met = event->getMET(); // MET as provided by SimpleAnalysis
    double metpt = met.Pt();
    auto jets = event->getJets(20.0, 2.8, NOT(ContainsSUSY)); // Jets containing a SUSY particle are removed - see xAODTruthReader.cxx
    auto electrons = event->getElectrons(10.0, 2.47, ELooseBLLH);
    auto muons = event->getMuons(10.0, 2.7, MuMedium);
    auto caloMuons = filterObjects(muons, 10.0, 2.7, MuCaloTaggedOnly);
    auto notCaloMuons = filterObjects(muons, 10.0, 2.7, NOT(MuCaloTaggedOnly));
    auto weights = event->getMCWeights();

    // Overlap Removal - see INT Note Section 4.7
    caloMuons = overlapRemoval(caloMuons, electrons, 0.05);
    electrons = overlapRemoval(electrons, notCaloMuons, 0.05);
    muons = caloMuons + notCaloMuons;
    jets = overlapRemoval(jets, electrons, 0.2);
    electrons = overlapRemoval(electrons, jets, 0.4);
    jets = overlapRemoval(jets, muons, 0.2, LessThan3Tracks);
    muons = overlapRemoval(muons, jets, 0.4);

    fill("EWKCutflow",0.5);
    fill("StrongCutflow",0.5);
    fill("EWKAccept",0.5);
    fill("StrongAccept",0.5);

    bool inStrongAcc = true;

    // acceptance requirements
    // met
    if (metpt > 250) {
        fill("StrongCutflow",1.5);
        fill("EWKCutflow",1.5);
    } else if (metpt > 200) {
        fill("EWKCutflow",1.5);
        inStrongAcc = false;
    } else return;

    // jet pt
    sortObjectsByPt(jets);
    if (jets.empty()) return; // must have a jet
    if (jets[0].Pt() < 100) return; // leadifng jet pt requirement
    fill("EWKCutflow",2.5);

    if (inStrongAcc) {
        if (jets.size() >= 3) {
            if (jets[1].Pt() < 20) inStrongAcc = false;
            if (jets[2].Pt() < 20) inStrongAcc = false;
        } else inStrongAcc = false;

        if (inStrongAcc) fill("StrongCutflow",2.5);
    }

    // DphiJetMET
    double minDphiJetMET = AnalysisClass::minDphi(met, jets, 4, 50.0);

    if (minDphiJetMET < 0.4) return;
    if (minDphiJetMET > 1.0) fill("EWKCutflow",3.5);

    if (inStrongAcc && minDphiJetMET > 0.4) fill("StrongCutflow",3.5);
    else inStrongAcc = false;
    

    // Lepton Veto
    if (!electrons.empty() || !muons.empty()) return; // lepton veto
    if (minDphiJetMET > 1.0) {
        fill("EWKCutflow",4.5);
        accept("EWKAccept");
        fill("EWKAccept",1.5);
    }
    

    if (inStrongAcc) {
        fill("StrongCutflow",4.5); 
        accept("StrongAccept"); 
        fill("StrongAccept",1.5);
    }

    // done
    return;
}