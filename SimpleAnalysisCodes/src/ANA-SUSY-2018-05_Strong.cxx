#include "SimpleAnalysisFramework/AnalysisClass.h"
DefineAnalysis(TwoLeptonTwoJetStrong2018)

void TwoLeptonTwoJetStrong2018::Init() 
{

    // Presel
    addRegions({"SUSY2_deriv","two_baseline10_lep","one30jet","twosig","2L2J","presel","Skim"});

    // Signal Regions
    addRegions({"SRC", "SRLow", "SRMed", "SRHigh", "SRZLow", "SRZMed", "SRZHigh"});

    // Validation Regions
    addRegions({"VRC", "VRLow", "VRMed", "VRHigh", "VRSS", "VR3L"});

    // Z+jets Control Regions
    addRegions({"CRC_Z", "CRLow_Z", "CRMed_Z", "CRHigh_Z", "CRZLow_Z", "CRZMed_Z", "CRZHigh_Z", "CRVRC_Z", "CRVRLow_Z", "CRVRMed_Z", "CRVRHigh_Z"});

    // Flavor symmetry Control Regions
    addRegions({"CRC", "CRLow", "CRMed", "CRHigh", "CRZLow", "CRZMed", "CRZHigh"});

    // Merged Flavor (SR (SF+DF))
    addRegions({"MFRC", "MFRLow", "MFRMed", "MFRHigh", "MFRZLow", "MFRZMed", "MFRZHigh"});
    // Merged Flavor (CR_Z (SF+DF))
    addRegions({"MFRC_Z", "MFRLow_Z", "MFRMed_Z", "MFRHigh_Z", "MFRZLow_Z", "MFRZMed_Z", "MFRZHigh_Z", "MFRVRC_Z", "MFRVRLow_Z", "MFRVRMed_Z", "MFRVRHigh_Z"});
    // Inclusive MET, no lepton Flavor (VR+SR)
    addRegions({"RC","RLow", "RMed", "RHigh","RZLow", "RZMed", "RZHigh"});
    addRegions({"RC_Z", "RLow_Z", "RMed_Z", "RHigh_Z", "RZLow_Z", "RZMed_Z", "RZHigh_Z", "RVRC_Z", "RVRLow_Z", "RVRMed_Z", "RVRHigh_Z"});
    

    // SR mll bin boundaries
    std::vector<float> bins_c    = {12,31,46,61,71,81,101,201};
    std::vector<float> bins_low  = {12,41,61,81,101,141,201,301,501};
    std::vector<float> bins_med  = {12,81,101,201,301,601};
    std::vector<float> bins_high = {12,101,301,1001};

    addHistogram("2L2J_mll", 50,0,1000);
    addHistogram("2L2J_Ht30", 15,0,1500);
    addHistogram("2L2J_met_Et", 20,0,500);
    addHistogram("2L2J_leppt", 20,0,100);
    addHistogram("2L2J_jetpt", 20,0,100);
    addHistogram("2L2J_nJet30", 10,0,10);
    addHistogram("2L2J_lepFlav", 4,0,4);
    addHistogram("2L2J_lepCharge", 2,-1,1);
    addHistogram("2L2J_nLep_combi", 10,0,10);
    addHistogram("2L2J_met_Sign", 30,0,30);
    addHistogram("2L2J_Ptll", 50,0,500);
    addHistogram("2L2J_mt2leplsp_0", 30,0,300);
    addHistogram("2L2J_minDPhi2JetsMet", 30,0,3);

    addHistogram("presel_mll", 50,0,1000);
    addHistogram("presel_Ht30", 15,0,1500);
    addHistogram("presel_met_Et", 20,0,500);
    addHistogram("presel_leppt", 20,0,100);
    addHistogram("presel_jetpt", 20,0,100);
    addHistogram("presel_nJet30", 10,0,10);
    addHistogram("presel_lepFlav", 4,0,4);
    addHistogram("presel_lepCharge", 2,-1,1);
    addHistogram("presel_nLep_combi", 10,0,10);
    addHistogram("presel_met_Sign", 30,0,30);
    addHistogram("presel_Ptll", 50,0,500);
    addHistogram("presel_mt2leplsp_0", 30,0,300);
    addHistogram("presel_minDPhi2JetsMet", 30,0,3);

    addHistogram("Skim_mll", 10,0,500);
    addHistogram("SRC_mll", bins_c);
    addHistogram("SRLow_mll", bins_low);
    addHistogram("SRMed_mll", bins_med);
    addHistogram("SRHigh_mll", bins_high);


    addHistogram("presel_mcw",100,-2,2);
    addHistogram("src_mcw",100,-2,2);
    addHistogram("srlow_mcw",100,-2,2);
    addHistogram("srmed_mcw",100,-2,2);
    addHistogram("srhigh_mcw",100,-2,2);
}


void TwoLeptonTwoJetStrong2018::ProcessEvent(AnalysisEvent *event)
{
    // Testing
    float mcWeight = event->getMCWeights()[0];

    // Presel

    // Gather Objects
    float gen_met      = event->getGenMET();
    float gen_ht       = event->getGenHT();

    auto metVec     = event->getMET();
    double met      = metVec.Et();


    auto electrons_susy2Deriv  = event->getElectrons(9, 2.6, ELooseLH);
    auto muons_susy2Deriv      = event->getMuons(9, 2.6, MuMedium);
    int n_derivation_leptons = (electrons_susy2Deriv+muons_susy2Deriv).size();

    auto electrons  = event->getElectrons(4.5, 2.47, ELooseLH|EZ05mm);
    auto muons      = event->getMuons(3., 2.7, MuMedium|EZ05mm);
    auto candJets   = event->getJets(20., 2.8);
    auto baselineLeptons = electrons+muons;
    sortObjectsByPt(baselineLeptons);
    int n_baseline_leptons = baselineLeptons.size();
    
    candJets = filterObjects(candJets, 20, 2.8, JVT50Jet); // For reco, should be medium workpoing point for pt<120, and tight for eta>2.5 and pt>50

    // Overlap removal
    auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
    auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
    auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

    electrons  = overlapRemoval(electrons, muons, 0.01);
    candJets   = overlapRemoval(candJets, electrons, 0.2);
    electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);
    candJets   = overlapRemoval(candJets, muons, radiusCalcJet, LessThan3Tracks);
    muons      = overlapRemoval(muons, candJets, radiusCalcMuon);
   
    int nTruthJets20 = candJets.size();

    auto signalJets      = filterObjects(candJets, 30);
    auto signalElectrons = filterObjects(electrons, 25, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoBoosted);
    auto signalMuons     = filterObjects(muons, 25, 2.6, MuD0Sigma3|MuZ05mm|MuIsoBoosted|MuNotCosmic);

    // combine into signalLeptons for easy counting
    auto signalLeptons   = signalElectrons + signalMuons;
    sortObjectsByPt( signalJets );
    sortObjectsByPt( signalLeptons );

    int n_jets        = signalJets.size();
    int n_leptons     = signalLeptons.size();

    auto HT = sumObjectsPt(signalJets);

    /// No Selection

    // SUSY 2 Derivation
    if (n_derivation_leptons < 2) return;
    accept("SUSY2_deriv");

    // Ntuple selection
    // cosmic/ badmuon / badjet

    if (filterObjects(baselineLeptons, 10).size() <2)  return;
    accept("two_baseline10_lep");
    
    if (n_jets <1) return;
    accept("one30jet");

    if (n_leptons <2) return;
    accept("twosig");

    //////////////////
    // Setup for preselection
    /////////////////


    // Channel
    bool isDF = false;
    bool isSF = false;
    if (signalElectrons.size()==1 && signalMuons.size()==1) isDF = true;
    else isSF = true;

    TLorentzVector dilepton = signalLeptons[0] + signalLeptons[1];
    //TLorentzVector dilepton = baselineLeptons[0] + baselineLeptons[1];
    auto mll = dilepton.M();
    auto ptll = dilepton.Pt();
    auto dphimetjet = minDphi(metVec, signalJets, 2);
    auto metsig = event->getMETSignificance();
    auto mt2 = calcMT2(baselineLeptons[0],baselineLeptons[1],metVec);

    // VR3L
    if (mll > 12 && ptll > 40 && n_jets>=2 && dphimetjet > 0.4 && n_leptons == 3 && n_baseline_leptons == 3 && isSF && baselineLeptons[0].charge()!=baselineLeptons[1].charge() && HT > 250 && met > 200 && mt2 > 100)
    {
        accept("VR3L");
    }

    // end VR3L

    // require 2 signal leptons
    if(n_leptons!=2) return;    

    if(n_jets<2) return;

    // 2L2J
    accept("2L2J");
    fill("2L2J_mll", mll);
    fill("2L2J_nJet30", n_jets);
    fill("2L2J_nLep_combi", n_baseline_leptons);
    fill("2L2J_Ht30", HT);    
    fill("2L2J_met_Et", met);
    fill("2L2J_lepFlav", (isDF? 1 : 0) );
    fill("2L2J_lepCharge", baselineLeptons[0].charge()*baselineLeptons[1].charge() );
    fill("2L2J_leppt", baselineLeptons[0].Pt() );
    fill("2L2J_jetpt", signalJets[0].Pt() );
    fill("2L2J_met_Sign", metsig);
    fill("2L2J_Ptll", ptll);
    fill("2L2J_mt2leplsp_0", mt2);
    fill("2L2J_minDPhi2JetsMet", dphimetjet);




    if (mll < 12) return;
    // "trig match"

    if (ptll < 40) return;


    // VRSS
    if (n_baseline_leptons ==2 && n_leptons==2 && baselineLeptons[0].charge()==baselineLeptons[1].charge() && dphimetjet > 0.4 && HT > 250 && met > 150 && mt2 > 75)
    {
        accept("VRSS");
    }

    // end VRSS


    // Preselection cuts
    if (n_baseline_leptons ==2 && n_leptons==2 && isSF && baselineLeptons[0].charge()!=baselineLeptons[1].charge() && dphimetjet > 0.4)
    {
        accept("presel");
        fill("presel_mll", mll);
        fill("presel_nJet30", n_jets);
        fill("presel_nLep_combi", n_baseline_leptons);
        fill("presel_Ht30", HT);    
        fill("presel_met_Et", met);
        fill("presel_lepFlav", (isDF? 1 : 0) );
        fill("presel_lepCharge", baselineLeptons[0].charge()*baselineLeptons[1].charge() );
        fill("presel_leppt", baselineLeptons[0].Pt() );
        fill("presel_jetpt", signalJets[0].Pt() );
        fill("presel_met_Sign", metsig);
        fill("presel_Ptll", ptll);
        fill("presel_mt2leplsp_0", mt2);
        fill("presel_minDPhi2JetsMet", dphimetjet);

        fill("presel_mcw",mcWeight);
    }


    if (met < 150) return;

    if (mt2 < 50) return;



    //// Skimmed
    if ( metsig < 10 ? HT > 200 : 1)
    {
        if (dphimetjet > 0.4)
        {
            accept("Skim");
            fill("Skim_mll",mll);


            if( n_baseline_leptons ==2 && n_leptons==2 && baselineLeptons[0].charge()!=baselineLeptons[1].charge()) 
            {

                ////////////////////
                // Validation Regions
                /////////////////////
                // VRC
                if (isSF && met > 150 && met < 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250)
                {
                    accept("VRC");
                }

                // VRLow
                if (isSF && met > 150 && met < 250 && ptll < 500 && mt2 > 100 && HT > 250)
                {
                    accept("VRLow");
                }


                // VRMed
                if (isSF && met >  150 && met < 250 && ptll < 800 && mt2 > 75 && HT > 500)
                {
                    accept("VRMed");
                }

                // VRHigh
                if (isSF && met > 150 && met < 250 && mt2 > 75 && HT > 800)
                {
                    accept("VRHigh");
                }

                /////////////////////
                // Signal Regions
                ////////////////////


                // SRC
                if (isSF && met > 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250)
                {
                    accept("SRC");

                    fill("SRC_mll",mll);
                    fill("src_mcw",mcWeight);
                }

                // SRLow
                if (isSF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250)
                {
                    accept("SRLow");

                    fill("SRLow_mll",mll);
                    fill("srlow_mcw",mcWeight);
                }


                // SRMed
                if (isSF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500)
                {
                    accept("SRMed");

                    fill("SRMed_mll",mll);
                    fill("srmed_mcw",mcWeight);
                }

                // SRHigh
                if (isSF && met > 300 && mt2 > 75 && HT > 800)
                {
                    accept("SRHigh");

                    fill("SRHigh_mll",mll);
                    fill("srhigh_mcw",mcWeight);
                }

                // onZ
                // SRZLow
                if (isSF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("SRZLow");
                }


                // SRZMed
                if (isSF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("SRZMed");
                }

                // SRZHigh
                if (isSF && met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("SRZHigh");
                }



                /////////////////////
                // Flavor Symmetry Cotrol Regions (DF)
                ////////////////////

                // CRC
                if (isDF && met > 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250)
                {
                    accept("CRC");
                }

                // CRLow
                if (isDF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250)
                {
                    accept("CRLow");
                }


                // CRMed
                if (isDF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500)
                {
                    accept("CRMed");
                }

                // CRHigh
                if (isDF && met > 300 && mt2 > 75 && HT > 800)
                {
                    accept("CRHigh");
                }

                // onZ
                // CRZLow
                if (isDF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 61 && mll < 121 && n_jets >3)
                {
                    accept("CRZLow");
                }


                // CRZMed
                if (isDF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 61 && mll < 121 && n_jets >3)
                {
                    accept("CRZMed");
                }

                // CRZHigh
                if (isDF && met > 300 && mt2 > 75 && HT > 800 && mll > 61 && mll < 121 && n_jets >3)
                {
                    accept("CRZHigh");
                }


                ///////////////
                // No lepton flav requirement
                //////////////

                // MFRC
                if (met > 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250)
                {
                    accept("MFRC");
                }

                // MFRLow
                if (met > 250 && ptll < 500 && mt2 > 100 && HT > 250)
                {
                    accept("MFRLow");
                }


                // MFRMed
                if (met > 300 && ptll < 800 && mt2 > 75 && HT > 500)
                {
                    accept("MFRMed");
                }

                // MFRHigh
                if (met > 300 && mt2 > 75 && HT > 800)
                {
                    accept("MFRHigh");
                }

                // onZ
                // MFRZLow
                if (met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("MFRZLow");
                }


                // MFRZMed
                if (met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("MFRZMed");
                }

                // MFRZHigh
                if (met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("MFRZHigh");
                }

                ///////////////
                // No lepton flav requirement and inclusive MET
                //////////////

                // RC
                if (met > 150 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250)
                {
                    accept("RC");
                }

                // RLow
                if (met > 150 && ptll < 500 && mt2 > 100 && HT > 250)
                {
                    accept("RLow");
                }


                // RMed
                if (met > 150 && ptll < 800 && mt2 > 75 && HT > 500)
                {
                    accept("RMed");
                }

                // RHigh
                if (met > 150 && mt2 > 75 && HT > 800)
                {
                    accept("RHigh");
                }

                // onZ
                // RZLow
                if (met > 150 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("RZLow");
                }


                // RZMed
                if (met > 150 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("RZMed");
                }

                // RZHigh
                if (met > 150 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
                {
                    accept("RZHigh");
                }
            }

        } else if (n_baseline_leptons ==2 && n_leptons==2 && baselineLeptons[0].charge()!=baselineLeptons[1].charge())
        {  // dphimetjet < 0.4

            /////////////////////
            // Z Control Regions
            ////////////////////


            // CRC_Z
            if (isSF && met > 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("CRC_Z");
            }

            // CRLow_Z
            if (isSF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("CRLow_Z");
            }


            // CRMed_Z
            if (isSF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101)
            {
                accept("CRMed_Z");
            }

            // CRHigh_Z
            if (isSF && met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101)
            {
                accept("CRHigh_Z");
            }

            // CRVRC_Z
            if (isSF && met > 150 && met < 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("CRVRC_Z");
            }

            // CRVRLow_Z
            if (isSF && met > 150 && met < 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("CRVRLow_Z");
            }


            // CRVRMed_Z
            if (isSF && met > 150 && met < 250 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101)
            {
                accept("CRVRMed_Z");
            }

            // CRVRHigh_Z
            if (isSF && met > 150 && met < 250 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101)
            {
                accept("CRVRHigh_Z");
            }

            // onZ
            // CRZLow_Z
            if (isSF && met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("CRZLow_Z");
            }


            // CRZMed_Z
            if (isSF && met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("CRZMed_Z");
            }

            // CRZHigh_Z
            if (isSF && met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("CRZHigh_Z");
            }



            ///////////
            // No lepton flavor requirement, Z CR
            //////////

            // MFRC_Z
            if (met > 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("MFRC_Z");
            }

            // MFRLow_Z
            if (met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("MFRLow_Z");
            }


            // MFRMed_Z
            if (met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101)
            {
                accept("MFRMed_Z");
            }

            // MFRHigh_Z
            if (met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101)
            {
                accept("MFRHigh_Z");
            }

            // MFRVRC_Z
            if (met > 150 && met < 250 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("MFRVRC_Z");
            }

            // MFRVRLow_Z
            if (met > 150 && met < 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("MFRVRLow_Z");
            }


            // MFRVRMed_Z
            if (met > 150 && met < 250 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101)
            {
                accept("MFRVRMed_Z");
            }

            // MFRVRHigh_Z
            if (met > 150 && met < 250 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101)
            {
                accept("MFRVRHigh_Z");
            }

            // onZ
            // MFRZLow_Z
            if (met > 250 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("MFRZLow_Z");
            }


            // MFRZMed_Z
            if (met > 300 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("MFRZMed_Z");
            }

            // MFRZHigh_Z
            if (met > 300 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("MFRZHigh_Z");
            }



            ///////////
            // No lepton flavor requirement, Z CR and inclusive met
            //////////

            // RC_Z
            if (met > 150 && ptll < 100 && mt2 > 90 && metsig > 10 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("RC_Z");
            }

            // RLow_Z
            if (met > 150 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101)
            {
                accept("RLow_Z");
            }


            // RMed_Z
            if (met > 150 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101)
            {
                accept("RMed_Z");
            }

            // RHigh_Z
            if (met > 150 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101)
            {
                accept("RHigh_Z");
            }

            // onZ
            // RZLow_Z
            if (met > 150 && ptll < 500 && mt2 > 100 && HT > 250 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("RZLow_Z");
            }


            // RZMed_Z
            if (met > 150 && ptll < 800 && mt2 > 75 && HT > 500 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("RZMed_Z");
            }

            // RZHigh_Z
            if (met > 150 && mt2 > 75 && HT > 800 && mll > 81 && mll < 101 && n_jets >3)
            {
                accept("RZHigh_Z");
            }
        }

        // Fill Ntuple
        ntupVar("mcDSID",event->getMCNumber());
        ntupVar("nTruthJets20",nTruthJets20);        


    }

    return;

}


