#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <bitset>

DefineAnalysis(MultiJets2018)

void MultiJets2018::Init()
{
  //Control regions

  //Validation regions

  //Multi-bin regions
  for (const std::string& njet : {"8", "9" ,"10"}){
    for (const std::string& nbjet : {"0", "1", "2"}){
      for (const std::string& MJ : {"0", "340", "500"}){
        addRegions({"SR_"+njet+"ij50_"+nbjet+"ib_MJ"+MJ});
      }
    }
  }

  //Single-bin signal regions not covered above.
  addRegions({"SR_11ij50_0ib"});
  addRegions({"SR_12ij50_2ib"});
  addRegions({"SR_9ij80_0ib"});

}

AnalysisObjects manualRecluster(AnalysisObjects smallJets, double radius = 1.0)
{
  // Manual implementation of the anti-kT algorithm to to re-cluster smaller radius jets into
  // larger radius jets. The input vector "smallJets" may include multiple leptons if they are input 
  int nSmallJets = smallJets.size();

  AnalysisObjects largeJets;

  while (nSmallJets>0){

    Double_t dRij=INFINITY;
    Double_t dRi=INFINITY;
    int dRiindex=0;
    int dRijindex1=0;
    int dRijindex2=0;
    for (Int_t i=0; i<nSmallJets; i++){
      if (pow(1./(smallJets.at(i)).Pt(),2)<dRi){
        dRi=pow(1.0/(smallJets.at(i)).Pt(),2);
        dRiindex=i;
      }
    }
    for (Int_t i=0; i<nSmallJets-1; i++){
      for (Int_t j=i+1; j<nSmallJets; j++){
        Double_t tempdRij = std::min(pow(smallJets.at(i).Pt(), -2), pow(smallJets.at(j).Pt(), -2) )
          * smallJets.at(i).DeltaR(smallJets.at(j) ) / radius;
        if (tempdRij<dRij){
          dRij=tempdRij;
          dRijindex1=i;
          dRijindex2=j;
        }
      }
    }

    int deleteIndex=0;
    if (dRi<dRij){//add jet to large jet collection
      deleteIndex=dRiindex;

      AnalysisObject largeJet(smallJets.at(dRiindex) );
      largeJets.push_back( largeJet );

    }else{//add jet j to jet i
      smallJets.at(dRijindex1) += smallJets.at(dRijindex2);
      deleteIndex=dRijindex2;
    }
    //remove the jet from the collection
    smallJets.erase(smallJets.begin()+deleteIndex);
    --nSmallJets;
  }
  return largeJets;
}

void MultiJets2018::ProcessEvent(AnalysisEvent *event)
{
  //Retrieve the required objects, baseline definitions first.
  //Check if we can use filterCrack function
  auto baselineElectrons = filterCrack(event->getElectrons(10, 2.47, ELooseBLLH));
  auto baselineMuons     = event->getMuons(10, 2.5, MuMedium); 
  auto baselinePhotons   = event->getPhotons(25, 2.37, PhotonTight);
  auto baselineJets      = event->getJets(20, 2.8, TightBadJet); //Is this right to use?

  //Reject events with bad jets.
  if (countObjects(baselineJets, 20, 2.8, NOT(LooseBadJet)) !=0) return;

  //overlap removal from SUSY tools proceedure.
  baselineJets       = overlapRemoval(baselineJets, baselineElectrons, 0.2);
  baselineJets       = overlapRemoval(baselineJets, baselineMuons, 0.4, LessThan3Tracks);
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineJets, 0.4);
  baselineMuons      = overlapRemoval(baselineMuons, baselineJets, 0.4); 

  //Number of baseline leptons used for lepton veto.
  int nbaselineLeptons = baselineElectrons.size() + baselineMuons.size();

  //Signal definitions for objects.
  auto signalElectrons = filterObjects(baselineElectrons, 20, 2.47, ETightLH|EIsoLoose|ED0Sigma5|EZ05mm);
  auto signalMuons     = filterObjects(baselineMuons, 20, 2.5, MuIsoLoose|MuZ05mm|MuD0Sigma3);
  auto signalLeptons   = signalElectrons + signalMuons;
  //Jet types.
  auto signalJets      = filterObjects(baselineJets, 20, 2.8, JVT59Jet);
  auto signalBJets     = filterObjects(signalJets, 20, 2.5, BTag70MV2c20);

  //Analysis transverse momentum streams and b-jet selections.
  auto signalJets50    = filterObjects(signalJets, 50, 2.0);
  auto signalBJets50   = filterObjects(signalBJets, 50, 2.0);
  auto signalJets80    = filterObjects(signalJets, 80, 2.0);
  auto signalBJets80   = filterObjects(signalBJets, 80, 2.0);

  //Recluster the large-R jets.
  AnalysisObjects largeRJets = ( signalLeptons.size()> 0 && signalLeptons.front().Pt() > 50)
    ? manualRecluster(signalJets + AnalysisObjects({signalLeptons.front()}) ) 
    : manualRecluster(signalJets);

  //Initialise and calculate the event level large-R jet mass.
  float MJSigma = 0;
  for (const AnalysisObject& largeRJet : largeRJets){
    if (largeRJet.Pt() > 100 && fabs(largeRJet.Eta()) < 1.5){
      MJSigma += largeRJet.M();
    }
  }

  //Define and calculate the hard term (the jet pT sum).
  float HT = sumObjectsPt(signalJets);

  //Retrieve the MetSignificance.
  auto METSig = event->getMETSignificance();

  //Jet type counts.
  int nJet50   = signalJets50.size();
  int nJet80   = signalJets80.size();
  int nBJet50  = countObjects(signalJets, 50., 2.0, BTag70MV2c10);
  int nBJet80  = countObjects(signalJets, 80, 2.0, BTag70MV2c10);

  //Only events with 5 or more jets considered.
  if (nJet50 < 5) return;
  //General HT filter.
  if (HT < 600) return;

  //Begin the analysis stream for signal regions (SRs).
  //Lepton veto.
  if (nbaselineLeptons == 0){
    //METSig cut is the same for all SRs.
    if (METSig > 5.0){

      //50 GeV multi-bin regions.
      for (int nj50 : {8,9,10}){
        if (nJet50 < nj50) break;
        for (int nbj50 : {0,1,2}){
          //std::cout << "N_b-jets: " << nBJet50 << std::endl;
          if (nBJet50 < nbj50) break;
          for (int MJ : {0, 340, 500}){
            if (MJSigma < MJ) break;
            accept("SR_"+std::to_string(nj50)+"ij50_"+std::to_string(nbj50)+"ib_MJ"+std::to_string(MJ));
          }
        }           
      }

      //50GeV and 80 GeV single-bin regions.
      if (nJet50 >= 11 && nBJet50 >= 0 && MJSigma > 0  ) accept("SR_11ij50_0ib");
      if (nJet50 >= 12 && nBJet50 >= 2 && MJSigma > 0  ) accept("SR_12ij50_2ib");
      if (nJet80 >= 9  && nBJet80 >= 0 && MJSigma > 0  ) accept("SR_9ij80_0ib");
      
    }
  }


  return;
  
}
