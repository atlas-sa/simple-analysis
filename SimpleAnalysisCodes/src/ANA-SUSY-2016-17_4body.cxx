#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopTwoLepton2016_4body)

void StopTwoLepton2016_4body::Init()
{
  addRegions({"SR4b"});

}

void StopTwoLepton2016_4body::ProcessEvent(AnalysisEvent *event)
{

  auto baselineElectrons = event->getElectrons(7, 2.47, ELooseLH);
  auto baselineMuons = event->getMuons(7, 2.5, MuMedium | MuQoPSignificance);
  auto jets          = event->getJets(20., 2.8, JVT50Jet);
  auto metVec        = event->getMET();

  // SUSY overlap removal
  jets               = overlapRemoval(jets, baselineElectrons, 0.2);
  jets               = overlapRemoval(jets, baselineMuons, 0.4, LessThan3Tracks);
  baselineElectrons  = overlapRemoval(baselineElectrons, jets, 0.4);
  baselineMuons      = overlapRemoval(baselineMuons, jets, 0.4);

  auto signalElectrons = filterObjects(baselineElectrons, 7, 2.47, EMediumLH | EZ05mm | EIsoGradientLoose);
  auto signalMuons     = filterObjects(baselineMuons, 7, 2.4, MuD0Sigma3 | MuZ05mm | MuIsoGradientLoose);
  auto bjets           = filterObjects(jets, 25., 2.5, BTag77MV2c20);
       jets            = filterObjects(jets, 25., 2.5);

  auto n_jets25         = countObjects(jets, 25, 2.5);

  AnalysisObjects signalLeptons = signalElectrons + signalMuons;

  int nLeptons = signalLeptons.size();
  if (nLeptons != 2) return;

  auto lep0 = signalLeptons[0];
  auto lep1 = signalLeptons[1];

  // Variables we'll be cutting on
  double MET                            = metVec.Et();
  double mll                            = (lep0 + lep1).M();

  double ptJet1;
  double ptJet2;
  double ptJet3;
  double ptJet4;

  if ( n_jets25 >= 1) ptJet1 = jets[0].Pt();
  if (n_jets25 < 1) ptJet1  = 0.;
  if ( n_jets25 >= 2) ptJet2 = jets[1].Pt();
  if (n_jets25 < 2) ptJet2  = 0.;
  if ( n_jets25 >= 3) ptJet3 = jets[2].Pt();
  if (n_jets25 < 3) ptJet3  = 0.;
  if ( n_jets25 >= 4) ptJet4 = jets[3].Pt();
  if (n_jets25 < 4) ptJet4  = 0.;

  double Rll                            = MET / (lep0.Pt() + lep1.Pt());
  double Rjl                            = MET / (MET + lep0.Pt() + lep1.Pt() + ptJet1 + ptJet2 + ptJet3 + ptJet4);

  //Opposite Sign leptons
  if (lep0.charge() == lep1.charge()) return;

  //SR4Body
  if (n_jets25 > 1 && mll > 10. && MET > 200. && jets[0].Pt() > 150. && ptJet3/MET < 0.14 && lep0.Pt() > 7. && lep0.Pt() < 80. && lep1.Pt() > 7. && lep1.Pt() < 35. && Rjl > 0.35 && Rll > 12. && !(jets[0].pass(BTag77MV2c20)) && !(jets[1].pass(BTag77MV2c20))) accept("SR4b");

  return;
}
