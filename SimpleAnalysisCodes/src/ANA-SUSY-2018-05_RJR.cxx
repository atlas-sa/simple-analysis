#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(EwkTwoLeptonRJ2018)

    void EwkTwoLeptonRJ2018::Init() {
        // add signal regions
        addRegions({"CR2L_VV","CR2L_TOP","CR2L_ISR_VV","CR2L_ISR_TOP","VR2L_VV","VR2L_TOP","VR2L_ISR_VV","VR2L_ISR_TOP","A_std","B_std","D_std","AVR_ISR","BE_ISR","D_ISR","SR2L_High","SR2L_Int","SR2L_Low","SR2L_ISR"});


        // set up RestFrames using the helper
        LabRecoFrame* LAB_2L2J  = m_RF_helper.addLabFrame("LAB_2L2J");
        DecayRecoFrame* C1N2_2L2J = m_RF_helper.addDecayFrame("C1N2_2L2J");
        DecayRecoFrame* C1a_2L2J  = m_RF_helper.addDecayFrame("C1a_2L2J");
        DecayRecoFrame* N2b_2L2J  = m_RF_helper.addDecayFrame("N2b_2L2J");

        VisibleRecoFrame* J1_2L2J = m_RF_helper.addVisibleFrame("J1_2L2J");
        VisibleRecoFrame* J2_2L2J = m_RF_helper.addVisibleFrame("J2_2L2J");
        VisibleRecoFrame* L1b_2L2J = m_RF_helper.addVisibleFrame("L1b_2L2J");
        VisibleRecoFrame* L2b_2L2J = m_RF_helper.addVisibleFrame("L2b_2L2J");

        InvisibleRecoFrame* X1a_2L2J  = m_RF_helper.addInvisibleFrame("X1a_2L2J");
        InvisibleRecoFrame* X1b_2L2J  = m_RF_helper.addInvisibleFrame("X1b_2L2J");


        LAB_2L2J->SetChildFrame(*C1N2_2L2J);

        C1N2_2L2J->AddChildFrame(*C1a_2L2J);
        C1N2_2L2J->AddChildFrame(*N2b_2L2J);

        C1a_2L2J->AddChildFrame(*J1_2L2J);
        C1a_2L2J->AddChildFrame(*J2_2L2J);
        C1a_2L2J->AddChildFrame(*X1a_2L2J);

        N2b_2L2J->AddChildFrame(*L1b_2L2J);
        N2b_2L2J->AddChildFrame(*L2b_2L2J);
        N2b_2L2J->AddChildFrame(*X1b_2L2J);


        LAB_2L2J->InitializeTree();

        //setting the invisible components
        InvisibleGroup* INV_2L2J = m_RF_helper.addInvisibleGroup("INV_2L2J");
        INV_2L2J->AddFrame(*X1a_2L2J);
        INV_2L2J->AddFrame(*X1b_2L2J);

        // Set di-LSP mass to minimum Lorentz-invariant expression
        InvisibleJigsaw* X1_mass_2L2J = m_RF_helper.addInvisibleJigsaw("X1_mass_2L2J", kSetMass);
        INV_2L2J->AddJigsaw(*X1_mass_2L2J);

        // Set di-LSP rapidity to that of visible particles and neutrino
        InvisibleJigsaw* X1_eta_2L2J = m_RF_helper.addInvisibleJigsaw("X1_eta_2L2J", kSetRapidity);
        INV_2L2J->AddJigsaw(*X1_eta_2L2J);
        X1_eta_2L2J->AddVisibleFrames(C1N2_2L2J->GetListVisibleFrames());

        InvisibleJigsaw* X1X1_contra_2L2J = m_RF_helper.addInvisibleJigsaw("X1X1_contra_2L2J",kContraBoost);
        INV_2L2J->AddJigsaw(*X1X1_contra_2L2J);
        X1X1_contra_2L2J->AddVisibleFrames(C1a_2L2J->GetListVisibleFrames(),0);
        X1X1_contra_2L2J->AddVisibleFrames(N2b_2L2J->GetListVisibleFrames(),1);
        X1X1_contra_2L2J->AddInvisibleFrames(C1a_2L2J->GetListInvisibleFrames(),0);
        X1X1_contra_2L2J->AddInvisibleFrames(N2b_2L2J->GetListInvisibleFrames(),1);

        LAB_2L2J->InitializeAnalysis();

        // Compressed tree time
        // combinatoric (transverse) tree
        // for jet assignment
        LabRecoFrame* LAB_comb = m_RF_helper.addLabFrame("LAB_comb");
        DecayRecoFrame* CM_comb  = m_RF_helper.addDecayFrame("CM_comb");
        DecayRecoFrame* S_comb   = m_RF_helper.addDecayFrame("S_comb");
        VisibleRecoFrame* ISR_comb = m_RF_helper.addVisibleFrame("ISR_comb");
        VisibleRecoFrame* J_comb   =  m_RF_helper.addVisibleFrame("J_comb");
        VisibleRecoFrame* L_comb   = m_RF_helper.addVisibleFrame("L_comb");
        InvisibleRecoFrame* I_comb   =  m_RF_helper.addInvisibleFrame("I_comb");
        //cout << "test 5" << endl;
        LAB_comb->SetChildFrame(*CM_comb);
        CM_comb->AddChildFrame(*ISR_comb);
        CM_comb->AddChildFrame(*S_comb);
        S_comb->AddChildFrame(*L_comb);
        S_comb->AddChildFrame(*J_comb);
        S_comb->AddChildFrame(*I_comb);
        // cout << "test 6" << endl;
        LAB_comb->InitializeTree();

        // 2L+1L tree (Z->ll + Z/W->l)
        LabRecoFrame* LAB_2LNJ = m_RF_helper.addLabFrame("LAB_2LNJ");
        DecayRecoFrame* CM_2LNJ  =  m_RF_helper.addDecayFrame("CM_2LNJ");
        DecayRecoFrame* S_2LNJ   = m_RF_helper.addDecayFrame("S_2LNJ");
        VisibleRecoFrame* ISR_2LNJ = m_RF_helper.addVisibleFrame("ISR_2LNJ");
        DecayRecoFrame* Ca_2LNJ  = m_RF_helper.addDecayFrame("Ca_2LNJ");
        DecayRecoFrame* Z_2LNJ   = m_RF_helper.addDecayFrame("Z_2LNJ");
        VisibleRecoFrame* L1_2LNJ  = m_RF_helper.addVisibleFrame("L1_2LNJ");
        VisibleRecoFrame* L2_2LNJ  = m_RF_helper.addVisibleFrame("L2_2LNJ");
        DecayRecoFrame* Cb_2LNJ  = m_RF_helper.addDecayFrame("Cb_2LNJ");
        SelfAssemblingRecoFrame* JSA_2LNJ = m_RF_helper.addSAFrame("JSA_2LNJ"); 
        VisibleRecoFrame* J_2LNJ = m_RF_helper.addVisibleFrame("J_2LNJ");
        InvisibleRecoFrame* Ia_2LNJ  = m_RF_helper.addInvisibleFrame("Ia_2LNJ");
        InvisibleRecoFrame* Ib_2LNJ  = m_RF_helper.addInvisibleFrame("Ib_2LNJ");

        LAB_2LNJ->SetChildFrame(*CM_2LNJ);
        CM_2LNJ->AddChildFrame(*ISR_2LNJ);
        CM_2LNJ->AddChildFrame(*S_2LNJ);
        S_2LNJ->AddChildFrame(*Ca_2LNJ);
        S_2LNJ->AddChildFrame(*Cb_2LNJ);
        Ca_2LNJ->AddChildFrame(*Z_2LNJ);
        Ca_2LNJ->AddChildFrame(*Ia_2LNJ);
        Z_2LNJ->AddChildFrame(*L1_2LNJ);
        Z_2LNJ->AddChildFrame(*L2_2LNJ);
        Cb_2LNJ->AddChildFrame(*JSA_2LNJ);
        Cb_2LNJ->AddChildFrame(*Ib_2LNJ);
        JSA_2LNJ->AddChildFrame(*J_2LNJ);
        LAB_2LNJ->InitializeTree();

        ////////////// Jigsaw rules set-up /////////////////

        // combinatoric (transverse) tree
        // for jet assignment
        InvisibleGroup* INV_comb = m_RF_helper.addInvisibleGroup("INV_comb");
        INV_comb->AddFrame(*I_comb);

        InvisibleJigsaw* InvMass_comb = m_RF_helper.addInvisibleJigsaw("InvMass_comb", kSetMass);
        INV_comb->AddJigsaw(*InvMass_comb);

        CombinatoricGroup* JETS_comb = m_RF_helper.addCombinatoricGroup("JETS_comb");
        JETS_comb->AddFrame(*ISR_comb);
        JETS_comb->SetNElementsForFrame(*ISR_comb, 1,false);
        JETS_comb->AddFrame(*J_comb);
        JETS_comb->SetNElementsForFrame(*J_comb, 0,false);

        MinMassesCombJigsaw* SplitJETS_comb = m_RF_helper.addCombinatoricJigsaw("SplitJETS_comb", kMinMasses);
        JETS_comb->AddJigsaw(*SplitJETS_comb);
        SplitJETS_comb->AddFrame(*ISR_comb, 0);
        SplitJETS_comb->AddFrame(*J_comb, 1);
        SplitJETS_comb->AddObjectFrame(*ISR_comb,0);
        SplitJETS_comb->AddObjectFrame(*S_comb,1);

        LAB_comb->InitializeAnalysis();

        // 2L+1L tree (Z->ll + Z/W->l)
        InvisibleGroup* INV_2LNJ = m_RF_helper.addInvisibleGroup("INV_2LNJ");
        INV_2LNJ->AddFrame(*Ia_2LNJ);
        INV_2LNJ->AddFrame(*Ib_2LNJ);

        InvisibleJigsaw* InvMass_2LNJ = m_RF_helper.addInvisibleJigsaw("InvMass_2LNJ", kSetMass);
        INV_2LNJ->AddJigsaw(*InvMass_2LNJ);
        InvisibleJigsaw* InvRapidity_2LNJ = m_RF_helper.addInvisibleJigsaw("InvRapidity_2LNJ", kSetRapidity);
        INV_2LNJ->AddJigsaw(*InvRapidity_2LNJ);
        InvRapidity_2LNJ->AddVisibleFrames(S_2LNJ->GetListVisibleFrames());
        InvisibleJigsaw* SplitINV_2LNJ = m_RF_helper.addInvisibleJigsaw("SplitINV_2LNJ", kContraBoost);
        INV_2LNJ->AddJigsaw(*SplitINV_2LNJ);
        SplitINV_2LNJ->AddVisibleFrames(Ca_2LNJ->GetListVisibleFrames(), 0);
        SplitINV_2LNJ->AddVisibleFrames(Cb_2LNJ->GetListVisibleFrames(), 1);
        SplitINV_2LNJ->AddInvisibleFrame(*Ia_2LNJ, 0);
        SplitINV_2LNJ->AddInvisibleFrame(*Ib_2LNJ, 1);

        CombinatoricGroup* JETS_2LNJ = m_RF_helper.addCombinatoricGroup("JETS_2LNJ");
        JETS_2LNJ->AddFrame(*J_2LNJ);
        JETS_2LNJ->SetNElementsForFrame(*J_2LNJ,0);

        LAB_2LNJ->InitializeAnalysis();

    }

void EwkTwoLeptonRJ2018::ProcessEvent(AnalysisEvent *event) {

    auto electrons = event->getElectrons(10.,2.47, ELooseLH);
    auto muons = event->getMuons(10.,2.7, MuMedium);
    auto loosejets = event->getJets(20.,4.5);
    auto loosebjets = filterObjects(loosejets, 20., 4.5, BTag77MV2c10);
    auto metVec = event->getMET();

    // Overlap removal 
    auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };

    electrons = overlapRemoval(electrons,muons,0.01);
    electrons = overlapRemoval(electrons,loosebjets,0.2,BTag85MV2c10);
    loosejets = overlapRemoval(loosejets,electrons,0.2,NOT(BTag85MV2c10));
    electrons = overlapRemoval(electrons,loosejets,0.4);
    loosejets = overlapRemoval(loosejets,muons,radiusCalcJet,LessThan3Tracks);
    muons = overlapRemoval(muons, loosejets, 0.4);

    // Filter objects to selection standard 
    auto sigelectrons = filterObjects(electrons,10,2.47,EMediumLH|ED0Sigma5|EZ05mm|EIsoBoosted);
    auto sigmuons = filterObjects(muons,10,2.4, MuD0Sigma3|MuZ05mm|MuIsoBoosted|MuNotCosmic);
    auto jets = filterObjects(loosejets,20,2.4);
    auto bjets = filterObjects(loosebjets,20,2.4);
    auto leptons = sigelectrons+sigmuons;

    // sort objects by pt just in case 
    sortObjectsByPt( jets );
    sortObjectsByPt( leptons );

    if (leptons.size()<2) {
        return;
    }
    int njets = jets.size();
    if (njets<2) return;

    int nbjets = bjets.size();

    // We need to use 3 and 4 lepton events to do VV CRs and VRs, so we make a requirement on 2L here
    if (leptons.size()==2) {
        // some preselections 
        auto lep1 = leptons[0];
        auto lep2 = leptons[1];

        if (lep1.Pt()<25.) return;
        if (lep2.Pt()<25.) return;

        bool iscomp = false;
        if (njets>2) iscomp = true;

        // let's do the open tree
        LabRecoFrame* LAB_2L2J = m_RF_helper.getLabFrame("LAB_2L2J");
        InvisibleGroup* INV_2L2J = m_RF_helper.getInvisibleGroup("INV_2L2J");
        VisibleRecoFrame* J1_2L2J = m_RF_helper.getVisibleFrame("J1_2L2J");
        VisibleRecoFrame* J2_2L2J = m_RF_helper.getVisibleFrame("J2_2L2J");
        VisibleRecoFrame* L1b_2L2J = m_RF_helper.getVisibleFrame("L1b_2L2J");
        VisibleRecoFrame* L2b_2L2J = m_RF_helper.getVisibleFrame("L2b_2L2J");

        LAB_2L2J->ClearEvent();

        // SFOS criteria
        double diff = 10000000000.0;
        int Zlep1 = -99;
        int Zlep2 = -99;
        double Zmass = -999.0;
        bool foundSFOS = false;

        for(unsigned int i=0; i<leptons.size(); i++)
        {
            for(unsigned int j=i+1; j<leptons.size(); j++)
            {
                if(IsSFOS(leptons[i],leptons[j]))  {
                    double mass = (leptons[i]+leptons[j]).M();
                    double massdiff = fabs(mass-91.1876);
                    if(massdiff<diff)
                    {
                        diff=massdiff;
                        Zmass=mass;
                        Zlep1 = i;
                        Zlep2 = j;
                        foundSFOS = true;
                    }
                }

            }
        }

        if(!foundSFOS)
        {
            return;
        }

        // Use first and second jets 
        double mjj = (jets[0]+jets[1]).M();

        double mindphi=100000.;
        double dphi=0;
        TLorentzVector tempjet;
        for(unsigned int ijet=0; ijet<jets.size();ijet++)
        { 
            tempjet.SetPtEtaPhiM(jets[ijet].Pt(),jets[ijet].Eta(),jets[ijet].Phi(),jets[ijet].M());
            dphi = fabs(metVec.DeltaPhi(tempjet));
            if(dphi<mindphi) mindphi=dphi;
        }

        // Send the leptons to their place
        L1b_2L2J->SetLabFrameFourVector(leptons[Zlep1]);
        L2b_2L2J->SetLabFrameFourVector(leptons[Zlep2]);
        J1_2L2J->SetLabFrameFourVector(jets[0]);
        J2_2L2J->SetLabFrameFourVector(jets[1]);

        // met
        INV_2L2J->SetLabFrameThreeVector(metVec.Vect());

        LAB_2L2J->AnalyzeEvent();

        // Set up the variables we cut on

        // PP frame 
        DecayRecoFrame* C1N2_2L2J = m_RF_helper.getDecayFrame("C1N2_2L2J");
        InvisibleRecoFrame* X1a_2L2J = m_RF_helper.getInvisibleFrame("X1a_2L2J");
        InvisibleRecoFrame* X1b_2L2J = m_RF_helper.getInvisibleFrame("X1b_2L2J");

        TLorentzVector vP_V1aPP  = J1_2L2J->GetFourVector(*C1N2_2L2J);
        TLorentzVector vP_V2aPP  = J2_2L2J->GetFourVector(*C1N2_2L2J);
        TLorentzVector vP_V1bPP  = L1b_2L2J->GetFourVector(*C1N2_2L2J);
        TLorentzVector vP_V2bPP  = L2b_2L2J->GetFourVector(*C1N2_2L2J);
        TLorentzVector vP_I1aPP  = X1a_2L2J->GetFourVector(*C1N2_2L2J);
        TLorentzVector vP_I1bPP  = X1b_2L2J->GetFourVector(*C1N2_2L2J);

        double H2PP = (vP_V1aPP + vP_V2aPP + vP_V1bPP + vP_V2bPP).P() + (vP_I1aPP + vP_I1bPP).P();//H(1,1)PP

        double H5PP = vP_V1aPP.P() + vP_V2aPP.P() + vP_V1bPP.P() + vP_V2bPP.P() + (vP_I1aPP + vP_I1bPP).P();//H(3,1)PP
        double HT5PP = vP_V1aPP.Pt() + vP_V2aPP.Pt() + vP_V1bPP.Pt() + vP_V2bPP.Pt() + (vP_I1aPP + vP_I1bPP).Pt();//HT(3,1)PP

        double R_H2PP_H5PP = H2PP/H5PP;

        TVector3 vP_PP = C1N2_2L2J->GetFourVector(*LAB_2L2J).Vect();
        double Pt_PP = vP_PP.Pt();
        double RPT_HT5PP = Pt_PP/(Pt_PP + HT5PP);

        double dphiVP = C1N2_2L2J->GetDeltaPhiDecayVisible();

        // P frame
        DecayRecoFrame* C1a_2L2J = m_RF_helper.getDecayFrame("C1a_2L2J");
        DecayRecoFrame* N2b_2L2J = m_RF_helper.getDecayFrame("N2b_2L2J"); 
        TLorentzVector vP_V1aPa  = J1_2L2J->GetFourVector(*C1a_2L2J);
        TLorentzVector vP_V2aPa = J2_2L2J->GetFourVector(*C1a_2L2J);
        TLorentzVector vP_I1aPa  = X1a_2L2J->GetFourVector(*C1a_2L2J);

        TLorentzVector vP_V1bPb = L1b_2L2J->GetFourVector(*N2b_2L2J);
        TLorentzVector vP_V2bPb = L2b_2L2J->GetFourVector(*N2b_2L2J);
        TLorentzVector vP_I1bPb = X1b_2L2J->GetFourVector(*N2b_2L2J);

        double H2Pa = (vP_V1aPa + vP_V2aPa).P() + (vP_I1aPa).P(); //H(1,1)P
        double H2Pb = (vP_V1bPb + vP_V2bPb).P() + vP_I1bPb.P();//H(1,1)P

        double H3Pa = vP_V1aPa.P() + vP_V2aPa.P() + vP_I1aPa.P();//H(1,1)P
        double H3Pb = vP_V1bPb.P() + vP_V2bPb.P() + vP_I1bPb.P();//H(2,1)P

        double minH2P = std::min(H2Pa,H2Pb);
        double minH3P = std::min(H3Pa,H3Pb);
        //    double R_H2Pa_H2Pb = H2Pa/H2Pb;
        //double R_H3Pa_H3Pb = H3Pa/H3Pb;
        double R_minH2P_minH3P = minH2P/minH3P;

        //std::cout << "STANDARD REGIONS" << std::endl;
        // we are ready to go

        // Low mass
        if (njets==2 && nbjets==0 && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>70. && mjj<90. && H5PP>400. && RPT_HT5PP<0.05 && R_H2PP_H5PP>0.35 && R_H2PP_H5PP<0.65 && mindphi>2.4) accept("SR2L_Low");

        // ABCD regions
        if (njets==2 && nbjets==0 && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>20. && mjj<70. && H5PP>400. && RPT_HT5PP<0.05 && R_H2PP_H5PP>0.35 && R_H2PP_H5PP<0.65 && mindphi>2.4) accept("A_std");
        
        if (njets==2 && nbjets==0 && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>20. && mjj<70. && H5PP>400. && RPT_HT5PP<0.05 && R_H2PP_H5PP>0.20 && R_H2PP_H5PP<0.35 && mindphi>2.4) accept("B_std");
        
        if (njets==2 && nbjets==0 && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>70. && mjj<90. && H5PP>400. && RPT_HT5PP<0.05 && R_H2PP_H5PP>0.20 && R_H2PP_H5PP<0.35 && mindphi>2.4) accept("D_std");
       
        // Intermediate
        if (nbjets==0 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>60. && mjj<100. && H5PP>600. && RPT_HT5PP<0.05 && dphiVP>0.6 && dphiVP<2.6 && R_minH2P_minH3P>0.8) accept("SR2L_Int");

        // High
        if (leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>60. && mjj<100. && H5PP>800. && RPT_HT5PP<0.05 && dphiVP>0.3 && dphiVP<2.8 && R_minH2P_minH3P>0.8) accept("SR2L_High");

        // VR2L-VV
        if (nbjets==0 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && ((mjj>40. && mjj<70.) || (mjj>90. && mjj<500.)) && R_minH2P_minH3P>0.4 && R_minH2P_minH3P<0.8 && RPT_HT5PP<0.05 && dphiVP>0.3 && dphiVP<2.8 && H2PP>250. && H5PP>500.) accept("VR2L_VV");

        // CR2L-Top
        if (nbjets==1 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && ((Zmass>20. && Zmass<80.) || (Zmass>100.)) && mjj>40. && mjj<250. && H5PP>400. && RPT_HT5PP<0.05 && dphiVP>0.3 && dphiVP<2.8 && R_minH2P_minH3P>0.5) accept("CR2L_TOP");
        // VR2L-Top
        if (nbjets==1 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && Zmass>80. && Zmass<100. && mjj>40. && mjj<250. && H5PP>400. && RPT_HT5PP<0.05 && dphiVP>0.3 && dphiVP<2.8 && R_minH2P_minH3P>0.5) accept("VR2L_TOP");

        // Compressed time 
        if(iscomp) {
            //std::cout << "ISR REGIONS" << std::endl;
            // Combinatoric setup for jets
            LabRecoFrame* LAB_comb = m_RF_helper.getLabFrame("LAB_comb");
            //        VisibleRecoFrame* ISR_comb = m_RF_helper.getVisibleFrame("ISR_comb");
            CombinatoricGroup* JETS_comb = m_RF_helper.getCombinatoricGroup("JETS_comb");    
            VisibleRecoFrame* J_comb = m_RF_helper.getVisibleFrame("J_comb");
            VisibleRecoFrame* L_comb = m_RF_helper.getVisibleFrame("L_comb");
            InvisibleGroup* INV_comb = m_RF_helper.getInvisibleGroup("INV_comb");
            LAB_comb->ClearEvent();

            double NjS = 0;
            double NjISR = 0;

            // do mass minimisation for jets 
            std::vector<RFKey> jetID;
            for (const auto jet : jets ) {
                jetID.push_back(JETS_comb->AddLabFrameFourVector(jet.transFourVect()));
            } 

            TLorentzVector lepSys(0.,0.,0.,0.);
            for(const auto lep1 : leptons){
                lepSys = lepSys + lep1.transFourVect();
            }
            L_comb->SetLabFrameFourVector(lepSys);

            INV_comb->SetLabFrameThreeVector(metVec.Vect());

            LAB_comb->AnalyzeEvent();

            for (unsigned int i=0; i<jets.size(); i++) {
                if (JETS_comb->GetFrame(jetID[i])==*J_comb) {
                    NjS++;

                }
                else {
                    NjISR++;
                }
            }
            LabRecoFrame* LAB_2LNJ = m_RF_helper.getLabFrame("LAB_2LNJ");
            VisibleRecoFrame* ISR_2LNJ = m_RF_helper.getVisibleFrame("ISR_2LNJ");
            VisibleRecoFrame* L1_2LNJ = m_RF_helper.getVisibleFrame("L1_2LNJ");
            VisibleRecoFrame* L2_2LNJ = m_RF_helper.getVisibleFrame("L2_2LNJ");

            InvisibleGroup* INV_2LNJ = m_RF_helper.getInvisibleGroup("INV_2LNJ");
            DecayRecoFrame* CM_2LNJ = m_RF_helper.getDecayFrame("CM_2LNJ");
            InvisibleRecoFrame* Ia_2LNJ = m_RF_helper.getInvisibleFrame("Ia_2LNJ");       
            InvisibleRecoFrame* Ib_2LNJ = m_RF_helper.getInvisibleFrame("Ib_2LNJ");       
            VisibleRecoFrame* J_2LNJ = m_RF_helper.getVisibleFrame("J_2LNJ");
            DecayRecoFrame* Z_2LNJ = m_RF_helper.getDecayFrame("Z_2LNJ");       
            CombinatoricGroup* JETS_2LNJ = m_RF_helper.getCombinatoricGroup("JETS_2LNJ");

            LAB_2LNJ->ClearEvent();

            TLorentzVector vISR(0.0,0.0,0.0,0.0);
            //std::cout << "no of jets: " << jetID.size() << std::endl;
            for (unsigned int i=0; i<jets.size(); i++) {
                if (JETS_comb->GetFrame(jetID[i])==*J_comb) {
                    JETS_2LNJ->AddLabFrameFourVector(jets[i]);

                }
                else {
                    vISR += jets[i];

                }
            }
            ISR_2LNJ->SetLabFrameFourVector(vISR);

            L1_2LNJ->SetLabFrameFourVector(leptons[Zlep1]);
            L2_2LNJ->SetLabFrameFourVector(leptons[Zlep2]);

            INV_2LNJ->SetLabFrameThreeVector(metVec.Vect());

            LAB_2LNJ->AnalyzeEvent();

            // Make the variables 
            TLorentzVector vP_CM;
            TLorentzVector vP_ISR;
            TLorentzVector vP_Ia;
            TLorentzVector vP_Ib;
            TLorentzVector vP_I;
            vP_Ia = Ia_2LNJ->GetFourVector();
            vP_Ib = Ib_2LNJ->GetFourVector();
            vP_CM  = CM_2LNJ->GetFourVector();
            vP_ISR = ISR_2LNJ->GetFourVector();
            vP_I   = vP_Ia + vP_Ib;

            double PTCM = vP_CM.Pt();

            TVector3 boostZ = vP_CM.BoostVector();
            boostZ.SetX(0.);
            boostZ.SetY(0.);

            vP_CM.Boost(-boostZ);
            vP_ISR.Boost(-boostZ);
            vP_I.Boost(-boostZ);

            TVector3 boostT = vP_CM.BoostVector();
            vP_ISR.Boost(-boostT);
            vP_I.Boost(-boostT);

            TVector3 vPt_ISR = vP_ISR.Vect();
            TVector3 vPt_I   = vP_I.Vect();
            vPt_ISR -= vPt_ISR.Dot(boostZ.Unit())*boostZ.Unit();
            vPt_I   -= vPt_I.Dot(boostZ.Unit())*boostZ.Unit();

            double PTISR =  vPt_ISR.Mag();
            double RISR  = -vPt_I.Dot(vPt_ISR.Unit()) / PTISR;
            double PTI = vPt_I.Mag();
            double dphiISRI = fabs(vPt_ISR.Angle(vPt_I));
            double MJ = J_2LNJ->GetMass();
            double MZ = Z_2LNJ->GetMass();

            // finally do the selections
            if (NjS==2 && NjISR<3 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>80. && MZ<100. && MJ>50. && MJ<110. && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI>100. && PTCM<20.) accept("SR2L_ISR");
        // ABCD regions
            if (NjS==2 && NjISR<3 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>80. && MZ<100. && ((MJ>0. && MJ<50.)||(MJ>110)) && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI>100. && PTCM<20.) accept("AVR_ISR");
            if (NjS==2 && NjISR<3 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>80. && MZ<100. && ((MJ>0. && MJ<50.)||(MJ>110)) && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI<100. && PTCM<20.) accept("BE_ISR");
            if (NjS==2 && NjISR<3 && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>80. && MZ<100. && MJ>50. && MJ<110. && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI<100. && PTCM<20.) accept("D_ISR");

            // CR2L_ISR-Top
            if (nbjets==1 && NjS==2 && (NjISR==1 || NjISR==2) && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>50. && MZ<200. && MJ>50. && MJ<200. && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI>100. && PTCM<20.) accept("CR2L_ISR_TOP");

            // VR2L_ISR-Top
            if (nbjets==1 && NjS==2 && (NjISR==1 || NjISR==2) && leptons[0].Pt()>25. && leptons[1].Pt()>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ>50. && MZ<200. && MJ>50. && MJ<200. && dphiISRI>2.8 && RISR>0.4 && RISR<0.75 && PTISR>180. && PTI>100. && PTCM>20.) accept("VR2L_ISR_TOP");
        }
    } // End of 2L requirement 

    // Now looking at 3 and 4L 
    if (leptons.size() == 3 || leptons.size() == 4) {
        //std::cout << "3 AND 4 LEPTONS" << std::endl;
        //find the lepton pair closer to the Z-pole
        int indexlep1 =-99;
        int indexlep2 =-99;
        int indexlep3 =-99;
        int indexlep4 =-99;
        double minMass= 1000000000;
        bool foundSFOS=false;
        double theMass = 0;
        for(unsigned int ilep=0; ilep<leptons.size(); ilep++) {
            for(unsigned int jlep=ilep+1; jlep<leptons.size(); jlep++) {
                if(IsSFOS(leptons[ilep],leptons[jlep])) {
                    double theMass = (leptons[ilep]+leptons[jlep]).M();
                    double Zdiff = fabs(theMass-91.1876);
                    if(Zdiff<minMass){
                        minMass=Zdiff;
                        indexlep1=ilep;
                        indexlep2=jlep;
                        foundSFOS=true;
                    }
                }
            }
        }
        if(!foundSFOS) return;

        if(leptons.size() == 3 && jets.size() >= 2) {
            if( (indexlep1==0 && indexlep2==1) || (indexlep1==1 && indexlep2==0) ) indexlep3 = 2;
            else if( (indexlep1==0 && indexlep2==2) || (indexlep1==2 && indexlep2==0) ) indexlep3 = 1;
            else if( (indexlep1==1 && indexlep2==2) || (indexlep1==2 && indexlep2==1) ) indexlep3 = 0;
        }
        else if(leptons.size() == 4 && jets.size() >= 2) {
            if( (indexlep1==0 && indexlep2==1) || (indexlep1==1 && indexlep2==0) ) {
                indexlep3 = 2;  indexlep4 = 3;
            }
            else if( (indexlep1==0 && indexlep2==2) || (indexlep1==2 && indexlep2==0) ) {
                indexlep3 = 1; indexlep4 = 3;
            }
            else if( (indexlep1==0 && indexlep2==3) || (indexlep1==3 && indexlep2==0) ) {
                indexlep3 = 1; indexlep4 = 2;
            }
            else if( (indexlep1==1 && indexlep2==2) || (indexlep1==2 && indexlep2==1) ) {
                indexlep3 = 0; indexlep4 = 3;
            }
            else if( (indexlep1==1 && indexlep2==3) || (indexlep1==3 && indexlep2==1) ) {
                indexlep3 = 1; indexlep4 = 2;
            }
            else if( (indexlep1==2 && indexlep2==3) || (indexlep1==3 && indexlep2==2) ) {
                indexlep3 = 0; indexlep4 = 1;
            }
        }

        if(leptons.size() == 4 && jets.size() >= 2) {
            if(!IsSFOS(leptons[indexlep3],leptons[indexlep4])) return;
        }

        double lept1Pt_VR = leptons[indexlep1].Pt();
        double lept2Pt_VR = leptons[indexlep2].Pt();
        double mll_VR = (leptons[indexlep1]+leptons[indexlep2]).M();
        double mjj_VR = (jets[0]+jets[1]).M();

        double mTl3 = calcMT(leptons[indexlep3],metVec);


        LabRecoFrame* LAB_2L2J = m_RF_helper.getLabFrame("LAB_2L2J");
        DecayRecoFrame* C1N2_2L2J = m_RF_helper.getDecayFrame("C1N2_2L2J");
        DecayRecoFrame* C1a_2L2J = m_RF_helper.getDecayFrame("C1a_2L2J");
        DecayRecoFrame* N2b_2L2J = m_RF_helper.getDecayFrame("N2b_2L2J"); 
        InvisibleRecoFrame* X1a_2L2J = m_RF_helper.getInvisibleFrame("X1a_2L2J");
        InvisibleRecoFrame* X1b_2L2J = m_RF_helper.getInvisibleFrame("X1b_2L2J");
        InvisibleGroup* INV_2L2J = m_RF_helper.getInvisibleGroup("INV_2L2J");
        VisibleRecoFrame* J1_2L2J = m_RF_helper.getVisibleFrame("J1_2L2J");
        VisibleRecoFrame* J2_2L2J = m_RF_helper.getVisibleFrame("J2_2L2J");
        VisibleRecoFrame* L1b_2L2J = m_RF_helper.getVisibleFrame("L1b_2L2J");
        VisibleRecoFrame* L2b_2L2J = m_RF_helper.getVisibleFrame("L2b_2L2J");

        if(jets.size() >=2) {
            // Now run the jigsaws with the extra leptons being added to the MET
            LAB_2L2J->ClearEvent();

            L1b_2L2J->SetLabFrameFourVector(leptons[indexlep1]);
            L2b_2L2J->SetLabFrameFourVector(leptons[indexlep2]);
            J1_2L2J->SetLabFrameFourVector(jets[0]);
            J2_2L2J->SetLabFrameFourVector(jets[1]);

            // Need to add the extra leptons to the MET depending on how many there are
            TVector3 MET;
            TVector3 Veclep1,Veclep2;
            if(leptons.size() == 3) {
                Veclep1.SetXYZ(leptons[indexlep3].Px(),leptons[indexlep3].Py(),0.);
                MET = metVec.Vect() + Veclep1;
            }
            if(leptons.size() == 4) {
                Veclep1.SetXYZ(leptons[indexlep3].Px(),leptons[indexlep3].Py(),0.);
                Veclep2.SetXYZ(leptons[indexlep4].Px(),leptons[indexlep4].Py(),0.);
                MET = metVec.Vect() + Veclep1 + Veclep2;
            }

            INV_2L2J->SetLabFrameThreeVector(MET);

            LAB_2L2J->AnalyzeEvent();

            //... then by setting the Variables
            TLorentzVector vP_V1aPP = J1_2L2J->GetFourVector(*C1N2_2L2J);
            TLorentzVector vP_V2aPP = J2_2L2J->GetFourVector(*C1N2_2L2J);
            TLorentzVector vP_V1bPP = L1b_2L2J->GetFourVector(*C1N2_2L2J);
            TLorentzVector vP_V2bPP = L2b_2L2J->GetFourVector(*C1N2_2L2J);
            TLorentzVector vP_IaPP  = X1a_2L2J->GetFourVector(*C1N2_2L2J);
            TLorentzVector vP_IbPP  = X1b_2L2J->GetFourVector(*C1N2_2L2J);

            TLorentzVector vP_V1aPa = J1_2L2J->GetFourVector(*C1a_2L2J);
            TLorentzVector vP_V2aPa = J2_2L2J->GetFourVector(*C1a_2L2J);
            TLorentzVector vP_IaPa  = X1a_2L2J->GetFourVector(*C1a_2L2J);
            TLorentzVector vP_V1bPb = L1b_2L2J->GetFourVector(*N2b_2L2J);
            TLorentzVector vP_V2bPb = L2b_2L2J->GetFourVector(*N2b_2L2J);
            TLorentzVector vP_IbPb  = X1b_2L2J->GetFourVector(*N2b_2L2J);


            //Variables w/ 4 objects 
            //Four vector sum of all visible objets + four vector sum of inv objects
            double H2PP_VR = (vP_V1aPP + vP_V2aPP + vP_V1bPP + vP_V2bPP).P() + (vP_IaPP+vP_IbPP).P();//H(1,1)PP
            //Scalar sum of all visible objects + vector sum of invisible momenta 
            double H5PP_VR = vP_V1aPP.P() + vP_V2aPP.P() + vP_V1bPP.P() + vP_V2bPP.P() + (vP_IaPP + vP_IbPP).P();//H(4,1)PP
            double HT5PP_VR = vP_V1aPP.Pt() + vP_V2aPP.Pt() + vP_V1bPP.Pt() + vP_V2bPP.Pt() + (vP_IaPP + vP_IbPP).Pt();//HT(4,1)PP

            double H2Pa_VR = (vP_V1aPa + vP_V2aPa).P() + vP_IaPa.P();
            double H2Pb_VR = (vP_V1bPb + vP_V2bPb).P() + vP_IbPb.P();
            double H3Pa_VR = vP_V1aPa.P() + vP_V2aPa.P() + vP_IaPa.P();
            double H3Pb_VR = vP_V1bPb.P() + vP_V2bPb.P() + vP_IbPb.P();
            double minH2P_VR = std::min(H2Pa_VR,H2Pb_VR);
            double minH3P_VR = std::min(H3Pa_VR,H3Pb_VR);
            double R_minH2P_minH3P_VR = minH2P_VR/minH3P_VR;

            TVector3 vP_PP = C1N2_2L2J->GetFourVector(*LAB_2L2J).Vect();
            double Pt_PP_VR = vP_PP.Pt();
            double RPT_HT5PP_VR = Pt_PP_VR / (Pt_PP_VR + HT5PP_VR);

            double dphiVP_VR = C1N2_2L2J->GetDeltaPhiDecayVisible();

            // Need to include CR2L-VV   

            if (lept1Pt_VR>25. && lept2Pt_VR>25. && jets.size()==2 && jets[0].Pt()>30. && jets[1].Pt()>30. && mll_VR>80. && mll_VR<100. && mjj_VR>20. && R_minH2P_minH3P_VR>0.2 && dphiVP_VR>0.3 && dphiVP_VR<2.8 && RPT_HT5PP_VR<0.05 && H5PP_VR>200) accept("CR2L_VV");
        } // end of standard tree

        // Now apply the ISR treatment if there are more than 2 jets
        if (jets.size() >=3) {
            //std::cout << "3 AND 4 LEPTON ISR" << std::endl;
            LabRecoFrame* LAB_comb = m_RF_helper.getLabFrame("LAB_comb");
            //        VisibleRecoFrame* ISR_comb = m_RF_helper.getVisibleFrame("ISR_comb");
            CombinatoricGroup* JETS_comb = m_RF_helper.getCombinatoricGroup("JETS_comb");    
            VisibleRecoFrame* J_comb = m_RF_helper.getVisibleFrame("J_comb");
            VisibleRecoFrame* L_comb = m_RF_helper.getVisibleFrame("L_comb");
            InvisibleGroup* INV_comb = m_RF_helper.getInvisibleGroup("INV_comb");

            LAB_comb->ClearEvent();

            std::vector<RFKey> jetID;
            for(int i = 0; i < int(jets.size()); i++){
                TLorentzVector jet;
                // transverse view of jet 4-vectors
                jet.SetPtEtaPhiM(jets[i].Pt(),0.0,jets[i].Phi(),jets[i].M());
                jetID.push_back(JETS_comb->AddLabFrameFourVector(jet));
            }

            TLorentzVector lepSys(0.,0.,0.,0.);
            TLorentzVector lep1,lep2;
            lep1.SetPtEtaPhiM(leptons[indexlep1].Pt(),0.0,leptons[indexlep1].Phi(),leptons[indexlep1].M());
            lep2.SetPtEtaPhiM(leptons[indexlep2].Pt(),0.0,leptons[indexlep2].Phi(),leptons[indexlep2].M());

            lepSys = lep1 + lep2;
            L_comb->SetLabFrameFourVector(lepSys);


            TVector3 MET;//
            TVector3 VecLep1, VecLep2;
            if(leptons.size() == 3){
                VecLep1.SetXYZ(leptons[indexlep3].Px(),leptons[indexlep3].Py(),0);
                MET = metVec.Vect() + VecLep1;
            }
            if(leptons.size() == 4){
                VecLep1.SetXYZ(leptons[indexlep3].Px(),leptons[indexlep3].Py(),0);
                VecLep2.SetXYZ(leptons[indexlep4].Px(),leptons[indexlep4].Py(),0);
                MET = metVec.Vect() + VecLep1 + VecLep2;
            }

            INV_comb->SetLabFrameThreeVector(MET);
            if(!LAB_comb->AnalyzeEvent())
                std::cout << "Something went wrong with \"INTERMEDIATE\" tree event analysis - VR" << std::endl;

            int NjS_VR = 0;
            int NjISR_VR = 0;
            for(int i = 0; i < int(jetID.size()); i++){
                if(JETS_comb->GetFrame(jetID[i]) == *J_comb){
                    NjS_VR++;
                } else {
                    NjISR_VR++;
                }
            }

            LabRecoFrame* LAB_2LNJ = m_RF_helper.getLabFrame("LAB_2LNJ");
            VisibleRecoFrame* ISR_2LNJ = m_RF_helper.getVisibleFrame("ISR_2LNJ");
            VisibleRecoFrame* L1_2LNJ = m_RF_helper.getVisibleFrame("L1_2LNJ");
            VisibleRecoFrame* L2_2LNJ = m_RF_helper.getVisibleFrame("L2_2LNJ");

            InvisibleGroup* INV_2LNJ = m_RF_helper.getInvisibleGroup("INV_2LNJ");
            DecayRecoFrame* CM_2LNJ = m_RF_helper.getDecayFrame("CM_2LNJ");
            InvisibleRecoFrame* Ia_2LNJ = m_RF_helper.getInvisibleFrame("Ia_2LNJ");       
            InvisibleRecoFrame* Ib_2LNJ = m_RF_helper.getInvisibleFrame("Ib_2LNJ");       
            VisibleRecoFrame* J_2LNJ = m_RF_helper.getVisibleFrame("J_2LNJ");
            DecayRecoFrame* Z_2LNJ = m_RF_helper.getDecayFrame("Z_2LNJ");       
            CombinatoricGroup* JETS_2LNJ = m_RF_helper.getCombinatoricGroup("JETS_2LNJ");

            LAB_2LNJ->ClearEvent();

            // put jets in their place
            int NJ = jetID.size();
            TLorentzVector vISR(0.,0.,0.,0.);
            for(int i = 0; i < NJ; i++){
                if(JETS_comb->GetFrame(jetID[i]) == *J_comb){
                    JETS_2LNJ->AddLabFrameFourVector(jets[i]);
                } else {
                    vISR += jets[i];
                }
            }

            ISR_2LNJ->SetLabFrameFourVector(vISR);

            // put leptons in their place
            L1_2LNJ->SetLabFrameFourVector(leptons[indexlep1]);
            L2_2LNJ->SetLabFrameFourVector(leptons[indexlep2]);

            INV_2LNJ->SetLabFrameThreeVector(MET);

            if(!LAB_2LNJ->AnalyzeEvent())
                std::cout << "Something went wrong with \"2LNJ\" tree event analysis - VR" << std::endl;

            TLorentzVector vP_CM;
            TLorentzVector vP_ISR;
            TLorentzVector vP_I;

            vP_CM  = CM_2LNJ->GetFourVector();
            vP_ISR = ISR_2LNJ->GetFourVector();
            vP_I   = (*Ia_2LNJ+*Ib_2LNJ).GetFourVector();

            double MZ_VR = Z_2LNJ->GetMass();
            double MJ_VR = J_2LNJ->GetMass();

            double PTCM_VR = vP_CM.Pt();

            TVector3 boostZ = vP_CM.BoostVector();
            boostZ.SetX(0.);
            boostZ.SetY(0.);

            vP_CM.Boost(-boostZ);
            vP_ISR.Boost(-boostZ);
            vP_I.Boost(-boostZ);

            TVector3 boostT = vP_CM.BoostVector();
            vP_ISR.Boost(-boostT);
            vP_I.Boost(-boostT);

            TVector3 vPt_ISR = vP_ISR.Vect();
            TVector3 vPt_I   = vP_I.Vect();
            vPt_ISR -= vPt_ISR.Dot(boostZ.Unit())*boostZ.Unit();
            vPt_I   -= vPt_I.Dot(boostZ.Unit())*boostZ.Unit();

            double PTISR_VR =  vPt_ISR.Mag();
            double RISR_VR  = -vPt_I.Dot(vPt_ISR.Unit()) / PTISR_VR;
            double PTI_VR = vPt_I.Mag();
            double dphiISRI_VR = fabs(vPt_ISR.Angle(vPt_I));

            // ISR CR and VR have 3/4 leptons 
            if (lept1Pt_VR>25. && lept2Pt_VR>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && MZ_VR>80. && MZ_VR<100. && MJ_VR>20. && dphiISRI_VR>2.0 && RISR_VR>0.0 && RISR_VR<0.5 && PTISR_VR>50. && PTI_VR>50. && PTCM_VR<30.) accept("CR2L_ISR_VV");
            if (lept1Pt_VR>25. && lept2Pt_VR>25. && jets[0].Pt()>30. && jets[1].Pt()>30. && ((MZ_VR>20. && MZ_VR<80.) || MZ_VR<100.) && MJ_VR>20. && dphiISRI_VR>2.0 && RISR_VR>0.0 && RISR_VR<1. && PTISR_VR>70. && PTI_VR>70. && PTCM_VR<30.) accept("VR2L_ISR_VV");
        } // end of ISR 
    }

    // Save some required info
    ntupVar("mcDSID",event->getMCNumber());
    ntupVar("mc_Weight",event->getMCWeights()[0]);
    ntupVar("mcWeights",event->getMCWeights());
    ntupVar("pdf_id1",event->getPDF_id1());
    ntupVar("pdf_x1",event->getPDF_x1());
    ntupVar("pdf_pdf1",event->getPDF_pdf1());
    ntupVar("pdf_id2",event->getPDF_id2());
    ntupVar("pdf_x2",event->getPDF_x2());
    ntupVar("pdf_pdf2",event->getPDF_pdf2());
    ntupVar("pdf_scale",event->getPDF_scale());

    return;
}
