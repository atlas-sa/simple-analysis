#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopTwoLepton2016)

void StopTwoLepton2016::Init()
{
  addRegions({"SRASF120","SRADF120","SRASF140","SRADF140","SRASF160","SRADF160","SRASF180","SRADF180","SRBSF120","SRBDF120","SRBSF140","SRBDF140","SRCSF110","SRCDF110"});
  // 3-body SRs
  addRegions({"SR3BodyTSF","SR3BodyTDF","SR3BodyWSF","SR3BodyWDF"});

  //
  // Set up RestFrames trees through m_RF_Helper (RestFramesHelper class)
  //
  LabRecoFrame*       lab  = m_RF_helper.addLabFrame("lab");
  DecayRecoFrame*     ss   = m_RF_helper.addDecayFrame("ss");
  DecayRecoFrame*     s1   = m_RF_helper.addDecayFrame("s1");
  DecayRecoFrame*     s2   = m_RF_helper.addDecayFrame("s2");
  VisibleRecoFrame*   v1   = m_RF_helper.addVisibleFrame("v1");
  VisibleRecoFrame*   v2   = m_RF_helper.addVisibleFrame("v2");
  InvisibleRecoFrame* i1   = m_RF_helper.addInvisibleFrame("i1");
  InvisibleRecoFrame* i2   = m_RF_helper.addInvisibleFrame("i2");

  // Connect the frames
  lab->SetChildFrame(*ss);
  ss->AddChildFrame(*s1);
  ss->AddChildFrame(*s2);
  s1->AddChildFrame(*v1);
  s1->AddChildFrame(*i1);
  s2->AddChildFrame(*v2);
  s2->AddChildFrame(*i2);

  // Initialize the tree
  lab->InitializeTree();

  // Define groups
  InvisibleGroup* inv = m_RF_helper.addInvisibleGroup("inv");
  inv->AddFrame(*i1);
  inv->AddFrame(*i2);

  CombinatoricGroup* vis = m_RF_helper.addCombinatoricGroup("vis");
  vis->AddFrame(*v1);
  vis->SetNElementsForFrame(*v1, 1, false);
  vis->AddFrame(*v2);
  vis->SetNElementsForFrame(*v2, 1, false);

  InvisibleJigsaw* MinMassJigsaw = m_RF_helper.addInvisibleJigsaw("MinMassJigsaw", kSetMass);
  inv->AddJigsaw(*MinMassJigsaw); 

  InvisibleJigsaw* RapidityJigsaw = m_RF_helper.addInvisibleJigsaw("RapidityJigsaw", kSetRapidity);
  inv->AddJigsaw(*RapidityJigsaw);
  RapidityJigsaw->AddVisibleFrames(lab->GetListVisibleFrames());
   
  InvisibleJigsaw* ContraBoostJigsaw = m_RF_helper.addInvisibleJigsaw("ContraBoostJigsaw", kContraBoost);
  inv->AddJigsaw(*ContraBoostJigsaw);
  ContraBoostJigsaw->AddVisibleFrames((s1->GetListVisibleFrames()), 0);
  ContraBoostJigsaw->AddVisibleFrames((s2->GetListVisibleFrames()), 1);
  ContraBoostJigsaw->AddInvisibleFrame(*i1, 0);
  ContraBoostJigsaw->AddInvisibleFrame(*i2, 1);

  MinMassesCombJigsaw* HemiJigsaw = m_RF_helper.addCombinatoricJigsaw("HemiJigsaw", kMinMasses);
  vis->AddJigsaw(*HemiJigsaw);
  HemiJigsaw->AddFrame(*v1, 0);
  HemiJigsaw->AddFrame(*v2, 1);

  // Initialize analysis
  lab->InitializeAnalysis();
}

void StopTwoLepton2016::ProcessEvent(AnalysisEvent *event)
{

  const float mZ = 91.2;

  auto baselineElectrons = event->getElectrons(10, 2.47, ELooseLH);
  auto baselineMuons = event->getMuons(10, 2.5, MuMedium | MuQoPSignificance);
  auto jets          = event->getJets(20., 2.8, JVT50Jet);
  auto metVec        = event->getMET();

  // SUSY overlap removal
  jets               = overlapRemoval(jets, baselineElectrons, 0.2);
  jets               = overlapRemoval(jets, baselineMuons, 0.4, LessThan3Tracks);
  baselineElectrons  = overlapRemoval(baselineElectrons, jets, 0.4);
  baselineMuons      = overlapRemoval(baselineMuons, jets, 0.4);

  auto signalElectrons = filterObjects(baselineElectrons, 20, 2.47, EMediumLH | EZ05mm | EIsoGradientLoose);
  auto signalMuons     = filterObjects(baselineMuons, 20, 2.4, MuD0Sigma3 | MuZ05mm | MuIsoGradientLoose);
  auto bjets           = filterObjects(jets, 25., 2.5, BTag77MV2c20);

  auto n_bjets          = countObjects(bjets, 25, 2.5);
  auto n_jets25         = countObjects(jets, 25, 2.5);

  AnalysisObjects signalLeptons = signalElectrons + signalMuons;

  int nLeptons = signalLeptons.size();
  if (nLeptons != 2) return;

  auto lep0 = signalLeptons[0];
  auto lep1 = signalLeptons[1];

  // Variables we'll be cutting on
  double MET                            = metVec.Et();
  double meff                           = sumObjectsPt(jets, 2, 25) + lep0.Pt() + lep1.Pt() + MET;
  auto pbll_TLV                         = metVec + lep0 + lep1;
  auto pll                              = lep0 + lep1;

  double MT2                            = calcMT2(lep0, lep1, metVec);;
  double R1                             = MET / meff;
  double dPhiEtmisspbll                 = fabs(metVec.DeltaPhi(pbll_TLV));

  double mll                            = (lep0 + lep1).M();

  int DX                                = fabs((2 * (lep0.Pz() + lep1.Pz())) / 13000.);

  bool isSF = false;
  if(lep0.type() == lep1.type()) isSF = true;

  //Opposite Sign leptons
  if (lep0.charge() == lep1.charge()) return;

  //Leading lepton with pT>25 GeV
  if (lep0.Pt() < 25.) return;

  //Reject low mass DY etc
  if (mll < 20.) return;

  //
  // Here BEGINs the 3-Body SRs
  //

  // Get the RJ tree
  LabRecoFrame* lab      = m_RF_helper.getLabFrame("lab");
  DecayRecoFrame* ss     = m_RF_helper.getDecayFrame("ss");
  DecayRecoFrame* s1     = m_RF_helper.getDecayFrame("s1");
  InvisibleGroup* inv    = m_RF_helper.getInvisibleGroup("inv");
  CombinatoricGroup* vis = m_RF_helper.getCombinatoricGroup("vis");
  VisibleRecoFrame* v1   = m_RF_helper.getVisibleFrame("v1");
  
  // Clear the event
  lab->ClearEvent();

  // Set MET
  inv->SetLabFrameThreeVector(metVec.Vect());

  // Add leptons to the visible group
  vis->AddLabFrameFourVector(lep0);
  vis->AddLabFrameFourVector(lep1);

  // Analyze the event
  lab->AnalyzeEvent();

  // Get the variables

  // RPT
  double shat_jigsaw = ss->GetMass();
  TVector3 vPTT      = (ss->GetFourVector(*lab)).Vect();
  double RPT         = vPTT.Pt() / (vPTT.Pt() + shat_jigsaw / 4.);
  
  // MDR
  double MDR         = 2.0 * v1->GetEnergy(*s1);

  // gamInvRp1
  double gamInvRp1   = ss->GetVisibleShape();

  // DPB_vSS
  double DPB_vSS     = ss->GetDeltaPhiBoostVisible();

  // cosTheta_b
  auto lepPos = lep0, lepNeg = lep1;
  if(signalLeptons[0].charge()<0.) { 
    lepPos = lep1; 
    lepNeg = lep0; 
  }
  
  TVector3 boost = pll.BoostVector();
  lepPos.Boost(-boost);
  lepNeg.Boost(-boost);

  double cosTheta_b = tanh((lepPos.Eta()-lepNeg.Eta())/2);

  // Apply selection

  // SR3Body W SF
  if(isSF  && RPT>0.7 && gamInvRp1>0.7 && DPB_vSS>(0.9*fabs(cosTheta_b)+1.6) && MDR>95.  && n_bjets==0 && fabs(mll-mZ)>20. ) accept("SR3BodyWSF");
  // SR3Body W DF
  if(!isSF && RPT>0.7 && gamInvRp1>0.7 && DPB_vSS>(0.9*fabs(cosTheta_b)+1.6) && MDR>95.  && n_bjets==0                     ) accept("SR3BodyWDF");
  // SR3Body T SF
  if(isSF  && RPT>0.7 && gamInvRp1>0.7 && DPB_vSS>(0.9*fabs(cosTheta_b)+1.6) && MDR>110. && n_bjets>=1 && fabs(mll-mZ)>20. ) accept("SR3BodyTSF");
  // SR3Body T DF
  if(!isSF && RPT>0.7 && gamInvRp1>0.7 && DPB_vSS>(0.9*fabs(cosTheta_b)+1.6) && MDR>110. && n_bjets>=1                     ) accept("SR3BodyTDF");

  //
  // Here ENDs the 3-Body SRs
  //

  //2-body SRs, everything has mT2>100
  if (MT2 < 100.) return;

  if (n_bjets == 0 && isSF && mll> 111.2 && R1 > 0.3 && DX < 0.07 && MT2 > 120. && MT2 < 140.) accept("SRASF120");
  if (n_bjets == 0 && !isSF && DX < 0.07 && MT2 > 120. && MT2 < 140.) accept("SRADF120");
  if (n_bjets == 0 && isSF && mll> 111.2 && R1 > 0.3 && DX < 0.07 && MT2 > 140. && MT2 < 160.) accept("SRASF140");
  if (n_bjets == 0 && !isSF && DX < 0.07 && MT2 > 140. && MT2 < 160.) accept("SRADF140");
  if (n_bjets == 0 && isSF && mll> 111.2 && R1 > 0.3 && DX < 0.07 && MT2 > 160. && MT2 < 180.) accept("SRASF160");
  if (n_bjets == 0 && !isSF && DX < 0.07 && MT2 > 160. && MT2 < 180.) accept("SRADF160");
  if (n_bjets == 0 && isSF && mll> 111.2 && R1 > 0.3 && DX < 0.07 && MT2 > 180.) accept("SRASF180");
  if (n_bjets == 0 && !isSF && DX < 0.07 && MT2 > 180.) accept("SRADF180");

  if (n_bjets > 0 && n_jets25 > 1 && isSF && fabs(mll-mZ)>20. && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140.) accept("SRBSF120");
  if (n_bjets > 0 && n_jets25 > 1 && !isSF && dPhiEtmisspbll < 1.5 && MT2 > 120. && MT2 < 140.) accept("SRBDF120");
  if (n_bjets > 0 && n_jets25 > 1 && isSF && fabs(mll-mZ)>20. && dPhiEtmisspbll < 1.5 && MT2 > 140.) accept("SRBSF140");
  if (n_bjets > 0 && n_jets25 > 1 && !isSF && dPhiEtmisspbll < 1.5 && MT2 > 140.) accept("SRBDF140");

  if (n_bjets > 0 && n_jets25 > 2 && isSF && fabs(mll-mZ)>20. && MET>200. && MET/(lep0.Pt()+lep1.Pt())>1.2 && MT2>110.) accept("SRCSF110");
  if (n_bjets > 0 && n_jets25 > 2 && !isSF && MET>200. && MET/(lep0.Pt()+lep1.Pt())>1.2 && MT2>110.) accept("SRCDF110");

  return;
}
