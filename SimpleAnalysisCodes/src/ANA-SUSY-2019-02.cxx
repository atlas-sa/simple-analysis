#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(EwkTwoLeptonZeroJets2020)

void EwkTwoLeptonZeroJets2020::Init() {
  // Slepton regions
  addRegions({"SR0j_100_infty", "SR0j_110_infty", "SR0j_120_infty", "SR0j_130_infty", "SR0j_140_infty", "SR0j_100_105", "SR0j_105_110", "SR0j_110_115",
              "SR0j_115_120", "SR0j_120_125", "SR0j_125_130", "SR0j_130_140", "SR1j_100_infty", "SR1j_110_infty", "SR1j_120_infty", "SR1j_130_infty",
              "SR1j_140_infty", "SR1j_100_105", "SR1j_105_110", "SR1j_110_115", "SR1j_115_120", "SR1j_120_125", "SR1j_125_130", "SR1j_130_140"});

  // Chargino regions
  addRegions({"SR_DF_81_inc", "SR_DF_81_8125", "SR_DF_8125_815", "SR_DF_815_8175", "SR_DF_8175_82", "SR_DF_82_8225", "SR_DF_8225_825", "SR_DF_825_8275",
              "SR_DF_8275_83", "SR_DF_83_8325", "SR_DF_8325_835", "SR_DF_835_8375", "SR_DF_8375_84", "SR_DF_84_845", "SR_DF_845_85", "SR_DF_85_86", "SR_DF_86_inc",
              "SR_SF_77_inc", "SR_SF_77_775", "SR_SF_775_78", "SR_SF_78_785", "SR_SF_785_79", "SR_SF_79_795", "SR_SF_795_80", "SR_SF_80_81", "SR_SF_81_inc",
              "SR_inc", "SR_DF_82_inc", "SR_DF_83_inc", "SR_DF_84_inc", "SR_DF_85_inc", "SR_SF_78_inc", "SR_SF_79_inc", "SR_SF_80_inc"});

  // Instances for BDT (in the order odd - even as implemented in SimpleAnalysisFramework/src/MVA.cxx)
  addMVAUtilsBDT("lgbm_DF", "ANA-SUSY-2019-02_DF0J_trained_odd.root", "ANA-SUSY-2019-02_DF0J_trained_even.root");
  addMVAUtilsBDT("lgbm_SF", "ANA-SUSY-2019-02_SF0J_trained_odd.root", "ANA-SUSY-2019-02_SF0J_trained_even.root");
}

void EwkTwoLeptonZeroJets2020::ProcessEvent(AnalysisEvent *event) {

  // Retrieve basic object lists
  auto electrons  = event->getElectrons(9., 2.47, ELooseBLLH | EZ05mm);
  auto muons      = event->getMuons(9., 2.7, MuMedium | MuZ05mm | MuNotCosmic);
  auto candJets   = event->getJets(20., 2.8, LooseBadJet | PFlowJet);
  auto METVec     = event->getMET();
  double METsig    = event->getMETSignificance();
  double MET       = METVec.Et();

  // Overlap removal - including with object Pt-dependent radius calculation
  // Electrons
  candJets   = overlapRemoval(candJets, electrons, 0.2);
  auto radiusCalcEle  = [] (const AnalysisObject& ele, const AnalysisObject&) { return std::min(0.4, 0.04 + 10/ele.Pt()); };
  electrons  = overlapRemoval(electrons, candJets, radiusCalcEle);
  // Muons
  candJets = overlapRemoval(candJets, muons, 0.4, LessThan3Tracks);
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  muons      = overlapRemoval(muons, candJets, radiusCalcMuon);

  // Basic filtering by pT, eta and "ID"
  auto signalJets      = filterObjects(candJets, 20., 2.4, JVTTight);
  auto bjets           = filterObjects(signalJets, 20., 2.4, BTag85DL1r);
  auto signalElectrons = filterObjects(electrons, 9., 2.47, ETightLH | ED0Sigma5 | EIsoFCLoose);
  auto signalMuons     = filterObjects(muons, 9., 2.6, MuMedium | MuD0Sigma3| MuIsoFCLoose);
  auto signalLeptons   = signalElectrons + signalMuons;

  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(signalJets);
  sortObjectsByPt(signalElectrons);
  sortObjectsByPt(signalMuons);

  // Object counting
  int nlep = signalLeptons.size();

  int nbjet   = bjets.size();
  int njet    = countObjects(candJets, 20, 2.4);
  int njet30  = countObjects(candJets, 30, 2.4);
  int njet40  = countObjects(candJets, 40, 2.4);
  int njet50  = countObjects(candJets, 50, 2.4);
  int njet60  = countObjects(candJets, 60, 2.4);

  // Preselection
  if(nlep != 2) return;
  if(signalLeptons[0].Pt() < 20.) return;
  if(signalLeptons[1].Pt() < 9.) return;
  if((signalLeptons[0] + signalLeptons[1]).M() < 10.) return;

  // Event labelling variables: these wouldn't work?
  bool isSF = (signalLeptons[0].type() == signalLeptons[1].type());
  bool isOS = (signalLeptons[0].charge() != signalLeptons[1].charge());

  // Are these needed? can calculate later if wanted
  //float mT = 0.;
  //if(signalLeptons.size()>0) mT=calcMT(signalLeptons[0], METVec);  // MT from leading lepton
  //float dphiMin4  = minDphi(METVec, signalJets, 4);               // Smallest Delta Phi between leading 4-jets and MET

  double MT2 = calcMT2(signalLeptons[0],signalLeptons[1],METVec);
  double mll = (signalLeptons[0] + signalLeptons[1]).M();
  double METphi = METVec.Phi();
  double jet1pT = 0.0, jet1phi = 0.0, jet1eta = 0.0;
  if (signalJets.size()>0){
    jet1pT = signalJets[0].Pt();
    jet1phi = signalJets[0].Phi();
    jet1eta = signalJets[0].Eta();
  }

  double jet2pT = 0.0, jet2phi = 0.0, jet2eta = 0.0;
  if (signalJets.size()>1) {
    jet2pT = signalJets[1].Pt();
    jet2phi = signalJets[1].Phi();
    jet2eta = signalJets[1].Eta();
  }

  //leptons
  double lep1pT = signalLeptons[0].Pt(), lep2pT = signalLeptons[1].Pt();
  double lep1phi = signalLeptons[0].Phi(), lep2phi = signalLeptons[1].Phi();
  double lep1eta = signalLeptons[0].Eta(), lep2eta = signalLeptons[1].Eta();

  //extra variables
  double MT2_100 = calcAMT2(signalLeptons[0],signalLeptons[1],METVec, 100., 100.); //mass of final state invis = 100
  double MT2_200 = calcAMT2(signalLeptons[0],signalLeptons[1],METVec, 200., 200.);

  double dRll = signalLeptons[0].DeltaR(signalLeptons[1]);
  double dRMETl1 = signalLeptons[0].DeltaR(METVec), dRMETl2 = signalLeptons[1].DeltaR(METVec);

  double dphill = std::fabs( signalLeptons[0].DeltaPhi(signalLeptons[1]) );
  double dphiMETll = std::fabs( METVec.DeltaPhi(signalLeptons[0] + signalLeptons[1]) );
  double dphiMETl1 = std::fabs( METVec.DeltaPhi(signalLeptons[0]) );
  double dphiMETl2 = std::fabs( METVec.DeltaPhi(signalLeptons[1]) );
  double dRMETll = METVec.DeltaR(signalLeptons[0] + signalLeptons[1]);

  double mtt = calcMTauTau(signalLeptons[0], signalLeptons[1], METVec);

  double pbll = (signalLeptons[0] + signalLeptons[1] + METVec).Pt();
  double R1 = MET / pbll;

  double meff2l1j = -10;
  double R2 = -10;
  if (signalJets.size()>0) { 
    meff2l1j =  (signalLeptons[0] + signalLeptons[1]+ METVec + signalJets[0]).Pt();
    R2 = MET / meff2l1j;
  }
  double DPhib = std::fabs(  METVec.DeltaPhi(signalLeptons[0] + signalLeptons[1] + METVec) ) ;

  double MllMET = (signalLeptons[0] + signalLeptons[1] + METVec).M();

  double lepflav1 = -10;
  if (signalLeptons[0].type()==ELECTRON) lepflav1 = 11;
  else if (signalLeptons[0].type()==MUON) lepflav1 = 13;

  double lepflav2 = -10;
  if (signalLeptons[1].type()==ELECTRON) lepflav2 = 11;
  else if (signalLeptons[1].type()==MUON) lepflav2 = 13;

  int njetTruth = 0;
  for(auto jet : candJets) {
    auto truth=event->getTruthParticle(jet);
    if (truth.valid()) {
      if(truth.Pt()>20 && truth.Eta()<2.4) njetTruth++;
    }
  }

  // Add additional variable for slepton analysis
  double cosTstar = std::fabs( std::tanh( 0.5*(signalLeptons[0].Eta() - signalLeptons[1].Eta()) ) );

  // Preselection
  if(nlep == 2 && signalLeptons[0].Pt() > 27. && signalLeptons[1].Pt() > 9. && isOS && mll > 11. && METsig > 3.) {

    ntupVar("isSF",isSF);
    ntupVar("isOS",isOS);
    ntupVar("nlep",nlep);
    ntupVar("nbjet",nbjet);
    ntupVar("njet",njet);
    ntupVar("njet30",njet30);
    ntupVar("njet40",njet40);
    ntupVar("njet50",njet50);
    ntupVar("njet60",njet60);
    ntupVar("njetTruth",njetTruth);
    ntupVar("MT2",MT2);
    ntupVar("mll",mll);
    ntupVar("jet1pT",jet1pT);
    ntupVar("jet1phi",jet1phi);
    ntupVar("jet1eta",jet1eta);
    ntupVar("jet2pT",jet2pT);
    ntupVar("jet2phi",jet2phi);
    ntupVar("jet2eta",jet2eta);
    ntupVar("lep1pT",lep1pT);
    ntupVar("lep2pT",lep2pT);
    ntupVar("lep1phi",lep1phi);
    ntupVar("lep2phi",lep2phi);
    ntupVar("lep1eta",lep1eta);
    ntupVar("lep2eta",lep2eta);
    ntupVar("MET", MET);
    ntupVar("genMET", event->getGenMET());
    ntupVar("METsig",METsig); 
    ntupVar("METphi",METphi); 

    ntupVar("cosTstar", cosTstar);
    ntupVar("MT2_100", MT2_100);
    ntupVar("MT2_200", MT2_200);
    ntupVar("dRll", dRll);
    ntupVar("dphill", dphill);
    ntupVar("dRMETl1", dRMETl1);
    ntupVar("dRMETl2", dRMETl2);
    ntupVar("dphiMETll", dphiMETll);
    ntupVar("dRMETll", dRMETll);
    ntupVar("dphiMETl1", dphiMETl1);
    ntupVar("dphiMETl2", dphiMETl2);
    ntupVar("mtt", mtt);
    ntupVar("pbll", pbll);
    ntupVar("R1", R1);
    ntupVar("meff2l1j", meff2l1j);
    ntupVar("R2", R2);
    ntupVar("DPhib", DPhib);
    ntupVar("MllMET", MllMET);
    ntupVar("lepflav1", lepflav1);
    ntupVar("lepflav2", lepflav2);
    //ntupVar("susyProcess",event->getSUSYChannel());
    //ntupVar("mcDSID",event->getMCNumber());

    /*
     ***************************************************************** SLEPTONS 0-jet
     */
    if (nbjet == 0 && njet == 0 && isSF && lep1pT > 140. && lep2pT > 20. && METsig > 7. && TMath::Abs(mll - 91.) > 15. && pbll < 5. && cosTstar < 0.2 && dphill > 2.2 && dphiMETl1 > 2.2) {
      if (MT2_100 >= 100.                  ) accept("SR0j_100_infty");
      if (MT2_100 >= 110.                  ) accept("SR0j_110_infty");
      if (MT2_100 >= 120.                  ) accept("SR0j_120_infty");
      if (MT2_100 >= 130.                  ) accept("SR0j_130_infty");
      if (MT2_100 >= 140.                  ) accept("SR0j_140_infty");
      if (MT2_100 >= 100. && MT2_100 < 105.) accept("SR0j_100_105");
      if (MT2_100 >= 105. && MT2_100 < 110.) accept("SR0j_105_110");
      if (MT2_100 >= 110. && MT2_100 < 115.) accept("SR0j_110_115");
      if (MT2_100 >= 115. && MT2_100 < 120.) accept("SR0j_115_120");
      if (MT2_100 >= 120. && MT2_100 < 125.) accept("SR0j_120_125");
      if (MT2_100 >= 125. && MT2_100 < 130.) accept("SR0j_125_130");
      if (MT2_100 >= 130. && MT2_100 < 140.) accept("SR0j_130_140");
    }

    /*
     ***************************************************************** SLEPTONS 1-jet
     */
    if (nbjet == 0 && njet == 1 && isSF && lep1pT > 100. && lep2pT > 50. && METsig > 7. && mll > 60. && TMath::Abs(mll - 91.) > 15. && cosTstar < 0.1 && dphill > 2.8) {
      if (MT2_100 >= 100.                  ) accept("SR1j_100_infty");
      if (MT2_100 >= 110.                  ) accept("SR1j_110_infty");
      if (MT2_100 >= 120.                  ) accept("SR1j_120_infty");
      if (MT2_100 >= 130.                  ) accept("SR1j_130_infty");
      if (MT2_100 >= 140.                  ) accept("SR1j_140_infty");
      if (MT2_100 >= 100. && MT2_100 < 105.) accept("SR1j_100_105");
      if (MT2_100 >= 105. && MT2_100 < 110.) accept("SR1j_105_110");
      if (MT2_100 >= 110. && MT2_100 < 115.) accept("SR1j_110_115");
      if (MT2_100 >= 115. && MT2_100 < 120.) accept("SR1j_115_120");
      if (MT2_100 >= 120. && MT2_100 < 125.) accept("SR1j_120_125");
      if (MT2_100 >= 125. && MT2_100 < 130.) accept("SR1j_125_130");
      if (MT2_100 >= 130. && MT2_100 < 140.) accept("SR1j_130_140");
    }

    double BDT = 0, BDTothers = 0;
    /*
     ***************************************************************** C1C1WW DF
     */
    if (!isSF && njet == 0 && nbjet == 0 && METsig > 8. && MT2 > 50.) {
      std::vector<double> inDF{lep1pT, lep2pT, MET, MT2, mll, DPhib, dphiMETl1, dphiMETl2, cosTstar, METsig};
      auto DF_MVA = getMVA("lgbm_DF");
      auto results = DF_MVA->evaluateMulti(inDF, 4);
      BDT = results[1];

      if (BDT > 0.81                   ) {
        accept("SR_inc");
        accept("SR_DF_81_inc");
      }
      if (BDT > 0.81   && BDT <= 0.8125) accept("SR_DF_81_8125");
      if (BDT > 0.8125 && BDT <= 0.815 ) accept("SR_DF_8125_815");
      if (BDT > 0.815  && BDT <= 0.8175) accept("SR_DF_815_8175");
      if (BDT > 0.8175 && BDT <= 0.82  ) accept("SR_DF_8175_82");
      if (BDT > 0.82                   ) accept("SR_DF_82_inc");
      if (BDT > 0.82   && BDT <= 0.8225) accept("SR_DF_82_8225");
      if (BDT > 0.8225 && BDT <= 0.825 ) accept("SR_DF_8225_825");
      if (BDT > 0.825  && BDT <= 0.8275) accept("SR_DF_825_8275");
      if (BDT > 0.8275 && BDT <= 0.83  ) accept("SR_DF_8275_83");
      if (BDT > 0.83                   ) accept("SR_DF_83_inc");
      if (BDT > 0.83   && BDT <= 0.8325) accept("SR_DF_83_8325");
      if (BDT > 0.8325 && BDT <= 0.835 ) accept("SR_DF_8325_835");
      if (BDT > 0.835  && BDT <= 0.8375) accept("SR_DF_835_8375");
      if (BDT > 0.8375 && BDT <= 0.84  ) accept("SR_DF_8375_84");
      if (BDT > 0.84                   ) accept("SR_DF_84_inc");
      if (BDT > 0.84   && BDT <= 0.845 ) accept("SR_DF_84_845");
      if (BDT > 0.845  && BDT <= 0.85  ) accept("SR_DF_845_85");
      if (BDT > 0.85                   ) accept("SR_DF_85_inc");
      if (BDT > 0.85   && BDT <= 0.86  ) accept("SR_DF_85_86");
      if (BDT > 0.86) accept("SR_DF_86_inc");
    }

    /*
     ***************************************************************** C1C1WW SF
     */
    if (isSF && TMath::Abs(mll - 91.) > 15. && njet == 0 && nbjet == 0 && METsig > 8. && MT2 > 50.) {
      std::vector<double> inSF{lep1pT, lep2pT, MET, MT2, mll, DPhib, dphiMETl1, dphiMETl2, cosTstar, METsig};
      auto SF_MVA = getMVA("lgbm_SF");
      auto results = SF_MVA->evaluateMulti(inSF, 4);
      BDT = results[1];
      BDTothers = results[3];

      if (BDTothers < 0.01 && BDT > 0.77                 ) {
        accept("SR_inc");
        accept("SR_SF_77_inc");
      }
      if (BDTothers < 0.01 && BDT > 0.77  && BDT <= 0.775) accept("SR_SF_77_775");
      if (BDTothers < 0.01 && BDT > 0.775 && BDT <= 0.78 ) accept("SR_SF_775_78");
      if (BDTothers < 0.01 && BDT > 0.78                 ) accept("SR_SF_78_inc");
      if (BDTothers < 0.01 && BDT > 0.78  && BDT <= 0.785) accept("SR_SF_78_785");
      if (BDTothers < 0.01 && BDT > 0.785 && BDT <= 0.79 ) accept("SR_SF_785_79");
      if (BDTothers < 0.01 && BDT > 0.79                 ) accept("SR_SF_79_inc");
      if (BDTothers < 0.01 && BDT > 0.79  && BDT <= 0.795) accept("SR_SF_79_795");
      if (BDTothers < 0.01 && BDT > 0.795 && BDT <= 0.80 ) accept("SR_SF_795_80");
      if (BDTothers < 0.01 && BDT > 0.80                 ) accept("SR_SF_80_inc");
      if (BDTothers < 0.01 && BDT > 0.80  && BDT <= 0.81 ) accept("SR_SF_80_81");
      if (BDTothers < 0.01 && BDT > 0.81) accept("SR_SF_81_inc");
    }
  }
  return;
}
