#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(MonoJet2018)

void MonoJet2018::Init()
{
  
  addRegion("cut_metTruth");
  addRegion("cut_evtCleaning");
  addRegion("cut_lep");
  addRegion("cut_njet");
  addRegion("cut_minDphi");
  addRegion("cut_jet_quality");
  addRegion("cut_jet_pt");  
  addRegions({"SR"});

  Float_t bins[] = {200, 250, 300, 350, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300};
  Int_t nbins = sizeof(bins) / sizeof(Float_t) -1;

  for (Int_t b = 0; b < nbins; b++) {
    addRegions({(TString::Format("SR_%d", (Int_t)bins[b])).Data()});
  }

  addHistogram("hist_met_et",nbins, bins);

}

void MonoJet2018::ProcessEvent(AnalysisEvent *event)
{

  // objects
  auto baseElectrons  = event->getElectrons(7, 2.47, ELooseBLLH);
  auto baseMuons      = event->getMuons(7, 2.5, MuMedium);
  auto basePhotons    = event->getPhotons(10., 2.37, PhotonTight);
  auto baseTaus       = event->getTaus(20., 2.5, TauLoose);
  auto baseJets       = event->getJets(30., 2.8);
  auto baseBjets      = filterObjects(baseJets, 30, 2.5, BTag60MV2c10);


  // overlap removal
  //  auto radiusCalcLepton = [](const AnalysisObject& lepton, const AnalysisObject&) {
  //    return std::max(0.1,std::min(0.4, 0.04 + 10 / lepton.Pt()));
  //  };

  auto muJetSpecial = [](const AnalysisObject& jet, const AnalysisObject&) {
    if (jet.pass(NOT(BTag60MV2c10)) && (jet.pass(LessThan3Tracks)))// || muon.Pt() / jet.Pt() > 0.7))
      return 0.2;
    else
      return 0.;
  };

  //to simulate the OR between electrons which shares the same track
  baseElectrons  = overlapRemoval(baseElectrons, baseElectrons, 0.01);

  /* actually the Delta R requirement is a shared track but here this is simulated by Delta R < 0.01 */
  baseMuons     = overlapRemoval(baseMuons, baseElectrons, 0.01);//, NOT(MuCaloTaggedOnly));
  baseElectrons = overlapRemoval(baseElectrons, baseMuons, 0.01);

  baseJets      = overlapRemoval(baseJets, baseElectrons, 0.2, NOT(BTag60MV2c10));
  baseElectrons = overlapRemoval(baseElectrons,baseJets, 0.4);

  baseJets      = overlapRemoval(baseJets, baseMuons, muJetSpecial);
  baseMuons     = overlapRemoval(baseMuons,baseJets, 0.4);

  // photons overlap
  //  baseJets = overlapRemoval(baseJets, basePhotons, 0.2);
  //  basePhotons = overlapRemoval(basePhotons, baseJets, 0.4);
  baseJets = overlapRemoval(baseJets, basePhotons, 0.4, NOT(BTag60MV2c10));
  basePhotons = overlapRemoval(basePhotons, baseElectrons, 0.4);
  basePhotons = overlapRemoval(basePhotons, baseMuons, 0.4);

  // taus overlap
  baseJets = overlapRemoval(baseJets, baseTaus, 0.2, NOT(BTag60MV2c10));
  baseTaus = overlapRemoval(baseTaus, baseElectrons, 0.2);
  baseTaus = overlapRemoval(baseTaus, baseMuons, 0.2);


  // define leptons
  auto baseLeptons = baseMuons + baseElectrons + baseTaus;

  // define good objects
  auto goodJets   = filterObjects(baseJets, 30, 2.8, JVT59Jet);
  auto goodBjets      = filterObjects(goodJets, 30, 2.5, BTag60MV2c10);
  auto metVec     = event->getMET();
  double met_et      = metVec.Et();
  double met_phi      = metVec.Phi();


  if(met_et < 150) return;
  accept("cut_metTruth");

  if (countObjects(goodJets, 30, 2.8, NOT(LooseBadJet))!=0) return;
  accept("cut_evtCleaning");

  int nJets=goodJets.size();

  // Pre-selection
  if(basePhotons.size()==0 && baseLeptons.size()==0){ 
    accept("cut_lep");

    if(nJets<5 && nJets>0){
      accept("cut_njet");

      if(minDphi(metVec,goodJets) > 0.4 || (met_et > 200 && met_et < 250 && minDphi(metVec,goodJets) > 0.6)){
	accept("cut_minDphi");

	if(goodJets[0].pass(NOT(TightBadJet))) return;
	accept("cut_jet_quality");

	if(goodJets[0].Pt()<150) return;
	if(TMath::Abs(goodJets[0].Eta()) > 2.4) return;
	accept("cut_jet_pt");

	// SR
	if(met_et <200) return;
	accept("SR"); //end SR 
	
	fill("hist_met_et", met_et);
	ntupVar("met_et", met_et);
	ntupVar("met_phi", met_phi);
	ntupVar("goodJets", goodJets, false, false, false, false);
	
	
	if (met_et > 200 && met_et < 250) accept("SR_200");
	if (met_et > 250 && met_et < 300) accept("SR_250");
	if (met_et > 300 && met_et < 350) accept("SR_300");
	if (met_et > 350 && met_et < 400) accept("SR_350");
	if (met_et > 400 && met_et < 500) accept("SR_400");
	if (met_et > 500 && met_et < 600) accept("SR_500");
	if (met_et > 600 && met_et < 700) accept("SR_600");
	if (met_et > 700 && met_et < 800) accept("SR_700");
	if (met_et > 800 && met_et < 900) accept("SR_800");
	if (met_et > 900 && met_et < 1000) accept("SR_900");
	if (met_et > 1000 && met_et < 1100) accept("SR_1000");
	if (met_et > 1100 && met_et < 1200) accept("SR_1100");
	if (met_et > 1200) accept("SR_1200"); //counting overflow
      }
    }
  }
  return;
}
