#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StrongDilepton2016)

void StrongDilepton2016::Init()
{
  addRegions({
              "SRZsoft",
              "SRZhard",
              "SRlow",
              "SRmedium",
              "SRhigh",
              "SRC",
              "SRCMET"
            });

  // here we define mll bins for the signal regions
  // the on-Z SRs are the on-Z bin of SR-med and SR-high
  float SRlow_edges[] = {12.,41.,61.,81.,101.,151.,201.,251.,301.,401.,501.,1001.,10000.};
  float SRmed_edges[] = {12.,41.,61.,81.,101.,151.,201.,251.,301.,401.,1001.,10000.};
  float SRC_edges[] = {4.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,65.,70.,75.,80.,85.,90.,95.,100.,10000.};
  float SRCMET_edges[] = {4.,20.,30.,40.,50.,70.,100.,10000.};


  addHistogram("SRlow_mll",12,SRlow_edges);
  addHistogram("SRmedium_mll",11,SRmed_edges);
  addHistogram("SRhigh_mll",12,SRlow_edges);
  addHistogram("SRC_mll",19,SRC_edges);
  addHistogram("SRCMET_mll",7,SRCMET_edges);

}

void StrongDilepton2016::ProcessEvent(AnalysisEvent *event)
{
  // baseline electrons are requested to pass the loose likelihood identification criteria
  //    and have pT > 10 GeV and |eta| < 2.47
  auto electrons  = event->getElectrons(10., 2.47, ELooseLH);
  // baseline muons are required to pass the Medium selections and to have pT > 10 GeV, |eta| < 2.5
  auto muons      = event->getMuons(10., 2.5, MuMedium);
  // jets: pT > 20 GeV, |eta| < 2.8
  auto candJets   = event->getJets(20., 2.8);

  auto metVec     = event->getMET();
  double met      = metVec.Et();

  // Loose bad jet veto for central jets 
  if(countObjects(candJets, 20, 2.8, NOT(LooseBadJet))!=0) return;

  candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);

  // Overlap removal
  auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

  electrons  = overlapRemoval(electrons, muons, 0.01);
  candJets   = overlapRemoval(candJets, electrons, 0.2, NOT(BTag77MV2c10));
  electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);
  candJets   = overlapRemoval(candJets, muons, radiusCalcJet, LessThan3Tracks);
  muons      = overlapRemoval(muons, candJets, radiusCalcMuon);

  // require signal jets to be 30 GeV
  auto signalJets      = filterObjects(candJets, 30, 2.5, JVT50Jet);
  // signal electrons are required to pass the Medium likelihood criteria and isolated using LooseTrackOnly
  auto signalElectrons = filterObjects(electrons, 25, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  // signal muons are required to be isolated using LooseTrackOnly
  auto signalMuons     = filterObjects(muons, 25, 2.5, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose|MuNotCosmic);
  // combine into signalLeptons for easy counting
  auto signalLeptons   = signalElectrons + signalMuons;
  sortObjectsByPt( signalLeptons );
  sortObjectsByPt( signalMuons );
  sortObjectsByPt( signalElectrons );
  sortObjectsByPt( signalJets );

  // get b-jets
  auto bjets = filterObjects(signalJets, 30., 2.5, BTag77MV2c10);

  int n_jets        = signalJets.size();
  int n_leptons     = signalLeptons.size();

  // require at least 2 signal leptons
  if(n_leptons>1 && n_jets>1){
 
    // Leading leptons mll
    TLorentzVector dilepton( signalLeptons[0].Px()+signalLeptons[1].Px() , signalLeptons[0].Py()+signalLeptons[1].Py() , 
			     signalLeptons[0].Pz()+signalLeptons[1].Pz() , signalLeptons[0].E() +signalLeptons[1].E()  );
    float mll = dilepton.M();

    // inclusive - HT 
    float HT  = sumObjectsPt(signalJets);

    // dphimin between leading 2 signal jets and met
    float dphiMin2 = n_jets>1 ? minDphi(metVec, signalJets, 2) : -1;

    // Leading two leptons
    float mt2     = calcMT2(signalLeptons[0],signalLeptons[1],metVec);


    // mm is channel 0; ee is channel 1; em is channel 2; me is channel 3
    int channel = -1;
    if (signalElectrons.size()==0) channel = 0;
    else if (signalMuons.size()==0) channel = 1;
    else if (n_leptons==2) channel = signalElectrons[0].Pt()>signalMuons[0].Pt()?2:3;
    else {
      // 3 or more leptons, need to check momenta
      if      (signalMuons.size()>1     && signalMuons[1].Pt()>signalElectrons[0].Pt()) channel = 0;
      else if (signalElectrons.size()>1 && signalElectrons[1].Pt()>signalMuons[0].Pt()) channel = 1;
      else channel = signalElectrons[0].Pt()>signalMuons[0].Pt()?2:3;
    }

    
    
    // Add OS requirement
    if ((signalLeptons[0].charge()*signalLeptons[1].charge()<0) && signalLeptons[0].Pt()>50){
      // "Hard" lepton regions
      // On-shell signal regions
      if (met>400 && HT> 400 && n_jets>1 && 81<mll && mll<101 && mt2>25 && channel<2 && dphiMin2>0.4) accept("SRZsoft");
      if (met>200 && HT>1200 && n_jets>1 && 81<mll && mll<101 && channel<2 && dphiMin2>0.4) accept("SRZhard");
      // Edge signal regions
      if (met>250 && HT> 200 && n_jets>1 && mt2>70 && mll>12 && channel<2 && dphiMin2>0.4){ accept("SRlow"); fill("SRlow_mll",mll); }
      if (met>400 && HT> 400 && n_jets>1 && mt2>25 && mll>12 && channel<2 && dphiMin2>0.4){ accept("SRmedium"); fill("SRmedium_mll",mll); }
      if (met>200 && HT>1200 && n_jets>1 &&           mll>12 && channel<2 && dphiMin2>0.4){ accept("SRhigh"); fill("SRhigh_mll",mll); }
    } 
  }  // end of high-pt

  // low-pt selection
  // baseline electrons are requested to pass the loose likelihood identification criteria
  //    and have pT > 10 GeV and |eta| < 2.47
  auto electrons_lowpt  = event->getElectrons(7., 2.47, ELooseLH);
  // baseline muons are required to pass the Medium selections and to have pT > 10 GeV, |eta| < 2.5
  auto muons_lowpt      = event->getMuons(7., 2.5, MuMedium);
  // small-R jets: pT > 20 GeV, |eta| < 2.8
  auto candJets_lowpt   = event->getJets(20., 2.8);

  candJets_lowpt = filterObjects(candJets_lowpt, 20, 2.8, JVT50Jet);

  electrons_lowpt  = overlapRemoval(electrons_lowpt, muons_lowpt, 0.01);
  candJets_lowpt   = overlapRemoval(candJets_lowpt, electrons_lowpt, 0.2, NOT(BTag77MV2c10));
  electrons_lowpt  = overlapRemoval(electrons_lowpt, candJets_lowpt, radiusCalcElec);
  candJets_lowpt   = overlapRemoval(candJets_lowpt, muons_lowpt, radiusCalcJet, LessThan3Tracks);
  muons_lowpt      = overlapRemoval(muons_lowpt, candJets_lowpt, radiusCalcMuon);


  // require signal jets to be 30 GeV
  auto signalJets_lowpt      = filterObjects(candJets_lowpt, 30, 2.5, JVT50Jet);
  // signal electrons are required to pass the Medium likelihood criteria and isolated using LooseTrackOnly
  auto signalElectrons_lowpt = filterObjects(electrons_lowpt, 7, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  // signal muons are required to be isolated using LooseTrackOnly
  auto signalMuons_lowpt     = filterObjects(muons_lowpt, 7, 2.5, MuD0Sigma3|MuZ05mm|MuIsoGradientLoose|MuNotCosmic);
  // combine into signalLeptons for easy counting
  auto signalLeptons_lowpt   = signalElectrons_lowpt + signalMuons_lowpt;
  sortObjectsByPt( signalLeptons_lowpt );
  sortObjectsByPt( signalMuons_lowpt );
  sortObjectsByPt( signalElectrons_lowpt );
  sortObjectsByPt( signalJets_lowpt );

  int n_jets_lowpt  = signalJets_lowpt.size();
  int n_leptons_lowpt     = signalLeptons_lowpt.size();

  if (n_leptons_lowpt>1){

    if ((signalLeptons_lowpt[0].charge()*signalLeptons_lowpt[1].charge()<0)){
      TLorentzVector dilepton_lowpt( signalLeptons_lowpt[0].Px()+signalLeptons_lowpt[1].Px() , signalLeptons_lowpt[0].Py()+signalLeptons_lowpt[1].Py() ,
				     signalLeptons_lowpt[0].Pz()+signalLeptons_lowpt[1].Pz() , signalLeptons_lowpt[0].E() +signalLeptons_lowpt[1].E()  );
      float mll_lowpt = dilepton_lowpt.M();
      float pTll_lowpt = dilepton_lowpt.Pt();

      // dphimin between leading 2 signal jets and met
      float dphiMin2_lowpt = n_jets_lowpt>1 ? minDphi(metVec, signalJets_lowpt, 2) : -1;
      
      // mm is channel 0; ee is channel 1; em is channel 2; me is channel 3
      int channel_lowpt = -1;
      if (signalElectrons_lowpt.size()==0) channel_lowpt = 0;
      else if (signalMuons_lowpt.size()==0) channel_lowpt = 1;
      else if (n_leptons_lowpt==2) channel_lowpt = signalElectrons_lowpt[0].Pt()>signalMuons_lowpt[0].Pt()?2:3;
      else {
	// 3 or more leptons, need to check momenta
	if      (signalMuons_lowpt.size()>1     && signalMuons_lowpt[1].Pt()>signalElectrons_lowpt[0].Pt()) channel_lowpt = 0;
	else if (signalElectrons_lowpt.size()>1 && signalElectrons_lowpt[1].Pt()>signalMuons_lowpt[0].Pt()) channel_lowpt = 1;
	else channel_lowpt = signalElectrons_lowpt[0].Pt()>signalMuons_lowpt[0].Pt()?2:3;
      }
      // low-pt SR histograms      
      if (!(signalLeptons_lowpt[0].Pt()>50 && signalLeptons_lowpt[1].Pt()>25)){
	if (met>250 && pTll_lowpt<20 && n_jets_lowpt>1 && ((mll_lowpt>4 && mll_lowpt<8.4) || mll_lowpt>11) && channel_lowpt<2 && dphiMin2_lowpt>0.4){ accept("SRC"); fill("SRC_mll",mll_lowpt); }
	if (met>500 && pTll_lowpt<75 && n_jets_lowpt>1 && ((mll_lowpt>4 && mll_lowpt<8.4) || mll_lowpt>11) && channel_lowpt<2 && dphiMin2_lowpt>0.4){ accept("SRCMET"); fill("SRCMET_mll",mll_lowpt); }
	
      }
    } 
  }

  return;
}
