#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <unordered_set>
#include <algorithm>

#ifdef ROOTCORE_PACKAGE_Ext_RestFrames

//---------------------------------------------------------
//  
//  SUSY EWK Compressed 'Higgsino' Analysis - VBF
//  ANA-SUSY-2018-16
//
//---------------------------------------------------------

DefineAnalysis(EwkCompressedVBF2018)

void EwkCompressedVBF2018::Init()
{

  //VBF exclusive SRs
  //SR-VBF-high, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_high_eMLLa","SR_VBF_high_eMLLb","SR_VBF_high_eMLLc","SR_VBF_high_eMLLd","SR_VBF_high_eMLLe","SR_VBF_high_eMLLf","SR_VBF_high_eMLLg"} );
  //SR-VBF-low, exclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_low_eMLLa","SR_VBF_low_eMLLb","SR_VBF_low_eMLLc","SR_VBF_low_eMLLd","SR_VBF_low_eMLLe","SR_VBF_low_eMLLf","SR_VBF_low_eMLLg"} );
  addRegions( {"SR_VBF_eMLLa","SR_VBF_eMLLb","SR_VBF_eMLLc","SR_VBF_eMLLd","SR_VBF_eMLLe","SR_VBF_eMLLf","SR_VBF_eMLLg"} );


  //VBF inclusive SRs
  //SR-VBF-high, inclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_high_iMLLa","SR_VBF_high_iMLLb","SR_VBF_high_iMLLc","SR_VBF_high_iMLLd","SR_VBF_high_iMLLe","SR_VBF_high_iMLLf","SR_VBF_high_iMLLg"} );
  //SR-VBF-low, inclusive mll bins (1, 2, 3, 5, 10, 20, 30, 40 GeV)
  addRegions( {"SR_VBF_low_iMLLa","SR_VBF_low_iMLLb","SR_VBF_low_iMLLc","SR_VBF_low_iMLLd","SR_VBF_low_iMLLe","SR_VBF_low_iMLLf","SR_VBF_low_iMLLg"} );
  addRegions( {"SR_VBF_iMLLa","SR_VBF_iMLLb","SR_VBF_iMLLc","SR_VBF_iMLLd","SR_VBF_iMLLe","SR_VBF_iMLLf","SR_VBF_iMLLg"} );


  //addHistogram("Met",20,0,2000);

  //---------------------------------------------------------
  //
  //                   Intitialize RJR
  //
  //---------------------------------------------------------

  LabRecoFrame* LAB        = m_RF_helper.addLabFrame("LAB");
  DecayRecoFrame* CM       = m_RF_helper.addDecayFrame("CM");
  DecayRecoFrame* S        = m_RF_helper.addDecayFrame("S");
  VisibleRecoFrame* VBF    = m_RF_helper.addVisibleFrame("VBF");
  VisibleRecoFrame* specJ  = m_RF_helper.addVisibleFrame("specJ");
  VisibleRecoFrame* LL      = m_RF_helper.addVisibleFrame("LL");
  InvisibleRecoFrame* I    = m_RF_helper.addInvisibleFrame("I");

  InvisibleGroup* INV      = m_RF_helper.addInvisibleGroup("INV");

  InvisibleJigsaw* RapidityJigsaw = m_RF_helper.addInvisibleJigsaw("RapidityJigsaw", kSetRapidity);
  //kSetRapidity does InvisibleJigsaw = new SetRapidityInvJigsaw("RapidityJigsaw","RapidityJigsaw")

  InvisibleJigsaw* InvMass = m_RF_helper.addInvisibleJigsaw("InvMass", kSetMass);
  //kSetMass does InvisibleJigsaw = new SetMassInvJigsaw("InvMass","InvMass");

  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*VBF);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*specJ);
  S->AddChildFrame(*I);
  S->AddChildFrame(*LL);

  LAB->InitializeTree();

  INV->AddFrame(*I);

  INV->AddJigsaw(*InvMass);
  INV->AddJigsaw(*RapidityJigsaw);
  InvMass->AddVisibleFrames(S->GetListVisibleFrames());
  RapidityJigsaw->AddVisibleFrames(S->GetListVisibleFrames());

  LAB->InitializeAnalysis();

}

//---------------------------------------------------------

//---------------------------------------------------------  

void EwkCompressedVBF2018::ProcessEvent(AnalysisEvent *event)
{
  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, EVeryLooseLH);
  auto baseMuons      = event->getMuons(3, 2.5, MuMedium); //actually lowPt
  auto preJets        = event->getJets(20., 4.5); 
  auto metVec         = event->getMET();
  float met = metVec.Pt();

  // Reject events with bad jets
  if (countObjects(preJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // hard lepton overlap removal and signal lepton/jet definitions
  auto baseJets        = overlapRemoval(preJets,       baseElectrons, 0.2, NOT(BTag85MV2c10));
       baseElectrons   = overlapRemoval(baseElectrons, baseJets,      0.4);
       baseElectrons   = overlapRemoval(baseElectrons, baseMuons,     0.01);
       baseJets        = overlapRemoval(baseJets,      baseMuons,     0.4, LessThan3Tracks);
       baseMuons       = overlapRemoval(baseMuons,     baseJets, 0.4);
  auto vbfJets         = filterObjects( baseJets,      30,  4.5,  JVT50Jet);
  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT50Jet);
  auto signalBJets     = filterObjects( baseJets,      20,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradient);
  auto signalMuons     = filterObjects( baseMuons,     3,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoFixedCutTightTrackOnly);

  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;

  // Preselect at least 2 baseline lepton
  // Final selection requires exactly 2 signal leptons
  if ( !(baseLeptons.size() >=2) ) return;
  //remove events with b-jets for cleaner calculation of VBF variables
  if ( !(signalBJets.size() == 0) ) return;
  // preselect at least 2 signal jets because this is a VBF analysis
  if ( !(vbfJets.size() >= 2) ) return;

  //---------------------------------------------------------
  //
  //                Calculate VBF variables
  //
  //---------------------------------------------------------

  bool validVBFtags = false;
  bool validSpecJet = false;

  int vbftag_index1 = -1, vbftag_index2 = -1;

  unsigned int nvbfJets = vbfJets.size();

  float tmp_mjj = 0;
  for (unsigned int i = 0; i < nvbfJets; ++i){
    for (unsigned int j = 0; j < nvbfJets; ++j){
      if ((vbfJets[i]+vbfJets[j]).M() > tmp_mjj) {
        tmp_mjj = (vbfJets[i]+vbfJets[j]).M();
        vbftag_index1 = i;
        vbftag_index2 = j;
      }
    }
  }

  if (vbftag_index1 < 0 || vbftag_index2 < 0) {
    std::cout << "Something went wrong..." << std::endl;
  } else {
    validVBFtags = true;
  }

  auto vbftag_j1 = vbfJets[vbftag_index1].Pt() > vbfJets[vbftag_index2].Pt() ? vbfJets[vbftag_index1] : vbfJets[vbftag_index2];
  auto vbftag_j2 = vbfJets[vbftag_index1].Pt() > vbfJets[vbftag_index2].Pt() ? vbfJets[vbftag_index2] : vbfJets[vbftag_index1];

  unsigned int nsignalJets = signalJets.size();

  TLorentzVector vbftag_j3(0.,0.,0.,0.);

  for (unsigned int i = 0; i < nsignalJets; ++i){
    if (signalJets[i].DeltaR(vbftag_j1) <= 2.0) continue;
    if (signalJets[i].DeltaR(vbftag_j2) <= 1.0) continue;
    vbftag_j3.SetPtEtaPhiM(signalJets[i].Pt(),signalJets[i].Eta(),signalJets[i].Phi(),signalJets[i].M());
    validSpecJet=true;
    break;
  }
  
  //---------------------------------------------------------
  //
  //                Calculate RJR variables
  //
  //---------------------------------------------------------

  // analyze event in RestFrames tree
  // Combinatoric setup for jets
  LabRecoFrame* LAB = m_RF_helper.getLabFrame("LAB");
  DecayRecoFrame* CM = m_RF_helper.getDecayFrame("CM");
  VisibleRecoFrame* VBF = m_RF_helper.getVisibleFrame("VBF");
  DecayRecoFrame* S = m_RF_helper.getDecayFrame("S");
  VisibleRecoFrame* specJ = m_RF_helper.getVisibleFrame("specJ");
  VisibleRecoFrame* LL = m_RF_helper.getVisibleFrame("LL");
  InvisibleRecoFrame* I = m_RF_helper.getInvisibleFrame("I");
  InvisibleGroup* INV = m_RF_helper.getInvisibleGroup("INV");

  LAB->ClearEvent();

  VBF->SetLabFrameFourVector((vbftag_j1 + vbftag_j1).transFourVect());

  TLorentzVector lepSys(0.,0.,0.,0.);
  for(const auto lep1 : signalLeptons){
    lepSys = lepSys + lep1.transFourVect();
  }
  LL->SetLabFrameFourVector(lepSys);

  INV->SetLabFrameThreeVector(metVec.Vect());

  specJ->SetLabFrameFourVector(vbftag_j3);
  
  if (!LAB->AnalyzeEvent()) std::cout << "Something went wrong..." << std::endl;

  //RJR variables
  float Pt_VBF = 0.0; 
  float R_VBF = 0.0;
  float M_VBF = 0.0;
  float M_S = 0.0;
  float Pt_S = 0.0;
  float DPhi_I_VBF = 0.0;

  TVector3 v_P_VBF = VBF->GetFourVector(*CM).Vect();
  TVector3 v_P_I   = I->GetFourVector(*CM).Vect();
  TVector3 v_P_S   = S->GetFourVector(*CM).Vect();

  Pt_VBF = v_P_VBF.Mag();
  Pt_S = v_P_S.Mag();
  R_VBF = fabs(v_P_I.Dot(v_P_VBF.Unit())) / Pt_VBF;
  M_S = S->GetMass();
  M_VBF = VBF->GetMass();
  DPhi_I_VBF = fabs(v_P_VBF.DeltaPhi(v_P_I));

  //---------------------------------------------------------
  //
  //                Calculate other variables
  //
  //---------------------------------------------------------

  // Events must have ==2 signal leptons or ==1 signal lepton plus >=1 track
  bool is2L  = (signalLeptons.size() == 2);
  if ( !(is2L) ) return;

  auto lep1 = signalLeptons[0];
  auto lep2 = signalLeptons[1];
  auto lep = lep1+lep2;

  // Minimum of the delta phi between all jets with MET
  float minDPhiAllJetsMet = minDphi(metVec,vbfJets);

  float lep1Pt = lep1.Pt();
  float lep2Pt = lep2.Pt();

  // SFOS decision
  bool isOS = ( lep1.charge() != lep2.charge() ); // opposite charge
  bool isSF = ( lep1.type() == lep2.type() ) ; // same flavour
  bool isee = ( lep1.type() == ELECTRON && lep2.type() == ELECTRON ); //di-electron
  bool ismm = ( lep1.type() == MUON && lep2.type() == MUON ); //di-muon

  // Higher level leptonic variables
  float mll          = (lep1+lep2).M();
  float drll         = lep1.DeltaR(lep2);
  float metOverHtLep = met / (lep1Pt + lep2Pt); 
  float MTauTau      = calcMTauTau(lep1, lep2, metVec);
  float mt           = calcMT( lep1,metVec );
  
  // mT2 with trial invisible particle mass at 100 GeV
  float mn = 100.; 
  //  float mt2 = calc_massive_MT2(baseLeptons[0], baseLeptons[1], metVec, mn);   
  float mt2=calcAMT2(lep1,lep2,metVec,mn,mn);

  auto vbf_jj_sys = vbftag_j1 + vbftag_j2;

  float jet1Pt   = vbftag_j1.Pt();
  float jet2Pt   = vbftag_j2.Pt();
  float vbfSpecJet_Pt  = validSpecJet ? vbftag_j3.Pt() : -1.;
  float vbfSpecJet_Eta = validSpecJet ? vbftag_j3.Eta() : -999.;

  float vbfjjPt = vbf_jj_sys.Pt();
  float vbfjjEta = vbf_jj_sys.Eta();
  float vbfjjPhi = vbf_jj_sys.Phi();
  float vbfjjM = vbf_jj_sys.M();

  float vbfjjDEta = TMath::Abs(vbftag_j1.Eta() - vbftag_j2.Eta());
  float vbfjjDPhi = TMath::Abs(vbftag_j1.DeltaPhi(vbftag_j2));
  float vbfjjDR = vbftag_j1.DeltaR(vbftag_j2);

  float vbfEtaEta = vbftag_j1.Eta()*vbftag_j2.Eta();
  float HTVBF = vbftag_j1.Pt() + vbftag_j2.Pt();
  float HTlep = lep1Pt + lep2Pt;

  // SUSYTools final state (fs) code
  int fs = event->getSUSYChannel();
  // stau veto for slepton signal - note this does not affect the denominator in the acceptance numbers (not needed in R21)
  bool isNotStau = ( fs != 206 && fs != 207 );

  int mcChannel = event->getMCNumber();
  //std::cout << "DSID: " << mcChannel <<std::endl;

  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------

  // Common 2L cuts
  bool pass_common_cuts = false;
  if (    ( is2L )
       && ( lep1Pt > 5. )
       && ( (drll > 0.05 && ismm) || (drll > 0.3 && isee) )
       && ( isOS && isSF )
       && ( (mll > 1. && mll < 60. && ismm) || (mll > 3. && mll < 60. && isee) )
       && ( mll < 3. || mll > 3.2 )
       && ( MTauTau < 0. || MTauTau > 160. )
       && ( met >  120. )
       && ( signalJets.size() >= 1 )
       && ( signalBJets.size()   == 0 ) 
       && ( jet1Pt               >  100. )
       && ( minDPhiAllJetsMet    >  0.4 )
      ) pass_common_cuts = true;

  bool keep_SR_VBF =  pass_common_cuts 
                              && ( mll < 40. )
                              && ( nvbfJets >=2 )
                              && ( jet2Pt > 40. )
                              && ( met > 150. )
                              && ( metOverHtLep > 2. )
                              && ( lep2Pt > std::min(10., 2. + mll/3.) )
                              && ( mt < 60. )
                              && ( R_VBF < 1. )
                              && ( R_VBF > std::max(0.6, 0.92 - mll/60.) )
                              && ( vbfEtaEta < 0 )
                              && ( vbfjjM > 400 )
                              && ( TMath::Abs(vbfjjDEta) > 2.0 );

  bool keep_SR_VBF_high = keep_SR_VBF && ( TMath::Abs(vbfjjDEta) > 4.0 );
  bool keep_SR_VBF_low  = keep_SR_VBF && ( TMath::Abs(vbfjjDEta) < 4.0 );

  bool keep_iMLLa = mll < 2.;
  bool keep_iMLLb = mll < 3.;
  bool keep_iMLLc = mll < 5.;
  bool keep_iMLLd = mll < 10.;
  bool keep_iMLLe = mll < 20.;
  bool keep_iMLLf = mll < 30.;
  bool keep_iMLLg = mll < 40.;

  //VBF inclusive SRs
  //high
  if (keep_SR_VBF_high && keep_iMLLa) accept("SR_VBF_high_iMLLa");
  if (keep_SR_VBF_high && keep_iMLLb) accept("SR_VBF_high_iMLLb");
  if (keep_SR_VBF_high && keep_iMLLc) accept("SR_VBF_high_iMLLc");
  if (keep_SR_VBF_high && keep_iMLLd) accept("SR_VBF_high_iMLLd");
  if (keep_SR_VBF_high && keep_iMLLe) accept("SR_VBF_high_iMLLe");
  if (keep_SR_VBF_high && keep_iMLLf) accept("SR_VBF_high_iMLLf");
  if (keep_SR_VBF_high && keep_iMLLg) accept("SR_VBF_high_iMLLg");
  //low
  if (keep_SR_VBF_low && keep_iMLLa) accept("SR_VBF_low_iMLLa");
  if (keep_SR_VBF_low && keep_iMLLb) accept("SR_VBF_low_iMLLb");
  if (keep_SR_VBF_low && keep_iMLLc) accept("SR_VBF_low_iMLLc");
  if (keep_SR_VBF_low && keep_iMLLd) accept("SR_VBF_low_iMLLd");
  if (keep_SR_VBF_low && keep_iMLLe) accept("SR_VBF_low_iMLLe");
  if (keep_SR_VBF_low && keep_iMLLf) accept("SR_VBF_low_iMLLf");
  if (keep_SR_VBF_low && keep_iMLLg) accept("SR_VBF_low_iMLLg");
  //inclusive
  if (keep_SR_VBF && keep_iMLLa) accept("SR_VBF_iMLLa");
  if (keep_SR_VBF && keep_iMLLb) accept("SR_VBF_iMLLb");
  if (keep_SR_VBF && keep_iMLLc) accept("SR_VBF_iMLLc");
  if (keep_SR_VBF && keep_iMLLd) accept("SR_VBF_iMLLd");
  if (keep_SR_VBF && keep_iMLLe) accept("SR_VBF_iMLLe");
  if (keep_SR_VBF && keep_iMLLf) accept("SR_VBF_iMLLf");
  if (keep_SR_VBF && keep_iMLLg) accept("SR_VBF_iMLLg");
  
  //Electroweakino exclusive SRs
  //high
  if (keep_SR_VBF_high && mll < 2.)       accept("SR_VBF_high_eMLLa");
  else if (keep_SR_VBF_high && mll < 3.)  accept("SR_VBF_high_eMLLb");
  else if (keep_SR_VBF_high && mll < 5.)  accept("SR_VBF_high_eMLLc");
  else if (keep_SR_VBF_high && mll < 10.) accept("SR_VBF_high_eMLLd");
  else if (keep_SR_VBF_high && mll < 20.) accept("SR_VBF_high_eMLLe");
  else if (keep_SR_VBF_high && mll < 30.) accept("SR_VBF_high_eMLLf");
  else if (keep_SR_VBF_high && mll < 40.) accept("SR_VBF_high_eMLLg");
  //low
  if (keep_SR_VBF_low && mll < 2.)       accept("SR_VBF_low_eMLLa");
  else if (keep_SR_VBF_low && mll < 3.)  accept("SR_VBF_low_eMLLb");
  else if (keep_SR_VBF_low && mll < 5.)  accept("SR_VBF_low_eMLLc");
  else if (keep_SR_VBF_low && mll < 10.) accept("SR_VBF_low_eMLLd");
  else if (keep_SR_VBF_low && mll < 20.) accept("SR_VBF_low_eMLLe");
  else if (keep_SR_VBF_low && mll < 30.) accept("SR_VBF_low_eMLLf");
  else if (keep_SR_VBF_low && mll < 40.) accept("SR_VBF_low_eMLLg");  
  //inclusive
  if (keep_SR_VBF && mll < 2.)       accept("SR_VBF_eMLLa");
  else if (keep_SR_VBF && mll < 3.)  accept("SR_VBF_eMLLb");
  else if (keep_SR_VBF && mll < 5.)  accept("SR_VBF_eMLLc");
  else if (keep_SR_VBF && mll < 10.) accept("SR_VBF_eMLLd");
  else if (keep_SR_VBF && mll < 20.) accept("SR_VBF_eMLLe");
  else if (keep_SR_VBF && mll < 30.) accept("SR_VBF_eMLLf");
  else if (keep_SR_VBF && mll < 40.) accept("SR_VBF_eMLLg");  

  // Fill in optional ntuple variables
  ntupVar("FS",                fs);               
  ntupVar("DSID",              mcChannel);
  ntupVar("mc_weight",  event->getMCWeights()[0]);  
  ntupVar("susyChannel",event->getSUSYChannel());
  ntupVar("mcWeights",event->getMCWeights());
  ntupVar("pdf_id1",event->getPDF_id1());
  ntupVar("pdf_x1",event->getPDF_x1());
  ntupVar("pdf_pdf1",event->getPDF_pdf1());
  ntupVar("pdf_id2",event->getPDF_id2());
  ntupVar("pdf_x2",event->getPDF_x2());
  ntupVar("pdf_pdf2",event->getPDF_pdf2());
  ntupVar("pdf_scale",event->getPDF_scale());
  ntupVar("met_Et",            met);               
  ntupVar("nLep_base",         static_cast<int>(baseLeptons.size()));               
  ntupVar("nLep_signal",       static_cast<int>(signalLeptons.size()));
  ntupVar("nEle_signal",       static_cast<int>(signalElectrons.size()));
  ntupVar("nMu_signal",        static_cast<int>(signalMuons.size()));         
  ntupVar("nBJet20_MV2c10",    static_cast<int>(signalBJets.size()));
  ntupVar("nJet30",            static_cast<int>(signalJets.size()));
  ntupVar("nJetVBF",           static_cast<int>(vbfJets.size()));
  ntupVar("isOS",              isOS);               
  ntupVar("isSF",              isSF);
  ntupVar("isee",              isee);               
  ntupVar("ismm",              ismm);
  ntupVar("is2L",              is2L);
  ntupVar("validVBFtags",      validVBFtags);
  ntupVar("validSpecJet",      validSpecJet);
  ntupVar("lep1Pt",            lep1Pt);               
  ntupVar("lep2Pt",            lep2Pt);               
  ntupVar("vbfTagJet1_Pt",     jet1Pt);
  ntupVar("vbfTagJet2_Pt",     jet2Pt);
  ntupVar("vbfSpecJet_Pt",     vbfSpecJet_Pt);
  ntupVar("vbfSpecJet_Eta",    vbfSpecJet_Eta);
  ntupVar("vbfjjPt",           vbfjjPt);
  ntupVar("vbfjjEta",          vbfjjEta);
  ntupVar("vbfjjPhi",          vbfjjPhi);
  ntupVar("vbfjjM",            vbfjjM);
  ntupVar("vbfEtaEta",         vbfEtaEta);
  ntupVar("vbfjjDEta",         vbfjjDEta);
  ntupVar("vbfjjDPhi",         vbfjjDPhi);
  ntupVar("vbfjjDR",           vbfjjDR);
  ntupVar("HTVBF",             HTVBF);
  ntupVar("mll",               mll);               
  ntupVar("Rll",               drll);               
  ntupVar("mt_lep1",           mt);               
  ntupVar("mt2leplsp_100",     mt2);               
  ntupVar("MTauTau",           MTauTau);               
  ntupVar("METOverHTLep",      metOverHtLep);               
  ntupVar("minDPhiAllJetsMet", minDPhiAllJetsMet);
  ntupVar("vbfRJR_PTVBF",      Pt_VBF);
  ntupVar("vbfRJR_RVBF",       R_VBF);
  ntupVar("vbfRJR_PTS",        Pt_S);
  ntupVar("vbfRJR_MS",         M_S);
  ntupVar("vbfRJR_MVBF",       M_VBF);
  ntupVar("vbfRJR_dphiVBFI",   DPhi_I_VBF);
  ntupVar("pass_common_cuts",  pass_common_cuts);
  ntupVar("pass_SR_VBF_high",  keep_SR_VBF_high);
  ntupVar("pass_SR_VBF_low",   keep_SR_VBF_low);
  ntupVar("pass_SR_VBF",       keep_SR_VBF);
}

#endif

