#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <TFile.h>
#include <TGraphErrors.h>

DefineAnalysis(EwkFullHad2018)

namespace
{
  class effMap {
  public:
    effMap(const char* name) {
      TFile fh(FindFile("EWK0L_BosonTaggingEfficiency.root").c_str());
      effHist = dynamic_cast < TGraphErrors * >(fh.FindObjectAny(name)->Clone());
      if (!effHist) throw std::runtime_error("Could not find efficiency map for EwkFullHad2018");
    };
    ~effMap() { delete effHist; };
    double eff(double pt) {
      return effHist->Eval(pt);
    };
  private:
    TGraphErrors* effHist;
  };

  // Efficiency maps depend on the rigin of the jet
  effMap *acceffWboson  = 0;
  effMap *acceffZboson  = 0;
  effMap *acceffWTagBkg = 0;
  effMap *acceffZTagBkg = 0;
}

void EwkFullHad2018::Init()
{
  addRegions({"Preselection0L","Preselection0L4Q","Preselection0L2B2Q","Preselection1L4Q","Preselection1L2B2Q","Preselection1Y4Q","Preselection1Y2B2Q"});
  addRegions({"SR4QWW","SR4QWZ","SR4QZZ","SR4QVV","SR2B2QWZ","SR2B2QWh","SR2B2QZZ","SR2B2QZh","SR2B2QVZ","SR2B2QVh"});
  addRegions({"CR4Q","CR2B2Q"});
  addRegions({"VR1L4Q1T1F","VR1L4Q2T","VR1L2B2Q1T1F","VR1L2B2Q2T","VR1Y4Q1T1F","VR1Y4Q2T","VR1Y2B2Q1T1F","VR1Y2B2Q2T"});
  //
  addHistogram("cutflow",30,0,30);

  // Read efficiency map
  acceffWboson  = new effMap("Wqq_Tag_efficiency");
  acceffZboson  = new effMap("Zqq_Tag_efficiency");
  acceffWTagBkg = new effMap("Wqq_Tag_rejection");
  acceffZTagBkg = new effMap("Zqq_Tag_rejection");


#ifdef PACKAGE_BTaggingTruthTagging
#pragma message "Compiling BTaggingTruthTagging for TRF usage"
  m_btt = new BTaggingTruthTaggingTool("MyBTaggingTruthTaggingTool");
  StatusCode code = m_btt->setProperty("TaggerName", "MV2c10");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool TaggerName property");
  code = m_btt->setProperty("OperatingPoint", "FixedCutBEff_85");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool OperatingPoint property");
  code = m_btt->setProperty("JetAuthor", "AntiKtVR30Rmax4Rmin02TrackJets");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool JetAuthor property");
  code = m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool ScaleFactorFileName property");
  // call initialize() function
  code = m_btt->initialize();
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error initializing the BTaggingTruthTaggingTool");
  std::cout << "Initialized BTaggingTruthTagging tool" << std::endl;
#endif

}

static int jetFlavor(AnalysisObject &jet) {
  if (jet.pass(TrueBJet)) return 5;
  if (jet.pass(TrueCJet)) return 4;
  if (jet.pass(TrueTau)) return 15;
  return 0;
}

void EwkFullHad2018::ProcessEvent(AnalysisEvent *event)
{

  float gen_filt_met = event->getGenMET();
  float gen_filt_ht  = event->getGenHT();
  int channel_number = event->getMCNumber();

  auto baselineElectrons  = event->getElectrons(7, 2.47, ELooseBLLH | EZ05mm);
  auto baselineMuons      = event->getMuons(6, 2.7, MuMedium | MuZ05mm | MuNotCosmic | MuQoPSignificance);
  auto baselineJets       = event->getJets(30., 4.5);
  auto baselinePhotons    = event->getPhotons(50, 2.37, PhotonTight);
  auto baselineTrackjets  = event->getTrackJets(5, 2.8);
  auto metVec     = event->getMET();
  double met      = metVec.Et();
  double met_phi  = metVec.Phi();
  auto trackjets          = filterObjects(baselineTrackjets, 20, 2.8);
  auto fatjets   = event->getFatJets(200, 2.0);
  
  // Overlap removal
  auto radiusCalcLepton = [] (const AnalysisObject& lepton, const AnalysisObject&) { return std::min(0.4, 0.04 + 10./lepton.Pt()); };
  auto radiusCalcTrackj = [] (const AnalysisObject& trackj, const AnalysisObject&) { return std::max(0.02, std::min( 0.4, 30./trackj.Pt())); };
  // 1. e-mu: shared ID track
  baselineElectrons = overlapRemoval(baselineElectrons, baselineMuons, 0.01);
  // 2. gamma-e/mu: deltaR < 0.4
  baselinePhotons   = overlapRemoval(baselinePhotons, baselineElectrons, 0.4);
  baselinePhotons   = overlapRemoval(baselinePhotons, baselineMuons, 0.4);
  // 3a. j-e: deltaR < 0.2
  baselineJets      = overlapRemoval(baselineJets, baselineElectrons, 0.2);
  // 3b. j-gamma: deltaR < 0.2
  baselineJets      = overlapRemoval(baselineJets, baselinePhotons, 0.2);
  // 3c. j-mu: deltaR < 0.2 but only LessThanThreeTrack
  //baselineJets      = overlapMuPFJRemoval(baselineJets, baselineMuons, 0.4, LessThan3Tracks); // for PFlow
  // 4. e/mu-j: (62.5:dR=0.2, 27.8: dR=0.4) variable radius: no leptons removed below/above 28/60
  baselineElectrons = overlapRemoval(baselineElectrons, baselineJets, radiusCalcLepton);
  baselineMuons     = overlapRemoval(baselineMuons, baselineJets, radiusCalcLepton);
  baselinePhotons   = overlapRemoval(baselinePhotons, baselineJets, 0.4);
  // 5. track j-e/mu : variable radius
  trackjets         = overlapRemoval(trackjets, baselineElectrons, radiusCalcTrackj);
  trackjets         = overlapRemoval(trackjets, baselineMuons, radiusCalcTrackj);
  // 6. large-R jet removal : dR(l/gamma, large-R jet)
  fatjets           = overlapRemoval(fatjets, baselineElectrons, 1.0);
  fatjets           = overlapRemoval(fatjets, baselineMuons, 1.0);
  fatjets           = overlapRemoval(fatjets, baselinePhotons, 1.0);

  //====================================================================================================
  auto baselineleptons = baselineElectrons + baselineMuons;
  int nbaselineleptons = baselineleptons.size();

  // signal objects
  auto electrons = filterObjects(baselineElectrons, 7, 2.47, ETightLH | ED0Sigma5 | EZ05mm | EIsoFCTight);
  auto muons     = filterObjects(baselineMuons, 6, 2.7, MuD0Sigma3 | MuZ05mm | MuIsoFixedCutLoose);
  auto photons   = filterObjects(baselinePhotons, 150, 2.37, PhotonIsoFixedCutTight);
  auto taus      = event->getTaus(20, 2.5, TauMedium);
  auto jets      = filterObjects(baselineJets, 30, 2.8, JVT50Jet);
  auto bjets     = filterObjects(jets, 30., 2.8, BTag85MV2c10);
  auto btrackjets= filterObjects(trackjets, 20, 2.8, BTag85MV2c10);
  auto leptons   = electrons + muons;

  int nleptons   = leptons.size();
  int nelectrons = electrons.size();
  int nmuons     = muons.size();
  int nphotons   = photons.size();
  int nbaselinejets      = baselineJets.size();
  int njets      = jets.size();
  int nbjets     = bjets.size();
  auto truthbjets = event->getHSTruth(15.,2.7,5);
  int ntrackbjets= truthbjets.size();
  int nfatjets   = fatjets.size();

  // pT order the objects
  sortObjectsByPt(leptons);
  sortObjectsByPt(jets);
  sortObjectsByPt(fatjets);

  int cutflowcounter = 0;
  fill("cutflow", cutflowcounter++);

  // add some variables
  float minDPhi_met_allJets30 = minDphi(metVec, jets, 10000, 30.);
  int fatJet1NGATrkB85 = 0;
  int fatJet2NGATrkB85 = 0;
  if ( nfatjets > 0) {
    fatJet1NGATrkB85 = fatjets.at(0).getNBtags();
    if(nfatjets > 1){
      fatJet2NGATrkB85 = fatjets.at(1).getNBtags();
    }
  }

  TLorentzVector fatjet1_2btagged;
  TLorentzVector fatjet2_2btagged;
  if(nfatjets > 0) fatjet1_2btagged = fatjets[0];
  if(nfatjets > 1) fatjet2_2btagged = fatjets[1];
  int n1add = 0;
  int n2add = 0;
  if ( nfatjets > 0) {
  for(const auto& muon : event->getMuons(0, 2.7, MuZ05mm | MuNotCosmic | MuQoPSignificance)){
	if(n1add >= fatJet1NGATrkB85) break;
	if(fatjet1_2btagged.DeltaR(muon) < 1.0){
		fatjet1_2btagged += muon;
		n1add += 1;
	}
  }
  }
  if ( nfatjets > 1) {
  for(const auto& muon : event->getMuons(0, 2.7, MuZ05mm | MuNotCosmic | MuQoPSignificance)){
	if(n2add >= fatJet2NGATrkB85) break;
	if(fatjet2_2btagged.DeltaR(muon) < 1.0){
		fatjet2_2btagged += muon;
		n2add += 1;
	}
  }
  }

   float mt2_BB_100 = 0.;
   if ( nfatjets > 1) mt2_BB_100 = calcAMT2(fatjets[0],fatjets[1], metVec, 100.0, 100.0) -100.;
   auto TLVW = metVec;
   TLorentzVector leadinglepton;
   float L1_minDPhi_met_allJets30 = 999.;
   if(nleptons >= 1){
	leadinglepton = leptons[0];
	TLVW += leadinglepton;
  	L1_minDPhi_met_allJets30 = minDphi(TLVW, jets, 10000, 30.);
   }
   float L1_mt2_BB_100 = 0.;
   if ( nfatjets > 1 && nleptons >= 1) L1_mt2_BB_100 = calcAMT2(fatjets[0],fatjets[1],metVec+leptons[0],100.0,100.0)-100.;

   TLorentzVector leadingphoton;
   float Y1_mt2_BB_100 = 0.;
   float Y1_minDPhi_met_allJets30 = 999.;
   if (nphotons == 1){
	auto tmpphoton = photons[0];
	leadingphoton = photons[0];
	Y1_minDPhi_met_allJets30 = minDphi(tmpphoton, jets, 10000, 30.);
   	if ( nfatjets > 1) Y1_mt2_BB_100 = calcAMT2(fatjets[0],fatjets[1],photons[0],100.0,100.0)-100.;
   }
   // for 0L temporary 
   bool pass0L_Precut = false;
   bool pass1L_Precut = false;
   bool pass1Y_Precut = false;
   if (nbaselineleptons == 0 && met > 200. && nfatjets >= 2 && minDPhi_met_allJets30 > 1.) pass0L_Precut = true;
   if (nleptons == 1 && TLVW.Pt() > 200. && nfatjets >= 2 && L1_minDPhi_met_allJets30 > 1. && leptons.at(0).Pt() > 30. && met > 50.) pass1L_Precut = true;
   if (nphotons == 1 && photons.at(0).Pt() > 200. && nfatjets >= 2 && Y1_minDPhi_met_allJets30 > 1. && met < 200. && nbaselineleptons == 0) pass1Y_Precut = true;

   double Jet1WEfficiency = 0.;
   double Jet1ZEfficiency = 0.;
   double Jet1VEfficiency = 0.;
   double Jet2WEfficiency = 0.;
   double Jet2ZEfficiency = 0.;
   double Jet2VEfficiency = 0.;

   if(nfatjets >= 1){
	//if(fatJet1NGATrkB85 < 2) std::cout << fatjets.at(0).pass(WTag) << " " << fatjets.at(0).pass(ZTag) << std::endl;
	if(fatjets.at(0).M() > 40.){
	if(fatjets.at(0).pass(WTag)){
		Jet1WEfficiency = acceffWboson->eff(fatjets.at(0).Pt());
		if(fatjets.at(0).Pt() > 1000.) Jet1WEfficiency = acceffWboson->eff(1000.);
		Jet1ZEfficiency = Jet1WEfficiency*0.74;
		Jet1VEfficiency = Jet1WEfficiency*1.07;
	}else if(fatjets.at(0).pass(ZTag)){
		Jet1ZEfficiency = acceffZboson->eff(fatjets.at(0).Pt());
		if(fatjets.at(0).Pt() > 1000.) Jet1ZEfficiency = acceffZboson->eff(1000.);
		Jet1WEfficiency = Jet1ZEfficiency*0.82;
		Jet1VEfficiency = Jet1ZEfficiency*1.14;
	}else{
		Jet1WEfficiency = 1./acceffWTagBkg->eff(fatjets.at(0).Pt());
		if(fatjets.at(0).Pt() > 1000.) Jet1WEfficiency = acceffWboson->eff(1000.);
		Jet1ZEfficiency = 1./acceffZTagBkg->eff(fatjets.at(0).Pt());
		if(fatjets.at(0).Pt() > 1000.) Jet1ZEfficiency = acceffZboson->eff(1000.);
		Jet1VEfficiency = Jet1WEfficiency*1.32;
	}
	}
   }
   if(nfatjets >= 2){
	//if(fatJet2NGATrkB85 < 2) std::cout << fatjets.at(1).pass(WTag) << " " << fatjets.at(1).pass(ZTag) << std::endl;
	if(fatjets.at(0).M() > 40.){
	if(fatjets.at(1).pass(WTag)){
		Jet2WEfficiency = acceffWboson->eff(fatjets.at(1).Pt());
		if(fatjets.at(1).Pt() > 1000.) Jet2WEfficiency = acceffWboson->eff(1000.);
		Jet2ZEfficiency = Jet2WEfficiency*0.74;
		Jet2VEfficiency = Jet2WEfficiency*1.07;
	}else if(fatjets.at(1).pass(ZTag)){
		Jet2ZEfficiency = acceffZboson->eff(fatjets.at(1).Pt());
		if(fatjets.at(1).Pt() > 1000.) Jet2ZEfficiency = acceffZboson->eff(1000.);
		Jet2WEfficiency = Jet2ZEfficiency*0.82;
		Jet2VEfficiency = Jet2ZEfficiency*1.14;
	}else{
		Jet2WEfficiency = 1./acceffWTagBkg->eff(fatjets.at(1).Pt());
		if(fatjets.at(1).Pt() > 1000.) Jet2WEfficiency = acceffWboson->eff(1000.);
		Jet2ZEfficiency = 1./acceffZTagBkg->eff(fatjets.at(1).Pt());
		if(fatjets.at(1).Pt() > 1000.) Jet2ZEfficiency = acceffZboson->eff(1000.);
		Jet2VEfficiency = Jet2WEfficiency*1.32;
	}
	}
   }

   if(minDPhi_met_allJets30 > 1.0 && nbaselineleptons==0 && met > 200. && nfatjets >= 2){
	accept("Preselection0L");
	if(ntrackbjets-fatJet1NGATrkB85-fatJet2NGATrkB85 == 0){
		if(fatJet1NGATrkB85+fatJet2NGATrkB85 <= 1){
			accept("Preselection0L4Q");
			if(met > 300. && met+fatjets.at(0).Pt()+fatjets.at(1).Pt() > 1300.){
				accept("SR4QWW",Jet1WEfficiency*Jet2WEfficiency);
				if(Jet1WEfficiency > Jet2WEfficiency) accept("SR4QWZ",Jet1WEfficiency*Jet2ZEfficiency);
				else accept("SR4QWZ",Jet1ZEfficiency*Jet2WEfficiency);
				accept("SR4QZZ",Jet1ZEfficiency*Jet2ZEfficiency);
				accept("SR4QVV",Jet1VEfficiency*Jet2VEfficiency);
			}
		}

		if((fatJet1NGATrkB85==2 && fatJet2NGATrkB85 <= 1) || (fatJet1NGATrkB85 <= 1 && fatJet2NGATrkB85 == 2)){
			accept("Preselection0L2B2Q");
			if(mt2_BB_100 > 250. && met+fatjets.at(0).Pt()+fatjets.at(1).Pt() > 1000.){
				bool fatJet1Zbb = fatJet1NGATrkB85==2 && fatjet1_2btagged.M() > 70. && fatjet1_2btagged.M() < 100.;
				bool fatJet1hbb = fatJet1NGATrkB85==2 && fatjet1_2btagged.M() > 100. && fatjet1_2btagged.M() < 135.;
				bool fatJet2Zbb = fatJet2NGATrkB85==2 && fatjet2_2btagged.M() > 70. && fatjet2_2btagged.M() < 100.;
				bool fatJet2hbb = fatJet2NGATrkB85==2 && fatjet2_2btagged.M() > 100. && fatjet2_2btagged.M() < 135.;
				if(fatJet1Zbb) accept("SR2B2QWZ",Jet2WEfficiency);
				else if(fatJet2Zbb) accept("SR2B2QWZ",Jet1WEfficiency);
				if(fatJet1hbb) accept("SR2B2QWh",Jet2WEfficiency);
				else if(fatJet2hbb) accept("SR2B2QWh",Jet1WEfficiency);
				if(fatJet1Zbb) accept("SR2B2QZZ",Jet2ZEfficiency);
				else if(fatJet2Zbb) accept("SR2B2QZZ",Jet1ZEfficiency);
				if(fatJet1hbb) accept("SR2B2QZh",Jet2ZEfficiency);
				else if(fatJet2hbb) accept("SR2B2QZh",Jet1ZEfficiency);
				if(fatJet1Zbb) accept("SR2B2QVZ",Jet2VEfficiency);
				else if(fatJet2Zbb) accept("SR2B2QVZ",Jet1VEfficiency);
				if(fatJet1hbb) accept("SR2B2QVh",Jet2VEfficiency);
				else if(fatJet2hbb) accept("SR2B2QVh",Jet1VEfficiency);
				/*
				if(fatJet1hbb){
					if(Jet2WEfficiency > 0.3 || Jet2ZEfficiency > 0.3){
					std::cout << Jet2WEfficiency << " " << Jet2ZEfficiency << " " << Jet2VEfficiency << " ";
					if(Jet2WEfficiency < Jet2ZEfficiency) std::cout << "#" << std::endl;
					else std::cout << "##" << std::endl;
					}
				}else if(fatJet2hbb){
					if(Jet1WEfficiency > 0.3 || Jet1ZEfficiency > 0.3){
					std::cout << Jet1WEfficiency << " " << Jet1ZEfficiency << " " << Jet1VEfficiency << " ";
					if(Jet1WEfficiency < Jet1ZEfficiency) std::cout << "#" << std::endl;
					else std::cout << "##" << std::endl;
					}
				}
				*/
			}
		}
	}
   }

   ntupVar("met_Et", met);
   ntupVar("met_Phi", metVec.Phi());
   ntupVar("L1_WPt", TLVW.Pt());
   ntupVar("L1_WPhi", TLVW.Phi());
   ntupVar("photon1Pt", leadingphoton.Pt());
   ntupVar("photon1Phi", leadingphoton.Phi());
   ntupVar("photon1Etai", leadingphoton.Eta());
   ntupVar("lep1Pt", leadinglepton.Pt());
   ntupVar("lep1Phi", leadinglepton.Phi());
   ntupVar("lep1Eta", leadinglepton.Eta());
   ntupVar("passPrecut", pass0L_Precut || pass1L_Precut || pass1Y_Precut);

   ntupVar("njets", njets);
   ntupVar("nJet30", nbaselinejets);
   ntupVar("nFatJets", nfatjets);
   ntupVar("nbjets", nbjets);
   ntupVar("nLep_base", nbaselineleptons);
   ntupVar("nLep_signal", nleptons);
   ntupVar("nTrackBJet20",ntrackbjets);
   if(nfatjets >= 1){
   	ntupVar("fatJet1Pt", fatjets.at(0).Pt());
   	ntupVar("fatJet1M", fatjets.at(0).M());
   	ntupVar("fatJet1Eta", fatjets.at(0).Eta());
   	ntupVar("fatJet1Phi", fatjets.at(0).Phi());
   	ntupVar("fatJet1IsW", fatjets.at(0).pass(WTag));
   	ntupVar("fatJet1IsZ", fatjets.at(0).pass(ZTag));
   	ntupVar("fatJet1IsV", fatjets.at(0).pass(WTag) || fatjets.at(0).pass(ZTag));
        ntupVar("fatJet1WEfficiency", Jet1WEfficiency);
        ntupVar("fatJet1ZEfficiency", Jet1ZEfficiency);
        ntupVar("fatJet1VEfficiency", Jet1VEfficiency);
	bool isWMass = fatjets.at(0).Pt() < 3000. ? (fatjets.at(0).M() > sqrt(pow((-1.17489e4)/fatjets.at(0).Pt()+(6.34351e1),2)+pow((-2.39185e-2)*fatjets.at(0).Pt()+(6.92862e1),2)) && fatjets.at(0).M() < sqrt(pow((1.59377e4)/fatjets.at(0).Pt()+(-2.90870e1),2)+pow((4.92512e-3)*fatjets.at(0).Pt()+(8.94082e1),2))) : (fatjets.at(0).M() > 59.570001 && fatjets.at(0).M() < 106.86177);
	bool isZMass = fatjets.at(0).Pt() < 3000. ? (fatjets.at(0).M() > sqrt(pow((-1.44222e4)/fatjets.at(0).Pt()+(7.25877e1),2)+pow((-2.75481e-2)*fatjets.at(0).Pt()+(7.87757e1),2)) && fatjets.at(0).M() < sqrt(pow((6.22596e3)/fatjets.at(0).Pt()+(8.27925),2)+pow((7.51060e-3)*fatjets.at(0).Pt()+(9.76893e1),2))) : (fatjets.at(0).M() > 67.890612 && fatjets.at(0).M() < 120.66619);
	ntupVar("fatJet1IsWMass",isWMass);
	ntupVar("fatJet1IsZMass",isZMass);
   }
   if(nfatjets >= 2){
   	ntupVar("fatJet2M", fatjets.at(1).M());
   	ntupVar("fatJet2Pt", fatjets.at(1).Pt());
   	ntupVar("fatJet2Eta", fatjets.at(1).Eta());
   	ntupVar("fatJet2Phi", fatjets.at(1).Phi());
   	ntupVar("fatJet2IsW", fatjets.at(1).pass(WTag));
   	ntupVar("fatJet2IsZ", fatjets.at(1).pass(ZTag));
   	ntupVar("fatJet2IsV", fatjets.at(1).pass(WTag) || fatjets.at(1).pass(ZTag));
        ntupVar("fatJet2WEfficiency", Jet2WEfficiency);
        ntupVar("fatJet2ZEfficiency", Jet2ZEfficiency);
        ntupVar("fatJet2VEfficiency", Jet2VEfficiency);
	bool isWMass = fatjets.at(1).Pt() < 3000. ? (fatjets.at(1).M() > sqrt(pow((-1.17489e4)/fatjets.at(1).Pt()+(6.34351e1),2)+pow((-2.39185e-2)*fatjets.at(1).Pt()+(6.92862e1),2)) && fatjets.at(1).M() < sqrt(pow((1.59377e4)/fatjets.at(1).Pt()+(-2.90870e1),2)+pow((4.92512e-3)*fatjets.at(1).Pt()+(8.94082e1),2))) : (fatjets.at(1).M() > 59.570001 && fatjets.at(1).M() < 106.86177);
	bool isZMass = fatjets.at(1).Pt() < 3000. ? (fatjets.at(1).M() > sqrt(pow((-1.44222e4)/fatjets.at(1).Pt()+(7.25877e1),2)+pow((-2.75481e-2)*fatjets.at(1).Pt()+(7.87757e1),2)) && fatjets.at(1).M() < sqrt(pow((6.22596e3)/fatjets.at(1).Pt()+(8.27925),2)+pow((7.51060e-3)*fatjets.at(1).Pt()+(9.76893e1),2))) : (fatjets.at(1).M() > 67.890612 && fatjets.at(1).M() < 120.66619);
	ntupVar("fatJet2IsWMass",isWMass);
	ntupVar("fatJet2IsZMass",isZMass);
   }
   ntupVar("fatJet1NGATrkB85", fatJet1NGATrkB85);
   ntupVar("fatJet2NGATrkB85", fatJet2NGATrkB85);
   ntupVar("fatJet1M_oneMuCor", fatjet1_2btagged.M());
   ntupVar("fatJet2M_oneMuCor", fatjet2_2btagged.M());
   ntupVar("minDPhi_met_allJets30", minDPhi_met_allJets30);
   ntupVar("L1_minDPhi_met_allJets30", L1_minDPhi_met_allJets30);
   ntupVar("Y1_minDPhi_met_allJets30", Y1_minDPhi_met_allJets30);
   ntupVar("mt2_BB_100", mt2_BB_100);
   ntupVar("L1_mt2_BB_100", L1_mt2_BB_100);
   ntupVar("Y1_mt2_BB_100", Y1_mt2_BB_100);

   ntupVar("mcDSID", event->getMCNumber());
   ntupVar("mcWeights", event->getMCWeights());
   //int runNum = event->getRunNumber();
   //ntupVar("mcRunNumber", runNum);

  /*
  int isZZ = 0;
  int isZh = 0;
  int ishh = 0;
  auto N2N3Zbosons  = event->getHSTruth(0., 999., 23);
  auto N2N3Zhosons  = event->getHSTruth(0., 999., 25);
  if(event->get_boson_pdg1() == 23 && event->get_boson_pdg2() == 23) isZZ = 1;
  else if(event->get_boson_pdg1() == 25 && event->get_boson_pdg2() == 25) ishh = 1;
  else if(!(event->get_boson_pdg1() == 23 || event->get_boson_pdg1() == 25) || !(event->get_boson_pdg2() == 23 || event->get_boson_pdg2() == 25)) std::cout << "Oh! My God!" << std::endl;
  else isZh = 1;
  ntupVar("isZZ",isZZ);
  ntupVar("isZh",isZh);
  ntupVar("ishh",ishh);
  */


  return;
}
