#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <TFile.h>
#include <TH2.h>
#include <TF1.h>

DefineAnalysis(DisappearingTrack2018_TrackletAcc)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This analysis is to compute the tracklet acceptance quoted in Method 2 in the auxillary material.                         //
// This is the geometrical and kinematic acceptance at generator-level, given an event that is in acceptance.                //
// Results are stored in the produced histograms, rather than the typical accept(region) method.                             //
// More explanation is in Section 1.3 of https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/SUSY-2018-19/hepdata_info.pdf //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void DisappearingTrack2018_TrackletAcc::Init() {
    addHistogram("EWKCharginosInAcc",3,0,3);
    addHistogram("StrongCharginosInAcc",3,0,3);

    // const int nEtaBins = 22;
    // double etaBins[nEtaBins+1] = {-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.45,-1.0,-0.55,-0.1,0,0.1,0.55,1.0,1.45,1.9,2.0,2.1,2.2,2.3,2.4,2.5};
    addHistogram("allCharginos",25,-2.5,2.5,20,0,500);
    addHistogram("charginosPassedEvent",25,-2.5,2.5,20,0,500);
    addHistogram("charginoAccPlot",25,-2.5,2.5,20,0,500);

    addHistogram("EWKEventCutflow",5,0,5);// All Events, MET, Jet Pt, minDPhi, lepton veto
    addHistogram("StrongEventCutflow",5,0,5);// All Events, MET, Jet Pt, minDPhi, lepton veto

    addHistogram("EWKTrackletCutflow",6,0,6);// All charginos, Charginos passed Kin, pt, eta, decay rad, minDR(jet)
    addHistogram("StrongTrackletCutflow",6,0,6);// All charginos, Charginos passed Kin, pt, eta, decay rad, minDR(jet)

    addHistogram("decayPos",100,0,1000,50,0,500);

}

void DisappearingTrack2018_TrackletAcc::ProcessEvent(AnalysisEvent *event) {
    // Object Definition - see INT Note Sections 4.2 -> 4.5
    auto met = event->getMET(); // MET as provided by SimpleAnalysis
    double metpt = met.Pt();
    auto jets = event->getJets(20.0, 2.8, NOT(ContainsSUSY)); // Jets containing a SUSY particle are removed - see xAODTruthReader.cxx
    auto electrons = event->getElectrons(10.0, 2.47, ELooseBLLH);
    auto muons = event->getMuons(10.0, 2.7, MuMedium);
    auto caloMuons = filterObjects(muons, 10.0, 2.7, MuCaloTaggedOnly);
    auto notCaloMuons = filterObjects(muons, 10.0, 2.7, NOT(MuCaloTaggedOnly));
    auto weights = event->getMCWeights();

    // Overlap Removal - see INT Note Section 4.7
    caloMuons = overlapRemoval(caloMuons, electrons, 0.05);
    electrons = overlapRemoval(electrons, notCaloMuons, 0.05);
    muons = caloMuons + notCaloMuons;
    jets = overlapRemoval(jets, electrons, 0.2);
    electrons = overlapRemoval(electrons, jets, 0.4);
    jets = overlapRemoval(jets, muons, 0.2, LessThan3Tracks);
    muons = overlapRemoval(muons, jets, 0.4);


    // Chargino
    auto Charginos  = event->getHSTruth(0,5,Chargino1|StablePart);
    if (Charginos.empty()) return;

    sortObjectsByPt(jets);

    for (auto& chargino : Charginos) {
        double charginoEta = chargino.Eta();
        double decayRad = chargino.decayVtx().Perp();
        
        fill("EWKCharginosInAcc",0.5);
        fill("StrongCharginosInAcc",0.5);
        fill("allCharginos",charginoEta, decayRad);
        fill("EWKTrackletCutflow",0.5);
        fill("StrongTrackletCutflow",0.5);
    }

    bool inStrongAcc = true;
    bool inEWKAcc = true;

    // Event sel
    // met
    if (metpt > 250) {
        fill("StrongEventCutflow",1.5);
        fill("EWKEventCutflow",1.5);
    } else if (metpt > 200) {
        fill("EWKEventCutflow",1.5);
        inStrongAcc = false;
    } else return;

    // jet pt
    sortObjectsByPt(jets);
    if (jets.empty()) return; // must have a jet
    if (jets[0].Pt() < 100) return; // leading jet pt requirement
    fill("EWKEventCutflow",2.5);

    if (inStrongAcc) {
        if (jets.size() >= 3) {
            if (jets[1].Pt() < 20) inStrongAcc = false;
            if (jets[2].Pt() < 20) inStrongAcc = false;
        } else inStrongAcc = false;

        if (inStrongAcc) fill("StrongEventCutflow",2.5);
    }

    // DphiJetMET
    double minDphiJetMET = AnalysisClass::minDphi(met, jets, 4, 50.0);

    if (minDphiJetMET < 0.4) return;
    if (minDphiJetMET > 1.0) fill("EWKEventCutflow",3.5);
    else inEWKAcc = false;

    if (inStrongAcc && minDphiJetMET > 0.4) fill("StrongEventCutflow",3.5);
    else inStrongAcc = false;
    

    // Lepton Veto
    if (!electrons.empty() || !muons.empty()) return; // lepton veto
    if (inEWKAcc) fill("EWKEventCutflow",4.5);
    if (inStrongAcc) fill("StrongEventCutflow",4.5);

    // Acceptance requirements
    for (auto chargino : Charginos) {
        double charginoEta = chargino.Eta();
        double absEta = TMath::Abs(charginoEta);
        double decayRad = chargino.decayVtx().Perp();
        double charginoPt = chargino.Pt();

        
        if (inEWKAcc) fill("EWKCharginosInAcc",1.5);
        if (inStrongAcc) fill("StrongCharginosInAcc",1.5);
        fill("charginosPassedEvent",charginoEta, decayRad);
        // fill("cutflow",1.5);
        if (inEWKAcc) fill("EWKTrackletCutflow",1.5);
        if (inStrongAcc) fill("StrongTrackletCutflow",1.5);

        // denominator

        if (charginoPt < 10) continue; 
        // fill("cutflow",2.5);
        if (inEWKAcc) fill("EWKTrackletCutflow",2.5);
        if (inStrongAcc) fill("StrongTrackletCutflow",2.5);
        
        if (absEta < 0.1 || absEta > 1.9) continue;
        // fill("cutflow",3.5);
        if (inEWKAcc) fill("EWKTrackletCutflow",3.5);
        if (inStrongAcc) fill("StrongTrackletCutflow",3.5);

        // if (decayRad < 122.5 || decayRad > 295) continue;
        // fill("cutflow",3.5);

        double decayZ = chargino.decayVtx().Z();
        fill("decayPos",decayZ,decayRad);
        double absDecayZ = TMath::Abs(decayZ);

        // Assumes C1 produced at (0,0,0) - true for truth, not for reco
        double etaSCTBr1 = 1.649; 
        double etaSCTEC1 = 1.658; 
        double etaPixBr1 = 1.9;   

        bool passedDecayRad = true;

        if      (decayRad < 122.5)      passedDecayRad=false;                            // Didn't leave pixel barrel
        else if (absEta < etaSCTBr1)    {if (decayRad    > 299.0) passedDecayRad=false;} // In pixel, rejected by SCT barrel
        else if (absEta < etaSCTEC1)    {if (absDecayZ   > 853.8) passedDecayRad=false;} // In pixel barrel, rejected by SCT EC1
        else if (absEta < etaPixBr1)    {if (absDecayZ   > 934.0) passedDecayRad=false;} // In pixel barrel, rejected by SCT EC2
        else                            passedDecayRad=false;                            // Missed last pixel layer
    
        if (passedDecayRad) {
            // fill("cutflow",4.5);
            if (inEWKAcc) fill("EWKTrackletCutflow",4.5);
            if (inStrongAcc) fill("StrongTrackletCutflow",4.5);
        } else continue;

        double minDphiCharginoJet = AnalysisClass::minDR(chargino,jets,4,50);
        if (minDphiCharginoJet < 0.4) continue;
        // fill("cutflow",5.5);
        if (inEWKAcc) fill("EWKTrackletCutflow",5.5);
        if (inStrongAcc) fill("StrongTrackletCutflow",5.5);

        // numerator
        fill("charginoAccPlot",charginoEta, decayRad);
        if (inEWKAcc) fill("EWKCharginosInAcc",2.5);
        if (inStrongAcc) fill("StrongCharginosInAcc",2.5);
    }

    return;
}