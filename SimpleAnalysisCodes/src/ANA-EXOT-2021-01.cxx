#include "SimpleAnalysisFramework/AnalysisClass.h"
#include "src/ReclusteredHadronicMass.h"

DefineAnalysis(tWPlusMet2018)
    // tW+MET 0/1L channel (Run2 data) [ANA-EXOT-2021-01]
    // Glance entry:
    // https://atlas-glance.cern.ch/atlas/analysis/analyses/details?id=5608
    // @authors : Josep Navarro Gonzalez & Ben Bruers.
    // Reference to previous-round analyses SimpleAnalysis:
    // - tWMET1L :
    // https://gitlab.cern.ch/atlas-phys/exot/hqt/R21_Wt_MET/SimpleAnalysis/blob/unfolding/SimpleAnalysis/Root/tW1LAna.cxx
    // - tWDM:
    // https://gitlab.cern.ch/atlas-phys/exot/hqt/R21_Wt_MET/SimpleAnalysis/-/blob/unfolding/SimpleAnalysis/Root/tWDM.cxx
    // Examples below are stolen from ANA-SUSY-2019-08.cxx

    std::map<int, float> MC_CHANNEL_FILTER_MAX;

void tWPlusMet2018::Init() {
  // DEFINE SIGNAL/CONTROL REGIONS

  addRegions({"preselection"});
  addRegions({"preselection_0L"});
  addRegions({"SR_0L"});
  addRegions({"SR_0L_Bin1"});
  addRegions({"SR_0L_Bin2"});
  addRegions({"SR_0L_Bin3"});
  addRegions({"SR_0L_Bin4"});
  addRegions({"SR_0L_Bin5"});
  addRegions({"SR_0L_1"});
  addRegions({"SR_0L_2"});
  addRegions({"SR_0L_3"});
  addRegions({"SR_0L_4"});
  addRegions({"SR_0L_5"});
  addRegions({"preselection_1L"});
  addRegions({"SR_1L_LeptonicTop"});
  addRegions({"SR_1L_HadronicTop"});
  addRegions({"SR_1L_HadronicTop_Bin1"});
  addRegions({"SR_1L_HadronicTop_Bin2"});
  addRegions({"SR_1L_HadronicTop_Bin3"});
  addRegions({"SR_1L_HadronicTop_Bin4"});
  addRegions({"SR_1L_HadronicTop_Bin5"});
  addRegions({"SR_1L_HadronicTop_1"});
  addRegions({"SR_1L_HadronicTop_2"});
  addRegions({"SR_1L_HadronicTop_3"});
  addRegions({"SR_1L_HadronicTop_4"});
  addRegions({"SR_1L_HadronicTop_5"});

  // Define histograms
  addHistogram("hist_met", 100, 0, 1000); // 1D

  std::vector<float> met0LBins = {250, 330, 400, 500, 600, 1000};
  std::vector<float> met1LBins = {250, 300, 350, 400, 450, 500};

  addHistogram("met_0L", 5, met0LBins.data());     // 1D
  addHistogram("met_1L_lep", 5, met1LBins.data()); // 1D
  addHistogram("met_1L_had", 5, met1LBins.data()); // 1D

  addHistogram("WrecM_pre1L", 120, -1000, 200); // 1D

  addHistogram("h_SumOfWeights", 3, -0.5, 2.5);
  addHistogram("h_SumOfWeights2D", 3, -0.5, 2.5, 1, 0.5, 1.5);

  // DSIDS affected by truth met filter

  int GeV = 1;
  MC_CHANNEL_FILTER_MAX = {
      // channel number, max truth met / ht in GeV
      {410470, 200 * GeV}, // ttbar inclusive
      {410464, 200 * GeV}, // ttbar 1L MgPy8
      {410465, 200 * GeV}, // ttbar 2L MgPy8
      {410557, 200 * GeV}, // ttbar 1L PhHw7
      {410558, 200 * GeV}, // ttbar 2L PhHw7
      {410646, 200 * GeV}, // Wt inclusive
      {410647, 200 * GeV}, // Wt inclusive
      {410654, 200 * GeV}, // WtDS inclusive
      {410655, 200 * GeV}, // WtDS inclusive
  };

  // cross-section is handeled by -x option
}

void tWPlusMet2018::ProcessEvent(AnalysisEvent *event) {
  fillWeighted("h_SumOfWeights2D", 0, 1,
               1); // Filling the raw number of events in the first bin; have to
                   // use 2D weighted filling function with yBin = 0
  fillWeighted(
      "h_SumOfWeights2D", 1, 1,
      event->getMCWeights()[0]); // Filling the weighted sum of events in the
                                 // second bin; have to use 2D weighted filling
                                 // function with yBin = 0
  fillWeighted(
      "h_SumOfWeights2D", 2, 1,
      event->getMCWeights()[0] *
          event->getMCWeights()[0]); // Filling the sum of squared event weights
                                     // into the third bin
  fill("h_SumOfWeights",
       event->getMCWeights()[0]); // Filling the weighted number of events in
                                  // the second bin

  int mcChannel = event->getMCNumber();

  // DEFINE BASE OBJECTS

  auto baseJets = event->getJets(20., 4.5);
  auto basePhoton = event->getPhotons(25., 2.37, PhotonTight);
  auto baseEle = event->getElectrons(4.5, 2.47, ELooseBLLH | EZ05mm);
  auto baseMuon = event->getMuons(4, 2.70, MuMedium | MuZ05mm);
  auto largeRJets = event->getFatJets(20., 2.5);

  // OVERLAP REMOVAL
  // auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject&
  // ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  // ele-mu
  baseEle = overlapRemoval(baseEle, baseMuon, 0.01);
  // ele-jet
  baseJets = overlapRemoval(baseJets, baseEle, 0.2);
  baseEle = overlapRemoval(baseEle, baseJets, 0.4);
  // mu-jet
  baseJets = overlapRemoval(baseJets, baseMuon, 0.2,
                            NOT(BTag77MV2c10) | LessThan3Tracks);
  baseMuon = overlapRemoval(baseMuon, baseJets, 0.4);
  largeRJets = overlapRemoval(largeRJets, baseEle, 1.0);

  auto baseLept = baseEle + baseMuon;

  // DEFINE SIGNAL OBJECTS
  auto signalJets = filterObjects(baseJets, 30., 2.5, PFlowJet);
  auto signalBJets = filterObjects(signalJets, 30., 2.5, BTag77DL1r);
  auto trackJets = event->getTrackJets(20., 2.5, BTag77DL1r);
  auto HSJets = event->getHSTruth(30., 2.5);
  auto HSWJets = event->getHSTruth(30, 2.32, 24);
  auto HSWJetsAll = event->getHSTruth(0, 20, 24);

  //  for(const auto &p: HSJets){
  //    std::cout << p.status() << std::endl;
  //  }

  auto signalPhoton = filterObjects(basePhoton, 130., 2.37,
                                    PhotonTight | PhotonIsoFixedCutTight);
  auto signalTau = event->getTaus(20., 2.5, TauMedium);
  auto signalEle = filterObjects(baseEle, 4.5, 2.47,
                                 EMediumLH | ED0Sigma5 | EZ05mm | EIsoFCLoose);
  AnalysisObjects signalEleLowPt;
  for (const auto &ele : signalEle) {
    if ((ele.Pt() < 200.))
      signalEleLowPt.push_back(ele);
  }
  auto signalEleHighPt =
      filterObjects(baseEle, 200., 2.47,
                    ETightLH | ED0Sigma5 | EZ05mm | MuIsoPflowLoose_VarRad);
  signalEle = signalEleLowPt + signalEleHighPt;
  auto signalMuon =
      filterObjects(baseMuon, 4., 2.7, MuD0Sigma3 | MuZ05mm | MuIsoFCLoose);
  auto signalLept = signalEle + signalMuon;

  // DEFINE VARIABLES
  auto met_Vect = event->getMET();
  float met = met_Vect.Pt();
  float MET_phi = met_Vect.Phi();

  // can get generator MET and HT (if available) to handle filters
  float generatorMET = event->getGenMET();
  float generatorHT = event->getGenHT();

  // truth MET filter for top samples
  bool PassTruthMetFilter = true;

  // if a filter check has to be done (i.e. the mcChannel is in the
  // MC_CHANNEL_FILTER_MAX map):
  if (MC_CHANNEL_FILTER_MAX.find(mcChannel) != MC_CHANNEL_FILTER_MAX.end()) {
    float max = MC_CHANNEL_FILTER_MAX.at(mcChannel);
    PassTruthMetFilter = generatorMET <= max;
  }

  AnalysisObjects signalWJets;
  float thresholdRadius = 0.75;

  float minDR_WlJ = -999.;
  bool decaysHadronically;
  // std::cout << "New event" << std::endl;

  float matchedW_pt = -999;
  float matchedW_phi = -999;
  float matchedW_eta = -999;
  float matchedW_m = -999;
  float matchedW_fromTop = -999;
  float matchedW_decaysHadronically = -999;

  if (largeRJets.size() > 0 && HSWJets.size() > 0) {
    for (const auto &ljet : largeRJets) {
      minDR_WlJ = minDR(ljet, HSWJets);
      decaysHadronically = true;
      if (minDR_WlJ < thresholdRadius) { // radius association to truth-W
        // check if associated W decays hadronically
        // std::cout << "New large-R jet" << std::endl;
        for (const auto &wBoson : HSWJets) {
          if (std::abs(ljet.DeltaR(wBoson) - minDR_WlJ) <
              1e-5) { // found the partner
            // std::cout << "Found partner! Observed dR: " <<
            // ljet.DeltaR(wBoson) << ", expected dR: " << minDR_WlJ <<
            // std::endl;
            if (wBoson.id() & (1 << 30)) {
              decaysHadronically = false;
              // std::cout << "Wboson decays leptonically" << std::endl;
            }
            // std::cout << wBoson.motherID() << std::endl;
            matchedW_pt = wBoson.Pt();
            matchedW_phi = wBoson.Phi();
            matchedW_eta = wBoson.Eta();
            matchedW_m = wBoson.M();
            matchedW_decaysHadronically = decaysHadronically;
            matchedW_fromTop = std::abs(wBoson.motherID()) == 37 ? 0 : 1;
            break;
          }
        }
        if (decaysHadronically && ljet.M() < 100 and
            ljet.M() > 50) { // mass requirement
          if (ljet.id() == GoodJet and ljet.Pt() > 200 and
              abs(ljet.Eta()) < 2.) {
            signalWJets.push_back(ljet); // ghost associated b-jets requirement
          }
        }
      }
    }
  }

  float dR_Wb = -999.;
  float m_Wb = -999.;
  if (signalWJets.size() > 0 && signalBJets.size() > 0) {
    dR_Wb = (signalWJets[0]).DeltaR(signalBJets[0]);
    m_Wb = (signalWJets[0] + signalBJets[0]).M();
  }

  float dRj1j2 = -999;
  if (signalJets.size() > 1) {
    dRj1j2 = (signalJets[0]).DeltaR(signalJets[1]);
  }

  unsigned int N_baseJets = baseJets.size();
  unsigned int N_baseEle = baseEle.size();
  unsigned int N_baseMuon = baseMuon.size();
  unsigned int N_baseLept = N_baseEle + N_baseMuon;

  unsigned int N_signalEle = signalEle.size();
  unsigned int N_signalMuon = signalMuon.size();
  unsigned int N_signalLept = N_signalEle + N_signalMuon;
  unsigned int N_signalJets = signalJets.size();
  unsigned int N_signalBJets = signalBJets.size();
  int N_signalWJets = signalWJets.size();

  AnalysisObject nothing = AnalysisObject(0., 0., 0., 0., 0, 0, NONE, 0, 0);

  // object attributes
  bool isSFOS = false;
  float mll = -99;
  float pT_ll = -99;
  float mt2 = -99;
  float min_mbl = -99;
  AnalysisObject met_Vect_corr = met_Vect;
  float met_corr = -999;
  float met_phi_corr = -999;
  float dPhimin_4_corr = -999;
  float mTb1METCorr = -999;
  float METSigCorr = -999;

  AnalysisObjects noObjects;
  if (signalLept.size() > 1) {
    isSFOS = IsSFOS(signalLept[0], signalLept[1]);
    mll = (signalLept[0] + signalLept[1]).M();
    pT_ll = (signalLept[0] + signalLept[1]).Pt();
    mt2 = calcMT2(signalLept[0], signalLept[1], met_Vect);
    if (signalBJets.size() > 0) {
      min_mbl = std::min((signalBJets[0] + signalLept[0]).M(),
                         (signalBJets[0] + signalLept[1]).M());
    }

    met_Vect_corr = met_Vect + signalLept[0] + signalLept[1];
    met_corr = met_Vect_corr.Pt();
    met_phi_corr = met_Vect_corr.Phi();
    dPhimin_4_corr = minDphi(met_Vect_corr, signalJets, 4);

    if (N_signalBJets > 0)
      mTb1METCorr = calcMT(signalBJets[0], met_Vect_corr);

    AnalysisObjects baseLeptNotLeading(baseLept.begin() + 2, baseLept.end());
    METSigCorr =
        event->getMETSignificance(baseLeptNotLeading, noObjects, noObjects,
                                  baseJets, noObjects, met_Vect_corr);
  }

  float dPhiMinAll = minDphi(met_Vect, signalJets);
  float dPhiMin4 = minDphi(met_Vect, signalJets, 4);
  float METSig = event->getMETSignificance(baseLept, noObjects, noObjects,
                                           baseJets, noObjects, met_Vect);
  float mTb1MET = -999.;
  if (N_signalBJets > 0) {
    mTb1MET = calcMT(signalBJets[0], met_Vect);
  }
  //{mTb1min = calcMTmin(signalBJets, met_Vect, 3);}  // Minimum MT of leading 3
  // b-signalJets

  // OTHER DEFINITIONS

  auto weights = event->getMCWeights();
  auto eventWeight = event->getMCWeights()[0];

  float mT = -999;
  if (N_signalLept > 0) {
    mT = 2 * signalLept[0].Pt() * met *
         (1 - TMath::Cos(signalLept[0].Phi() - MET_phi));
    mT = (mT >= 0.) ? sqrt(mT) : sqrt(-mT);
  }

  float mb1l = 999;
  if (N_signalBJets > 0 && N_signalLept > 0) {
    mb1l = (signalBJets[0] + signalLept[0]).M();
  }

  float mb1j1 = 9999;
  if (N_signalBJets > 0 && N_signalJets > 0) {
    for (unsigned int j = 0; j < N_signalJets; j++) {
      AnalysisObject current = signalJets[j];
      if (current.pass(NOT(BTag77DL1r))) {
        mb1j1 = (signalBJets[0] + current).M();
        break;
      }
    }
  }

  // amT2 calculation - it gives problems if defined externally
  float amt2 = -999;
  if (N_signalJets > 1 && N_signalLept > 0) {

    AnalysisObject jet1B = AnalysisObject(0., 0., 0., 0., 0, 0, NONE, 0, 0);
    AnalysisObject jet2B = AnalysisObject(0., 0., 0., 0., 0, 0, NONE, 0, 0);
    int id_j1(-1), id_j2(-1);

    // Normally you would use the jets with the highest, b-weight, but this is
    // not in SimpleAnalysis. Therefore, pick the two jets that pass the
    // tightest b-taggers. If no two jets are found, just fill them up with the
    // highest pT jets.
    for (unsigned int j = 0; j < N_signalJets; j++) {
      AnalysisObject current = signalJets[j];
      if (current.pass(BTag60DL1r)) {
        jet1B = current;
        id_j1 = j;
        break;
      }
    }

    if (id_j1 == -1) {
      for (unsigned int j = 0; j < N_signalJets; j++) {
        AnalysisObject current = signalJets[j];
        if (current.pass(BTag70DL1r)) {
          jet1B = current;
          id_j1 = j;
          break;
        }
      }
    }

    if (id_j1 == -1) {
      for (unsigned int j = 0; j < N_signalJets; j++) {
        AnalysisObject current = signalJets[j];
        if (current.pass(BTag77DL1r)) {
          jet1B = current;
          id_j1 = j;
          break;
        }
      }
    }

    if (id_j1 == -1) {
      for (unsigned int j = 0; j < N_signalJets; j++) {
        AnalysisObject current = signalJets[j];
        if (current.pass(BTag85DL1r)) {
          jet1B = current;
          id_j1 = j;
          break;
        }
      }
    }

    if (id_j1 == -1) {
      jet1B = signalJets[0];
      jet2B = signalJets[1];
    }

    if (id_j1 != -1) { // if I finally managed to find an index for the first b,
                       // look for the second b
      for (unsigned int j = 0; j < N_signalJets; j++) {
        AnalysisObject current = signalJets[j];
        if (id_j1 == (int)j)
          continue;
        if (current.pass(BTag60DL1r)) {
          jet2B = current;
          id_j2 = j;
          break;
        }
      }

      if (id_j2 == -1) {
        for (unsigned int j = 0; j < N_signalJets; j++) {
          AnalysisObject current = signalJets[j];
          if (id_j1 == (int)j)
            continue;
          if (current.pass(BTag70DL1r)) {
            jet2B = current;
            id_j2 = j;
            break;
          }
        }
      }

      if (id_j2 == -1) {
        for (unsigned int j = 0; j < N_signalJets; j++) {
          AnalysisObject current = signalJets[j];
          if (id_j1 == (int)j)
            continue;
          if (current.pass(BTag77DL1r)) {
            jet2B = current;
            id_j2 = j;
            break;
          }
        }
      }

      if (id_j2 == -1) {
        for (unsigned int j = 0; j < N_signalJets; j++) {
          AnalysisObject current = signalJets[j];
          if (id_j1 == (int)j)
            continue;
          if (current.pass(BTag85DL1r)) {
            jet2B = current;
            id_j2 = j;
            break;
          }
        }
      }

      if (id_j2 == -1) {
        for (unsigned int j = 0; j < N_signalJets; j++) {
          AnalysisObject current = signalJets[j];
          if (id_j1 == (int)j)
            continue;
          jet2B = current;
          id_j2 = j;
          break;
        }
      }
    }

    float mT2a = calcAMT2(jet1B + signalLept[0], jet2B, met_Vect, 0., 80.4);
    float mT2b = calcAMT2(jet2B + signalLept[0], jet1B, met_Vect, 0., 80.4);
    amt2 = std::min(mT2a, mT2b);
  }
  // end of amT2

  // Calculate reclustered W variables from hadronic decay products
  float reclusteredW_Mass = -999.;
  float reclusteredW_Theta = -999.;
  float reclusteredW_Eta = -999.;
  float reclusteredW_Phi = -999.;
  float reclusteredW_Pt = -999.;
  ReclusteredHadronicMass reclustering;
  std::pair<double, TLorentzVector> WCand =
      reclustering.applyReclustering(signalJets, 80);
  reclusteredW_Mass = std::get<1>(WCand).M();
  reclusteredW_Theta = std::get<1>(WCand).Theta();
  reclusteredW_Eta = std::get<1>(WCand).Eta();
  reclusteredW_Phi = std::get<1>(WCand).Phi();
  reclusteredW_Pt = std::get<1>(WCand).Pt();

  // Skimming cuts
  if ((N_baseLept >= 3) ||
      (N_signalLept == 0 && met >= 250 && N_signalBJets >= 1 &&
       signalBJets[0].Pt() >= 50 && dPhiMin4 >= 0.5) ||
      (N_signalLept >= 1 && met >= 250 && mT >= 30 && N_signalBJets >= 1 &&
       signalBJets[0].Pt() >= 50 && dPhiMin4 >= 0.5) ||
      (N_signalLept == 2 && N_signalBJets >= 1 && signalBJets[0].Pt() >= 50 &&
       (mll > 75 && mll < 105) &&
       met_corr >= 250)) { // accepting event in general preselection
  } else {
    return;
  }

  // ACCEPT REGIONS
  accept("preselection");

  if (met > 250 && N_signalBJets >= 1 && dPhiMin4 >= 0.5 &&
      signalBJets[0].Pt() >= 50. &&
      ((N_signalBJets >= 2 && signalBJets[1].Pt() < 50.) ||
       N_signalBJets == 1)) {
    if (N_baseLept == 0 && N_signalLept == 0) {
      if (N_baseJets >= 4 && N_signalJets >= 4 && signalJets[0].Pt() >= 100 &&
          signalJets[1].Pt() >= 60 && signalJets[2].Pt() >= 60 &&
          signalJets[3].Pt() >= 40) {
        accept("preselection_0L");
        if (dPhiMin4 >= 0.9 && mTb1MET >= 180 && METSig >= 14 &&
            N_signalWJets >= 1 && dR_Wb >= 1 && m_Wb >= 220) {
          accept("SR_0L");
          fill("met_0L", met); // fill 1D histogram
          if (met > 250) {
            accept("SR_0L_1");
            if (met < 330) {
              accept("SR_0L_Bin1");
            }
          }
          if (met > 330) {
            accept("SR_0L_2");
            if (met < 400) {
              accept("SR_0L_Bin2");
            }
          }
          if (met > 400) {
            accept("SR_0L_3");
            if (met < 500) {
              accept("SR_0L_Bin3");
            }
          }
          if (met > 500) {
            accept("SR_0L_4");
            if (met < 600) {
              accept("SR_0L_Bin4");
            }
          }
          if (met > 600) {
            accept("SR_0L_5");
            accept("SR_0L_Bin5");
          }
        }
      }
    } else if (N_baseLept == 1 && N_signalLept == 1 &&
               signalLept[0].Pt() >= 30) {
      if (N_baseJets >= 2 && N_signalJets >= 2 && signalJets[0].Pt() >= 30 &&
          signalJets[1].Pt() >= 30 && mT >= 30) {
        accept("preselection_1L");
        if ((mT >= 130 && amt2 >= 180 && mb1j1 >= 200 && N_signalWJets >= 1 &&
             METSig >= 15) == 0) {
          fill("WrecM_pre1L", reclusteredW_Mass);
        }
        if (mT >= 130 && amt2 >= 180) {
          if (mb1j1 >= 200 && N_signalWJets >= 1 && METSig >= 15) {
            accept("SR_1L_LeptonicTop");
            fill("met_1L_lep", met); // fill 1D histogram
          } else if (mb1j1 <= 200 && mT >= 200 && reclusteredW_Mass >= 60 &&
                     N_signalJets >= 3) {
            accept("SR_1L_HadronicTop");
            fill("met_1L_had", met); // fill 1D histogram
            if (met > 250) {
              accept("SR_1L_HadronicTop_1");
              if (met < 300) {
                accept("SR_1L_HadronicTop_Bin1");
              }
            }
            if (met > 300) {
              accept("SR_1L_HadronicTop_2");
              if (met < 350) {
                accept("SR_1L_HadronicTop_Bin2");
              }
            }
            if (met > 350) {
              accept("SR_1L_HadronicTop_3");
              if (met < 400) {
                accept("SR_1L_HadronicTop_Bin3");
              }
            }
            if (met > 400) {
              accept("SR_1L_HadronicTop_4");
              if (met < 450) {
                accept("SR_1L_HadronicTop_Bin4");
              }
            }
            if (met > 450) {
              accept("SR_1L_HadronicTop_5");
              accept("SR_1L_HadronicTop_Bin5");
            }
          }
        }
      }
    }
  }

  // FILL HISTOGRAMS
  fill("hist_met", met); // fill 1D histogram

  // OUTPUT VARIABLES:
  // for both channels
  //
  // general lep / bjet / jet properties
  for (unsigned int i = 0; i < 6; i++) {
    std::string myJetLabel = "jet";
    std::string thisJetLabel = myJetLabel + std::to_string(i + 1);
    if (signalJets.size() > i)
      ntupVar(thisJetLabel, signalJets[i], true, true);
    else
      ntupVar(thisJetLabel, nothing, true, true);
  }

  for (unsigned int i = 0; i < 4; i++) {
    std::string myJetLabel = "bjet";
    std::string thisJetLabel = myJetLabel + std::to_string(i + 1);
    if (signalBJets.size() > i)
      ntupVar(thisJetLabel, signalBJets[i], true, true);
    else
      ntupVar(thisJetLabel, nothing, true, true);
  }

  AnalysisObject nothingFat =
      AnalysisObject(0., 0., 0., 0., 0, 0, FATJET, 0, 0);
  for (unsigned int i = 0; i < 4; i++) {
    std::string myJetLabel = "fatJet";
    std::string thisJetLabel = myJetLabel + std::to_string(i + 1);
    if (largeRJets.size() > i)
      ntupVar(thisJetLabel, largeRJets[i], true, true);
    else
      ntupVar(thisJetLabel, nothingFat, true, true);
  }

  // for testing
  // ntupVar("fatJetsVec", largeRJets, true, true);
  // ntupVar("WTaggedJetsVec", signalWJets, true, true);
  // ntupVar("leptonsVec", signalLept, true, true);

  for (unsigned int i = 0; i < 4; i++) {
    std::string myJetLabel = "WTaggedJets";
    std::string thisJetLabel = myJetLabel + std::to_string(i + 1);
    if (signalWJets.size() > i)
      ntupVar(thisJetLabel, signalWJets[i], true, true);
    else
      ntupVar(thisJetLabel, nothingFat, true, true);
  }

  for (unsigned int i = 0; i < 4; i++) {
    std::string myBosonLabel = "WBoson";
    std::string thisBosonLabel = myBosonLabel + std::to_string(i + 1);
    std::string thisBosonDecayLabel =
        myBosonLabel + std::to_string(i + 1) + "_decaysLeptonically";
    if (HSWJetsAll.size() > i) {
      ntupVar(thisBosonLabel, HSWJetsAll[i], true, true);
      ntupVar(thisBosonDecayLabel, (HSWJetsAll[i].id() & (1 << 30)) > 0);
    } else {
      ntupVar(thisBosonLabel, nothing, true, true);
      ntupVar(thisBosonDecayLabel, -1);
    }
  }

  int lep1_is_electron = -2;
  int lep2_is_electron = -3;

  for (unsigned int i = 0; i < 4; i++) {
    std::string myLepLabel = "lep";
    std::string thisLepLabel = myLepLabel + std::to_string(i + 1);
    std::string thisLepTypeLabel =
        myLepLabel + std::to_string(i + 1) + "_is_electron";
    if (signalLept.size() > i) {
      ntupVar(thisLepLabel, signalLept[i], true, true);
      if (((std::find(signalEle.begin(), signalEle.end(), signalLept[i]) !=
            signalEle.end()))) {
        ntupVar(thisLepTypeLabel, 1);
        if (i == 0)
          lep1_is_electron = 1;
        if (i == 1)
          lep2_is_electron = 1;
      } else if (((std::find(signalMuon.begin(), signalMuon.end(),
                             signalLept[i]) != signalMuon.end()))) {
        ntupVar(thisLepTypeLabel, 2);
        if (i == 0)
          lep1_is_electron = 2;
        if (i == 1)
          lep2_is_electron = 2;
      } else {
        ntupVar(thisLepTypeLabel, 0);
        if (i == 0)
          lep1_is_electron = 0;
        if (i == 1)
          lep2_is_electron = 0;
      }
    } else {
      ntupVar(thisLepLabel, nothing, true, true);
      ntupVar(thisLepTypeLabel, 0);
    }
  }

  // more unique variables
  ntupVar("mcChannelNumber", mcChannel);
  ntupVar("PassTruthMetFilter", PassTruthMetFilter);
  ntupVar("n_lep_signal", (int)N_signalLept);
  ntupVar("n_lep", (int)N_baseLept);
  ntupVar("n_lep_veto", (int)(N_baseLept - N_signalLept));
  ntupVar("n_base_jet", (int)N_baseJets);
  ntupVar("n_jet", (int)N_signalJets);
  ntupVar("n_bjet", (int)N_signalBJets);
  ntupVar("lep1_is_electron", lep1_is_electron);
  ntupVar("lep2_is_electron", lep2_is_electron);
  ntupVar("met", met);
  ntupVar("met_phi", MET_phi);
  ntupVar("dPhimin_4", dPhiMin4);
  ntupVar("dPhimin_all", dPhiMinAll);
  ntupVar("DR_j1_j2", dRj1j2);
  ntupVar("MET_Significance", METSig);
  ntupVar("n_fatJet_WTagged", (int)N_signalWJets);
  ntupVar("GenFiltMET", generatorMET);
  ntupVar("GenFiltHT", generatorHT);

  ntupVar("weightsVec", weights);
  ntupVar("WeightEvents", eventWeight);

  ntupVar("matchedW_pt", matchedW_pt);
  ntupVar("matchedW_phi", matchedW_phi);
  ntupVar("matchedW_eta", matchedW_eta);
  ntupVar("matchedW_m", matchedW_m);
  ntupVar("matchedW_fromTop", matchedW_fromTop);
  ntupVar("matchedW_decaysHadronically", matchedW_decaysHadronically);

  // 0L vars
  ntupVar("mT_b1_MET", mTb1MET);
  ntupVar("DR_WTagged_b1", dR_Wb);
  ntupVar("m_WTagged_b1", m_Wb);
  ntupVar("minDR_WlJ", minDR_WlJ);

  // 1L Vars
  ntupVar("mt", mT);
  ntupVar("amt2", amt2);
  ntupVar("mb1l", mb1l);
  ntupVar("mb1j1", mb1j1);
  ntupVar("ReclusteredW_Mass", reclusteredW_Mass);
  ntupVar("ReclusteredW_Phi", reclusteredW_Phi);
  ntupVar("ReclusteredW_Pt", reclusteredW_Pt);
  ntupVar("ReclusteredW_Eta", reclusteredW_Eta);
  ntupVar("ReclusteredW_Theta", reclusteredW_Theta);

  // 2L Vars
  ntupVar("min_mbl", min_mbl);
  ntupVar("mt2", mt2);
  ntupVar("mll", mll);
  ntupVar("pT_ll", pT_ll);
  ntupVar("isSFOS", isSFOS);
  ntupVar("met_corr", met_corr);
  ntupVar("met_phi_corr", met_phi_corr);
  ntupVar("dPhimin_4_corr", dPhimin_4_corr);
  ntupVar("mT_b1_MET_corr", mTb1METCorr);
  ntupVar("met_sig_corr", METSigCorr);

  return;
}
