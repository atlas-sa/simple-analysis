#include "SimpleAnalysisFramework/AnalysisClass.h"
#include "MVAUtils/BDT.h"
#include "PathResolver/PathResolver.h"

DefineAnalysis(EwkMultiBjets2021)

static float CalcXwt(const AnalysisObjects &bjets,
                     const AnalysisObjects &nonbjets);
static std::pair<std::pair<double, double>, std::pair<double, double>>
  pair_h_h(const AnalysisObjects &bjets, unsigned int mode);
static std::pair<std::pair<double, double>, std::pair<double, double>> 
  heavy_pair_h_h(const AnalysisObject &heavy_b, const AnalysisObject &bjet1,
                 const AnalysisObject &bjet2);

void EwkMultiBjets2021::Init()
{
  addRegions({"NoHiggs","OneHiggs","TwoHiggs"});
  addRegions({"Presel_LM","Presel_0L","Presel_ZCR"});
  addRegions({"Presel_0L_2Higgs","Presel_0L_1Higgs","Presel_0L_0Higgs"});
  addRegions({"Presel_ZCR_2Higgs","Presel_ZCR_1Higgs","Presel_ZCR_0Higgs"});
  addRegions({"SR_low_allbins"});
  addRegions({"SR_low_bin1","SR_low_bin2","SR_low_bin3","SR_low_bin4","SR_low_bin5","SR_low_bin6","SR_low_bin7","SR_low_bin8","SR_low_bin9","SR_low_bin10","SR_low_bin11","SR_low_bin12","SR_low_bin13","SR_low_bin14","SR_low_bin15","SR_low_bin16","SR_low_bin17","SR_low_bin18","SR_low_bin19","SR_low_bin20","SR_low_bin21","SR_low_bin22","SR_low_bin23","SR_low_bin24","SR_low_bin25","SR_low_bin26","SR_low_bin27","SR_low_bin28","SR_low_bin29","SR_low_bin30","SR_low_bin31","SR_low_bin32","SR_low_bin33","SR_low_bin34","SR_low_bin35","SR_low_bin36","SR_low_bin37","SR_low_bin38","SR_low_bin39","SR_low_bin40","SR_low_bin41","SR_low_bin42","SR_low_bin43","SR_low_bin44","SR_low_bin45","SR_low_bin46","SR_low_bin47","SR_low_bin48","SR_low_bin49","SR_low_bin50","SR_low_bin51","SR_low_bin52","SR_low_bin53","SR_low_bin54","SR_low_bin55","SR_low_bin56","SR_low_bin57","SR_low_bin58","SR_low_bin59","SR_low_bin60","SR_low_bin61","SR_low_bin62","SR_low_bin63","SR_low_bin64","SR_low_bin65","SR_low_bin66","SR_low_bin67","SR_low_bin68","SR_low_bin69","SR_low_bin70","SR_low_bin71","SR_low_bin72","SR_low_bin73","SR_low_bin74","SR_low_bin75","SR_low_bin76","SR_low_bin77","SR_low_bin78","SR_low_bin79","SR_low_bin80","SR_low_bin81","SR_low_bin82","SR_low_bin83","SR_low_bin84","SR_low_bin85","SR_low_bin86","SR_low_bin87","SR_low_bin88"});
  addRegions({"SR_low_150","SR_low_300"});
  // HM
  addRegions({"SR_1_200","SR_2_200","SR_3_200"});
  addRegions({"SR_1_250","SR_2_250","SR_3_250","SR_4_250"});
  addRegions({"SR_1_300","SR_2_300","SR_3_300","SR_4_300"});
  addRegions({"SR_1_400","SR_2_400","SR_3_400","SR_4_400"});
  addRegions({"SR_1_500","SR_2_500","SR_3_500"});
  addRegions({"SR_1_600","SR_2_600","SR_3_600"});
  addRegions({"SR_1_700","SR_2_700","SR_3_700"});
  addRegions({"SR_1_800","SR_2_800"});
  addRegions({"SR_1_900","SR_2_900"});
  addRegion({"SR_1_1000"});
  addRegion({"SR_1_1100"});
  // Need each hh_type for acceptances
  addRegions({"SR_1_200_2Higgs","SR_2_200_2Higgs","SR_3_200_2Higgs"});
  addRegions({"SR_1_250_2Higgs","SR_2_250_2Higgs","SR_3_250_2Higgs","SR_4_250_2Higgs"});
  addRegions({"SR_1_300_2Higgs","SR_2_300_2Higgs","SR_3_300_2Higgs","SR_4_300_2Higgs"});
  addRegions({"SR_1_400_2Higgs","SR_2_400_2Higgs","SR_3_400_2Higgs","SR_4_400_2Higgs"});
  addRegions({"SR_1_500_2Higgs","SR_2_500_2Higgs","SR_3_500_2Higgs"});
  addRegions({"SR_1_600_2Higgs","SR_2_600_2Higgs","SR_3_600_2Higgs"});
  addRegions({"SR_1_700_2Higgs","SR_2_700_2Higgs","SR_3_700_2Higgs"});
  addRegions({"SR_1_800_2Higgs","SR_2_800_2Higgs"});
  addRegions({"SR_1_900_2Higgs","SR_2_900_2Higgs"});
  addRegion({"SR_1_1000_2Higgs"});
  addRegion({"SR_1_1100_2Higgs"});
  addRegions({"SR_1_200_1Higgs","SR_2_200_1Higgs","SR_3_200_1Higgs"});
  addRegions({"SR_1_250_1Higgs","SR_2_250_1Higgs","SR_3_250_1Higgs","SR_4_250_1Higgs"});
  addRegions({"SR_1_300_1Higgs","SR_2_300_1Higgs","SR_3_300_1Higgs","SR_4_300_1Higgs"});
  addRegions({"SR_1_400_1Higgs","SR_2_400_1Higgs","SR_3_400_1Higgs","SR_4_400_1Higgs"});
  addRegions({"SR_1_500_1Higgs","SR_2_500_1Higgs","SR_3_500_1Higgs"});
  addRegions({"SR_1_600_1Higgs","SR_2_600_1Higgs","SR_3_600_1Higgs"});
  addRegions({"SR_1_700_1Higgs","SR_2_700_1Higgs","SR_3_700_1Higgs"});
  addRegions({"SR_1_800_1Higgs","SR_2_800_1Higgs"});
  addRegions({"SR_1_900_1Higgs","SR_2_900_1Higgs"});
  addRegions({"SR_1_1000_1Higgs"});
  addRegions({"SR_1_1100_1Higgs"});

  addRegions({"SR_1_200_0Higgs","SR_2_200_0Higgs","SR_3_200_0Higgs"});
  addRegions({"SR_1_250_0Higgs","SR_2_250_0Higgs","SR_3_250_0Higgs","SR_4_250_0Higgs"});
  addRegions({"SR_1_300_0Higgs","SR_2_300_0Higgs","SR_3_300_0Higgs","SR_4_300_0Higgs"});
  addRegions({"SR_1_400_0Higgs","SR_2_400_0Higgs","SR_3_400_0Higgs","SR_4_400_0Higgs"});
  addRegions({"SR_1_500_0Higgs","SR_2_500_0Higgs","SR_3_500_0Higgs"});
  addRegions({"SR_1_600_0Higgs","SR_2_600_0Higgs","SR_3_600_0Higgs"});
  addRegions({"SR_1_700_0Higgs","SR_2_700_0Higgs","SR_3_700_0Higgs"});
  addRegions({"SR_1_800_0Higgs","SR_2_800_0Higgs"});
  addRegions({"SR_1_900_0Higgs","SR_2_900_0Higgs"});
  addRegions({"SR_1_1000_0Higgs"});
  addRegions({"SR_1_1100_0Higgs"});

  addRegions({"CR_st_200","CR_tt4b_200","CR_tt3b_200","VR_st_200","VR_tt_200","VR_Z_200","CR_Z_200"});
  addRegions({"CR_st_250","CR_tt4b_250","CR_tt3b_250","VR_st_250","VR_tt_250","VR_Z_250","CR_Z_250"});
  addRegions({"CR_st_300","CR_tt4b_300","CR_tt3b_300","VR_st_300","VR_tt_300","VR_Z_300","CR_Z_300"});
  addRegions({"CR_st_400","CR_tt4b_400","CR_tt3b_400","VR_st_400","VR_tt_400","VR_Z_400","CR_Z_400"});
  addRegions({"CR_st_500","CR_tt4b_500","CR_tt3b_500","VR_st_500","VR_tt_500","VR_Z_500","CR_Z_500"});
  addRegions({"CR_st_600","CR_tt4b_600","CR_tt3b_600","VR_st_600","VR_tt_600","VR_Z_600","CR_Z_600"});
  addRegions({"CR_st_700","CR_tt4b_700","CR_tt3b_700","VR_st_700","VR_tt_700","VR_Z_700","CR_Z_700"});
  addRegions({"CR_st_800","CR_tt4b_800","CR_tt3b_800","VR_st_800","VR_tt_800","VR_Z_800","CR_Z_800"});
  addRegions({"CR_st_900","CR_tt4b_900","CR_tt3b_900","VR_st_900","VR_tt_900","VR_Z_900","CR_Z_900"});
  addRegions({"CR_st_1000","CR_tt4b_1000","CR_tt3b_1000","VR_st_1000","VR_tt_1000","VR_Z_1000","CR_Z_1000"});
  addRegions({"CR_st_1100","CR_tt4b_1100","CR_tt3b_1100","VR_st_1100","VR_tt_1100","VR_Z_1100","CR_Z_1100"});

  addRegions({"CR_st_200_2Higgs","CR_tt4b_200_2Higgs","CR_tt3b_200_2Higgs","VR_st_200_2Higgs","VR_tt_200_2Higgs","VR_Z_200_2Higgs","CR_Z_200_2Higgs"});
  addRegions({"CR_st_250_2Higgs","CR_tt4b_250_2Higgs","CR_tt3b_250_2Higgs","VR_st_250_2Higgs","VR_tt_250_2Higgs","VR_Z_250_2Higgs","CR_Z_250_2Higgs"});
  addRegions({"CR_st_300_2Higgs","CR_tt4b_300_2Higgs","CR_tt3b_300_2Higgs","VR_st_300_2Higgs","VR_tt_300_2Higgs","VR_Z_300_2Higgs","CR_Z_300_2Higgs"});
  addRegions({"CR_st_400_2Higgs","CR_tt4b_400_2Higgs","CR_tt3b_400_2Higgs","VR_st_400_2Higgs","VR_tt_400_2Higgs","VR_Z_400_2Higgs","CR_Z_400_2Higgs"});
  addRegions({"CR_st_500_2Higgs","CR_tt4b_500_2Higgs","CR_tt3b_500_2Higgs","VR_st_500_2Higgs","VR_tt_500_2Higgs","VR_Z_500_2Higgs","CR_Z_500_2Higgs"});
  addRegions({"CR_st_600_2Higgs","CR_tt4b_600_2Higgs","CR_tt3b_600_2Higgs","VR_st_600_2Higgs","VR_tt_600_2Higgs","VR_Z_600_2Higgs","CR_Z_600_2Higgs"});
  addRegions({"CR_st_700_2Higgs","CR_tt4b_700_2Higgs","CR_tt3b_700_2Higgs","VR_st_700_2Higgs","VR_tt_700_2Higgs","VR_Z_700_2Higgs","CR_Z_700_2Higgs"});
  addRegions({"CR_st_800_2Higgs","CR_tt4b_800_2Higgs","CR_tt3b_800_2Higgs","VR_st_800_2Higgs","VR_tt_800_2Higgs","VR_Z_800_2Higgs","CR_Z_800_2Higgs"});
  addRegions({"CR_st_900_2Higgs","CR_tt4b_900_2Higgs","CR_tt3b_900_2Higgs","VR_st_900_2Higgs","VR_tt_900_2Higgs","VR_Z_900_2Higgs","CR_Z_900_2Higgs"});
  addRegions({"CR_st_1000_2Higgs","CR_tt4b_1000_2Higgs","CR_tt3b_1000_2Higgs","VR_st_1000_2Higgs","VR_tt_1000_2Higgs","VR_Z_1000_2Higgs","CR_Z_1000_2Higgs"});
  addRegions({"CR_st_1100_2Higgs","CR_tt4b_1100_2Higgs","CR_tt3b_1100_2Higgs","VR_st_1100_2Higgs","VR_tt_1100_2Higgs","VR_Z_1100_2Higgs","CR_Z_1100_2Higgs"});

  addRegions({"CR_st_200_1Higgs","CR_tt4b_200_1Higgs","CR_tt3b_200_1Higgs","VR_st_200_1Higgs","VR_tt_200_1Higgs","VR_Z_200_1Higgs","CR_Z_200_1Higgs"});
  addRegions({"CR_st_250_1Higgs","CR_tt4b_250_1Higgs","CR_tt3b_250_1Higgs","VR_st_250_1Higgs","VR_tt_250_1Higgs","VR_Z_250_1Higgs","CR_Z_250_1Higgs"});
  addRegions({"CR_st_300_1Higgs","CR_tt4b_300_1Higgs","CR_tt3b_300_1Higgs","VR_st_300_1Higgs","VR_tt_300_1Higgs","VR_Z_300_1Higgs","CR_Z_300_1Higgs"});
  addRegions({"CR_st_400_1Higgs","CR_tt4b_400_1Higgs","CR_tt3b_400_1Higgs","VR_st_400_1Higgs","VR_tt_400_1Higgs","VR_Z_400_1Higgs","CR_Z_400_1Higgs"});
  addRegions({"CR_st_500_1Higgs","CR_tt4b_500_1Higgs","CR_tt3b_500_1Higgs","VR_st_500_1Higgs","VR_tt_500_1Higgs","VR_Z_500_1Higgs","CR_Z_500_1Higgs"});
  addRegions({"CR_st_600_1Higgs","CR_tt4b_600_1Higgs","CR_tt3b_600_1Higgs","VR_st_600_1Higgs","VR_tt_600_1Higgs","VR_Z_600_1Higgs","CR_Z_600_1Higgs"});
  addRegions({"CR_st_700_1Higgs","CR_tt4b_700_1Higgs","CR_tt3b_700_1Higgs","VR_st_700_1Higgs","VR_tt_700_1Higgs","VR_Z_700_1Higgs","CR_Z_700_1Higgs"});
  addRegions({"CR_st_800_1Higgs","CR_tt4b_800_1Higgs","CR_tt3b_800_1Higgs","VR_st_800_1Higgs","VR_tt_800_1Higgs","VR_Z_800_1Higgs","CR_Z_800_1Higgs"});
  addRegions({"CR_st_900_1Higgs","CR_tt4b_900_1Higgs","CR_tt3b_900_1Higgs","VR_st_900_1Higgs","VR_tt_900_1Higgs","VR_Z_900_1Higgs","CR_Z_900_1Higgs"});
  addRegions({"CR_st_1000_1Higgs","CR_tt4b_1000_1Higgs","CR_tt3b_1000_1Higgs","VR_st_1000_1Higgs","VR_tt_1000_1Higgs","VR_Z_1000_1Higgs","CR_Z_1000_1Higgs"});
  addRegions({"CR_st_1100_1Higgs","CR_tt4b_1100_1Higgs","CR_tt3b_1100_1Higgs","VR_st_1100_1Higgs","VR_tt_1100_1Higgs","VR_Z_1100_1Higgs","CR_Z_1100_1Higgs"});

  addRegions({"CR_st_200_0Higgs","CR_tt4b_200_0Higgs","CR_tt3b_200_0Higgs","VR_st_200_0Higgs","VR_tt_200_0Higgs","VR_Z_200_0Higgs","CR_Z_200_0Higgs"});
  addRegions({"CR_st_250_0Higgs","CR_tt4b_250_0Higgs","CR_tt3b_250_0Higgs","VR_st_250_0Higgs","VR_tt_250_0Higgs","VR_Z_250_0Higgs","CR_Z_250_0Higgs"});
  addRegions({"CR_st_300_0Higgs","CR_tt4b_300_0Higgs","CR_tt3b_300_0Higgs","VR_st_300_0Higgs","VR_tt_300_0Higgs","VR_Z_300_0Higgs","CR_Z_300_0Higgs"});
  addRegions({"CR_st_400_0Higgs","CR_tt4b_400_0Higgs","CR_tt3b_400_0Higgs","VR_st_400_0Higgs","VR_tt_400_0Higgs","VR_Z_400_0Higgs","CR_Z_400_0Higgs"});
  addRegions({"CR_st_500_0Higgs","CR_tt4b_500_0Higgs","CR_tt3b_500_0Higgs","VR_st_500_0Higgs","VR_tt_500_0Higgs","VR_Z_500_0Higgs","CR_Z_500_0Higgs"});
  addRegions({"CR_st_600_0Higgs","CR_tt4b_600_0Higgs","CR_tt3b_600_0Higgs","VR_st_600_0Higgs","VR_tt_600_0Higgs","VR_Z_600_0Higgs","CR_Z_600_0Higgs"});
  addRegions({"CR_st_700_0Higgs","CR_tt4b_700_0Higgs","CR_tt3b_700_0Higgs","VR_st_700_0Higgs","VR_tt_700_0Higgs","VR_Z_700_0Higgs","CR_Z_700_0Higgs"});
  addRegions({"CR_st_800_0Higgs","CR_tt4b_800_0Higgs","CR_tt3b_800_0Higgs","VR_st_800_0Higgs","VR_tt_800_0Higgs","VR_Z_800_0Higgs","CR_Z_800_0Higgs"});
  addRegions({"CR_st_900_0Higgs","CR_tt4b_900_0Higgs","CR_tt3b_900_0Higgs","VR_st_900_0Higgs","VR_tt_900_0Higgs","VR_Z_900_0Higgs","CR_Z_900_0Higgs"});
  addRegions({"CR_st_1000_0Higgs","CR_tt4b_1000_0Higgs","CR_tt3b_1000_0Higgs","VR_st_1000_0Higgs","VR_tt_1000_0Higgs","VR_Z_1000_0Higgs","CR_Z_1000_0Higgs"});
  addRegions({"CR_st_1100_0Higgs","CR_tt4b_1100_0Higgs","CR_tt3b_1100_0Higgs","VR_st_1100_0Higgs","VR_tt_1100_0Higgs","VR_Z_1100_0Higgs","CR_Z_1100_0Higgs"});

  // Add histograms for vars
  addHistogram("numHiggs",10,-0.5,9.5);
  addHistogram("meff_rand_hc",20,0,2000);
  addHistogram("met",20,0,2000);
  addHistogram("met_mu",20,0,2000);
  addHistogram("hist_met_meff",20,0,2000,20,0,2000);
  addHistogram("n_jets_40", 20, -0.5, 19.5);
  addHistogram("n_bjets_40",20, -0.5, 19.5);
  addHistogram("n_electrons", 20, -0.5, 19.5);
  addHistogram("n_muons", 20, -0.5, 19.5);
  addHistogram("n_leptons", 20, -0.5, 19.5);
  addHistogram("n_electrons_preor_loose", 20, -0.5, 19.5);
  addHistogram("n_muons_preor_loose", 20, -0.5, 19.5);
  addHistogram("n_leptons_preor_loose", 20, -0.5, 19.5);
  addHistogram("n_electrons_preor_med", 20, -0.5, 19.5);
  addHistogram("n_muons_preor_med", 20, -0.5, 19.5);
  addHistogram("n_leptons_preor_med", 20, -0.5, 19.5);
  addHistogram("m_h1_lm",20,50,200);
  addHistogram("m_h2_lm",20,50,200);
  addHistogram("m_h1_hm",20,50,200);
  addHistogram("m_h2_hm",20,50,200);
  addHistogram("dr_h1_hm",50,0,5);
  addHistogram("dr_h2_hm",50,0,5);
  addHistogram("xwt",20,0,20);
  addHistogram("BDT_score_200",50,0,1);
  addHistogram("BDT_score_250",50,0,1);
  addHistogram("BDT_score_300",50,0,1);
  addHistogram("BDT_score_400",50,0,1);
  addHistogram("BDT_score_500",50,0,1);
  addHistogram("BDT_score_600",50,0,1);
  addHistogram("BDT_score_700",50,0,1);
  addHistogram("BDT_score_800",50,0,1);
  addHistogram("BDT_score_900",50,0,1);
  addHistogram("BDT_score_1000",50,0,1);
  addHistogram("BDT_score_1100",50,0,1);

  addHistogram("n_jets_25", 20, -0.5, 19.5);
  addHistogram("n_bjets_25",20, -0.5, 19.5);
  addHistogram("ht",20,0,2000);
  addHistogram("meffi",20,0,2000);
  addHistogram("mjsum",20,0,2000);
  addHistogram("mTb_min",20,0,800);
  addHistogram("mT", 20, 0, 500);
  addHistogram("dphiMin4", 50, 0, 5);
  addHistogram("dphi1jet", 50, 0, 5);
  addHistogram("dr_min_bjets", 50, 0, 5);
  addHistogram("met_sig", 50, 0, 100);

  addHistogram("GenMET",40,0,2000);
  addHistogram("GenHT",40,0,2000);

  addHistogram("mc_weight", 2, 0, 2);


  //Initialise the BDT from the ROOT file in data

  addMVAUtilsBDT("xgboost:test", "ANA-SUSY-2020-16-BDT_test.root","ANA-SUSY-2020-16-BDT_test.root");
  addMVAUtilsBDT("xgboost:train", "ANA-SUSY-2020-16-BDT_training.root","ANA-SUSY-2020-16-BDT_training.root");

  // Setting BTaggingTruth property 
#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#pragma message "Compiling BTaggingTruthTagging for TRF usage"
  m_btt = new BTaggingTruthTaggingTool("MyBTaggingTruthTaggingTool");
  StatusCode code = m_btt->setProperty("TaggerName",          "DL1r");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool TaggerName property");
  code = m_btt->setProperty("OperatingPoint", "FixedCutBEff_77");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool OperatingPoint property");
  code = m_btt->setProperty("JetAuthor", "AntiKt4EMPFlowJets");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool JetAuthor property");
  //code = m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-01-31_v1.root");
  code = m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool ScaleFactorFileName property");
  code = m_btt->setProperty("MaxNtagged", 4);
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool MaxNtagged property");
  // call initialize() function
  code = m_btt->initialize();
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error initializing the BTaggingTruthTaggingTool");
  std::cout << "Initialized BTaggingTruthTagging tool" << std::endl;
#endif

}

static int jetFlavor(AnalysisObject &jet) {
  if (jet.pass(TrueBJet)) return 5;
  if (jet.pass(TrueCJet)) return 4;
  if (jet.pass(TrueTau)) return 15;
  return 0;
}

void EwkMultiBjets2021::ProcessEvent(AnalysisEvent *event)
{

  float gen_met      = event->getGenMET();
  float gen_ht       = event->getGenHT();
  int channel_number = event->getMCNumber();

  fill("mc_weight", event->getMCWeights()[0]);

  // handle HT slicing now
  if(channel_number==410000 && gen_ht>600) return;
  if(channel_number==410001 && gen_ht>600) return;
  if(channel_number==410002 && gen_ht>600) return;
  if(channel_number==410225 && gen_ht>600) return;
  if(channel_number==410004 && gen_ht>600) return;
  if(channel_number==407018 && gen_ht>500) return;
  if(channel_number==407020 && gen_ht>500) return;

  // baseline electrons are requested to pass the loose likelihood identification criteria
  //    and have pT > 20 GeV and |eta| < 2.47
  auto electrons  = event->getElectrons(20, 2.47, ELooseBLLH|EZ05mm);//What corresponds to LooseAndBLayerLLH? Tutorial says ELooseBLLH.
  //do I need to add z0=0.5 to electrons and muons? It's in default SUSYTools, so yes.
  // baseline muons are required to pass the Medium selections and to have pT > 20 GeV, |eta| < 2.7
  auto muons      = event->getMuons(20, 2.7, MuMedium|MuZ05mm);

  //For combination lepton veto
  auto electrons_loose  = event->getElectrons(7, 2.47, ELooseBLLH|EZ05mm);//We have a derivation slimming of 6 GeV for muons, 7 GeV for electrons
  auto muons_loose      = event->getMuons(6, 2.7, MuMedium|MuZ05mm);

  // baseline small-R jets: pT > 20 GeV. These are hardcoded in SUSYTools, and there is no eta cut.
  auto candJets   = event->getJets(20., 4.5);
  
  auto metVec     = event->getMET();
  double met      = metVec.Et();
  if(countObjects(candJets, 20, 4.5, NOT(LooseBadJet))!=0) return;//Bad jet veto
  // No bad muon veto implemented
  // Reject events with bad muon (**NOT SURE if this cut is doing that...**)
  if (countObjects(muons,20,2.7,NOT(MuQoPSignificance)) != 0) return; //Bad muon is before OR, before signal

  // Overlap removal
  auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

  //First object set gets things removed, second does not. third arg is radius, fourth is function.

  auto truthHiggs = event->getHSTruth(-1.0, 250, 25); // pT and eta cuts should cover all cases
  int numHiggs = truthHiggs.size();
  numHiggs = 0.5*numHiggs; // Changes in SimpleAnalysis doubled number of Higgs on same files

  if (numHiggs == 2){
    accept("TwoHiggs");
  } else if (numHiggs == 1){
    accept("OneHiggs");
  } else if (numHiggs ==0 ){
    accept("NoHiggs");
  }

  AnalysisObjects taus; //empty container for MET calc
  AnalysisObjects photons;
  float met_sig = event->getMETSignificance(electrons, muons, candJets, photons, taus, metVec);//Ours uses TST term. This is standard.
                                              //Should I apply OR? N/A given below.
                                              //Should I do on custom jets/electrons/muons? Yes.
                                              //In MBJ, this is done on baseline objects before OR.

  //muons = overlapRemoval(muons, electrons, 0.01, NOT(MuCaloTaggedOnly));//Could do this first, but calo-tagged muons do not exist in truth.

  electrons  = overlapRemoval(electrons, muons, 0.01);//If a muon shares an ID track with an electron, the electron is removed. From documentation: "Since ID tracks are not included in the truth content, we will just remove electrons close to another muon with a fixed ΔR = 0.01."
  candJets   = overlapRemoval(candJets, electrons, 0.2); //electron-jet: If dR(j,e) < 0.2 the jet is removed. For any surviving jets, if dR(j; e) <min(0.4, 0.04 + 10 GeV/peT), the electron is removed.
  electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);

  candJets   = overlapRemoval(candJets, muons, 0.2, LessThan3Tracks);//If dR(j;mu) < 0.2 or the muon ID track is ghost associated to the jet, then the jet is removed if the jet has less than three associated tracks with pT > 500 MeV  


  //b-jet muon correction
  candJets   = muonCorrection(candJets, muons, 0.4); //TODO need b-jets with abs(eta)<2.5 BEFORE correction. This is supremely negligible.

  muons      = overlapRemoval(muons, candJets, radiusCalcMuon); //For any surviving jets, if dR(j,mu) < min(0.4, 0.04 + 10 GeV/pT), the muon is removed.

  if (countObjects(muons, 20, 2.7, NOT(MuNotCosmic))!=0) return;//Cosmic muon is after OR, before signal
  candJets = filterObjects(candJets, 20, 2.8, JVTMedium);//Hopefully this does PFlow version, not EMTopo

  // require signal jets to be 25 GeV
  auto signalJets_25      = filterObjects(candJets, 25);
  auto signalJets_40      = filterObjects(candJets, 40);
  auto signalJets_40_eta      = filterObjects(candJets, 40, 2.5);

  // reclusterJets(jets, ReclusterRadius, RCJetPtMin, RCJetSubjetRadius, RCJetPtFrac)
  // We run on signal jets with pT>30
  auto signalJets_30 = filterObjects(candJets, 30);
  auto fatJets = reclusterJets(signalJets_30, 0.8, 100, 0.01, 0.10);
  // signal electrons are required to pass the Medium likelihood criteria and isolated using FCLoose
  auto signalElectrons = filterObjects(electrons, 20, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoFCLoose);//We use FCLoose. But https://simpleanalysis.docs.cern.ch/tutorial/objdef/ claims FCLoose=EIsoFixedCutLoose. Doesn't matter as truth. 
  // signal muons are required to be isolated using LooseTrackOnly
  auto signalMuons     = filterObjects(muons, 20, 2.5, MuD0Sigma3|MuZ05mm|MuIsoTightTrackOnly_VarRad);//We have Muon.d0sig: 3., Muon.z0: 0.5 and MuonCosmic.z0: 1., MuonCosmic.d0: 0.2. Note says Cosmic after OR.
  
  // combine into signalLeptons for easy counting
  auto signalLeptons   = signalElectrons + signalMuons;
  auto electrons_med = filterObjects(electrons_loose,8);
  auto muons_med = filterObjects(muons_loose,8);

  auto looseLeptons = electrons_loose + muons_loose; 
  auto medLeptons = electrons_med + muons_med;

  // get candidate b-jets and signal b-jets
  auto candBJets_25 = filterObjects(signalJets_25, 25., 2.5);
  auto bjets_25 = filterObjects(signalJets_25, 25., 2.5, BTag77DL1r);
  auto candBJets_40 = filterObjects(signalJets_40, 40., 2.5);
  auto bjets_40 = filterObjects(signalJets_40, 40., 2.5, BTag77DL1r);
  auto non_bjets_40_part1 = filterObjects(signalJets_40, 40., 2.8, NOT(BTag77DL1r));//will this include b-jets with 2.5<eta<2.8? I want it to. The next line should add any that aren't, and indeed the size is non-zero.
  auto non_bjets_40_part2 = filterObjectsRange(signalJets_40, 40., 999999999999., 2.5, 2.8, BTag77DL1r);
  //auto non_bjets_40_part2fake = filterObjectsRange(signalJets_40, 40., 999999999999., 2.4, 2.8, BTag77DL1r);

  auto non_bjets_25_part1 = filterObjects(signalJets_25, 25., 2.8, NOT(BTag77DL1r));
  auto non_bjets_25_part2 = filterObjectsRange(signalJets_25, 25., 999999999999., 2.5, 2.8, BTag77DL1r);
  
  auto metVecMu = metVec;//Add leptons to MET
  if (signalMuons.size()==2 and signalElectrons.size()==0){
    if(signalMuons[0].charge() != signalMuons[1].charge()){
        for (const auto &cand : signalMuons){
            metVecMu = metVecMu + cand;
        }
    }
  }
  if (signalMuons.size()==0 and signalElectrons.size()==2){
    if(signalElectrons[0].charge() != signalElectrons[1].charge()){
        for (const auto &cand : signalElectrons){
            metVecMu = metVecMu + cand;
        }
    }
  }

  float met_mu = metVecMu.Et();
  //Check there's no overlap
  for (const auto &cand : non_bjets_40_part1) {
    for (const auto &other : non_bjets_40_part2) {
        if(cand == other){
            throw std::runtime_error("Overlap in parts 1 and 2 of bjets_40");
        }
    }
  }
  for (const auto &cand : non_bjets_25_part1) {
    for (const auto &other : non_bjets_25_part2) {
        if(cand == other){
            throw std::runtime_error("Overlap in parts 1 and 2 of bjets_25");
        }
    }
  }

  auto non_bjets_40 = non_bjets_40_part1 + non_bjets_40_part2;
  auto non_bjets_25 = non_bjets_25_part1 + non_bjets_25_part2;

  int n_bjets_25       = bjets_25.size();
  int n_bjets_40       = bjets_40.size();

  int n_jets_25        = signalJets_25.size();
  int n_jets_40        = signalJets_40.size();
  int n_jets_40_eta        = signalJets_40_eta.size();

  int n_leptons     = signalLeptons.size();
  int n_leptons_med = medLeptons.size();
  int n_leptons_loose = looseLeptons.size();
  // require at least 4 signal jets
  
  if(n_jets_25 < 4) return;
#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
  int seed=n_jets_25 + 10*n_leptons + 100*met + 1000*electrons.size() + 10000*muons.size();

  std::vector<double> pt   = std::vector<double>(candBJets_25.size(), 0);
  std::vector<double> eta  = std::vector<double>(candBJets_25.size(), 0);
  std::vector<int>    flav = std::vector<int>   (candBJets_25.size(), 1);
  std::vector<double> tagw = std::vector<double>(candBJets_25.size(), 1);

  for (unsigned int index = 0; index < candBJets_25.size(); ++index){
    auto jet = candBJets_25.at(index);
    pt[index]   = jet.Pt()*1.e3;
    eta[index]  = jet.Eta();
    flav[index] = jetFlavor(jet);
  }

  // compute TRF weight
  Analysis::TruthTagResults m_TTres;
  StatusCode code = m_btt->CalculateResults(pt,eta,flav,tagw,m_TTres,seed);
  if (!code.isSuccess()) throw std::runtime_error("error in retrieving the weights");
#else
  // require at least 3 b-bjets
  if(n_bjets_25<3) return;
#endif
   
  sortObjectsByPt(bjets_40);
  sortObjectsByPt(bjets_25);
  sortObjectsByPt(non_bjets_40);
  sortObjectsByPt(non_bjets_25);
  sortObjectsByPt(signalJets_40);
  sortObjectsByPt(signalJets_25);
  sortObjectsByPt(signalLeptons);
  sortObjectsByPt(fatJets);

  float meff_rand_hc = 0;
  if(n_bjets_40>=4){
    meff_rand_hc = met + sumObjectsPt(bjets_40,4); //This assumes sorted by pT- is this the case? Yes, I explicitly sort them.
  }
  float ht = sumObjectsPt(signalJets_25);
  // inclusive - all jets + leptons
  float meffi  = met + sumObjectsPt(signalJets_25) + sumObjectsPt(signalLeptons);
  // min mT of leading 3 bjets
  float mTbmin    = calcMTmin(bjets_25, metVec,3);
  // dphimin between leading 4 signal jets and met
  float dphiMin4  = minDphi(metVec, signalJets_25, 4);
  // leading lepton and met
  float mT = (signalLeptons.size()>0)? calcMT(signalLeptons[0], metVec) : 0.0;
  // sum of leading 4 reclustered jet masses
  float mjsum = sumObjectsM(fatJets, 4);
  // dPhi(j1, MET) for Gbb
  float dphi1jet = minDphi(metVec, signalJets_25, 1);
  float dr_min_bjets = minDR(bjets_25);

  // Higgs mass variables
  double mass_h1_lm = 0;
  double mass_h2_lm = 0;
  double mass_h1_hm = 0;
  double mass_h2_hm = 0;
  double dr_h1_hm = 0;
  double dr_h2_hm = 0;
 
  float xwt = 999.0;
  if (bjets_25.size() >= 4){
    std::pair<std::pair<double, double>,std::pair<double, double>> m_h_h_hm = pair_h_h(bjets_25,3);
    mass_h1_hm = (m_h_h_hm.first).first;
    mass_h2_hm = (m_h_h_hm.first).second;
    dr_h1_hm = (m_h_h_hm.second).first;
    dr_h2_hm = (m_h_h_hm.second).second;

  } else if (bjets_25.size() == 3){
    //Find heaviest
    //double mass_heavy = 0;
    sortObjectsByMass(bjets_25);
        //If there's a b-jet with mass>100 GeV, treat as two merged b-jets
    if(bjets_25[0].M()>100){
        std::pair<std::pair<double, double>,std::pair<double, double>> m_h_h_hm = heavy_pair_h_h(bjets_25[0],bjets_25[1],bjets_25[2]);
        mass_h1_hm = (m_h_h_hm.first).first;
        mass_h2_hm = (m_h_h_hm.first).second;
        dr_h1_hm = (m_h_h_hm.second).first;
        dr_h2_hm = (m_h_h_hm.second).second;

    } else {//Otherwise, minimize dR. Actually, pick the one that gives the lowest mh1.
        double temp_mh1 = 999;
        for (const auto &nonbjet : non_bjets_25) {
            //Make container with 3 b-jets, one other jet
            AnalysisObjects tempList;
            tempList.push_back(nonbjet);
            AnalysisObjects custom_jets_25 = tempList + bjets_25;
            //Get mass
            std::pair<std::pair<double, double>,std::pair<double, double>> m_h_h_hm = pair_h_h(custom_jets_25,3);
            //If mh1 lower, use this one
            if((m_h_h_hm.first).first<temp_mh1){
                mass_h1_hm = (m_h_h_hm.first).first;
                mass_h2_hm = (m_h_h_hm.first).second;
                dr_h1_hm = (m_h_h_hm.second).first;
                dr_h2_hm = (m_h_h_hm.second).second;
            }
        }
    }
    sortObjectsByPt(bjets_25);
  }

  if (bjets_40.size() >= 4){
    // here higgs pair mode can be defined
    std::pair<std::pair<double, double>,std::pair<double, double>> m_h_h = pair_h_h(bjets_40, 5);
    mass_h1_lm = (m_h_h.first).first;
    mass_h2_lm = (m_h_h.first).second;

    xwt = CalcXwt(bjets_40,non_bjets_40);
  } 
  Int_t m_EWK_masses[11] = { 200, 250, 300, 400, 500, 600, 700, 800, 900, 1000, 1100};//, 1200, 1300, 1400, 1500 };
  Float_t BDT_score[11] = {0}; //!

  int seed2=n_jets_25 + 10*n_leptons + 100*met + 1000*electrons.size() + 10000*muons.size();
  std::srand(seed2);
  int useBDT = ((float)std::rand() / RAND_MAX > 0.5?1:-1);//Randomly select BDT to use
  auto *bdt_test = getMVA("test");
  auto *bdt_train = getMVA("train");
  for (int imass = 0; imass<11; imass++){
    int m_GGMH_mass = m_EWK_masses[imass];
    std::vector<double> input_features;
    input_features.push_back(n_bjets_25);
    input_features.push_back(n_jets_25);
    input_features.push_back(dphiMin4);
    input_features.push_back(mTbmin);
    input_features.push_back(mass_h1_hm);
    input_features.push_back(mass_h2_hm);
    input_features.push_back(dr_h1_hm);
    input_features.push_back(dr_h1_hm);
    input_features.push_back(ht);
    input_features.push_back(met);
    input_features.push_back(mjsum);
    input_features.push_back(met_sig);
    input_features.push_back(dr_min_bjets);
    input_features.push_back(m_GGMH_mass);
    
    if(useBDT==1){
        BDT_score[imass] = bdt_test->evaluateWeighted(input_features);
    } else {
        BDT_score[imass] = bdt_train->evaluateWeighted(input_features);
    }

  }

  float BDT_score_200 = BDT_score[0];
  float BDT_score_250 = BDT_score[1];
  float BDT_score_300 = BDT_score[2];
  float BDT_score_400 = BDT_score[3];
  float BDT_score_500 = BDT_score[4];
  float BDT_score_600 = BDT_score[5];
  float BDT_score_700 = BDT_score[6];
  float BDT_score_800 = BDT_score[7];
  float BDT_score_900 = BDT_score[8];
  float BDT_score_1000 = BDT_score[9];
  float BDT_score_1100 = BDT_score[10];

  float m_pt_jet_1  = 0;
  float m_eta_jet_1 = 0;
  float m_phi_jet_1 = 0;
  float m_m_jet_1   = 0;
  int m_isb_jet_1 = 0;
  float m_pt_jet_2  = 0;
  float m_eta_jet_2 = 0;
  float m_phi_jet_2 = 0;
  float m_m_jet_2   = 0;
  int m_isb_jet_2 = 0;
  float m_pt_jet_3  = 0;
  float m_eta_jet_3 = 0;
  float m_phi_jet_3 = 0;
  float m_m_jet_3   = 0;
  int m_isb_jet_3 = 0;
  float m_pt_jet_4  = 0;
  float m_eta_jet_4 = 0;
  float m_phi_jet_4 = 0;
  float m_m_jet_4   = 0;
  int m_isb_jet_4 = 0;
  float m_pt_jet_5  = 0;
  float m_eta_jet_5 = 0;
  float m_phi_jet_5 = 0;
  float m_m_jet_5   = 0;
  int m_isb_jet_5 = 0;
  float m_pt_jet_6  = 0;
  float m_eta_jet_6 = 0;
  float m_phi_jet_6 = 0;
  float m_m_jet_6   = 0;
  int m_isb_jet_6 = 0;
  float m_pt_jet_7  = 0;
  float m_eta_jet_7 = 0;
  float m_phi_jet_7 = 0;
  float m_m_jet_7   = 0;
  int m_isb_jet_7 = 0;
  float m_pt_jet_8  = 0;
  float m_eta_jet_8 = 0;
  float m_phi_jet_8 = 0;
  float m_m_jet_8   = 0;
  int m_isb_jet_8 = 0;
  float m_pt_jet_9  = 0;
  float m_eta_jet_9 = 0;
  float m_phi_jet_9 = 0;
  float m_m_jet_9   = 0;
  int m_isb_jet_9 = 0;
  float m_pt_jet_10  = 0;
  float m_eta_jet_10 = 0;
  float m_phi_jet_10 = 0;
  float m_m_jet_10   = 0;
  int m_isb_jet_10 = 0;

  std::vector<bool> btagged_25;
  for(const auto& jet: signalJets_25){
    btagged_25.push_back(std::find(bjets_25.begin(), bjets_25.end(), jet) != bjets_25.end());
  }
  // jets and leptons are in pT-ordered
  for (unsigned int index = 0; index < signalJets_25.size(); ++index){
    auto jet = signalJets_25.at(index);
    if(index==0){
      m_pt_jet_1 = jet.Pt();
      m_eta_jet_1 = jet.Eta();
      m_phi_jet_1 = jet.Phi();
      m_m_jet_1 = jet.M();
      m_isb_jet_1 = btagged_25[index]?1:0;
    } else if(index==1){
      m_pt_jet_2 = jet.Pt();
      m_eta_jet_2 = jet.Eta();
      m_phi_jet_2 = jet.Phi();
      m_m_jet_2 = jet.M();
      m_isb_jet_2 = btagged_25[index]?1:0;
    } else if(index==2){
      m_pt_jet_3 = jet.Pt();
      m_eta_jet_3 = jet.Eta();
      m_phi_jet_3 = jet.Phi();
      m_m_jet_3 = jet.M();
      m_isb_jet_3 = btagged_25[index]?1:0;
    } else if(index==3){
      m_pt_jet_4 = jet.Pt();
      m_eta_jet_4 = jet.Eta();
      m_phi_jet_4 = jet.Phi();
      m_m_jet_4 = jet.M();
      m_isb_jet_4 = btagged_25[index]?1:0;
    } else if(index==4){
      m_pt_jet_5 = jet.Pt();
      m_eta_jet_5 = jet.Eta();
      m_phi_jet_5 = jet.Phi();
      m_m_jet_5 = jet.M();
      m_isb_jet_5 = btagged_25[index]?1:0;
    } else if(index==5){
      m_pt_jet_6 = jet.Pt();
      m_eta_jet_6 = jet.Eta();
      m_phi_jet_6 = jet.Phi();
      m_m_jet_6 = jet.M();
      m_isb_jet_6 = btagged_25[index]?1:0;
    } else if(index==6){
      m_pt_jet_7 = jet.Pt();
      m_eta_jet_7 = jet.Eta();
      m_phi_jet_7 = jet.Phi();
      m_m_jet_7 = jet.M();
      m_isb_jet_7 = btagged_25[index]?1:0;
    } else if(index==7){
      m_pt_jet_8 = jet.Pt();
      m_eta_jet_8 = jet.Eta();
      m_phi_jet_8 = jet.Phi();
      m_m_jet_8 = jet.M();
      m_isb_jet_8 = btagged_25[index]?1:0;
    } else if(index==8){
      m_pt_jet_9 = jet.Pt();
      m_eta_jet_9 = jet.Eta();
      m_phi_jet_9 = jet.Phi();
      m_m_jet_9 = jet.M();
      m_isb_jet_9 = btagged_25[index]?1:0;
    } else if(index==9){
      m_pt_jet_10 = jet.Pt();
      m_eta_jet_10 = jet.Eta();
      m_phi_jet_10 = jet.Phi();
      m_m_jet_10 = jet.M();
      m_isb_jet_10 = btagged_25[index]?1:0;
    }  
  }

  fill("ht", ht);
  fill("meffi", meffi);
  fill("met", met);
  fill("met_mu", met_mu);
  fill("mjsum", mjsum);
  fill("mTb_min", mTbmin);
  fill("mT", mT);
  fill("dphiMin4", dphiMin4);
  fill("dphi1jet", dphi1jet);
  fill("dr_min_bjets", dr_min_bjets);
  fill("met_sig", met_sig);

  fill("n_jets_25", n_jets_25);
  fill("n_bjets_25", n_bjets_25);
  fill("n_electrons", signalElectrons.size());
  fill("n_muons", signalMuons.size());
  fill("n_leptons", n_leptons);

  fill("meff_rand_hc", meff_rand_hc);
  fill("hist_met_meff", meff_rand_hc, met);
  fill("n_jets_40", n_jets_40);
  fill("n_bjets_40", n_bjets_40);

  fill("n_electrons_preor_loose", electrons_loose.size());
  fill("n_muons_preor_loose", muons_loose.size());
  fill("n_leptons_preor_loose", n_leptons_loose);
  fill("n_electrons_preor_med", electrons_med.size());
  fill("n_muons_preor_med", muons_med.size());
  fill("n_leptons_preor_med", n_leptons_med);
  fill("numHiggs",numHiggs);

  fill("m_h1_lm", mass_h1_lm);
  fill("m_h2_lm", mass_h2_lm);
  fill("xwt", xwt);
  fill("BDT_score_200", BDT_score_200);
  fill("BDT_score_250", BDT_score_250);
  fill("BDT_score_300", BDT_score_300);
  fill("BDT_score_400", BDT_score_400);
  fill("BDT_score_500", BDT_score_500);
  fill("BDT_score_600", BDT_score_600);
  fill("BDT_score_700", BDT_score_700);
  fill("BDT_score_800", BDT_score_800);
  fill("BDT_score_900", BDT_score_900);
  fill("BDT_score_1000", BDT_score_1000);
  fill("BDT_score_1100", BDT_score_1100);

  fill("m_h1_hm", mass_h1_hm);
  fill("m_h2_hm", mass_h2_hm);
  fill("dr_h1_hm", dr_h1_hm);
  fill("dr_h2_hm", dr_h2_hm);

  //HM
  if(signalMuons.size()==2 && signalElectrons.size()==0 && n_jets_25>=4 && n_jets_25<=7 && n_bjets_25>=3 && met<=75 && met_mu>=175){//ZCR, ZVR 
    if(signalMuons[0].charge() != signalMuons[1].charge()){//opposite sign
        float mll = ( signalMuons[0] + signalMuons[1] ).M();
        if ( mll>=70 && mll<=110 ) {//|mll-mZ|<20 GeV
            accept("Presel_ZCR");
            if(numHiggs == 2) accept("Presel_ZCR_2Higgs");
            if(numHiggs == 1) accept("Presel_ZCR_1Higgs");
            if(numHiggs == 0) accept("Presel_ZCR_0Higgs");
            if(BDT_score_200>0.866617){
                accept("VR_Z_200");
                if(numHiggs == 2) accept("VR_Z_200_2Higgs");
                if(numHiggs == 1) accept("VR_Z_200_1Higgs");
                if(numHiggs == 0) accept("VR_Z_200_0Higgs");
            } else if(BDT_score_200>0.360367){
                accept("CR_Z_200");
                if(numHiggs == 2) accept("CR_Z_200_2Higgs");
                if(numHiggs == 1) accept("CR_Z_200_1Higgs");
                if(numHiggs == 0) accept("CR_Z_200_0Higgs");
            }
            if(BDT_score_250>0.538771){
                accept("VR_Z_250");
                if(numHiggs == 2) accept("VR_Z_250_2Higgs");
                if(numHiggs == 1) accept("VR_Z_250_1Higgs");
                if(numHiggs == 0) accept("VR_Z_250_0Higgs");
            } else if(BDT_score_250>0.0961619){
                accept("CR_Z_250");
                if(numHiggs == 2) accept("CR_Z_250_2Higgs");
                if(numHiggs == 1) accept("CR_Z_250_1Higgs");
                if(numHiggs == 0) accept("CR_Z_250_0Higgs");
            }
            if(BDT_score_300>0.282506){
                accept("VR_Z_300");
                if(numHiggs == 2) accept("VR_Z_300_2Higgs");
                if(numHiggs == 1) accept("VR_Z_300_1Higgs");
                if(numHiggs == 0) accept("VR_Z_300_0Higgs");
            } else if(BDT_score_300>0.0400408){
                accept("CR_Z_300");
                if(numHiggs == 2) accept("CR_Z_300_2Higgs");
                if(numHiggs == 1) accept("CR_Z_300_1Higgs");
                if(numHiggs == 0) accept("CR_Z_300_0Higgs");
            }
            if(BDT_score_400>0.139823){
                accept("VR_Z_400");
                if(numHiggs == 2) accept("VR_Z_400_2Higgs");
                if(numHiggs == 1) accept("VR_Z_400_1Higgs");
                if(numHiggs == 0) accept("VR_Z_400_0Higgs");
            } else if(BDT_score_400>0.0177804){
                accept("CR_Z_400");
                if(numHiggs == 2) accept("CR_Z_400_2Higgs");
                if(numHiggs == 1) accept("CR_Z_400_1Higgs");
                if(numHiggs == 0) accept("CR_Z_400_0Higgs");
            }
            if(BDT_score_500>0.0454809){
                accept("VR_Z_500");
                if(numHiggs == 2) accept("VR_Z_500_2Higgs");
                if(numHiggs == 1) accept("VR_Z_500_1Higgs");
                if(numHiggs == 0) accept("VR_Z_500_0Higgs");
            } else if(BDT_score_500>0.00534011){
                accept("CR_Z_500");
                if(numHiggs == 2) accept("CR_Z_500_2Higgs");
                if(numHiggs == 1) accept("CR_Z_500_1Higgs");
                if(numHiggs == 0) accept("CR_Z_500_0Higgs");
            }
            if(BDT_score_600>0.0252005){
                accept("VR_Z_600");
                if(numHiggs == 2) accept("VR_Z_600_2Higgs");
                if(numHiggs == 1) accept("VR_Z_600_1Higgs");
                if(numHiggs == 0) accept("VR_Z_600_0Higgs");
            } else if(BDT_score_600>0.00302006){
                accept("CR_Z_600");
                if(numHiggs == 2) accept("CR_Z_600_2Higgs");
                if(numHiggs == 1) accept("CR_Z_600_1Higgs");
                if(numHiggs == 0) accept("CR_Z_600_0Higgs");
            }
            if(BDT_score_700>0.0110402){
                accept("VR_Z_700");
                if(numHiggs == 2) accept("VR_Z_700_2Higgs");
                if(numHiggs == 1) accept("VR_Z_700_1Higgs");
                if(numHiggs == 0) accept("VR_Z_700_0Higgs");
            } else if(BDT_score_700>0.00112002){
                accept("CR_Z_700");
                if(numHiggs == 2) accept("CR_Z_700_2Higgs");
                if(numHiggs == 1) accept("CR_Z_700_1Higgs");
                if(numHiggs == 0) accept("CR_Z_700_0Higgs");
            }
            if(BDT_score_800>0.00418008){
                accept("VR_Z_800");
                if(numHiggs == 2) accept("VR_Z_800_2Higgs");
                if(numHiggs == 1) accept("VR_Z_800_1Higgs");
                if(numHiggs == 0) accept("VR_Z_800_0Higgs");
            } else if(BDT_score_800>0.00050001){
                accept("CR_Z_800");
                if(numHiggs == 2) accept("CR_Z_800_2Higgs");
                if(numHiggs == 1) accept("CR_Z_800_1Higgs");
                if(numHiggs == 0) accept("CR_Z_800_0Higgs");
            }
            if(BDT_score_900>0.00174003){
                accept("VR_Z_900");
                if(numHiggs == 2) accept("VR_Z_900_2Higgs");
                if(numHiggs == 1) accept("VR_Z_900_1Higgs");
                if(numHiggs == 0) accept("VR_Z_900_0Higgs");
            } else if(BDT_score_900>0.000220004){
                accept("CR_Z_900");
                if(numHiggs == 2) accept("CR_Z_900_2Higgs");
                if(numHiggs == 1) accept("CR_Z_900_1Higgs");
                if(numHiggs == 0) accept("CR_Z_900_0Higgs");
            }
            if(BDT_score_1000>0.000820016){
                accept("VR_Z_1000");
                if(numHiggs == 2) accept("VR_Z_1000_2Higgs");
                if(numHiggs == 1) accept("VR_Z_1000_1Higgs");
                if(numHiggs == 0) accept("VR_Z_1000_0Higgs");
            } else if(BDT_score_1000>0.000100002){
                accept("CR_Z_1000");
                if(numHiggs == 2) accept("CR_Z_1000_2Higgs");
                if(numHiggs == 1) accept("CR_Z_1000_1Higgs");
                if(numHiggs == 0) accept("CR_Z_1000_0Higgs");
            }
            if(BDT_score_1100>0.000720014){
                accept("VR_Z_1100");
                if(numHiggs == 2) accept("VR_Z_1100_2Higgs");
                if(numHiggs == 1) accept("VR_Z_1100_1Higgs");
                if(numHiggs == 0) accept("VR_Z_1100_0Higgs");
            } else if(BDT_score_1100>(8.00016*0.00001)){//8.00016e-05
                accept("CR_Z_1100");
                if(numHiggs == 2) accept("CR_Z_1100_2Higgs");
                if(numHiggs == 1) accept("CR_Z_1100_1Higgs");
                if(numHiggs == 0) accept("CR_Z_1100_0Higgs");
            }
        }
    }
  }

  if(n_leptons == 0 && n_leptons_med <= 1 && n_jets_25>=4 && n_jets_25<=7 && n_bjets_25>=3 && met>=150 && dphiMin4>0.4){
    accept("Presel_0L");
    if(numHiggs == 2) accept("Presel_0L_2Higgs");
    if(numHiggs == 1) accept("Presel_0L_1Higgs");
    if(numHiggs == 0) accept("Presel_0L_0Higgs");
    if(BDT_score_200>0.99974){
        accept("SR_1_200");
        if(numHiggs == 2) accept("SR_1_200_2Higgs");
        if(numHiggs == 1) accept("SR_1_200_1Higgs");
        if(numHiggs == 0) accept("SR_1_200_0Higgs");
    } else if(BDT_score_200>0.9994){
        accept("SR_2_200");
        if(numHiggs == 2) accept("SR_2_200_2Higgs");
        if(numHiggs == 1) accept("SR_2_200_1Higgs");
        if(numHiggs == 0) accept("SR_2_200_0Higgs");
    } else if(BDT_score_200>0.99898){
        accept("SR_3_200");
        if(numHiggs == 2) accept("SR_3_200_2Higgs");
        if(numHiggs == 1) accept("SR_3_200_1Higgs");
        if(numHiggs == 0) accept("SR_3_200_0Higgs");
    } else if(BDT_score_200>0.99472 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_200");
        if(numHiggs == 2) accept("VR_tt_200_2Higgs");
        if(numHiggs == 1) accept("VR_tt_200_1Higgs");
        if(numHiggs == 0) accept("VR_tt_200_0Higgs");
    } else if(BDT_score_200>0.971659 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_200");
        if(numHiggs == 2) accept("VR_st_200_2Higgs");
        if(numHiggs == 1) accept("VR_st_200_1Higgs");
        if(numHiggs == 0) accept("VR_st_200_0Higgs");
    } else if (BDT_score_200>0.888558 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_200");
        if(numHiggs == 2) accept("CR_st_200_2Higgs");
        if(numHiggs == 1) accept("CR_st_200_1Higgs");
        if(numHiggs == 0) accept("CR_st_200_0Higgs");
    } else if(BDT_score_200>0.9783 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_200");
        if(numHiggs == 2) accept("CR_tt3b_200_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_200_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_200_0Higgs");
    } else if(BDT_score_200>0.98318 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_200");
        if(numHiggs == 2) accept("CR_tt4b_200_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_200_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_200_0Higgs");
    }

    if(BDT_score_250>0.9997){
        accept("SR_1_250");
        if(numHiggs == 2) accept("SR_1_250_2Higgs");
        if(numHiggs == 1) accept("SR_1_250_1Higgs");
        if(numHiggs == 0) accept("SR_1_250_0Higgs");
    } else if(BDT_score_250>0.9994){
        accept("SR_2_250");
        if(numHiggs == 2) accept("SR_2_250_2Higgs");
        if(numHiggs == 1) accept("SR_2_250_1Higgs");
        if(numHiggs == 0) accept("SR_2_250_0Higgs");
    } else if(BDT_score_250>0.99846){
        accept("SR_3_250");
        if(numHiggs == 2) accept("SR_3_250_2Higgs");
        if(numHiggs == 1) accept("SR_3_250_1Higgs");
        if(numHiggs == 0) accept("SR_3_250_0Higgs");
    } else if(BDT_score_250>0.99786){
        accept("SR_4_250");
        if(numHiggs == 2) accept("SR_4_250_2Higgs");
        if(numHiggs == 1) accept("SR_4_250_1Higgs");
        if(numHiggs == 0) accept("SR_4_250_0Higgs");
    } else if(BDT_score_250>0.969239 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_250");
        if(numHiggs == 2) accept("VR_tt_250_2Higgs");
        if(numHiggs == 1) accept("VR_tt_250_1Higgs");
        if(numHiggs == 0) accept("VR_tt_250_0Higgs");
    } else if(BDT_score_250>0.98232 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_250");
        if(numHiggs == 2) accept("VR_st_250_2Higgs");
        if(numHiggs == 1) accept("VR_st_250_1Higgs");
        if(numHiggs == 0) accept("VR_st_250_0Higgs");
    } else if (BDT_score_250>0.940219 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_250");
        if(numHiggs == 2) accept("CR_st_250_2Higgs");
        if(numHiggs == 1) accept("CR_st_250_1Higgs");
        if(numHiggs == 0) accept("CR_st_250_0Higgs");
    } else if(BDT_score_250>0.963639 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_250");
        if(numHiggs == 2) accept("CR_tt3b_250_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_250_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_250_0Higgs");
    } else if(BDT_score_250>0.955179 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_250");
        if(numHiggs == 2) accept("CR_tt4b_250_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_250_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_250_0Higgs");
    }


    if(BDT_score_300>0.99956){
        accept("SR_1_300");
        if(numHiggs == 2) accept("SR_1_300_2Higgs");
        if(numHiggs == 1) accept("SR_1_300_1Higgs");
        if(numHiggs == 0) accept("SR_1_300_0Higgs");
    } else if(BDT_score_300>0.99904){
        accept("SR_2_300");
        if(numHiggs == 2) accept("SR_2_300_2Higgs");
        if(numHiggs == 1) accept("SR_2_300_1Higgs");
        if(numHiggs == 0) accept("SR_2_300_0Higgs");
    } else if(BDT_score_300>0.99826){
        accept("SR_3_300");
        if(numHiggs == 2) accept("SR_3_300_2Higgs");
        if(numHiggs == 1) accept("SR_3_300_1Higgs");
        if(numHiggs == 0) accept("SR_3_300_0Higgs");
    } else if(BDT_score_300>0.99738){
        accept("SR_4_300");
        if(numHiggs == 2) accept("SR_4_300_2Higgs");
        if(numHiggs == 1) accept("SR_4_300_1Higgs");
        if(numHiggs == 0) accept("SR_4_300_0Higgs");
    } else if(BDT_score_300>0.97996 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_300");
        if(numHiggs == 2) accept("VR_tt_300_2Higgs");
        if(numHiggs == 1) accept("VR_tt_300_1Higgs");
        if(numHiggs == 0) accept("VR_tt_300_0Higgs");
    } else if(BDT_score_300>0.98548 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_300");
        if(numHiggs == 2) accept("VR_st_300_2Higgs");
        if(numHiggs == 1) accept("VR_st_300_1Higgs");
        if(numHiggs == 0) accept("VR_st_300_0Higgs");
    } else if (BDT_score_300>0.959699 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_300");
        if(numHiggs == 2) accept("CR_st_300_2Higgs");
        if(numHiggs == 1) accept("CR_st_300_1Higgs");
        if(numHiggs == 0) accept("CR_st_300_0Higgs");
    } else if(BDT_score_300>0.965719 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_300");
        if(numHiggs == 2) accept("CR_tt3b_300_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_300_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_300_0Higgs");
    } else if(BDT_score_300>0.952459 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_300");
        if(numHiggs == 2) accept("CR_tt4b_300_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_300_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_300_0Higgs");
    }


    if(BDT_score_400>0.99974){
        accept("SR_1_400");
        if(numHiggs == 2) accept("SR_1_400_2Higgs");
        if(numHiggs == 1) accept("SR_1_400_1Higgs");
        if(numHiggs == 0) accept("SR_1_400_0Higgs");
    } else if(BDT_score_400>0.99936){
        accept("SR_2_400");
        if(numHiggs == 2) accept("SR_2_400_2Higgs");
        if(numHiggs == 1) accept("SR_2_400_1Higgs");
        if(numHiggs == 0) accept("SR_2_400_0Higgs");
    } else if(BDT_score_400>0.999){
        accept("SR_3_400");
        if(numHiggs == 2) accept("SR_3_400_2Higgs");
        if(numHiggs == 1) accept("SR_3_400_1Higgs");
        if(numHiggs == 0) accept("SR_3_400_0Higgs");
    } else if(BDT_score_400>0.99798){
        accept("SR_4_400");
        if(numHiggs == 2) accept("SR_4_400_2Higgs");
        if(numHiggs == 1) accept("SR_4_400_1Higgs");
        if(numHiggs == 0) accept("SR_4_400_0Higgs");
    } else if(BDT_score_400>0.98858 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_400");
        if(numHiggs == 2) accept("VR_tt_400_2Higgs");
        if(numHiggs == 1) accept("VR_tt_400_1Higgs");
        if(numHiggs == 0) accept("VR_tt_400_0Higgs");
    } else if(BDT_score_400>0.98814 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_400");
        if(numHiggs == 2) accept("VR_st_400_2Higgs");
        if(numHiggs == 1) accept("VR_st_400_1Higgs");
        if(numHiggs == 0) accept("VR_st_400_0Higgs");
    } else if (BDT_score_400>0.969879 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_400");
        if(numHiggs == 2) accept("CR_st_400_2Higgs");
        if(numHiggs == 1) accept("CR_st_400_1Higgs");
        if(numHiggs == 0) accept("CR_st_400_0Higgs");
    } else if(BDT_score_400>0.968759 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_400");
        if(numHiggs == 2) accept("CR_tt3b_400_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_400_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_400_0Higgs");
    } else if(BDT_score_400>0.935299 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_400");
        if(numHiggs == 2) accept("CR_tt4b_400_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_400_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_400_0Higgs");
    }


    if(BDT_score_500>0.9997){
        accept("SR_1_500");
        if(numHiggs == 2) accept("SR_1_500_2Higgs");
        if(numHiggs == 1) accept("SR_1_500_1Higgs");
        if(numHiggs == 0) accept("SR_1_500_0Higgs");
    } else if(BDT_score_500>0.9994){
        accept("SR_2_500");
        if(numHiggs == 2) accept("SR_2_500_2Higgs");
        if(numHiggs == 1) accept("SR_2_500_1Higgs");
        if(numHiggs == 0) accept("SR_2_500_0Higgs");
    } else if(BDT_score_500>0.99878){
        accept("SR_3_500");
        if(numHiggs == 2) accept("SR_3_500_2Higgs");
        if(numHiggs == 1) accept("SR_3_500_1Higgs");
        if(numHiggs == 0) accept("SR_3_500_0Higgs");
    } else if(BDT_score_500>0.974519 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_500");
        if(numHiggs == 2) accept("VR_tt_500_2Higgs");
        if(numHiggs == 1) accept("VR_tt_500_1Higgs");
        if(numHiggs == 0) accept("VR_tt_500_0Higgs");
    } else if(BDT_score_500>0.98888 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_500");
        if(numHiggs == 2) accept("VR_st_500_2Higgs");
        if(numHiggs == 1) accept("VR_st_500_1Higgs");
        if(numHiggs == 0) accept("VR_st_500_0Higgs");
    } else if (BDT_score_500>0.954699 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_500");
        if(numHiggs == 2) accept("CR_st_500_2Higgs");
        if(numHiggs == 1) accept("CR_st_500_1Higgs");
        if(numHiggs == 0) accept("CR_st_500_0Higgs");
    } else if(BDT_score_500>0.924498 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_500");
        if(numHiggs == 2) accept("CR_tt3b_500_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_500_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_500_0Higgs");
    } else if(BDT_score_500>0.787956 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_500");
        if(numHiggs == 2) accept("CR_tt4b_500_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_500_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_500_0Higgs");
    }


    if(BDT_score_600>0.99974){
        accept("SR_1_600");
        if(numHiggs == 2) accept("SR_1_600_2Higgs");
        if(numHiggs == 1) accept("SR_1_600_1Higgs");
        if(numHiggs == 0) accept("SR_1_600_0Higgs");
    } else if(BDT_score_600>0.99944){
        accept("SR_2_600");
        if(numHiggs == 2) accept("SR_2_600_2Higgs");
        if(numHiggs == 1) accept("SR_2_600_1Higgs");
        if(numHiggs == 0) accept("SR_2_600_0Higgs");
    } else if(BDT_score_600>0.99842){
        accept("SR_3_600");
        if(numHiggs == 2) accept("SR_3_600_2Higgs");
        if(numHiggs == 1) accept("SR_3_600_1Higgs");
        if(numHiggs == 0) accept("SR_3_600_0Higgs");
    } else if(BDT_score_600>0.973659 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_600");
        if(numHiggs == 2) accept("VR_tt_600_2Higgs");
        if(numHiggs == 1) accept("VR_tt_600_1Higgs");
        if(numHiggs == 0) accept("VR_tt_600_0Higgs");
    } else if(BDT_score_600>0.98982 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_600");
        if(numHiggs == 2) accept("VR_st_600_2Higgs");
        if(numHiggs == 1) accept("VR_st_600_1Higgs");
        if(numHiggs == 0) accept("VR_st_600_0Higgs");
    } else if (BDT_score_600>0.949279 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_600");
        if(numHiggs == 2) accept("CR_st_600_2Higgs");
        if(numHiggs == 1) accept("CR_st_600_1Higgs");
        if(numHiggs == 0) accept("CR_st_600_0Higgs");
    } else if(BDT_score_600>0.917818 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_600");
        if(numHiggs == 2) accept("CR_tt3b_600_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_600_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_600_0Higgs");
    } else if(BDT_score_600>0.739655 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_600");
        if(numHiggs == 2) accept("CR_tt4b_600_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_600_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_600_0Higgs");
    }


    if(BDT_score_700>0.99966){
        accept("SR_1_700");
        if(numHiggs == 2) accept("SR_1_700_2Higgs");
        if(numHiggs == 1) accept("SR_1_700_1Higgs");
        if(numHiggs == 0) accept("SR_1_700_0Higgs");
    } else if(BDT_score_700>0.99928){
        accept("SR_2_700");
        if(numHiggs == 2) accept("SR_2_700_2Higgs");
        if(numHiggs == 1) accept("SR_2_700_1Higgs");
        if(numHiggs == 0) accept("SR_2_700_0Higgs");
    } else if(BDT_score_700>0.99832){
        accept("SR_3_700");
        if(numHiggs == 2) accept("SR_3_700_2Higgs");
        if(numHiggs == 1) accept("SR_3_700_1Higgs");
        if(numHiggs == 0) accept("SR_3_700_0Higgs");
    } else if(BDT_score_700>0.953299 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_700");
        if(numHiggs == 2) accept("VR_tt_700_2Higgs");
        if(numHiggs == 1) accept("VR_tt_700_1Higgs");
        if(numHiggs == 0) accept("VR_tt_700_0Higgs");
    } else if(BDT_score_700>0.98378 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_700");
        if(numHiggs == 2) accept("VR_st_700_2Higgs");
        if(numHiggs == 1) accept("VR_st_700_1Higgs");
        if(numHiggs == 0) accept("VR_st_700_0Higgs");
    } else if (BDT_score_700>0.899218 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_700");
        if(numHiggs == 2) accept("CR_st_700_2Higgs");
        if(numHiggs == 1) accept("CR_st_700_1Higgs");
        if(numHiggs == 0) accept("CR_st_700_0Higgs");
    } else if(BDT_score_700>0.846257 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_700");
        if(numHiggs == 2) accept("CR_tt3b_700_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_700_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_700_0Higgs");
    } else if(BDT_score_700>0.49627 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_700");
        if(numHiggs == 2) accept("CR_tt4b_700_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_700_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_700_0Higgs");
    }


    if(BDT_score_800>0.99948){
        accept("SR_1_800");
        if(numHiggs == 2) accept("SR_1_800_2Higgs");
        if(numHiggs == 1) accept("SR_1_800_1Higgs");
        if(numHiggs == 0) accept("SR_1_800_0Higgs");
    } else if(BDT_score_800>0.99852){
        accept("SR_2_800");
        if(numHiggs == 2) accept("SR_2_800_2Higgs");
        if(numHiggs == 1) accept("SR_2_800_1Higgs");
        if(numHiggs == 0) accept("SR_2_800_0Higgs");
    } else if(BDT_score_800>0.905638 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_800");
        if(numHiggs == 2) accept("VR_tt_800_2Higgs");
        if(numHiggs == 1) accept("VR_tt_800_1Higgs");
        if(numHiggs == 0) accept("VR_tt_800_0Higgs");
    } else if(BDT_score_800>0.9761 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_800");
        if(numHiggs == 2) accept("VR_st_800_2Higgs");
        if(numHiggs == 1) accept("VR_st_800_1Higgs");
        if(numHiggs == 0) accept("VR_st_800_0Higgs");
    } else if (BDT_score_800>0.806536 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_800");
        if(numHiggs == 2) accept("CR_st_800_2Higgs");
        if(numHiggs == 1) accept("CR_st_800_1Higgs");
        if(numHiggs == 0) accept("CR_st_800_0Higgs");
    } else if(BDT_score_800>0.694074 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_800");
        if(numHiggs == 2) accept("CR_tt3b_800_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_800_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_800_0Higgs");
    } else if(BDT_score_800>0.250605 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_800");
        if(numHiggs == 2) accept("CR_tt4b_800_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_800_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_800_0Higgs");
    }


    if(BDT_score_900>0.99924){
        accept("SR_1_900");
        if(numHiggs == 2) accept("SR_1_900_2Higgs");
        if(numHiggs == 1) accept("SR_1_900_1Higgs");
        if(numHiggs == 0) accept("SR_1_900_0Higgs");
    } else if(BDT_score_900>0.99804){
        accept("SR_2_900");
        if(numHiggs == 2) accept("SR_2_900_2Higgs");
        if(numHiggs == 1) accept("SR_2_900_1Higgs");
        if(numHiggs == 0) accept("SR_2_900_0Higgs");
    } else if(BDT_score_900>0.827217 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_900");
        if(numHiggs == 2) accept("VR_tt_900_2Higgs");
        if(numHiggs == 1) accept("VR_tt_900_1Higgs");
        if(numHiggs == 0) accept("VR_tt_900_0Higgs");
    } else if(BDT_score_900>0.957619 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_900");
        if(numHiggs == 2) accept("VR_st_900_2Higgs");
        if(numHiggs == 1) accept("VR_st_900_1Higgs");
        if(numHiggs == 0) accept("VR_st_900_0Higgs");
    } else if (BDT_score_900>0.624412 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_900");
        if(numHiggs == 2) accept("CR_st_900_2Higgs");
        if(numHiggs == 1) accept("CR_st_900_1Higgs");
        if(numHiggs == 0) accept("CR_st_900_0Higgs");
    } else if(BDT_score_900>0.50943 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_900");
        if(numHiggs == 2) accept("CR_tt3b_900_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_900_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_900_0Higgs");
    } else if(BDT_score_900>0.120882 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_900");
        if(numHiggs == 2) accept("CR_tt4b_900_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_900_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_900_0Higgs");
    }


    if(BDT_score_1000>0.9991){
        accept("SR_1_1000");
        if(numHiggs == 2) accept("SR_1_1000_2Higgs");
        if(numHiggs == 1) accept("SR_1_1000_1Higgs");
        if(numHiggs == 0) accept("SR_1_1000_0Higgs");
    } else if(BDT_score_1000>0.721194 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_1000");
        if(numHiggs == 2) accept("VR_tt_1000_2Higgs");
        if(numHiggs == 1) accept("VR_tt_1000_1Higgs");
        if(numHiggs == 0) accept("VR_tt_1000_0Higgs");
    } else if(BDT_score_1000>0.922958 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_1000");
        if(numHiggs == 2) accept("VR_st_1000_2Higgs");
        if(numHiggs == 1) accept("VR_st_1000_1Higgs");
        if(numHiggs == 0) accept("VR_st_1000_0Higgs");
    } else if (BDT_score_1000>0.332187 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_1000");
        if(numHiggs == 2) accept("CR_st_1000_2Higgs");
        if(numHiggs == 1) accept("CR_st_1000_1Higgs");
        if(numHiggs == 0) accept("CR_st_1000_0Higgs");
    } else if(BDT_score_1000>0.331647 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_1000");
        if(numHiggs == 2) accept("CR_tt3b_1000_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_1000_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_1000_0Higgs");
    } else if(BDT_score_1000>0.0630013 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_1000");
        if(numHiggs == 2) accept("CR_tt4b_1000_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_1000_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_1000_0Higgs");
    }


    if(BDT_score_1100>0.99912){
        accept("SR_1_1100");
        if(numHiggs == 2) accept("SR_1_1100_2Higgs");
        if(numHiggs == 1) accept("SR_1_1100_1Higgs");
        if(numHiggs == 0) accept("SR_1_1100_0Higgs");
    } else if(BDT_score_1100>0.702774 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) ){
        accept("VR_tt_1100");
        if(numHiggs == 2) accept("VR_tt_1100_2Higgs");
        if(numHiggs == 1) accept("VR_tt_1100_1Higgs");
        if(numHiggs == 0) accept("VR_tt_1100_0Higgs");
    } else if(BDT_score_1100>0.911338 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("VR_st_1100");
        if(numHiggs == 2) accept("VR_st_1100_2Higgs");
        if(numHiggs == 1) accept("VR_st_1100_1Higgs");
        if(numHiggs == 0) accept("VR_st_1100_0Higgs");
    } else if (BDT_score_1100>0.295986 && mTbmin>200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_st_1100");
        if(numHiggs == 2) accept("CR_st_1100_2Higgs");
        if(numHiggs == 1) accept("CR_st_1100_1Higgs");
        if(numHiggs == 0) accept("CR_st_1100_0Higgs");
    } else if(BDT_score_1100>0.306246 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25==3){
        accept("CR_tt3b_1100");
        if(numHiggs == 2) accept("CR_tt3b_1100_2Higgs");
        if(numHiggs == 1) accept("CR_tt3b_1100_1Higgs");
        if(numHiggs == 0) accept("CR_tt3b_1100_0Higgs");
    } else if(BDT_score_1100>0.0536611 && mTbmin<=200 && (mass_h1_hm<100||mass_h1_hm>150) && n_bjets_25>=4){
        accept("CR_tt4b_1100");
        if(numHiggs == 2) accept("CR_tt4b_1100_2Higgs");
        if(numHiggs == 1) accept("CR_tt4b_1100_1Higgs");
        if(numHiggs == 0) accept("CR_tt4b_1100_0Higgs");
    }


  }

  //LM
  if (numHiggs == 2){//TODO change to 2, this is for testing
  if(n_leptons == 0 && n_bjets_40 >= 4 && xwt >= 1.8 && n_leptons_med <= 1 && n_leptons_loose <= 2){//LM presel
    accept("Presel_LM");
    if(mass_h1_lm>0.0 && mass_h2_lm>0.0){
        if( ( ((mass_h1_lm-120.0)/(0.1*mass_h1_lm))*((mass_h1_lm-120.0)/(0.1*mass_h1_lm)) + ((mass_h2_lm-110.0)/(0.1*mass_h2_lm))*((mass_h2_lm-110.0)/(0.1*mass_h2_lm)))<(1.6*1.6) ){//SR
           accept("SR_low_allbins");
           if(met>20 && meff_rand_hc>560){
                accept("SR_low_150");
           }
           if(met>150 && meff_rand_hc>340){
                accept("SR_low_300");
           }
           if(met<20){
                if(meff_rand_hc<200){
                    accept("SR_low_bin1");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin2");                
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin3");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin4");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin5");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin6");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin7");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin8");
                }
           } else if(met<40){
                if(meff_rand_hc<200){
                    accept("SR_low_bin9");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin10");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin11");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin12");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin13");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin14");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin15");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin16");
                }
           } else if(met<60){
                if(meff_rand_hc<200){
                    accept("SR_low_bin17");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin18");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin19");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin20");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin21");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin22");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin23");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin24");
                }
           } else if(met<80){
                if(meff_rand_hc<200){
                    accept("SR_low_bin25");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin26");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin27");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin28");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin29");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin30");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin31");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin32");
                }
           } else if(met<100){
                if(meff_rand_hc<200){
                    accept("SR_low_bin33");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin34");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin35");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin36");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin37");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin38");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin39");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin40");
                }
           } else if(met<120){
                if(meff_rand_hc<200){
                    accept("SR_low_bin41");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin42");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin43");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin44");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin45");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin46");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin47");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin48");
                }
           } else if(met<140){
                if(meff_rand_hc<200){
                    accept("SR_low_bin49");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin50");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin51");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin52");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin53");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin54");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin55");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin56");
                }
           } else if(met<160){
                if(meff_rand_hc<200){
                    accept("SR_low_bin57");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin58");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin59");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin60");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin61");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin62");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin63");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin64");
                }
           } else if(met<180){
                if(meff_rand_hc<200){
                    accept("SR_low_bin65");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin66");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin67");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin68");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin69");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin70");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin71");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin72");
                }
           } else if(met<200){
                if(meff_rand_hc<200){
                    accept("SR_low_bin73");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin74");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin75");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin76");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin77");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin78");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin79");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin80");
                }
            } else if(met<13000){
                if(meff_rand_hc<200){
                    accept("SR_low_bin81");
                } else if (meff_rand_hc<260){
                    accept("SR_low_bin82");
                } else if (meff_rand_hc<340){
                    accept("SR_low_bin83");
                } else if (meff_rand_hc<440){
                    accept("SR_low_bin84");
                } else if (meff_rand_hc<560){
                    accept("SR_low_bin85");
                } else if (meff_rand_hc<700){
                    accept("SR_low_bin86");
                } else if (meff_rand_hc<860){
                    accept("SR_low_bin87");
                } else if (meff_rand_hc<13000){
                    accept("SR_low_bin88");
                }
            }
        }
    } 
  }
  }

  ntupVar("meff_rand_hc", meff_rand_hc);
  ntupVar("met", met);
  ntupVar("met_mu", met_mu);
  ntupVar("n_jets_40", n_jets_40);
  ntupVar("n_bjets_40", n_bjets_40);
  ntupVar("n_electrons", static_cast<int>(signalElectrons.size()));
  ntupVar("n_muons", static_cast<int>(signalMuons.size()));
  ntupVar("n_leptons", n_leptons);
  ntupVar("n_electrons_preor_loose", static_cast<int>(electrons_loose.size()));
  ntupVar("n_muons_preor_loose", static_cast<int>(muons_loose.size()));
  ntupVar("n_leptons_preor_loose", n_leptons_loose);
  ntupVar("n_electrons_preor_med", static_cast<int>(electrons_med.size()));
  ntupVar("n_muons_preor_med", static_cast<int>(muons_med.size()));
  ntupVar("n_leptons_preor_med", n_leptons_med);
  ntupVar("m_h1_lm", mass_h1_lm);
  ntupVar("m_h2_lm", mass_h2_lm);
  ntupVar("m_h1_hm", mass_h1_hm);
  ntupVar("m_h2_hm", mass_h2_hm);
  ntupVar("dr_h1_hm", dr_h1_hm);
  ntupVar("dr_h2_hm", dr_h2_hm);
  ntupVar("xwt", xwt);
  ntupVar("BDT_score_200", BDT_score_200);
  ntupVar("BDT_score_250", BDT_score_250);
  ntupVar("BDT_score_300", BDT_score_300);
  ntupVar("BDT_score_400", BDT_score_400);
  ntupVar("BDT_score_500", BDT_score_500);
  ntupVar("BDT_score_600", BDT_score_600);
  ntupVar("BDT_score_700", BDT_score_700);
  ntupVar("BDT_score_800", BDT_score_800);
  ntupVar("BDT_score_900", BDT_score_900);
  ntupVar("BDT_score_1000", BDT_score_1000);
  ntupVar("BDT_score_1100", BDT_score_1100);

  ntupVar("numHiggs", numHiggs);

  ntupVar("mc_weight", event->getMCWeights()[0]);

  ntupVar("gen_met", gen_met);
  ntupVar("gen_ht", gen_ht);
  ntupVar("channel_number", channel_number);

  ntupVar("ht", ht);
  ntupVar("meffi", meffi);
  ntupVar("mjsum", mjsum);
  ntupVar("mTb_min", mTbmin);
  ntupVar("mT", mT);
  ntupVar("dphiMin4", dphiMin4);
  ntupVar("dphi1jet", dphi1jet);
  ntupVar("dr_min_bjets", dr_min_bjets);
  ntupVar("met_sig", met_sig);
  ntupVar("n_jets_25", n_jets_25);
  ntupVar("n_bjets_25", n_bjets_25);

  ntupVar("signalJets0_pass_BTag77DL1r", signalJets_25[0].pass(BTag77DL1r));
  ntupVar("signalJets0_Pt", signalJets_25[0].Pt());
  ntupVar("signalJets3_Pt", signalJets_25[3].Pt());

  ntupVar("truth_id0", jetFlavor(signalJets_25[0]));
  ntupVar("truth_id1", jetFlavor(signalJets_25[1]));
  ntupVar("truth_id2", jetFlavor(signalJets_25[2]));
  ntupVar("truth_id3", jetFlavor(signalJets_25[3]));

  ntupVar("small_R_jets_pt_1", m_pt_jet_1);
  ntupVar("small_R_jets_eta_1", m_eta_jet_1);
  ntupVar("small_R_jets_phi_1", m_phi_jet_1);
  ntupVar("small_R_jets_m_1", m_m_jet_1);
  ntupVar("small_R_jets_isb_1", m_isb_jet_1);
  ntupVar("small_R_jets_pt_2", m_pt_jet_2);
  ntupVar("small_R_jets_eta_2", m_eta_jet_2);
  ntupVar("small_R_jets_phi_2", m_phi_jet_2);
  ntupVar("small_R_jets_m_2", m_m_jet_2);
  ntupVar("small_R_jets_isb_2", m_isb_jet_2);
  ntupVar("small_R_jets_pt_3", m_pt_jet_3);
  ntupVar("small_R_jets_eta_3", m_eta_jet_3);
  ntupVar("small_R_jets_phi_3", m_phi_jet_3);
  ntupVar("small_R_jets_m_3", m_m_jet_3);
  ntupVar("small_R_jets_isb_3", m_isb_jet_3);
  ntupVar("small_R_jets_pt_4", m_pt_jet_4);
  ntupVar("small_R_jets_eta_4", m_eta_jet_4);
  ntupVar("small_R_jets_phi_4", m_phi_jet_4);
  ntupVar("small_R_jets_m_4", m_m_jet_4);
  ntupVar("small_R_jets_isb_4", m_isb_jet_4);
  ntupVar("small_R_jets_pt_5", m_pt_jet_5);
  ntupVar("small_R_jets_eta_5", m_eta_jet_5);
  ntupVar("small_R_jets_phi_5", m_phi_jet_5);
  ntupVar("small_R_jets_m_5", m_m_jet_5);
  ntupVar("small_R_jets_isb_5", m_isb_jet_5);
  ntupVar("small_R_jets_pt_6", m_pt_jet_6);
  ntupVar("small_R_jets_eta_6", m_eta_jet_6);
  ntupVar("small_R_jets_phi_6", m_phi_jet_6);
  ntupVar("small_R_jets_m_6", m_m_jet_6);
  ntupVar("small_R_jets_isb_6", m_isb_jet_6);
  ntupVar("small_R_jets_pt_7", m_pt_jet_7);
  ntupVar("small_R_jets_eta_7", m_eta_jet_7);
  ntupVar("small_R_jets_phi_7", m_phi_jet_7);
  ntupVar("small_R_jets_m_7", m_m_jet_7);
  ntupVar("small_R_jets_isb_7", m_isb_jet_7);
  ntupVar("small_R_jets_pt_8", m_pt_jet_8);
  ntupVar("small_R_jets_eta_8", m_eta_jet_8);
  ntupVar("small_R_jets_phi_8", m_phi_jet_8);
  ntupVar("small_R_jets_m_8", m_m_jet_8);
  ntupVar("small_R_jets_isb_8", m_isb_jet_8);
  ntupVar("small_R_jets_pt_9", m_pt_jet_9);
  ntupVar("small_R_jets_eta_9", m_eta_jet_9);
  ntupVar("small_R_jets_phi_9", m_phi_jet_9);
  ntupVar("small_R_jets_m_9", m_m_jet_9);
  ntupVar("small_R_jets_isb_9", m_isb_jet_9);
  ntupVar("small_R_jets_pt_10", m_pt_jet_10);
  ntupVar("small_R_jets_eta_10", m_eta_jet_10);
  ntupVar("small_R_jets_phi_10", m_phi_jet_10);
  ntupVar("small_R_jets_m_10", m_m_jet_10);
  ntupVar("small_R_jets_isb_10", m_isb_jet_10);


#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
  ntupVar("weight_3b_in", m_TTres.map_trf_weight_in["Nominal"].at(3));
  ntupVar("weight_4b_in", m_TTres.map_trf_weight_in["Nominal"].at(4));
#endif

  return;
}

float CalcXwt(const AnalysisObjects &bjets,
              const AnalysisObjects &nonbjets) {

  double xwt = 999;
  double pre_xwt = 999;
  // auto bjets_40_HC = filterObjects(bjets, 40., 2.5, BTag77DL1r,4);

  // Jet 1 is W jet, must NOT be HC jet (must NOT be in first 4 bjets)
  // Jet 2 is other W, can be any in bjets or nonbjets
  // Jet 3 is b, must be HC jet (must be in first 4 bjets)

  // Loop over jet for b in top decay
  for (unsigned int i = 0; i < 4; i++) {
    auto topbjet = bjets.at(i);
    // Jet 1 in W candidate. Cannot be a HC jet.
    for (unsigned int j = 0; j < nonbjets.size();
         j++) { // Test W1 jets that aren't bjets
      auto w1jet = nonbjets.at(j);
      // Jet 2 in W candidate. Can be any of the jets.
      for (unsigned int k = 0; k < nonbjets.size();
           k++) { // Test W2 that aren't bjets
        auto w2jet = nonbjets.at(k);
        // make sure w2 jet != w1 jet
        if (j == k) {
          pre_xwt = 999;
          // make sure w2 jet != topbjet. This is guaranteed as w2 is in nonb.
          // make sure w1 jet != topbjet. This is guaranteed as w1 is in nonb.
        } else {
          TLorentzVector vj_b;
          TLorentzVector vj_w1;
          TLorentzVector vj_w2;

          vj_b.SetPtEtaPhiE(topbjet.Pt(), topbjet.Eta(), topbjet.Phi(),
                            topbjet.E());
          vj_w1.SetPtEtaPhiE(w1jet.Pt(), w1jet.Eta(), w1jet.Phi(), w1jet.E());
          vj_w2.SetPtEtaPhiE(w2jet.Pt(), w2jet.Eta(), w2jet.Phi(), w2jet.E());
          // Combine jets to form t, W
          TLorentzVector vjjt, vjjw;
          vjjt = vj_b + vj_w1 + vj_w2;
          vjjw = vj_w1 + vj_w2;

          double mt = vjjt.M();
          double mW = vjjw.M();
          pre_xwt = pow(pow((mW - 80.4) / (0.1 * mW), 2) +
                            pow((mt - 172.5) / (0.1 * mt), 2),
                        0.5);
        }
        if (pre_xwt < xwt) { // Let jet combination with lowest xwt be our top
                             // candidate for this event
          xwt = pre_xwt;
        }
      }
      for (unsigned int k = 0; k < bjets.size(); k++) { // Test W2 that are
                                                        // bjets
        auto w2jet = bjets.at(k);
        // make sure w2 jet != w1 jet. This is guaranteed as w2 is in bjets, w1
        // in nonbjets. make sure w2 jet != topbjet.
        if (i == k) {
          pre_xwt = 999;
          // make sure w1 jet != topbjet. This is guaranteed as w1 is in nonb.
        } else {
          TLorentzVector vj_b;
          TLorentzVector vj_w1;
          TLorentzVector vj_w2;

          vj_b.SetPtEtaPhiE(topbjet.Pt(), topbjet.Eta(), topbjet.Phi(),
                            topbjet.E());
          vj_w1.SetPtEtaPhiE(w1jet.Pt(), w1jet.Eta(), w1jet.Phi(), w1jet.E());
          vj_w2.SetPtEtaPhiE(w2jet.Pt(), w2jet.Eta(), w2jet.Phi(), w2jet.E());
          // Combine jets to form t, W
          TLorentzVector vjjt, vjjw;
          vjjt = vj_b + vj_w1 + vj_w2;
          vjjw = vj_w1 + vj_w2;

          double mt = vjjt.M();
          double mW = vjjw.M();
          pre_xwt = pow(pow((mW - 80.4) / (0.1 * mW), 2) +
                            pow((mt - 172.5) / (0.1 * mt), 2),
                        0.5);
        }
        if (pre_xwt < xwt) { // Let jet combination with lowest xwt be our top
                             // candidate for this event
          xwt = pre_xwt;
        }
      }
    }
    for (unsigned int j = 0; j < bjets.size();
         j++) { // Test W1 jets that are bjets but aren't HC
      auto w1jet = bjets.at(j);
      // Jet 2 in W candidate. Can be any of the jets.
      for (unsigned int k = 0; k < nonbjets.size();
           k++) { // Test W2 that aren't bjets
        auto w2jet = nonbjets.at(k);
        // make sure w2 jet != w1 jet. This is guaranteed as w2 is in nonbjets,
        // w1 is in bjets. make sure w2 jet != topbjet. This is guaranteed as w2
        // is in nonb. make sure w1 jet != topbjet. Also, w1 can't be HC jet at
        // all.
        if (j < 4) {
          pre_xwt = 999;
        } else {
          TLorentzVector vj_b;
          TLorentzVector vj_w1;
          TLorentzVector vj_w2;

          vj_b.SetPtEtaPhiE(topbjet.Pt(), topbjet.Eta(), topbjet.Phi(),
                            topbjet.E());
          vj_w1.SetPtEtaPhiE(w1jet.Pt(), w1jet.Eta(), w1jet.Phi(), w1jet.E());
          vj_w2.SetPtEtaPhiE(w2jet.Pt(), w2jet.Eta(), w2jet.Phi(), w2jet.E());
          // Combine jets to form t, W
          TLorentzVector vjjt, vjjw;
          vjjt = vj_b + vj_w1 + vj_w2;
          vjjw = vj_w1 + vj_w2;

          double mt = vjjt.M();
          double mW = vjjw.M();
          pre_xwt = pow(pow((mW - 80.4) / (0.1 * mW), 2) +
                            pow((mt - 172.5) / (0.1 * mt), 2),
                        0.5);
        }
        if (pre_xwt < xwt) { // Let jet combination with lowest xwt be our top
                             // candidate for this event
          xwt = pre_xwt;
        }
      }
      for (unsigned int k = 0; k < bjets.size(); k++) { // Test W2 that are
                                                        // bjets
        auto w2jet = bjets.at(k);
        // make sure w2 jet != w1 jet.
        if (j == k) {
          pre_xwt = 999;
        } else if (i == k) {
          // make sure w2 jet != topbjet.
          pre_xwt = 999;
          // make sure w1 jet != topbjet. Also, w1 can't be HC jet at all.
        } else if (j < 4) {
          pre_xwt = 999;
        } else {
          TLorentzVector vj_b;
          TLorentzVector vj_w1;
          TLorentzVector vj_w2;

          vj_b.SetPtEtaPhiE(topbjet.Pt(), topbjet.Eta(), topbjet.Phi(),
                            topbjet.E());
          vj_w1.SetPtEtaPhiE(w1jet.Pt(), w1jet.Eta(), w1jet.Phi(), w1jet.E());
          vj_w2.SetPtEtaPhiE(w2jet.Pt(), w2jet.Eta(), w2jet.Phi(), w2jet.E());
          // Combine jets to form t, W
          TLorentzVector vjjt, vjjw;
          vjjt = vj_b + vj_w1 + vj_w2;
          vjjw = vj_w1 + vj_w2;

          double mt = vjjt.M();
          double mW = vjjw.M();
          pre_xwt = pow(pow((mW - 80.4) / (0.1 * mW), 2) +
                            pow((mt - 172.5) / (0.1 * mt), 2),
                        0.5);
        }
        if (pre_xwt < xwt) { // Let jet combination with lowest xwt be our top
                             // candidate for this event
          xwt = pre_xwt;
        }
      } // Finish loop over k
    }   // Finish loop over j
  }     // Finish loop over i
  return xwt;
}

std::pair<std::pair<double, double>, std::pair<double, double>>
heavy_pair_h_h(const AnalysisObject &heavy_b,
                              const AnalysisObject &bjet1,
                              const AnalysisObject &bjet2) {

  double mass_h1 = heavy_b.M();
  double mass_h2 = (bjet1 + bjet2).M();

  double dR_h1 = 0;
  double dR_h2 = bjet1.DeltaR(bjet2);

  std::pair<double, double> HMasses(-1, -1);
  std::pair<double, double> HdR(-1, -1);

  HMasses = std::pair<double, double>(mass_h1, mass_h2);
  HdR = std::pair<double, double>(dR_h1, dR_h2);
  std::pair<std::pair<double, double>, std::pair<double, double>> Higgs_info =
      std::make_pair(HMasses, HdR);

  return Higgs_info;
}

std::pair<std::pair<double, double>, std::pair<double, double>>
pair_h_h(const AnalysisObjects &bjets, unsigned int mode) {
  std::pair<double, double> HMasses(-1, -1);
  std::pair<double, double> HdR(-1, -1);
  std::vector<TLorentzVector> tlv_j;
  for (unsigned int i = 0; i < 4; i++) {
    TLorentzVector vj;
    auto bjet = bjets.at(i);
    vj.SetPtEtaPhiE(bjet.Pt(), bjet.Eta(), bjet.Phi(), bjet.E());
    tlv_j.push_back(vj);
  }

  double pt_max = 0;
  double dm = 999;
  double dm_dhh = 999;
  double dRmin = 999;
  double pt1 = 0, pt2 = 0;
  double mass_h1 = 0, mass_h2 = 0;
  double dR_h1 = 0, dR_h2 = 0;

  // define Lorentz vector and dR for two b-jet pairs (all 4 possibilities)
  TLorentzVector vjj1, vjj2;
  std::vector<int> index_appo = {1, 2, 3};
  // index_b1_h1 = 0;
  for (unsigned int i = 1; i < tlv_j.size(); i++) {
    vjj1 = tlv_j.at(0) + tlv_j.at(i);
    double dR1_appo = tlv_j.at(0).DeltaR(tlv_j.at(i));
    std::vector<int> index = index_appo;
    index.erase(index.begin() + (i - 1));
    vjj2 = tlv_j.at(index.at(0)) + tlv_j.at(index.at(1));
    double dR2_appo = tlv_j.at(index.at(0)).DeltaR(tlv_j.at(index.at(1)));

    double m1_appo = vjj1.M();
    double m2_appo = vjj2.M();
    double pt1_appo = vjj1.Pt();
    double pt2_appo = vjj2.Pt();
    if (mode == 1 && fabs(m1_appo - m2_appo) < dm) {
      dm = fabs(m1_appo - m2_appo);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
    }
    if (mode == 2 && (fabs(m1_appo - 115) < dm || fabs(m2_appo - 115) < dm)) {
      dm = fabs(m1_appo - 115);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
    }
    if ((mode == 3 || mode == 5) && std::max(dR1_appo, dR2_appo) < dRmin) {
      dRmin = std::max(dR1_appo, dR2_appo);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
      pt1 = pt1_appo;
      pt2 = pt2_appo;
    }
    if (mode == 4 && std::min(pt1_appo, pt2_appo) > pt_max) {
      pt_max = std::min(pt1_appo, pt2_appo);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
    }
    if (mode == 6 && (pt1_appo >= pt2_appo &&
                      fabs(m1_appo - 120. / 110. * m2_appo) < dm_dhh)) {
      dm_dhh = fabs(m1_appo - 120. / 110. * m2_appo);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
      pt1 = pt1_appo;
      pt2 = pt2_appo;
    }
    if (mode == 6 && (pt1_appo < pt2_appo &&
                      fabs(m2_appo - 120. / 110. * m1_appo) < dm_dhh)) {
      dm_dhh = fabs(m2_appo - 120. / 110. * m1_appo);
      mass_h1 = m1_appo;
      mass_h2 = m2_appo;
      dR_h1 = dR1_appo;
      dR_h2 = dR2_appo;
      pt1 = pt1_appo;
      pt2 = pt2_appo;
    }
  }

  if (mode < 5 && mass_h2 > mass_h1) {
    double mass_h1_appo = mass_h1;
    double dR_h1_appo = dR_h1;
    mass_h1 = mass_h2;
    dR_h1 = dR_h2;
    mass_h2 = mass_h1_appo;
    dR_h2 = dR_h1_appo;
  } else if (mode >= 5 && pt2 > pt1) { // change so pt(h1)>pt(h2)
    double mass_h1_appo = mass_h1;
    double dR_h1_appo = dR_h1;
    mass_h1 = mass_h2;
    dR_h1 = dR_h2;
    mass_h2 = mass_h1_appo;
    dR_h2 = dR_h1_appo;
  }

  HMasses = std::pair<double, double>(mass_h1, mass_h2);
  HdR = std::pair<double, double>(dR_h1, dR_h2);
  std::pair<std::pair<double, double>, std::pair<double, double>> Higgs_info =
      std::make_pair(HMasses, HdR);

  return Higgs_info;
}
