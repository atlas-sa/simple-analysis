#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopStau2018)

void StopStau2018::Init()
{
  addRegions(
    {
      // Preselections:
      "Presel_2tau",
      "Presel_1tau",

      // CRs:
      "CR_ttbar2Real_2tau",
      "CR_ttbar1Real_2tau",
      "CR_ttbar1Real_1tau",
      "CR_singletop_1tau",

      // VRs:
      "VR_ttbar2Real_2tau",
      "VR_ttbar1Real_2tau",
      "VR_ttbar1Real_1tau",
      "VR_singletop_1tau",

      // Discovery SRs:
      "SR_2tau",
      "SR_1tau",

      // Multi-bin 1-tau exclusion SR:
      "SRexcl_1tau",  // merged
      "SRexcl_1tau_50_100",
      "SRexcl_1tau_100_200",
      "SRexcl_1tau_200_plus",
      }
    );
}

void StopStau2018::ProcessEvent(AnalysisEvent *event)
{

  auto electrons = event->getElectrons(10, 2.47, ELooseBLLH | EZ05mm);
  auto muons     = event->getMuons(10, 2.7, MuMedium | MuZ05mm);
  auto taus      = filterCrack(event->getTaus(20., 2.5, TauRNNMedium), 1.37, 1.52);
  auto jets      = event->getJets(20., 2.8);
  auto metVec    = event->getMET();

  electrons = overlapRemoval(electrons, electrons, 0.05);
  taus      = overlapRemoval(taus, electrons, 0.2);
  taus      = overlapRemoval(taus, muons, 0.2);
  muons     = overlapRemoval(muons, electrons, 0.01, NOT(MuCaloTaggedOnly));
  electrons = overlapRemoval(electrons, muons, 0.01);
  jets      = overlapRemoval(jets, electrons, 0.2);
  electrons = overlapRemoval(electrons, jets, 0.4);
  jets      = overlapRemoval(jets, muons, 0.2);
  muons     = overlapRemoval(muons, jets, 0.4);
  jets      = overlapRemoval(jets, taus, 0.2);

  auto bjets = filterObjects(jets, 20., 2.5, BTag77DL1r);

  int n_electrons = electrons.size();
  int n_muons = muons.size();
  int n_taus = taus.size();
  int n_jets = jets.size();
  int n_bjets = bjets.size();
  float met = metVec.Pt();
  int mc_number = event->getMCNumber();
  float gen_met = event->getGenMET();
  float gen_ht = event->getGenHT();

  ntupVar("n_taus", n_taus);
  ntupVar("n_jets", n_jets);
  ntupVar("n_bjets", n_bjets);
  ntupVar("MET", met);
  ntupVar("SUSYProcess", event->getSUSYChannel());
  ntupVar("mcChannelNumber", mc_number);
  ntupVar("mcWeights", event->getMCWeights());
  ntupVar("GenFiltMET", gen_met);
  ntupVar("GenFiltHT", gen_ht);

  if (n_electrons > 0) return;
  if (n_muons > 0) return;
  if (n_taus < 1) return;
  if (n_jets < 2) return;
  if (n_bjets < 1) return;
  if (met < 250) return;

  sortObjectsByPt(taus);
  sortObjectsByPt(jets);
  sortObjectsByPt(bjets);

  // Remove GenFiltMET overlap:
  // (Requires merging with MET200/300/400 samples, see
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SUSYTheoreticalUncertainties)
  // ttbar:
  if (mc_number == 410470 && gen_met > 200) return;  // Nominal
  if ((mc_number == 410464 || mc_number == 410465) && gen_met > 200) return;  // Hard-scattering
  if ((mc_number == 410557 || mc_number == 410558) && gen_met > 200) return;  // Parton-showering
  // Single-top:
  if ((mc_number == 410646 || mc_number == 410647) && gen_met > 200) return;  // Nominal
  if ((mc_number == 410654 && gen_met > 300) || (mc_number == 410655 && gen_met > 200)) return;  // Wt-ttbar Interference

  float tau1pt = taus[0].Pt();
  float tau1mt = calcMT(taus[0], metVec);
  float jet1pt = jets[0].Pt();
  float jet2pt = jets[1].Pt();
  float jet1mt = calcMT(jets[0], metVec);
  float jet2mt = calcMT(jets[1], metVec);

  float st = tau1pt + jet1pt + jet2pt;
  float mtsumbb = 0;
  if (n_bjets >= 2) mtsumbb = calcMT(bjets[0], metVec) + calcMT(bjets[1], metVec);

  float tau2pt = 0;
  float tau2mt = 0;
  bool OS = false;
  float minvtautau = 0;
  float mt2tautau = 0;
  if (n_taus >= 2) {
    tau2pt = taus[1].Pt();
    tau2mt = calcMT(taus[1], metVec);
    OS = taus[0].charge() != taus[1].charge();
    minvtautau = (taus[0] + taus[1]).M();
    mt2tautau = calcMT2(taus[0], taus[1], metVec);
    st += tau2pt;
  }

  ntupVar("OS", OS);
  ntupVar("tau1Pt", tau1pt);
  ntupVar("tau2Pt", tau2pt);
  ntupVar("tau1Mt", tau1mt);
  ntupVar("tau2Mt", tau2mt);
  ntupVar("jet1Pt", jet1pt);
  ntupVar("jet2Pt", jet2pt);
  ntupVar("jet1Mt", jet1mt);
  ntupVar("jet2Mt", jet2mt);
  ntupVar("sT", st);
  ntupVar("mtsumbb", mtsumbb);
  ntupVar("mt2tautau", mt2tautau);
  ntupVar("minvtautau", minvtautau);

  bool Presel_2tau = met > 250 && n_electrons + n_muons == 0 && n_jets >=2 && n_taus >= 2 && n_bjets >= 1;
  bool Presel_1tau = met > 250 && n_electrons + n_muons == 0 && n_jets >=2 && n_taus == 1 && n_bjets >= 2;

  // Preselections:
  if (Presel_2tau) accept("Presel_2tau");
  if (Presel_1tau) accept("Presel_1tau");

  // CRs:
  if (
    Presel_2tau && OS && mt2tautau < 35 && minvtautau > 50 && tau1mt > 50
  ) accept("CR_ttbar2Real_2tau");
  if (
    Presel_2tau && mt2tautau < 35 && minvtautau > 50 && tau1mt < 50
  ) accept("CR_ttbar1Real_2tau");
  if (
    Presel_1tau && met > 280 && st > 500 && st < 600 && mtsumbb > 600 && mtsumbb < 700
  ) accept("CR_ttbar1Real_1tau");
  if (
    Presel_1tau && met > 280 && mtsumbb < 800 && tau1mt < 50 && tau1pt > 80
  ) accept("CR_singletop_1tau");

  // VRs:
  if (
    Presel_2tau && OS && mt2tautau > 35 && mt2tautau < 70 && tau1mt > 70
  ) accept("VR_ttbar2Real_2tau");
  if (
    Presel_2tau && mt2tautau > 35 && mt2tautau < 70 && tau1mt < 70
  ) accept("VR_ttbar1Real_2tau");
  if (
    Presel_1tau && met > 280 && st > 600 && mtsumbb > 600 && mtsumbb < 700
  ) accept("VR_ttbar1Real_1tau");
  if (
    Presel_1tau && met > 280 && mtsumbb > 800 && tau1mt > 50 && tau1mt < 150 && tau1pt > 80
  ) accept("VR_singletop_1tau");

  // Discovery SRs:
  if (Presel_2tau && met > 280 && OS && mt2tautau > 70) accept("SR_2tau");
  if (Presel_1tau && met > 280 && st > 800 && mtsumbb > 700 && tau1mt > 300) accept("SR_1tau");

  // Multi-bin 1-tau exclusion SR:
  bool SRexcl_1tau = Presel_1tau && met > 280 && st > 600 && mtsumbb > 700 && tau1mt > 150 && tau1pt > 50;
  if (SRexcl_1tau) accept("SRexcl_1tau");
  if (SRexcl_1tau && tau1pt < 100) accept("SRexcl_1tau_50_100");
  if (SRexcl_1tau && tau1pt > 100 && tau1pt < 200) accept("SRexcl_1tau_100_200");
  if (SRexcl_1tau && tau1pt > 200) accept("SRexcl_1tau_200_plus");

  return;

}

