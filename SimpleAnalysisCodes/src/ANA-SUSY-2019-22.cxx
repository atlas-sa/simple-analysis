#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(SS3L2020)



static float GetSFOSmass(AnalysisObjects& leptons)
{
    float c(0);
    int nLep = leptons.size();
    switch(nLep){
        case 2:
            c = 0;
            break;
        case 3:
	  if ( leptons[0].charge() != leptons[1].charge() && leptons[0].type()==leptons[1].type() ) {
	    c=(leptons[0]+leptons[1]).M();
	  }
	  if ( leptons[0].charge() != leptons[2].charge() && leptons[0].type()==leptons[2].type() ) {
	    float calt=(leptons[0]+leptons[2]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[2].charge() != leptons[1].charge() && leptons[2].type()==leptons[1].type() ) {
	    float calt=(leptons[1]+leptons[2]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  break;
        case 4:
	  if ( leptons[0].charge() != leptons[1].charge() && leptons[0].type()==leptons[1].type() ) {
	    float calt=(leptons[0]+leptons[1]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[0].charge() != leptons[2].charge() && leptons[0].type()==leptons[2].type() ) {
	    float calt=(leptons[0]+leptons[2]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[0].charge() != leptons[3].charge() && leptons[0].type()==leptons[3].type() ) {
	    float calt=(leptons[0]+leptons[3]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[1].charge() != leptons[2].charge() && leptons[1].type()==leptons[2].type() ) {
	    float calt=(leptons[1]+leptons[2]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[1].charge() != leptons[3].charge() && leptons[1].type()==leptons[3].type() ) {
	    float calt=(leptons[1]+leptons[3]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  if ( leptons[2].charge() != leptons[3].charge() && leptons[2].type()==leptons[3].type() ) {
	    float calt=(leptons[2]+leptons[3]).M();
	    if (fabs(calt-91)<fabs(c-91)) c=calt;
	  }
	  break;
    
    default: //FIXME? doesn't handle more than 4 leptons?
      break;
    }
    return c;
}

static float getDeltaRLepLep(AnalysisObjects& leptons, bool ForSS, int Flag){
    float dR(-99.), dR_min(99.), dR_max(-99.);
    if(leptons.size()==0) return dR;
    if(leptons.size() < 2) return dR;
	
    int charges_size = leptons.size();	
    if(!ForSS) {dR = leptons.at(0).DeltaR(leptons.at(1)); return dR;}
    else{
      for(int i=0; i<charges_size-1; i++){
	for(int j=i+1; j<charges_size; j++){
	  if(leptons[i].charge()*leptons[j].charge()>0){
	    dR = leptons.at(i).DeltaR(leptons.at(j));
	    if (Flag == 0) { break; return dR;}
	    else {
	      if (dR<dR_min) dR_min = dR;
	      if (dR>dR_max) dR_max = dR;
	    }
	  }
	}
      }
      if (Flag == 0 ) return dR;
      else if (Flag == 1 ) return  dR_max;
      else return dR_min;
    }
}
	
static bool SSleptons(AnalysisObjects& leptons)
{
  bool passCharges(false);
  unsigned int LepSize = leptons.size();
  if( LepSize<2 ) {
    passCharges=false;
  }
  if(LepSize==2){
    passCharges = leptons[0].charge()*leptons[1].charge()>0;
  }
  if(LepSize>2){
    passCharges = true;
  }
  return passCharges;
}

static float getDeltaRLepJet(AnalysisObjects& leptons, AnalysisObjects& jets, int N, bool useLep){
    float minDR = 99., dR=-99., maxDR = -100.0;
		bool getMax=false;

    if (!useLep){

        if (N>=0) {

            if (int(leptons.size())<(N+1)) return -99.;
            for (unsigned int j=0;j<jets.size();j++){

	      if (leptons.at(N).DrEtaPhi(jets.at(j))<minDR && jets.at(j).Pt()>25){
		minDR=leptons.at(N).DrEtaPhi(jets.at(j));
		
	      }
	      if (leptons.at(N).DrEtaPhi(jets.at(j))>maxDR && jets.at(j).Pt()>25){
		maxDR=leptons.at(N).DrEtaPhi(jets.at(j));

	      }
            }
        }
        else {
            for (int j=0;j<int(jets.size());j++){
                for (int i=0;i<int(leptons.size());i++){
		  dR = leptons.at(i).DrEtaPhi(jets.at(j));	
		  if (dR<minDR) minDR = dR;
		  if (dR>maxDR) maxDR = dR;

                }
            }
        }
    }
    else {

        if (int(jets.size())<(N+1)) return -99.;
        for (unsigned int j=0;j<leptons.size();j++){
	  if (jets.at(N).DrEtaPhi(leptons.at(j))<minDR){
	    minDR=leptons.at(j).DrEtaPhi(jets.at(N));
	  }
	  if (jets.at(N).DrEtaPhi(leptons.at(j))>maxDR){
	    maxDR=leptons.at(j).DrEtaPhi(jets.at(N));
	  }
        }
    }
    if (!getMax) return minDR;
    else return maxDR;
}
static int isSSLepPtCut(AnalysisObjects& leptons){
  int isAbove30 = 0;
  int isSSpair = 0;
  for(unsigned int i(0); i<leptons.size(); i++){
    for(unsigned int j(0); j<leptons.size(); j++){
      if(i==j || isSSpair==1)
	continue;
      if(leptons[i].charge()==leptons[j].charge())
	isSSpair = 1;
      if(isSSpair==1 && leptons.at(i).Pt()>30 && leptons.at(j).Pt()>30)
	isAbove30=1;
    }
  }
  return isAbove30;
}

/**********************************init***************************************/

void SS3L2020::Init() {
  addRegion("preselection");
  addRegion("CRWZ2j");
  addRegion("VRTTV");
  addRegion("VRWZ4j");
  addRegion("VRWZ5j");
  
  addRegion("bRPV2L");
  addRegion("bRPV3L");
  addRegion("preselection1B");
  addRegion("preselection2B");
  addRegion("preselection3B");
  addRegion("Rpv2L1bL");
  addRegion("Rpv2L1bM");
  addRegion("Rpv2L2bL");
  addRegion("Rpv2L2bM");
  addRegion("Rpv2L2bH");
  addRegion("Rpv2L3bL");
  addRegion("Rpv2L3bM");
  addRegion("Rpv2L3bH");
  addRegion("SRWZonshell1");
  addRegion("SRWZonshell2");
  
  addRegion("preCRWZ");
  addHistogram("met",25,0,250);
  addHistogram("SumJetPt",20,0,1000);
  addHistogram("Pt_l",20,0,1000);
  addHistogram("SumBJetPt",20,0,1000);
  addHistogram("sumPtLep",20,0,1000);
  addHistogram("dRl1j",10,0,5);
  addHistogram("mSFOS",20,0,200);
  addHistogram("Mll",20,0,1000);
  addHistogram("meff",30,0,1500);
  addHistogram("mjj",20,0,1000);
  addHistogram("Pt_jet",25,0,500);
  addHistogram("nJets25",10,0,10);
  addHistogram("nBjets20",10,0,10);
  addHistogram("mT2",20,0,500);
}

void SS3L2020::ProcessEvent(AnalysisEvent *event){
//	double weights = event->getMCWeights()[0];

	auto baselineElectrons = filterCrack(event->getElectrons(10, 2.47, ELooseBLLH|ED0Sigma5|EZ05mm));
	auto baselineMuons = event->getMuons(10, 2.5, MuMedium|MuZ05mm|MuQoPSignificance|MuNotCosmic);

	auto Jets = event->getJets(20, 2.8,JVTTight|LooseBadJet);
	auto overlapBjet = filterObjectsRange(Jets,0,100,-99,99,BTag70DL1r);

//********overlap removal******************//
	auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ){ return std::min(0.4, 0.1 + 9.6/muon.Pt()); };
	auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.1 + 9.6/elec.Pt()); };

	baselineElectrons=overlapRemoval(baselineElectrons, baselineElectrons , 0.05);
	baselineElectrons=overlapRemoval(baselineElectrons, baselineMuons , 0.01);
	Jets = overlapRemoval(Jets, baselineElectrons, 0.2, NOT(BTag70DL1r));
	baselineElectrons = overlapRemoval(baselineElectrons, Jets, radiusCalcElec);

	Jets = overlapRemoval(Jets,baselineMuons,0.4,LessThan3Tracks);
	baselineMuons = overlapRemoval(baselineMuons, Jets, radiusCalcMuon);
	baselineElectrons=overlapRemoval(baselineElectrons, overlapBjet , 0.2);

	auto signalElectrons = filterObjects(baselineElectrons, 10, 2.0, EMediumLH|EIsoFCTight);
	auto signalMuons = filterObjects(baselineMuons,10, 2.5, MuIsoFCTightTrackOnly|MuD0Sigma3);
	auto Bjets = filterObjects(Jets,20,2.5,BTag70DL1r);

	auto CombiElectrons = event->getElectrons(4.5, 2.47, ELooseBLLH);
	auto CombiMuons = event->getMuons(3, 2.7, MuMedium);

	auto CombiLepton = CombiElectrons+CombiMuons;
	auto baseLeptons = baselineMuons+baselineElectrons;
	auto signalLeptons = signalElectrons+signalMuons;
	auto metVec = event->getMET();
	double metSignificance =event->getMETSignificance();
	double met = metVec.Et();
	int nBjets = Bjets.size();
	float Pt_Bjet=0;
	if(nBjets>0)Pt_Bjet=Bjets[0].Pt();
	int nJets25  = countObjects(Jets, 25.); 
	int nJets40  = countObjects(Jets, 40.);
	int nJets50  = countObjects(Jets, 50.);
	int nSignalLep=signalLeptons.size();
	int nCombiLep=CombiLepton.size();
	int nBaseLep=baseLeptons.size();
	int nBjets20 = countObjects(Bjets,20);
	double meff = sumObjectsPt(Jets) + sumObjectsPt(signalLeptons) + met;

	float dRll =-99;
	float dRl1j=-99;
	float mjj(0),Mll(0);
	float sumPtLep=sumObjectsPt(signalLeptons);
	float SumJetPt=sumObjectsPt(Jets);
	float SumBJetPt=sumObjectsPt(Bjets);
/*********************************select********************************/
	if (signalLeptons.size()<2) return;
	if (baseLeptons.size()<2) return;
	if (Jets.size()<1) return;

	
	bool SameSign = SSleptons(signalLeptons);
	if(!SameSign)return;

	float mSFOS(0),Pt_jet(0),Pt_subjet(0),Pt_thirdl(0);\
	float MT2(0),mt_min(0);

	mSFOS=GetSFOSmass(signalLeptons);
	int isSS30 = isSSLepPtCut(signalLeptons);
	Pt_jet=Jets[0].Pt();
	if (Jets.size()>1) Pt_subjet=Jets[1].Pt();
	if (signalLeptons.size()>2) Pt_thirdl=signalLeptons[2].Pt();

	float Pt_l=signalLeptons[0].Pt();
	float Pt_subl=signalLeptons[1].Pt();
	mjj = Jets.size()>1 ? (Jets.at(0) + Jets.at(1)).M() : 0.;
	Mll = (signalLeptons.at(0) + signalLeptons.at(1)).M();

	MT2=calcAMT2(signalLeptons[0],signalLeptons[1],metVec,signalLeptons[0].M(),signalLeptons[1].M());

	mt_min = calcMTmin(signalLeptons,metVec);

	dRll  = getDeltaRLepLep(signalLeptons, false, 0);
	dRl1j = getDeltaRLepJet(signalLeptons, Jets, 0, false);
	
	bool bRPV2L,bRPV3L=false;
	bRPV2L=nSignalLep==2&&Pt_l>20&&Pt_subl>20&&met>=100&&MT2>=60&&nBjets20==0&&nJets25>=1&&nJets40>=4;
	bRPV3L=nSignalLep==3&&Pt_l>20&&Pt_subl>20&&Pt_thirdl>10&&met>=120&&meff>=350&&MT2>=80&&nJets25>=1&&(mSFOS<81||mSFOS>101);

/******************************higgsinoN1N2RPV*********************************/
	bool preselection1B=nSignalLep==2&&nBaseLep==2&&Pt_l>25&&Pt_subl>25&&nBjets20==1&&nJets25>=1&&nJets25<=6&&met>50;
	bool Rpv2L1bL=nJets25<=2&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.7)&&sumObjectsPt(Jets)>120&&dRl1j<1.2&&met>100;
	bool Rpv2L1bM=(nJets25==2||nJets25==3)&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.45)&&sumObjectsPt(Jets)>400&&dRl1j<1.0;

	bool preselection2B=nSignalLep==2&&nBaseLep==2&&Pt_l>25&&Pt_subl>25&&nBjets20==2&&nJets25>=1&&nJets25<=6;
	bool Rpv2L2bL=nJets25<=3&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.9)&&SumJetPt>300&&dRl1j<1.0;
	bool Rpv2L2bM=(nJets25==3||nJets25==4)&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.75)&&SumJetPt>420&&dRl1j<1.0;
	bool Rpv2L2bH=nJets25>=5&&SumJetPt>420&&dRl1j<1.0;

	bool preselection3B=nSignalLep==2&&nBaseLep==2&&Pt_l>25&&Pt_subl>25&&nBjets20>=3&&nJets25>=1&&nJets25<=6&&met>20;
	bool Rpv2L3bL=nJets25<=3&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.8)&&dRll>2.0&&dRl1j<1.5;
	bool Rpv2L3bM=nJets25<=3&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.8);
	bool Rpv2L3bH=nJets25<=6&&(sumObjectsPt(Bjets)/sumObjectsPt(Jets)>0.5)&&SumJetPt>350&&dRll>2.0&&dRl1j<1.0;
/*****************************wino C1N2 WZ on shell*****************************/
	bool SRWZonshell1=nCombiLep==2&&nSignalLep==2&&nBaseLep==2&&Pt_l>=25&&Pt_subl>=25&&nJets25>=1&&nBjets20==0&&mjj<=350&&MT2>=100&&mt_min>=100&&met>=100;
	bool SRWZonshell2=nCombiLep==2&&nSignalLep==2&&nBaseLep==2&&Pt_l>=25&&Pt_subl>=25&&nJets25>=1&&nBjets20==0&&mjj<=350&&MT2<=100&&mt_min>=130&&met>=140&&meff<=600&&dRll<=3;

	bool BlindingVeto=nBjets20<3 && !(nBjets20>=1 && nJets50>=4 && met>=130) && !(nBjets20==0 && nJets50>=3 && met>=130) && !(nBjets20==0 && nJets50>=5);
	bool CRWZ2j=nCombiLep==3&&nSignalLep==3&&nBaseLep==3&& Pt_l>20 && Pt_subl>20&&nBjets20==0&&nJets25>=2&&met>50&&met<150&&meff<1000&&mSFOS>81&&mSFOS<101&&Pt_l>20&&Pt_subl>20&&BlindingVeto;
	bool VRTTV=nBaseLep>=2&&nSignalLep>=2&& Pt_l>20 && Pt_subl>20&&nBjets20>=1&&nJets40>=3&&meff>=600&&isSS30&&dRl1j>1.1&&(SumBJetPt/SumJetPt>0.4)&&met/meff>0.1&&met<250&&meff<1500&&BlindingVeto;
	bool VRWZ4j=nCombiLep==3 && nSignalLep==3 && nBaseLep==3 && Pt_l>20 && Pt_subl>20 && nBjets20==0 && nJets25>=4 && met>50 && met<250 && meff>600 && meff<1500 && mSFOS>81 && mSFOS<101 &&BlindingVeto;
	bool VRWZ5j=nCombiLep==3 && nSignalLep==3 && nBaseLep==3 && Pt_l>20 && Pt_subl>20 && nBjets20==0 && nJets25>=5 && met>50 && met<250 && meff>400 && meff<1500 && mSFOS>81 && mSFOS<101 &&BlindingVeto;

	bool preCRWZ=nCombiLep==3&&nSignalLep==3&&nBaseLep==3;

	if(CRWZ2j)accept("CRWZ2j");
	if(VRTTV)accept("VRTTV");
	if(VRWZ4j)accept("VRWZ4j");
	if(VRWZ5j)accept("VRWZ5j");
	if(preCRWZ)accept("preCRWZ");

	if(bRPV2L)accept("bRPV2L");
	if(bRPV3L)accept("bRPV3L");
	if(preselection1B)accept("preselection1B");
	if(preselection2B)accept("preselection2B");
	if(preselection3B)accept("preselection3B");
	if(Rpv2L1bL&&preselection1B)accept("Rpv2L1bL");
	if(Rpv2L1bM&&preselection1B)accept("Rpv2L1bM");
	if(Rpv2L2bL&&preselection2B)accept("Rpv2L2bL");
	if(Rpv2L2bM&&preselection2B)accept("Rpv2L2bM");
	if(Rpv2L2bH&&preselection2B)accept("Rpv2L2bH");
	if(Rpv2L3bL&&preselection3B)accept("Rpv2L3bL");
	if(Rpv2L3bM&&preselection3B)accept("Rpv2L3bM");
	if(Rpv2L3bH&&preselection3B)accept("Rpv2L3bH");
	if(SRWZonshell1)accept("SRWZonshell1");
	if(SRWZonshell2)accept("SRWZonshell2");

	if(true){
		accept("preselection");
		fill("met",met);
		fill("SumJetPt",SumJetPt);
		fill("Pt_l",Pt_l);
		fill("SumBJetPt",SumBJetPt);
		fill("sumPtLep",sumPtLep);
		fill("dRl1j",dRl1j);
		fill("mSFOS",mSFOS);
		fill("Mll",Mll);
		fill("meff",meff);
		fill("mjj",mjj);
		fill("Pt_jet",Pt_jet);
		fill("nJets25",nJets25);
		fill("nBjets20",nBjets20);
		fill("mT2",MT2);
		ntupVar("Pt_subjet",Pt_subjet);
		ntupVar("Pt_jet",Pt_jet);
		ntupVar("Pt_l",Pt_l);
		ntupVar("Pt_subl",Pt_subl);
		ntupVar("Pt_thirdl",Pt_thirdl );
		ntupVar("met",met);
		ntupVar("meff",meff );
		ntupVar("nBjets20",nBjets20 );
		ntupVar("nJets25",nJets25);
		ntupVar("nJets40",nJets40);
		ntupVar("nJets50",nJets50);
		ntupVar("isSS30",isSS30);
		ntupVar("Pt_Bjet",Pt_Bjet );
		ntupVar("SumBJetPt", SumBJetPt);
		ntupVar("sumPtLep",sumPtLep );
		ntupVar("SumJetPt",SumJetPt );
		ntupVar("dRll",dRll);
		ntupVar("dRl1j",dRl1j );
		ntupVar("nSignalLep",nSignalLep );
		ntupVar("nBaseLep",nBaseLep );
		ntupVar("nCombiLep",nCombiLep );
		ntupVar("mjj",mjj );
		ntupVar("mSFOS",mSFOS );
		ntupVar("MT2",MT2 );
		ntupVar("mt_min",mt_min );
		ntupVar("met_Sig",metSignificance );
	}

	return;
}
