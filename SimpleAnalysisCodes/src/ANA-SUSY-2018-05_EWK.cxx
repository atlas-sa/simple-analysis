#include "SimpleAnalysisFramework/AnalysisClass.h"
DefineAnalysis(EwkTwoLeptonTwoJet2018)

// Select boson decays for GGM model.
#define ZHSTATUS ANY_CODE // modify this at compile time to select one; sorry.
enum BOSONS_CODE { ANY_CODE=0, ZZ_CODE=1, ZH_CODE=2, HH_CODE=3 };

void EwkTwoLeptonTwoJet2018::Init()
{
    // Presel
    addRegions({"Ewk_2Ljets_presel"});

    // Same-sign special case for fake validation
    addRegions({"VR_SS"});

    // Signal Regions (13)
    addRegions({
        "SR_High_8_a",
        "SR_High_8_b",
        "SR_High_16_a",
        "SR_High_16_b",
        "SR_High_4",
        "SR_llbb",
        "SR_Int_a",
        "SR_Int_b",
        "SR_Low_a",
        "SR_Low_b",
        "SR_Low_2",
        "SR_OffShell_a",
        "SR_OffShell_b"
    });

    // Validation Regions (8)
    addRegions({
        "VR_High_4",
        "VR_High",
        "VR_High_R",
        "VR_llbb",
        "VR_Int",
        "VR_Low",
        "VR_Low_2",
        "VR_OffShell"
    });

    // Control Regions (4)
    addRegions({
        "CR_VZ",
        "CR_tt",
        "CR_Z",
        "CR_DY"
    });

    // Extras
    addRegions({
        "VR_sys_main",
        "VR_sys_main_low",
        "VR_sys_main_int",
        "VR_sys_main_high",

        "VR_sys_bb",
        "VR_sys_bb_low",
        "VR_sys_bb_int",
        "VR_sys_bb_high",

        "VR_sys_off",
        "VR_sys_off_9",
    });

    // Histograms
    addHistogram("SR_High_8_a", 6, 0, 30);
    addHistogram("SR_High_8_b", 6, 0, 30);
    addHistogram("SR_High_16_a", 6, 0, 30);
    addHistogram("SR_High_16_b", 6, 0, 30);
    addHistogram("SR_High_4", 6, 0, 30);
    addHistogram("SR_llbb", 6, 0, 30);
    addHistogram("SR_Int_a", 6, 0, 30);
    addHistogram("SR_Int_b", 6, 0, 30);
    addHistogram("SR_Low_a", 6, 0, 30);
    addHistogram("SR_Low_b", 6, 0, 30);
    addHistogram("SR_Low_2", 6, 0, 30);
    addHistogram("SR_OffShell_a", 6, 0, 30);
    addHistogram("SR_OffShell_b", 6, 0, 30);

    addHistogram("VR_High", 6, 0, 30);
    addHistogram("VR_High_R", 6, 0, 30);
    addHistogram("VR_High_4", 6, 0, 30);
    addHistogram("VR_llbb", 6, 0, 30);
    addHistogram("VR_Int", 6, 0, 30);
    addHistogram("VR_Low", 6, 0, 30);
    addHistogram("VR_Low_2", 6, 0, 30);
    addHistogram("VR_OffShell", 6, 0, 30);

    addHistogram("CR_VZ", 6, 0, 30);
    addHistogram("CR_tt", 6, 0, 30);
    addHistogram("CR_Z", 6, 0, 30);
    addHistogram("CR_DY", 6, 0, 30);
}


void EwkTwoLeptonTwoJet2018::ProcessEvent(AnalysisEvent *event)
{
    bool isVR_SS = false;

    bool isSR_High_8_a = false;
    bool isSR_High_8_b = false;
    bool isSR_High_16_a = false;
    bool isSR_High_16_b = false;
    bool isSR_High_4 = false;
    bool isSR_llbb = false;
    bool isSR_Int_a = false;
    bool isSR_Int_b = false;
    bool isSR_Low_a = false;
    bool isSR_Low_b = false;
    bool isSR_Low_2 = false;
    bool isSR_OffShell_a = false;
    bool isSR_OffShell_b = false;
    bool isVR_High_4 = false;
    bool isVR_High = false;
    bool isVR_High_R = false;
    bool isVR_llbb = false;
    bool isVR_Int = false;
    bool isVR_Low = false;
    bool isVR_Low_2 = false;
    bool isVR_OffShell = false;
    bool isCR_VZ = false;
    bool isCR_tt = false;
    bool isCR_Z = false;
    bool isCR_DY = false;

    bool isVR_sys_main = false;
    bool isVR_sys_main_low = false;
    bool isVR_sys_main_int = false;
    bool isVR_sys_main_high = false;
    bool isVR_sys_bb = false;
    bool isVR_sys_bb_low = false;
    bool isVR_sys_bb_int = false;
    bool isVR_sys_bb_high = false;
    bool isVR_sys_off = false;
    bool isVR_sys_off_9 = false;

    // Gather Objects
    auto metVec = event->getMET();
    double met = metVec.Et();

    auto electrons = event->getElectrons(4.5, 2.47, ELooseLH | EZ05mm);
    auto muons = event->getMuons(3, 2.7, MuMedium | EZ05mm);
    auto candJets = event->getJets(20., 2.8);
    auto baselineLeptons = electrons + muons;
    sortObjectsByPt(baselineLeptons);
    int n_baseline_leptons = baselineLeptons.size();

    // should be medium working point for pt<120, and tight for eta>2.5 and pt>50
    candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);

    // Overlap removal
    auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) {
        return std::min(0.4, 0.04 + 10 / muon.Pt());
    };
    auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) {
        return std::min(0.4, 0.04 + 10 / muon.Pt());
    };
    auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) {
        return std::min(0.4, 0.04 + 10 / elec.Pt());
    };

    electrons  = overlapRemoval(electrons, muons, 0.01);
    candJets = overlapRemoval(candJets, electrons, 0.2, NOT(BTag77MV2c10));
    electrons = overlapRemoval(electrons, candJets, radiusCalcElec);
    candJets = overlapRemoval(candJets, muons, radiusCalcJet, LessThan3Tracks);
    muons = overlapRemoval(muons, candJets, radiusCalcMuon);

    int nTruthJets20 = candJets.size();

    auto signalJets = filterObjects(candJets, 30);
    auto signalJets20 = filterObjects(candJets, 20);
    auto signalElectrons = filterObjects(electrons, 25, 2.47, EMediumLH | ED0Sigma5 | EZ05mm | EIsoBoosted);
    auto signalMuons = filterObjects(muons, 25, 2.6, MuD0Sigma3 | MuZ05mm | MuIsoBoosted | MuNotCosmic);

    // combine into signalLeptons
    auto signalLeptons = signalElectrons + signalMuons;
    sortObjectsByPt(signalJets);
    sortObjectsByPt(signalLeptons);

    int n_jets = signalJets.size();
    int n_leptons = signalLeptons.size();

    auto bjets = filterObjects(signalJets20, 20, 2.5, BTag77MV2c10);
    int nbjets = bjets.size();

    // Spliting into ZZ, ZH and HH decay channels.
    int nZBosons = 0;
    int nHBosons = 0;

    auto bosons = event->getHSTruth();

    for (auto b : bosons) {
        if (b.pdgId() == 23)
            nZBosons++;
        if (b.pdgId() == 25)
            nHBosons++;
    }

    bool ZZ = (nZBosons == 2 && nHBosons == 0);
    bool ZH = (nZBosons == 1 && nHBosons == 1);
    bool HH = (nZBosons == 0 && nHBosons == 2);


    /* Preselection */
    // Require:
    // exactly two leptons (> 25 GeV)
    // both leptons of the Same Flavour (SF)
    // both leptons of Opposite Sign (OS) charge *** except VR_SS
    // lepton pair mass in (12, 111)
    // at least one jet (> 30 GeV)
    // missing energy: met > 100 GeV, met significance > 6

    // exactly two leptons (> 25 GeV)
    if (n_leptons != 2 || n_baseline_leptons != 2)
        return;

    // both leptons of the Same Flavour (SF)
    // since we have exactly two leptons,
    // 0 electron <=> 2 muon => SF
    // 1 electron <=> 1 muon => DF
    // 2 electron <=> 0 muon => SF
    if (signalElectrons.size() == 1)
        return;
    // common lepton pair mass selection
    TLorentzVector dilepton = signalLeptons[0] + signalLeptons[1];
    auto mll = dilepton.M();

    // lepton pair mass in (12, 111)
    if (!(12 < mll && mll < 111))
        return;

    // at least one jet (> 30 GeV)
    if (n_jets < 1)
        return;

    // missing energy: met > 100 GeV, met significance > 6
    if (!(met > 100))
        return;

    auto metsig = event->getMETSignificance();

    if (!(metsig > 6))
        return;

    // compile-time choice of ZH content
    if (ZHSTATUS == ZZ_CODE && !ZZ)
        return;
    if (ZHSTATUS == ZH_CODE && !ZH)
        return;
    if (ZHSTATUS == HH_CODE && !HH)
        return;


    /* More event variables */
    auto dPhiPllMet = fabs(dilepton.DeltaPhi(metVec));
    auto dphiJ1met = fabs(signalJets[0].DeltaPhi(metVec));

    float mjj = 0.;
    float Rjj = 0.;
    if (n_jets >= 2) {
        TLorentzVector dijets = signalJets[0] + signalJets[1];
        mjj = dijets.M();
        Rjj = fabs(signalJets[0].DeltaR(signalJets[1]));
    }

    float mbb = 0.;
    if (nbjets >= 2) {
        TLorentzVector dibjets = bjets[0] + bjets[1];
        mbb = dibjets.M();
    }

    float jetpt1 = signalJets[0].Pt();
    float jetm1 = signalJets[0].M();

    auto Rll = fabs(signalLeptons[0].DeltaR(signalLeptons[1]));

    auto mt2 = calcMT2(signalLeptons[0], signalLeptons[1], metVec);

    /* Same-sign special case */
    if (signalLeptons[0].charge() == signalLeptons[1].charge()
        && n_jets >= 2
        && nbjets <= 1) {

        accept("VR_SS");
        isVR_SS = true;
    }
    ntupVar("isVR_SS", isVR_SS);

    // *** both leptons of Opposite Signed charge (OS) *** except VR_SS

    // both leptons of Opposite Sign (OS) charge
    if (signalLeptons[0].charge() == signalLeptons[1].charge())
        return;

    accept("Ewk_2Ljets_presel");

    /* Signal Regions */

    // SR High presel
    if (71 < mll && mll < 111
        && mt2 > 80) {

        // SR_High_8_a
        if (n_jets >= 2
            && 18 < metsig && metsig < 21
            && Rjj < 0.8
            && 60 < mjj && mjj < 110
            && nbjets <= 1) {

            accept("SR_High_8_a");
            isSR_High_8_a = true;
            fill("SR_High_8_a", metsig);
        }

        // SR_High_8_b
        if (n_jets >= 2
            && metsig > 21
            && Rjj < 0.8
            && 60 < mjj && mjj < 110
            && nbjets <= 1) {

            accept("SR_High_8_b");
            isSR_High_8_b = true;
            fill("SR_High_8_b", metsig);
        }

        // SR_High_16_a
        if (n_jets >= 2
            && 18 < metsig && metsig < 21
            && 0.8 < Rjj && Rjj < 1.6
            && 60 < mjj && mjj < 110
            && nbjets <= 1) {

            accept("SR_High_16_a");
            isSR_High_16_a = true;
            fill("SR_High_16_a", metsig);
        }

        // SR_High_16_b
        if (n_jets >= 2
            && metsig > 21
            && 0.8 < Rjj && Rjj < 1.6
            && 60 < mjj && mjj < 110
            && nbjets <= 1) {

            accept("SR_High_16_b");
            isSR_High_16_b = true;
            fill("SR_High_16_b", metsig);
        }

        // SR_High_4
        if (n_jets == 1
            && metsig > 12
            && 60 < jetm1 && jetm1 < 110
            && nbjets <= 1) {

            accept("SR_High_4");
            isSR_High_4 = true;
            fill("SR_High_4", metsig);
        }

        // SR_llbb
        if (n_jets >= 2
            && metsig > 18
            && 60 < mbb && mbb < 150
            && nbjets >= 2) {

            accept("SR_llbb");
            isSR_llbb = true;
            fill("SR_llbb", metsig);
        }

    } // End SR High


    // SR Int presel
    if (81 < mll && mll < 101
        && mt2 > 80
        && n_jets >= 2
        && nbjets == 0
        && 60 < mjj && mjj < 110
        && jetpt1 > 60) {

        // SR_Int_a
        if (12 < metsig && metsig < 15) {

            accept("SR_Int_a");
            isSR_Int_a = true;
            fill("SR_Int_a", metsig);
        }

        // SR_Int_b
        if (15 < metsig && metsig < 18) {

            accept("SR_Int_b");
            isSR_Int_b = true;
            fill("SR_Int_b", metsig);
        }

    } // End SR Int


    // SR Low presel
    if (81 < mll && mll < 101
        && n_jets == 2
        && nbjets == 0
        && 60 < mjj && mjj < 110) {

        // SR_Low_a
        if (mt2 > 80
            && 6 < metsig && metsig < 9
            && Rll < 1) {

            accept("SR_Low_a");
            isSR_Low_a = true;
            fill("SR_Low_a", metsig);
        }

        // SR_Low_b
        if (mt2 > 80
            && 9 < metsig && metsig < 12
            && Rll < 1) {

            accept("SR_Low_b");
            isSR_Low_b = true;
            fill("SR_Low_b", metsig);
        }

        // SR_Low_2
        if (mt2 < 80
            && 6 < metsig && metsig < 9
            && Rll < 1.6
            && dPhiPllMet < 0.6) {

            accept("SR_Low_2");
            isSR_Low_2 = true;
            fill("SR_Low_2", metsig);
    }

    } // End SR Low


    // SR Off Shell
    if (metsig > 9
        && mt2 > 100
        && n_jets >= 2
        && nbjets == 0
        && jetpt1 > 100
        && dphiJ1met > 2) {

        // SR_OffShell_a
        if (12 < mll && mll < 40) {

            accept("SR_OffShell_a");
            isSR_OffShell_a = true;
            fill("SR_OffShell_a", metsig);
        }

        // SR_OffShell_b
        if (40 < mll && mll < 71) {

            accept("SR_OffShell_b");
            isSR_OffShell_b = true;
            fill("SR_OffShell_b", metsig);
        }

    } // End SR Off Shell


    /* Validation Regions */

    // VR_High
    if (metsig > 18
        && 71 < mll && mll < 111
        && mt2 > 80
        && n_jets >= 2
        && nbjets <= 1
        && 20 < mjj && !(60 < mjj && mjj < 110)
        && Rjj < 1.6) {

        accept("VR_High");
        isVR_High = true;
        fill("VR_High", metsig);
    }

    // VR_High_R
    if (metsig > 18
        && 71 < mll && mll < 111
        && mt2 > 80
        && n_jets >= 2
        && nbjets <= 1
        && mjj > 20
        && Rjj > 1.6) {

        accept("VR_High_R");
        isVR_High_R = true;
        fill("VR_High_R", metsig);
    }

    // VR_High_4
    if (metsig > 12
        && 71 < mll && mll < 111
        && mt2 > 80
        && n_jets == 1
        && nbjets <= 1
        && jetm1 > 20 && !(60 < jetm1 && jetm1 < 110)) {

        accept("VR_High_4");
        isVR_High_4 = true;
        fill("VR_High_4", metsig);
    }

    // VR_llbb
    if (12 < metsig && metsig < 18
        && 71 < mll && mll < 111
        && mt2 > 80
        && nbjets >= 2
        && n_jets >= 2
        && 60 < mbb && mbb < 150) {

        accept("VR_llbb");
        isVR_llbb = true;
        fill("VR_llbb", metsig);
    }

    // VR_Int
    if (12 < metsig && metsig < 18
        && 81 < mll && mll < 101
        && mt2 > 80
        && nbjets == 0
        && n_jets >= 2
        && jetpt1 < 60
        && 60 < mjj && mjj < 110) {

        accept("VR_Int");
        isVR_Int = true;
        fill("VR_Int", metsig);
    }

    // VR_Low
    if (6 < metsig && metsig < 12
        && 81 < mll && mll < 101
        && mt2 > 80
        && 1 < Rll && Rll < 1.4
        && n_jets == 2
        && nbjets == 0
        && 60 < mjj && mjj < 110) {

        accept("VR_Low");
        isVR_Low = true;
        fill("VR_Low", metsig);
    }

    // VR_Low_2
    if (6 < metsig && metsig < 9
        && 81 < mll && mll < 101
        && mt2 < 80
        && Rll < 1.6
        && dPhiPllMet < 0.6
        && n_jets == 2
        && nbjets == 0
        && mjj > 20 && !(60 < mjj && mjj < 110.)) {

        accept("VR_Low_2");
        isVR_Low_2 = true;
        fill("VR_Low_2", metsig);
    }

    // VR_OffShell
    if (metsig > 9
        && 12 < mll && mll < 71
        && 80 < mt2 && mt2 < 100
        && n_jets >= 2
        && nbjets == 0
        && jetpt1 > 100
        && dphiJ1met > 2.) {

        accept("VR_OffShell");
        isVR_OffShell = true;
        fill("VR_OffShell", metsig);
    }

    /* Control Regions */

    // CR_VZ
    if (12 < metsig && metsig < 18
        && 81 < mll && mll < 101
        && mt2 > 80
        && n_jets >= 2
        && nbjets == 0
        && mjj > 20 && !(60 < mjj && mjj < 110.)) {

        accept("CR_VZ");
        isCR_VZ = true;
        fill("CR_VZ", metsig);
    }

    // CR_tt
    if (9 < metsig && metsig < 12
        && 81 < mll && mll < 101
        && mt2 > 80
        && n_jets >= 2
        && nbjets >= 1
        && jetpt1 > 60
        && mjj > 20) {

        accept("CR_tt");
        isCR_tt = true;
        fill("CR_tt", metsig);
    }

    // CR_Z
    if (6 < metsig && metsig < 9
        && 81 < mll && mll < 101
        && mt2 > 80
        && Rll < 1.
        && n_jets == 2
        && nbjets == 0
        && mjj > 20 && !(60 < mjj && mjj < 110.)) {

        accept("CR_Z");
        isCR_Z = true;
        fill("CR_Z", metsig);
    }

    // CR_DY
    if (6 < metsig && metsig < 9
        && 12 < mll && mll < 71
        && mt2 > 100
        && n_jets >= 2
        && nbjets == 0) {
        accept("CR_DY");
        isCR_DY = true;
        fill("CR_DY", metsig);
    }

    /* Looser regions for theory variation evaluation. */
    if (n_jets >= 2
        && nbjets <= 1
        && 60 < mjj && mjj < 110.) {

        accept("VR_sys_main");
        isVR_sys_main = true;

        if (6 < metsig && metsig < 12) {
            accept("VR_sys_main_low");
            isVR_sys_main_low = true;
        }

        if (12 < metsig && metsig < 18) {
            accept("VR_sys_main_int");
            isVR_sys_main_int = true;
        }

        if (18 < metsig) {
            accept("VR_sys_main_high");
            isVR_sys_main_high = true;
        }

    } // End of VR_sys_main


    if (n_jets >= 2
        && nbjets >= 2
        && 60 < mbb && mbb < 150.) {

        accept("VR_sys_bb");
        isVR_sys_bb = true;

        if (6 < metsig && metsig < 12) {
            accept("VR_sys_bb_low");
            isVR_sys_bb_low = true;
        }

        if (12 < metsig && metsig < 18) {
            accept("VR_sys_bb_int");
            isVR_sys_bb_int = true;
        }

        if (18 < metsig) {
            accept("VR_sys_bb_high");
            isVR_sys_bb_high = true;
        }

    } // End of VR_sys_bb


    if (n_jets >= 2
        && nbjets == 0
        && jetpt1 > 100
        && dphiJ1met > 2.) {

        accept("VR_sys_off");
        isVR_sys_off = true;

        if (9 < metsig) {
            accept("VR_sys_off_9");
            isVR_sys_off_9 = true;
        }

    } // End of VR_sys_off

    /* Register branches */
    ntupVar("mcDSID", event->getMCNumber());
    ntupVar("nTruthJets20", nTruthJets20);

    ntupVar("isSR_High_8_a", isSR_High_8_a);
    ntupVar("isSR_High_8_b", isSR_High_8_b);
    ntupVar("isSR_High_16_a", isSR_High_16_a);
    ntupVar("isSR_High_16_b", isSR_High_16_b);
    ntupVar("isSR_High_4", isSR_High_4);
    ntupVar("isSR_llbb", isSR_llbb);
    ntupVar("isSR_Int_a", isSR_Int_a);
    ntupVar("isSR_Int_b", isSR_Int_b);
    ntupVar("isSR_Low_a", isSR_Low_a);
    ntupVar("isSR_Low_b", isSR_Low_b);
    ntupVar("isSR_Low_2", isSR_Low_2);
    ntupVar("isSR_OffShell_a", isSR_OffShell_a);
    ntupVar("isSR_OffShell_b", isSR_OffShell_b);
    ntupVar("isVR_High_4", isVR_High_4);
    ntupVar("isVR_High", isVR_High);
    ntupVar("isVR_High_R", isVR_High_R);
    ntupVar("isVR_llbb", isVR_llbb);
    ntupVar("isVR_Int", isVR_Int);
    ntupVar("isVR_Low", isVR_Low);
    ntupVar("isVR_Low_2", isVR_Low_2);
    ntupVar("isVR_OffShell", isVR_OffShell);
    ntupVar("isCR_VZ", isCR_VZ);
    ntupVar("isCR_tt", isCR_tt);
    ntupVar("isCR_Z", isCR_Z);
    ntupVar("isCR_DY", isCR_DY);

    ntupVar("isVR_sys_main", isVR_sys_main);
    ntupVar("isVR_sys_main_low", isVR_sys_main_low);
    ntupVar("isVR_sys_main_int", isVR_sys_main_int);
    ntupVar("isVR_sys_main_high", isVR_sys_main_high);
    ntupVar("isVR_sys_bb", isVR_sys_bb);
    ntupVar("isVR_sys_bb_low", isVR_sys_bb_low);
    ntupVar("isVR_sys_bb_int", isVR_sys_bb_int);
    ntupVar("isVR_sys_bb_high", isVR_sys_bb_high);
    ntupVar("isVR_sys_off", isVR_sys_off);
    ntupVar("isVR_sys_off_9", isVR_sys_off_9);
}
