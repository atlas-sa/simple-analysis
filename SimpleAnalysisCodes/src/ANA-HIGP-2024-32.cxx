#include "SimpleAnalysisFramework/AnalysisClass.h"
#include "TVectorD.h"

#include <set>
#include <iostream>
#include <iterator>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

// Function declarations

static TVectorD calcEigenValues(const AnalysisObjects &jets, int r);
static float getSkewness(std::vector<float> values);
static float getRMS(std::vector<float> values);
static std::set<std::vector<size_t> > generatePairings(size_t njet);
static void permutePairs(std::vector<size_t>& vec, size_t currI, size_t endI, std::set<std::vector<size_t> >& result);
static bool checkOrdering(std::vector<size_t>& vec, size_t endI);
static AnalysisObjects muonCorrection(const AnalysisObjects &jets, const AnalysisObjects &muons);
static float pairingScore(float mHa, float mHb, float mHc);
static void setupDNNpair(std::string DNNname);

template <typename T>
std::vector<T> as_vector(boost::property_tree::ptree const& pt, boost::property_tree::ptree::key_type const& key)
{
    std::vector<T> r;
    for (auto& item : pt.get_child(key))
        r.push_back(item.second.get_value<T>());
    return r;
}

TVectorD calcEigenValues(const AnalysisObjects &jets, int r) {
  TMatrixDSym momTensor(3);
  TVectorD eigenValues(3);
  momTensor.Zero();
  float norm = 0;
  for (const auto &jet : jets) {
    float mod_p = jet.Vect().Mag();
    float weight = std::pow(mod_p, r - 2);
    momTensor(0, 0) += jet.Px() * jet.Px() * weight;
    momTensor(0, 1) += jet.Px() * jet.Py() * weight;
    momTensor(0, 2) += jet.Px() * jet.Pz() * weight;
    momTensor(1, 0) += jet.Py() * jet.Px() * weight;
    momTensor(1, 1) += jet.Py() * jet.Py() * weight;
    momTensor(1, 2) += jet.Py() * jet.Pz() * weight;
    momTensor(2, 0) += jet.Pz() * jet.Px() * weight;
    momTensor(2, 1) += jet.Pz() * jet.Py() * weight;
    momTensor(2, 2) += jet.Pz() * jet.Pz() * weight;
    norm += std::pow(mod_p, r);
  }
  momTensor *= 1. / norm;
  momTensor.EigenVectors(eigenValues);
  return eigenValues;
}

float getSkewness(std::vector<float> values) {
    float offset = 0.;
    float rms = 0.;
    float skewness = 0.;
    int entries = 0.;
    for (size_t i = 0; i < values.size(); i++) {
        offset += values[i];
        rms += pow(values[i],2);
    }
    offset /= values.size();
    rms /= values.size();
    rms = (rms-pow(offset,2)>=0?sqrt(rms-pow(offset,2)):0);
    for (size_t i = 0; i < values.size(); i++) {
        skewness += pow((values[i]-offset)/rms,3);
    }
    skewness /= values.size();
    return skewness;
}

float getRMS(std::vector<float> values){
    if (values.size()==0) return 0.;
    float sumsq = 0.;
    float sum = 0.;
    for (auto val: values){
        sumsq += pow(val,2);
        sum += val;
    }
    float Expx2 = sumsq/values.size();
    float Expx = sum/values.size();
    float rms = 0;
    if (Expx2 > pow(Expx,2)) rms = sqrt(Expx2 - pow(Expx,2));
    return rms;
}

std::set<std::vector<size_t> > generatePairings(size_t njet){
    std::set<std::vector<size_t> > result;
    if(njet % 2 != 0) return result;
    std::vector<size_t> baseIndices;
    for(size_t i = 0; i < njet; i++) baseIndices.push_back(i);
    permutePairs(baseIndices, 1, baseIndices.size() - 1, result);
    return result;
}

void permutePairs(std::vector<size_t>& vec, size_t currI, size_t endI, std::set<std::vector<size_t> >& result){
    if(currI == endI){
        // Define a unique ordering so pairs aren't float-counted
        if(checkOrdering(vec, currI)) result.insert(vec);
        return;
    }
    for(size_t i = currI; i <= endI; i++){
        std::swap(vec[currI], vec[i]);
        // Check ordering each time to avoid combinatorial waste of CPU
        if(checkOrdering(vec, currI)) permutePairs(vec, currI + 1, endI, result);
        std::swap(vec[currI], vec[i]); // swap back
    }
}

bool checkOrdering(std::vector<size_t>& vec, size_t endI){
    if(vec.size() == 0) return true;
    if(endI >= vec.size()) endI = vec.size() - 1;
    // Order pairs by the lower jet index in each pair.
    // Within each pair, put lower jet index first.
    for(size_t i = 0; i + 2 <= endI; i += 2){
        if(vec[i] > vec[i+1]) return false;
        if(vec[i] > vec[i+2]) return false;
    }
    // Jet index ordering for last pair
    if(endI >= 1 && endI % 2 == 1) return vec[endI-1] < vec[endI];
    return true;
}

AnalysisObjects muonCorrection(const AnalysisObjects &jets,
                                              const AnalysisObjects &muons) {

  if (muons.size() == 0) {
    return jets;
  }
  // For each jet, if there exists a baseline muon within dR=min(0.4, 0.04+10/pTmuon)
  // add the muon four momentum to the jet's.
  AnalysisObjects reducedList;
  for (const auto &jet : jets) {
    // Step 1: Calculate dR to each muon
    auto correction_muon = muons[0];
    float minDR;
    auto jet_corr = jet;
    for (const auto &muon : muons) {
        minDR = std::min(0.4, 0.04 + (10/muon.Pt()));
        if (jet.DeltaR(muon) < minDR) {
            // Step 2: If muon in jet - correct jet momentum with muon momentum
            jet_corr = jet_corr + muon;
    }
    // Step 3: Add corrected (or uncorrected) jet to container
    reducedList.push_back(jet_corr);
    }
  }
  return reducedList;
}

float pairingScore(float mHa, float mHb, float mHc){
    float pair_score = std::abs(mHa - 120.) + std::abs(mHb - 115.) + std::abs(mHc - 110.);
    return pair_score;
}

boost::property_tree::ptree loadDNN(std::string DNNname, std::string version){

    std::string config_file_path(FindFile("ANA-HIGP-2024-32_"+DNNname+"_model_"+version+"_variables.json"));
    std::ifstream config_file(config_file_path);
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(config_file, pt);
    std::stringstream ss;
    boost::property_tree::json_parser::write_json(ss, pt);
    
    return pt;
}


DefineAnalysis(TriHiggs6b)

void TriHiggs6b::Init()
{

    // Define the analysis regions 
    addRegion("Cutflow_trigger"); // Pass trigger selections 2b+2j / 3b+1j
    addRegion("Cutflow_6jets");  // At least 6 jets with pT>=20GeV
    addRegion("Cutflow_4jets40"); // At least 4 jets with pT>40GeV
    addRegion("Cutflow_4bjets"); // At least 4 b-tagged jets at the 77% Efficiency WP
    addRegion("Region_4b"); // Analysis region exactly 4 b-tagged jets (regino to derive background systematics, not in fit)
    addRegion("Region_5b"); // Analysis region exactly 5 b-tagged jets (region to derive background estimate, in fit)
    addRegion("Region_6b"); // Analysis region at least 6 b-tagged jets (The signal region, in fit)

   // Define kinematic variable histograms to plot in the 6b Region
    addHistogram("mHradius", 25, 0., 250.);
    addHistogram("RMSdeltaRjj", 20, 0., 2.);
    addHistogram("RMSmjj", 20, 0., 400.);
    addHistogram("RMSetaH", 20, 0., 2.);
    addHistogram("RMSdeltaAjj", 20, -2., 4.);
    addHistogram("deltaRH1", 25, 0., 5.);
    addHistogram("deltaRH2", 25, 0., 5.);
    addHistogram("deltaRH3", 25, 0., 5.);
    addHistogram("massH1", 17, 0., 350.);
    addHistogram("HT6j", 20, 0., 1500.);
    addHistogram("eta_mHHH_frac", 20, 0., 1.);
    addHistogram("cosTheta", 20, -1., 1.);
    addHistogram("Aplanarity_6j", 20, 0., 0.4);
    addHistogram("Sphericity_allj", 20, 0., 1.);
    addHistogram("Sphericity_6j", 20, 0., 1.);
    addHistogram("Transverse_Sphericity_6j", 20, 0., 1.);

    addHistogram("resDNN_Score", 20., 0., 1.);
    addHistogram("nonresDNN_Score", 20., 0., 1.);
    addHistogram("heavyresDNN_Score", 20., 0., 1.);

    addHistogram("TotalWeight", 3, -0.5, 2.5, 1, 0.5, 1.5);


    // load in ONNX files and standardisation constants config for the neural networks
    // Variable standardisation will then be performed automatically during evaluation.
    // k=2-fold splitting is used, hence the even/odd DNNs. These are split via event number also during evaluation.

    std::vector<std::string> DNNs = {"resDNN", "nonresDNN", "heavyresDNN"};

    for (std::string DNNname : DNNs){

        // Even model setup and scaling
        boost::property_tree::ptree pt_even = loadDNN(DNNname, "even");
        const std::vector<double> even_offsets = as_vector<double>(pt_even, "offset");
        const std::vector<double> even_scales = as_vector<double>(pt_even, "scale");
    
        // Odd model setup and scaling
        boost::property_tree::ptree pt_odd = loadDNN(DNNname, "odd");
        const std::vector<double> odd_offsets = as_vector<double>(pt_odd, "offset");
        const std::vector<double> odd_scales = as_vector<double>(pt_odd, "scale");
        
        // initialise DNN pair from ONNX and scaling info
        addONNX(DNNname, "ANA-HIGP-2024-32_"+DNNname+"_model_even.onnx", "ANA-HIGP-2024-32_"+DNNname+"_model_odd.onnx", even_offsets, even_scales, odd_offsets, odd_scales);
    }

}

void TriHiggs6b::ProcessEvent(AnalysisEvent *event)
{

    static int entry = 0;
    bool verbose = false;
    if (verbose) std::cout << "Entry: " << entry << std::endl;

    // Fill weight histo for bookkeeping.
    double eventWeight = event->getMCWeights()[0];
    fillWeighted("TotalWeight", 0, 1, 1);
    fillWeighted("TotalWeight", 1, 1, eventWeight);
    fillWeighted("TotalWeight", 2, 1, eventWeight*eventWeight);
    
    // Define basic objects
    auto jets = event->getJets(20., 2.5, LooseBadJet | JVTTight); // Jets with a pT>=20GeV, |eta|<2.5, passing LooseBad jet cleaning and JVT pileup jet removal.
    auto muons = event->getMuons(4., 2.5, MuMedium); // Muons with pT>=4GeV, |eta|<2.5, passing the Medium ID working point.
    // Muon pT correction
    jets = muonCorrection(jets, muons);

    // define b-tagged jets, and multiplicities at different pT thresholds.
    auto bjets = filterObjects(jets, 20., 2.5, BTag77DL1r);
    auto ljets = filterObjects(jets, 20., 2.5, NOT(BTag77DL1r));
    int njets20 = jets.size();
    int nljets20 = ljets.size();
    int nbjets20 = bjets.size();

    auto jets35 = filterObjects(jets, 35., 2.5);
    int njets35 = jets35.size();

    // Note we use different btagging reqs. here to the analysis selection to emulate the trigger algorithms in each year.
    auto bjets35_mv2c60 = filterObjects(jets, 35., 2.5, BTag60MV2c10);
    auto ljets35_mv2c60 = filterObjects(jets, 35., 2.5, NOT(BTag60MV2c10));
    int nljets35_mv2c60 = ljets35_mv2c60.size();
    int nbjets35_mv2c60 = bjets35_mv2c60.size();

    auto bjets35_mv2c70 = filterObjects(jets, 35., 2.5, BTag70MV2c10);
    auto ljets35_mv2c70 = filterObjects(jets, 35., 2.5, NOT(BTag70MV2c10));
    int nljets35_mv2c70 = ljets35_mv2c70.size();
    int nbjets35_mv2c70 = bjets35_mv2c70.size();

    auto jets40 = filterObjects(jets, 40., 2.5);
    auto bjets40 = filterObjects(jets, 40., 2.5, BTag77DL1r);
    auto ljets40 = filterObjects(jets, 40., 2.5, NOT(BTag77DL1r));
    int njets40 = jets40.size();
    int nljets40 = ljets40.size();
    int nbjets40 = bjets40.size();

    if (verbose) std::cout << "Defined the jet collections" << std::endl;

    // Trigger selection
    bool pass_trig_2b2j = (nbjets35_mv2c60>=2 && nljets35_mv2c60>=2);
    bool pass_trig_3b1j = (nbjets35_mv2c70>=3 && nljets35_mv2c70>=1);

    if (verbose) std::cout << "Defined the trigger selections" << std::endl;

    // Cutflow selections
    bool pass_6jets = (njets20 >= 6);
    bool pass_trigger = (pass_trig_2b2j || pass_trig_3b1j);
    bool pass_4jets40 = (njets40 >= 4);
    bool pass_4bjets = (nbjets40 >= 4);
    bool region_4b = (nbjets20 == 4);
    bool region_5b = (nbjets20 == 5);
    bool region_6b = (nbjets20 >= 6);

    if (verbose) std::cout << "Defined the cutflow selections" << std::endl;

    // initialise variables
    float HT6j = -999.;
    float deltaRH1 = -999.;
    float deltaRH2 = -999.;
    float deltaRH3 = -999.;
    float massH1 = -999.;
    float massH2 = -999.;
    float massH3 = -999.;
    float ptH1 = -999. ;
    float ptH2 = -999. ;
    float ptH3 = -999. ;
    float massHHH = -999.;
    float mHradius = -999.;
    float etaH1 = -999.;
    float etaH2 = -999.;
    float etaH3 = -999.;
    float RMSetaH = -999.;
    float RMSmjj = -999.;
    float RMSdeltaRjj = -999.;
    float RMSdeltaAjj = -999.;
    float eta_mHHH_frac = -999.;
    float cosTheta = -999.;
    float Aplanarity_6j = -999.;
    float Sphericity_6j = -999.;
    float Sphericity_allj = -999.;
    float Transverse_Sphericity_6j = -999.;

    float resDNN_Score = -999.;
    float nonresDNN_Score = -999.;
    float heavyresDNN_Score = -999.;

    if (verbose) std::cout << "initialised the variables" << std::endl;


    if (pass_trigger){
        accept("Cutflow_trigger");
        // Pair the jets to form 3 H candidates
        float pair_score = -999.;
        std::vector<size_t> pair_indices;
        std::set<std::vector<size_t>> m_pairings = generatePairings(6);
        if (pass_6jets){
            accept("Cutflow_6jets");  // At least 6 jets with pT>=20GeV
            int counter = 0;
            for(const std::vector<size_t>& p : m_pairings){
                std::vector<size_t> tmp_p (p.size(), 0);
                float pTHa = (jets[p[0]] + jets[p[1]]).Pt();
                float mHa = (jets[p[0]] + jets[p[1]]).M();
                float pTHb = (jets[p[2]] + jets[p[3]]).Pt();
                float mHb = (jets[p[2]] + jets[p[3]]).M();
                float pTHc = (jets[p[4]] + jets[p[5]]).Pt();
                float mHc = (jets[p[4]] + jets[p[5]]).M();
                float pTH1, pTH2, pTH3, mH1, mH2, mH3;
                if ((pTHa >= pTHb) && (pTHb >= pTHc)){
                        pTH1 = pTHa; pTH2 = pTHb; pTH3 = pTHc; mH1 = mHa; mH2 = mHb; mH3 = mHc;
                        tmp_p[0] = p[0]; tmp_p[1] = p[1]; tmp_p[2] = p[2]; tmp_p[3] = p[3]; tmp_p[4] = p[4]; tmp_p[5] = p[5];
                } else if ((pTHa >= pTHc) && (pTHc >= pTHb)){
                        pTH1 = pTHa; pTH2 = pTHc; pTH3 = pTHb; mH1 = mHa; mH2 = mHc; mH3 = mHb;
                        tmp_p[0] = p[0]; tmp_p[1] = p[1]; tmp_p[2] = p[4]; tmp_p[3] = p[5]; tmp_p[4] = p[2]; tmp_p[5] = p[3];
                } else if ((pTHb >= pTHa) && (pTHa >= pTHc)){
                        pTH1 = pTHb; pTH2 = pTHa; pTH3 = pTHc; mH1 = mHb; mH2 = mHa; mH3 = mHc;
                        tmp_p[0] = p[2]; tmp_p[1] = p[3]; tmp_p[2] = p[0]; tmp_p[3] = p[1]; tmp_p[4] = p[4]; tmp_p[5] = p[5];
                } else if ((pTHb >= pTHc) && (pTHc >= pTHa)){
                        pTH1 = pTHb; pTH2 = pTHc; pTH3 = pTHa; mH1 = mHb; mH2 = mHc; mH3 = mHa;
                        tmp_p[0] = p[2]; tmp_p[1] = p[3]; tmp_p[2] = p[4]; tmp_p[3] = p[5]; tmp_p[4] = p[0]; tmp_p[5] = p[1]; 
                } else if ((pTHc >= pTHa) && (pTHa >= pTHb)){
                        pTH1 = pTHc; pTH2 = pTHa; pTH3 = pTHb; mH1 = mHc; mH2 = mHa; mH3 = mHb;
                        tmp_p[0] = p[4]; tmp_p[1] = p[5]; tmp_p[2] = p[0]; tmp_p[3] = p[1]; tmp_p[4] = p[2]; tmp_p[5] = p[3];
                } else {
                        pTH1 = pTHc; pTH2 = pTHb; pTH3 = pTHa; mH1 = mHc; mH2 = mHb; mH3 = mHa;
                        tmp_p[0] = p[4]; tmp_p[1] = p[5]; tmp_p[2] = p[2]; tmp_p[3] = p[3]; tmp_p[4] = p[0]; tmp_p[5] = p[1];
                } 
                float tmp_pair_score = 1/pairingScore(mH1, mH2, mH3);
                if (tmp_pair_score > pair_score){
                    pair_score = tmp_pair_score;
                    pair_indices = tmp_p;
                }
                counter++;
            }
            if (verbose) std::cout << "done the pairing" << std::endl;
    
            // Define Higgs candidate 4momenta
            auto H1_j1 = jets[pair_indices[0]];
            auto H1_j2 = jets[pair_indices[1]];
            auto H2_j1 = jets[pair_indices[2]];
            auto H2_j2 = jets[pair_indices[3]];
            auto H3_j1 = jets[pair_indices[4]];
            auto H3_j2 = jets[pair_indices[5]];
            AnalysisObjects HiggsJets;
            for (int j=0; j<6; j++) HiggsJets.push_back(jets[pair_indices[j]]);
            HT6j = H1_j1.Pt() + H1_j2.Pt() + H2_j1.Pt() + H2_j2.Pt() + H3_j1.Pt() + H3_j2.Pt();
    
            deltaRH1 = H1_j1.DeltaR(H1_j2);
            deltaRH2 = H2_j1.DeltaR(H2_j2);
            deltaRH3 = H3_j1.DeltaR(H3_j2);
    
            ptH1 = (jets[pair_indices[0]] + jets[pair_indices[1]]).Pt();
            ptH2 = (jets[pair_indices[2]] + jets[pair_indices[3]]).Pt();
            ptH3 = (jets[pair_indices[4]] + jets[pair_indices[5]]).Pt();
            massH1 = (jets[pair_indices[0]] + jets[pair_indices[1]]).M();
            massH2 = (jets[pair_indices[2]] + jets[pair_indices[3]]).M();
            massH3 = (jets[pair_indices[4]] + jets[pair_indices[5]]).M();
            massHHH = (jets[pair_indices[0]] + jets[pair_indices[1]] + jets[pair_indices[2]] + jets[pair_indices[3]] + jets[pair_indices[4]] + jets[pair_indices[5]]).M();
            mHradius = sqrt(pow(massH1-120.,2) + pow(massH2-115.,2) + pow(massH3-110.,2));
    
            etaH1 = (jets[pair_indices[0]] + jets[pair_indices[1]]).Eta();
            etaH2 = (jets[pair_indices[2]] + jets[pair_indices[3]]).Eta();
            etaH3 = (jets[pair_indices[4]] + jets[pair_indices[5]]).Eta();
            std::vector<float> etaH = {etaH1, etaH2, etaH3};
            RMSetaH = getRMS(etaH);
    
            if (verbose) std::cout << "done the basic Higgs kinematics" << std::endl;
    
            std::vector<float> deltaAjjs;
            std::vector<float> mjjs;
            std::vector<float> deltaRjjs;
            float sumpTcosh = 0.;
            for (long unsigned int a=0; a<6; a++){
                for (long unsigned int b=a+1; b<6; b++){
                    //if (a>=b) continue;
                    mjjs.push_back((jets[a]+jets[b]).M());
                    deltaRjjs.push_back(jets[a].DeltaR(jets[b]));
                    deltaAjjs.push_back(cosh(std::abs(jets[a].Eta() - jets[b].Eta())) - cos(jets[a].DeltaPhi(jets[b])));
                    sumpTcosh += 2 * jets[a].Pt() * jets[b].Pt() * (cosh(std::abs(jets[a].Eta() - jets[b].Eta())) -1);
                }
            }
            RMSmjj = getRMS(mjjs);
            RMSdeltaRjj = getRMS(deltaRjjs);
            RMSdeltaAjj = getSkewness(deltaAjjs);
            eta_mHHH_frac = sumpTcosh/pow(massHHH,2);
    
            if (verbose) std::cout << "done the other angular kinematics" << std::endl;
    
            std::vector<float> distance_pair_center = {massH1 - 120., massH2 - 115., massH3 - 110.};
            std::vector<float> unit = {1./sqrt(3), 1./sqrt(3), 1./sqrt(3)};
            cosTheta = std::inner_product(std::begin(distance_pair_center), std::end(distance_pair_center), std::begin(unit), 0.0)/mHradius;
    
            if (verbose) std::cout << "done mH-radius and cosTheta " << cosTheta << std::endl;
    
            TVectorD EMomTensor_evals_1_6j = calcEigenValues(HiggsJets, 1);
            TVectorD EMomTensor_evals_1_allj = calcEigenValues(jets, 1);
            TVectorD EMomTensor_evals_2_6j = calcEigenValues(HiggsJets, 2);
            TVectorD EMomTensor_evals_2_allj = calcEigenValues(jets, 2);
    
            std::vector<float> evals_1_6j = {EMomTensor_evals_1_6j[0], EMomTensor_evals_1_6j[1], EMomTensor_evals_1_6j[2]};
            std::vector<float> evals_2_6j = {EMomTensor_evals_2_6j[0], EMomTensor_evals_2_6j[1], EMomTensor_evals_2_6j[2]};
            std::vector<float> evals_1_allj = {EMomTensor_evals_1_allj[0], EMomTensor_evals_1_allj[1], EMomTensor_evals_1_allj[2]};
            std::vector<float> evals_2_allj = {EMomTensor_evals_2_allj[0], EMomTensor_evals_2_allj[1], EMomTensor_evals_2_allj[2]};
    
            // Eigenvalues sorted in Descending order
            std::sort(evals_1_6j.begin(), evals_1_6j.end());
            std::sort(evals_1_allj.begin(), evals_1_allj.end());
            std::sort(evals_2_6j.begin(), evals_2_6j.end());
            std::sort(evals_2_allj.begin(), evals_2_allj.end());
    
            Aplanarity_6j = 1.5 * evals_1_6j[0];
            Sphericity_6j = 1.5 * (evals_1_6j[0] + evals_1_6j[1]);
            Sphericity_allj = 1.5 * (evals_1_allj[0] + evals_1_allj[1]);
            Transverse_Sphericity_6j = 2 * evals_2_6j[1] / (evals_2_6j[1] + evals_2_6j[2]);
    
            if (verbose) std::cout << "done event shape variables" << std::endl;
    
            if (pass_4jets40){
                accept("Cutflow_4jets40");
                if (pass_4bjets){
                    accept("Cutflow_4bjets");
                    if (region_4b) accept("Region_4b");

                    // Add kinematic variables to TTree:
                    // DNN input features
                    ntupVar("HT6j", HT6j);
                    ntupVar("mHradius", mHradius);
                    ntupVar("RMSdeltaRjj", RMSdeltaRjj);
                    ntupVar("RMSdeltaAjj", RMSdeltaAjj);
                    ntupVar("RMSmjj", RMSmjj);
                    ntupVar("deltaRH1", deltaRH1);
                    ntupVar("deltaRH2", deltaRH2);
                    ntupVar("deltaRH3", deltaRH3);
                    ntupVar("massH1", massH1);
                    ntupVar("RMSetaH", RMSetaH);
                    ntupVar("eta_mHHH_frac", eta_mHHH_frac);
                    ntupVar("cosTheta", cosTheta);
                    ntupVar("Aplanarity_6j", Aplanarity_6j);
                    ntupVar("Sphericity_allj", Sphericity_allj);
                    ntupVar("Sphericity_6j", Sphericity_6j);
                    ntupVar("Transverse_Sphericity_6j", Transverse_Sphericity_6j);
                    // Other kinematic variables
                    ntupVar("massH2", massH2);
                    ntupVar("massH3", massH3);
                    ntupVar("massHHH", massHHH);
                    ntupVar("pTH1", ptH1);
                    ntupVar("pTH2", ptH2);
                    ntupVar("pTH3", ptH3);
                    ntupVar("pT_H1_jet1", H1_j1.Pt());
                    ntupVar("pT_H1_jet2", H1_j2.Pt());
                    ntupVar("pT_H2_jet1", H2_j1.Pt());
                    ntupVar("pT_H2_jet2", H2_j2.Pt());
                    ntupVar("pT_H3_jet1", H3_j1.Pt());
                    ntupVar("pT_H3_jet2", H3_j2.Pt());

                    // Define the DNNs and their input features, then evaluate them to fill the ntuple.
                    MVA* resDNN = getMVA("resDNN");
                    MVA* nonresDNN = getMVA("nonresDNN");
                    MVA* heavyresDNN = getMVA("heavyresDNN");

                    std::vector<float> resDNN_features = {HT6j, RMSdeltaRjj, deltaRH2, eta_mHHH_frac, Sphericity_6j, Aplanarity_6j, deltaRH1, deltaRH3, RMSdeltaAjj, cosTheta};
                    std::vector<float> nonresDNN_features = {mHradius, RMSmjj, RMSdeltaRjj, deltaRH1, RMSetaH, massH1, deltaRH2, deltaRH3, Aplanarity_6j, Transverse_Sphericity_6j};
                    std::vector<float> heavyresDNN_features = {mHradius, RMSdeltaRjj, RMSmjj, deltaRH1, deltaRH2, deltaRH3, massH1, RMSetaH, Aplanarity_6j, Sphericity_allj};

                    resDNN_Score = resDNN->evaluate(resDNN_features, "node_0");
                    nonresDNN_Score = nonresDNN->evaluate(nonresDNN_features, "node_0");
                    heavyresDNN_Score = heavyresDNN->evaluate(heavyresDNN_features, "node_0");

                    ntupVar("resDNN_Score", resDNN_Score);
                    ntupVar("nonresDNN_Score", nonresDNN_Score);
                    ntupVar("heavyresDNN_Score", heavyresDNN_Score);

                    // now finish the cutflow
                    if (region_5b) accept("Region_5b");
                    if (region_6b){
                        
                        accept("Region_6b");
                        
                        // Fill Histograms of kinematic variables in the 6b Region.
                        fill("mHradius", mHradius);
                        fill("RMSdeltaRjj", RMSdeltaRjj);
                        fill("RMSdeltaAjj", RMSdeltaAjj);
                        fill("RMSmjj", RMSmjj);
                        fill("deltaRH1", deltaRH1);
                        fill("deltaRH2", deltaRH2);
                        fill("deltaRH3", deltaRH3);
                        fill("massH1", massH1);
                        fill("RMSetaH", RMSetaH);
                        fill("Aplanarity_6j", Aplanarity_6j);
                        fill("Sphericity_allj", Sphericity_allj);
                        fill("HT6j", HT6j);
                        fill("eta_mHHH_frac", eta_mHHH_frac);
                        fill("cosTheta", cosTheta);
                        fill("Sphericity_6j", Sphericity_6j);
                        fill("Transverse_Sphericity_6j", Transverse_Sphericity_6j);

                        fill("resDNN_Score", resDNN_Score);
                        fill("nonresDNN_Score", nonresDNN_Score);
                        fill("heavyresDNN_Score", heavyresDNN_Score);

                    }
                }
            }
        }
    }

    entry++;

}
