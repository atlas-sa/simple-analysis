#include "SimpleAnalysisFramework/AnalysisClass.h"

#include <TFile.h>
#include <TH2.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>

#include <algorithm>

// SimpleAnalysis implementation for 2021 Disappearing Track analysis (ANA-SUSY-2018-19)
// Based off INT Note ATL-COM-PHYS-2019-494 and main analysis code

DefineAnalysis(DisappearingTrack2018)

/**
 * @brief Class to obtain and work with efficiency maps.
 * 
 * Based on previous DisappearingTrack2016.cxx implementation
 */
class effMap {
public:
    /**
     * @brief Construct a new efficiency map object
     * 
     * @param mapname   Name of 2D hist to use as map
     * @param filename  Name of .root file containing map. Defaults to maps that ship with this analysis.
     */
    effMap(std::string mapname, std::string filename="DisappearingTrack2018-EfficiencyMaps.root") {
        TFile fh(FindFile(filename).c_str());
        h_eff = dynamic_cast < TH2* >(fh.FindObjectAny(mapname.c_str())->Clone());
        if (!h_eff) throw std::runtime_error("Could not find efficiency map " + mapname + " in " + filename);
        h_eff->SetDirectory(0);
    };

    /**
     * @brief Destroy the efficiency map object
     * 
     */
    ~effMap() {
        delete h_eff;
    };

    /**
     * @brief Get the efficiency for the bin at a co-ordinate.
     * 
     * @param x         x co-ordinate
     * @param y         y co-ordinate
     * @return double   efficiency of bin at (x,y)
     */
    double getEff(double x, double y) {
        return h_eff->GetBinContent(h_eff->FindBin(x,y));
    };

    bool passes(double x, double y) {
        double eff = this->getEff(x,y);
        double rnd = gRandom->Rndm();

        if (rnd < eff) return true; else return false;
    };

    TH2D* getHist() {return (TH2D*)h_eff;};

    operator TH2D*() {return (TH2D*)h_eff;};

private:
    /**
     * @brief The underlying 2D hist representing the efficiency map
     * 
     */
    TH2* h_eff;
};

/**
 * @brief Simplified double-sided crystal ball functor, as described in INT Note Section 5.3.2
 * 
 */
class smearingFunction { 
public:
    /**
     * @brief Construct a new smearing function functor.
     * 
     * This is a simplified double-sided crystal ball function, as described in INT Note Section 5.3.2
     * 
     * @param alpha     Slope parameter of double-sided crystal ball function
     * @param sigma     Standard Deviation of the variable to smear
     * @param mean      Mean of the variable to smear. Default of 0.
     */
    smearingFunction(double alpha, double sigma, double mean=0.0) : m_alpha(alpha),m_mean(mean),m_sigma(sigma) { m_uninitialised=false;}

    /**
     * @brief Construct a new smearing Function object without initialising params
     * 
     */
    smearingFunction() {m_uninitialised=true;}

    /**
     * @brief Apply the smearing function.
     * 
     * @param x         Variable to smear
     * @param p         Array of parameters
     * @return double   Smeared variable
     */
    double operator()(double *x, double* p) {
        // parse param array if uninitialised
        if (m_uninitialised) {
            m_alpha = p[0];
            m_mean  = p[1];
            m_sigma = p[2];
        }
        
        double z = (x[0] - m_mean)/m_sigma;

        if      (z < -m_alpha) return TMath::Exp(m_alpha*(z + (m_alpha/2.0)));
        else if (z > m_alpha)  return TMath::Exp(-m_alpha*(z - (m_alpha/2.0)));
        else                   return TMath::Exp(-(z*z)/2.0);
    }

private:
    bool m_uninitialised;
    double m_alpha;
    double m_mean;
    double m_sigma;
};

////////////////////////
// Smearing functions //
////////////////////////

// Tracklet smearing functions derived from Z->ee events - see Section 6.2 in arxiv:2201.02472
//                                                                           alpha sigma   min   max  nvars
auto electronSmear_10_15 = new  TF1("electronSmear_10_15",  smearingFunction(1.86, 20.94), -800, 800, 3);
auto electronSmear_15_20 = new  TF1("electronSmear_15_20",  smearingFunction(1.86, 19.54), -800, 800, 3);
auto electronSmear_20_25 = new  TF1("electronSmear_20_25",  smearingFunction(1.86, 18.33), -800, 800, 3);
auto electronSmear_25_35 = new  TF1("electronSmear_25_35",  smearingFunction(1.86, 17.01), -800, 800, 3);
auto electronSmear_35_45 = new  TF1("electronSmear_35_45",  smearingFunction(1.82, 15.42), -800, 800, 3);
auto electronSmear_45_60 = new  TF1("electronSmear_45_60",  smearingFunction(1.66, 14.49), -800, 800, 3);
auto electronSmear_60_100 = new TF1("electronSmear_60_100", smearingFunction(1.54, 13.90), -800, 800, 3);
auto electronSmear_gt100 = new  TF1("electronSmear_gt100",  smearingFunction(1.64, 14.03), -800, 800, 3);

double smear(double inputPt, double charge, TF1* sf) {
    double QoverPt = charge / (inputPt*1e-3); // [TeV^-1]
    double QoverPtSmeared = TMath::Abs(QoverPt + sf->GetRandom());
    double PtSmeared = (1 / QoverPtSmeared) * 1e+3;

    return PtSmeared;
}

double electronSmear(double inputPt, double charge) {
    TF1* sf;

    if      (inputPt < 0.0)   return -1.0;
    else if (inputPt < 10.0)  return -1.0;
    else if (inputPt < 15.0)  sf=electronSmear_10_15;
    else if (inputPt < 20.0)  sf=electronSmear_15_20;
    else if (inputPt < 25.0)  sf=electronSmear_20_25;
    else if (inputPt < 35.0)  sf=electronSmear_25_35;
    else if (inputPt < 45.0)  sf=electronSmear_35_45;
    else if (inputPt < 60.0)  sf=electronSmear_45_60;
    else if (inputPt < 100.0) sf=electronSmear_60_100;
    else                         sf=electronSmear_gt100;

    return smear(inputPt, charge, sf);
}
double electronSmear(AnalysisObject particle) {return electronSmear(particle.Pt(), particle.charge());} 

bool sortSmeared(std::pair<AnalysisObject,double> lhs, std::pair<AnalysisObject,double> rhs) {
    double lhsPt = lhs.second;
    double rhsPt = rhs.second;
    return (lhsPt > rhsPt);
}

/////////////////////////////
// Efficiency Map Pointers //
/////////////////////////////

// Pointer to trigger efficiency map
effMap* eff_trigger = nullptr;

// Pointer to tracklet reco maps
effMap* eff_track_EWK = nullptr;
effMap* eff_track_Strong = nullptr;


void DisappearingTrack2018::Init() {

    // Trigger efficiency map
    eff_trigger = new effMap("eff_trigger_average");

    // Track reconstruction efficiency map
    eff_track_EWK = new effMap("h_effmap_average_EWK");
    eff_track_Strong = new effMap("h_effmap_average_Strong");

    // Regions
    addRegions({"Pre_Kinematic_EWK","Pre_Kinematic_Strong"}); // Preselection regions
    addRegions({"SR_EWK","SR_Strong"}); // Signal Regions

    // Histograms 
    addHistogram("hist_MET",100,0,2000);
    addHistogram("hist_Chargino_Pt",100,0,2000);
    addHistogram("hist_Chargino_Pt_smeared",100,0,2000);
    // Tracklet = Chargino surviving selection
    addHistogram("hist_Tracklet_Pt",100,0,1000);
    addHistogram("hist_cutflow_kin_EWK",7,0,7); // All, GRL & Cleaning, MET Trigger, Lepton Veto, MET > 200 GeV, 1st Jet pT > 100 GeV, min(∆𝜙(Jet, MET)) > 1.0
    addHistogram("hist_cutflow_kin_Strong",7,0,7); // All, GRL & Cleaning, MET Trigger, Lepton Veto, MET > 250 GeV, 1st Jet pT > 100 GeV & 2nd & 3rd > 20 GeV, min(∆𝜙(Jet, MET)) > 0.4
    addHistogram("hist_cutflow_SR_EWK",8,0,8); // All, Kinematic, Tracklet Emulation, Leading tracklet, ∆R(jets) > 0.4,  ∆R(electron) > 0.4, ∆R(muon) > 0.4, 0.1 < |η| < 1.9
    addHistogram("hist_cutflow_SR_Strong",8,0,8);// All, Kinematic, Tracklet Emulation, Leading tracklet, ∆R(jets) > 0.4,  ∆R(electron) > 0.4, ∆R(muon) > 0.4, 0.1 < |η| < 1.9
    addHistogram("hist_jet1pt",100,0,2000);
    addHistogram("hist_jet2pt",100,0,2000);
    addHistogram("hist_jet3pt",100,0,2000);
    addHistogram("hist_jet4pt",100,0,2000);
    addHistogram("hist_mDp_met_jets",50,0,3.15);
    addHistogram("hist_mDp_met_jet1",50,0,3.15);
    addHistogram("hist_mDp_met_jet2",50,0,3.15);
    addHistogram("hist_mDp_met_jet3",50,0,3.15);
    addHistogram("hist_mDp_met_jet4",50,0,3.15);
    addHistogram("hist_mDp_C1_jets",50,0,3.15);
    addHistogram("hist_mDp_C1_jet1",50,0,3.15);
    addHistogram("hist_dR_C1_jets",100,0,5);
    addHistogram("hist_dR_C1_jet1",100,0,5);
    addHistogram("hist_charginoEta",50,-2.5,2.5);
    addHistogram("hist_charginoDecayRad",50,0,500);
    addHistogram("hist_charginosPerEvent",5,0,5);
}

void DisappearingTrack2018::ProcessEvent(AnalysisEvent *event) { 

    int MCNumber = event->getMCNumber();

    bool passedKinStrong = false;
    bool passedKinEWK = false;
    
    ////////////////////////
    // Object Definitions //
    ////////////////////////

    // Chargino
    auto Charginos  = event->getHSTruth(0,5,Chargino1|StablePart);

    // MET
    auto met = event->getMET(); // MET as provided by SimpleAnalysis
    double metpt = met.Pt();

    // Jets
    auto jets = event->getJets(20.0, 2.8, NOT(ContainsSUSY)); // Get jets with Pt > 20 GeV, eta < 2.8
    // JVT 0.59, for other params check code

    // Muons
    auto muons = event->getMuons(10.0, 2.7, MuMedium);
    auto caloMuons = filterObjects(muons, 10.0, 2.7, MuCaloTaggedOnly);
    auto notCaloMuons = filterObjects(muons, 10.0, 2.7, NOT(MuCaloTaggedOnly));
    
    // Electrons  
    auto electrons = event->getElectrons(10.0, 2.47, ELooseBLLH);
    
    // Weights
    auto weights = event->getMCWeights();


    /////////////////////
    // Overlap Removal //
    /////////////////////

    // Real Analysis: If an electron candidate and a muon candidate share the same ID track, the muon 
    // candidate is removed if the muon is a calo-muon and the electron candidate is removed if the 
    // muon is not a calo-muon.
    // SimpleAnalysis has no concept of ID Track - ∆R=0.05 is close enough
    caloMuons = overlapRemoval(caloMuons, electrons, 0.05);
    electrons = overlapRemoval(electrons, notCaloMuons, 0.05);
    muons = caloMuons + notCaloMuons;

    // Real Analysis: If an electron candidate and a jet are found within ∆R<0.2, they are classified as an 
    // electron and the jet is removed.
    jets = overlapRemoval(jets, electrons, 0.2);

    // Real Analysis: If an electron candidate and a jet are found within ∆R<0.4, they are classified as a jet 
    // and the electron is removed.
    electrons = overlapRemoval(electrons, jets, 0.4);

    // Real Analysis: If a muon candidate and a jet are ghost-associated or found within ∆R<0.2, they are 
    // classified as a muon and the jet is removed as long as the number of tracks with Pt>500MeV 
    // associated to the jet is less than three.
    jets = overlapRemoval(jets, muons, 0.2, LessThan3Tracks);

    // If a muon candidate and a jet are found within ∆R<0.4, they are classified as a jet and the 
    // muon is removed.
    muons = overlapRemoval(muons, jets, 0.4);



    ///////////////////
    // Filling Hists //
    ///////////////////

    // Fill hists of kinematic variables before events are rejected

    if (jets.size() >= 1) {
        fill("hist_jet1pt",jets[0].Pt());
        fill("hist_mDp_met_jet1",minDphi(met, jets[0]));
    }
    if (jets.size() >= 2) {
        fill("hist_jet2pt",jets[1].Pt());
        fill("hist_mDp_met_jet2",minDphi(met, jets[1]));
    }
    if (jets.size() >= 3) {
        fill("hist_jet3pt",jets[2].Pt());
        fill("hist_mDp_met_jet3",minDphi(met, jets[2]));
    }
    if (jets.size() >= 4) {
        fill("hist_jet4pt",jets[3].Pt());
        fill("hist_mDp_met_jet4",minDphi(met, jets[3]));
    }

    fill("hist_mDp_met_jets",minDphi(met, jets, 4, 50.0));

    fill("hist_MET",metpt);

    double nCharginos = 0.5;

    for (auto &chargino : Charginos) {
        nCharginos+=1.0;
        if (jets.size() >= 1) {
            fill("hist_mDp_C1_jet1", minDphi(chargino,jets[0]));
            fill("hist_dR_C1_jet1", chargino.DeltaR(jets[0]));
        }
        for (auto &jet : jets) {
            fill("hist_mDp_C1_jets",minDphi(chargino,jet));
            fill("hist_dR_C1_jets", chargino.DeltaR(jet));
        }

        double charginoEta = chargino.Eta();
        double charginoDecayRad = chargino.decayVtx().Perp();

        // Make chargino plots
        fill("hist_Chargino_Pt",chargino.Pt());
        double smearedPt = electronSmear(chargino);
        if (smearedPt > 0.0) fill("hist_Chargino_Pt_smeared",smearedPt);
        fill("hist_charginoEta",charginoEta);
        fill("hist_charginoDecayRad",charginoDecayRad);
    }
    fill("hist_charginosPerEvent",nCharginos);


    ///////////////////
    // Preselections //
    ///////////////////

    fill("hist_cutflow_kin_EWK",0.5);
    fill("hist_cutflow_kin_Strong",0.5);
    fill("hist_cutflow_SR_EWK",0.5);
    fill("hist_cutflow_SR_Strong",0.5);

    //// Event Cleaning
    
    // Bad Jet: any jet failing LooseBad, leading jet failing TightBad, eta>2.4 fails
    if (!jets.empty()) {
        for (auto &jet : jets) {
            if (!jet.pass(LooseBadJet)) return;
            if (jet.Eta()>2.4) return;
        }
        if (!(jets[0].pass(TightBadJet))) return;
    } 

    // Bad Muon: sig{q/p} > 0.4 fails -- MuQoPSignificance?
    if (!muons.empty()) {
        for (auto &muon : muons) {
            if (!muon.pass(MuQoPSignificance)) return;
        }
    }       

    // Bad MET: -muon/met*cos( dPhi(-muon, met)) > 0.5
    // negmuon = negative vector sum of muon Pt()
    if (!muons.empty()) {
        for (auto &muon : muons) { //TODO: Test bad met veto
            auto negmuon = -muon;
            if ( negmuon.Pt()/metpt * cos(negmuon.Phi() - met.Phi()) > 0.5 ) return;
        }
    }

    fill("hist_cutflow_kin_EWK",1.5);
    fill("hist_cutflow_kin_Strong",1.5);

    // Calc leading jet pt
    if (jets.empty()) return;
    double jet1pt = jets[0].Pt();

    // Trigger
    // Logic for emulation: 
    // Get trigger efficiency (eg. 0.8)
    // Get a random number between 0 and 1, uniformally distributed
    // random number should be less than the efficiency that fraction of the time (80% in this example)
    // So if the random number is greater than the efficiency (~20% of the time), fail the event (return)
    // Same logic used for tracklet reco emulation later
    //
    // Trigger eff only parameterised from 0-500GeV for MET and Jet 1 pT - extrapolated above there

    if (metpt>500) { 
        // Assume passed
    } else if (jet1pt>500) {
        // Assume approximately same as jet1pt=500
        if (!eff_trigger->passes(metpt, 499.9)) return;
    } else {
        // in space of parameterisation
        if (!eff_trigger->passes(metpt, jet1pt)) return;
    }

    fill("hist_cutflow_kin_EWK",2.5);
    fill("hist_cutflow_kin_Strong",2.5);

    // Lepton Veto
    if (!electrons.empty() || !muons.empty()) return;
    fill("hist_cutflow_kin_EWK",3.5);
    fill("hist_cutflow_kin_Strong",3.5);


    // EWK-Specific Pre-Selection
    passedKinEWK = true;

    // MET cut
    if (metpt < 200.0) passedKinEWK=false;
    else fill("hist_cutflow_kin_EWK",4.5);
    
    // Jet pT cut
    if (passedKinEWK) { 
        if (jet1pt < 100.0) passedKinEWK=false;
        else fill("hist_cutflow_kin_EWK",5.5);
    }
    
    // min(∆𝜙(Jet, MET)) cut
    if (passedKinEWK) { 
        double minDphiJetMET = minDphi(met, jets, 4, 50.0);
        if (minDphiJetMET>1.0) {
            accept("Pre_Kinematic_EWK");
            fill("hist_cutflow_kin_EWK",6.5);
            passedKinEWK = true;
        } else passedKinEWK = false;
    }


    // Strong-Specific Pre-Selection
    passedKinStrong = true;

    // MET cut
    if (metpt < 250.0) passedKinStrong = false;
    else fill("hist_cutflow_kin_Strong",4.5);
    
    // Jet pT cuts
    if (passedKinStrong) {
        if (jet1pt < 100.0) passedKinStrong = false;
        else if (jets.size() < 3 ) passedKinStrong = false;
        else if (jets[1].Pt() < 20 || jets[2].Pt() < 20) passedKinStrong = false;
        else fill("hist_cutflow_kin_Strong",5.5);
    }
    
    // min(∆𝜙(Jet, MET)) cut
    if (passedKinStrong) {
        double minDphiJetMET = minDphi(met, jets, 4, 50.0);
        if (minDphiJetMET>0.4) {
            accept("Pre_Kinematic_Strong");
            fill("hist_cutflow_kin_Strong",6.5);
            passedKinStrong = true;
        } else passedKinStrong = false;
    }


    // Event must pass at least one preselection
    if (passedKinEWK==false && passedKinStrong == false) return;


    ////////////////////
    // Signal Regions //
    ////////////////////

    // Emulate track & tracklet reco efficiency
    std::vector<std::pair<AnalysisObject,double>> EWKCharginos = {};
    std::vector<std::pair<AnalysisObject,double>> StrongCharginos = {};

    // Emulate chargino reconstruction
    for (auto &chargino : Charginos) {
        double charginoEta = chargino.Eta();
        double charginoDecayRad = chargino.decayVtx().Perp();
        double smearedPt = electronSmear(chargino);

        // Make chargino plots
        // fill("hist_Chargino_Pt",chargino.Pt());
        // double smearedPt = electronSmear(chargino);
        // if (smearedPt > 0.0) fill("hist_Chargino_Pt_smeared",smearedPt);
        // fill("hist_charginoEta",charginoEta);
        // fill("hist_charginoDecayRad",charginoDecayRad);

        // Tracklet Emulation
        if (passedKinEWK) {
            fill("hist_cutflow_SR_EWK",1.5);
            if (eff_track_EWK->passes(charginoEta,charginoDecayRad)) {
                EWKCharginos.push_back(std::make_pair(chargino,smearedPt));
            }
        }

        if (passedKinStrong) {
            fill("hist_cutflow_SR_Strong",1.5);
            if (eff_track_Strong->passes(charginoEta,charginoDecayRad)) {
                StrongCharginos.push_back({chargino,smearedPt});
            }
        }
    }

    if (EWKCharginos.empty() && StrongCharginos.empty()) return; // No charginos passed emulation
    if (!EWKCharginos.empty()) std::sort(EWKCharginos.begin(), EWKCharginos.end(), sortSmeared);
    if (!StrongCharginos.empty()) std::sort(StrongCharginos.begin(), StrongCharginos.end(), sortSmeared);

    // EWK tracklet selection
    bool firstEWK = true;
    for (auto smearedChargino : EWKCharginos) { // cutflows
        auto chargino = smearedChargino.first;
        double smearedPt = smearedChargino.second; 
        firstEWK = false;
        fill("hist_cutflow_SR_EWK",2.5);

        // 20GeV < track(let)pT 
        if (smearedPt < 20) break; // sorted by smeared pT - no further charginos to consider
        // cutflow step  

        // 4 pixel layers and nSCT Hits == 0
        // in efficiency map

        // nGangedFlaggedFake == 0
        // in efficiency map

        // Pixel spoilt hits == 0
        // in efficiency map

        // nPixel outliers == 0
        // in efficiency map

        // |d0significance| < 1.5
        // in efficiency map

        // |z0sinθ| < 0.5
        // in efficiency map

        // ptcone40pT<0.04 (Isolated)
        // in efficiency map

        // Isolated leading:
        if (!firstEWK) continue; // sorted by pt - first=leading - only consider the first chargino past this point
        fill("hist_cutflow_SR_EWK",3.5);

        // ∆R(jets) > 0.4
        for (auto &jet : jets) if (chargino.DeltaR(jet)<0.4) continue;
        fill("hist_cutflow_SR_EWK",4.5);

        // ∆R(electron) > 0.4
        for (auto &electron : electrons) if (chargino.DeltaR(electron)<0.4) continue;
        fill("hist_cutflow_SR_EWK",5.5);

        // ∆R(muon) > 0.4
        for (auto &muon : muons) if (chargino.DeltaR(muon)<0.4) continue;
        fill("hist_cutflow_SR_EWK",6.5);

        // 0.1 < |η| < 1.9
        double trackletEta = TMath::Abs(chargino.Eta());
        if (trackletEta < 0.1) continue;
        if (trackletEta > 1.9) continue;
        fill("hist_cutflow_SR_EWK",7.5);

        // calo-veto
        // in efficiency map

        accept("SR_EWK");
        fill("hist_Tracklet_Pt",chargino.Pt());
    }


    // Strong tracklet selection
    bool firstStrong = true;
    for (auto smearedChargino : StrongCharginos) { // cutflows
        auto chargino = smearedChargino.first;
        double smearedPt = smearedChargino.second; 
        firstStrong = false;
        fill("hist_cutflow_SR_Strong",2.5);

        // 20GeV < track(let)pT 
        if (smearedPt < 20) continue; // sorted by smeared pT - no further charginos to consider
        // cutflow step  

        // 4 pixel layers and nSCT Hits == 0
        // in efficiency map

        // nGangedFlaggedFake == 0
        // in efficiency map

        // Pixel spoilt hits == 0
        // in efficiency map

        // nPixel outliers == 0
        // in efficiency map

        // |d0significance| < 1.5
        // in efficiency map

        // |z0sinθ| < 0.5
        // in efficiency map

        // ptcone40pT<0.04 (Isolated)
        // in efficiency map

        // Isolated leading:
        if (!firstStrong) continue; // sorted by pt - first=leading - only consider the first chargino past this point
        fill("hist_cutflow_SR_Strong",3.5);

        // ∆R(jets) > 0.4
        for (auto &jet : jets) if (chargino.DeltaR(jet)<0.4) continue;
        fill("hist_cutflow_SR_Strong",4.5);

        // ∆R(electron) > 0.4
        for (auto &electron : electrons) if (chargino.DeltaR(electron)<0.4) continue;
        fill("hist_cutflow_SR_Strong",5.5);

        // ∆R(muon) > 0.4
        for (auto &muon : muons) if (chargino.DeltaR(muon)<0.4) continue;
        fill("hist_cutflow_SR_Strong",6.5);

        // 0.1 < |η| < 1.9
        double trackletEta = TMath::Abs(chargino.Eta());
        if (trackletEta < 0.1) continue;
        if (trackletEta > 1.9) continue;
        fill("hist_cutflow_SR_Strong",7.5);

        // calo-veto
        // in efficiency map

        accept("SR_Strong");
    }

    return; 
}
