#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(DisplacedTrack2020)

void DisplacedTrack2020::Init()
{
  // Define signal/control regions
  addRegions({"noselection"});
  addRegions({"preselection_jets","preselection_leptonveto","preselection_minDPhiAllJetsMet"});
  addRegions({"SR_0L_AD"});
  addRegions({"SR_0L_A_High", "SR_0L_A_Low", "SR_0L_A_All"});
  addRegions({"VR_0L_B_High", "VR_0L_B_Low", "VR_0L_B_All"});
  addRegions({"CR_0L_HighMET", "CR_0L_LowMET"});
  addRegions({"CR_0L_HighMET_500"});
  addRegions({"CR_TauHad_A", "CR_TauLep_A"});
  addRegions({"CR_TauHad_B", "CR_TauLep_B"});


  addRegions({"SR_0L_A_TauHad", "VR_0L_B_TauHad"});
  addRegions({"SR_0L_A_TauLep", "VR_0L_B_TauLep"});
  addRegions({"CR_TauHad_A_TauHad", "CR_TauLep_A_TauHad"});
  addRegions({"CR_TauHad_A_TauLep", "CR_TauLep_A_TauLep"});
  addRegions({"CR_TauHad_B_TauHad", "CR_TauLep_B_TauHad"});
  addRegions({"CR_TauHad_B_TauLep", "CR_TauLep_B_TauLep"});

  // Initialise some histograms
  addHistogram("hist_met",20,200,1000); //1D
  addHistogram("hist_minDPhiAllJetsMet",16,0,3.2); //1D
  addHistogram("hist_nJets",5,0,5); //1D
  addHistogram("hist_jet1Pt",20,200,1000); //1D

  addHistogram("hist_R_C1",100,0,1000);  //1D
  addHistogram("hist_R_N2",100,0,1000);  //1D
  addHistogram("hist_Sd0",15,0,30);    //1D


}

void DisplacedTrack2020::ProcessEvent(AnalysisEvent * event)
{

  //---------------------------------------------------------
  //
  //                   Prepare objects
  //
  //---------------------------------------------------------

  auto baseElectrons  = event->getElectrons(4.5, 2.47, ELooseBLLH|EZ05mm);
  auto baseMuons      = event->getMuons(3, 2.7, MuMedium|MuZ05mm);
  auto baseJets       = event->getJets(20., 4.5);
  auto metVec         = event->getMET();
  float met = metVec.Pt();
  float met_Phi = metVec.Phi();

  // Muon invisible MET
  auto met_muons_invis_vec = event->getMET();
  for( const auto& muon : baseMuons ){
    met_muons_invis_vec += muon;
  }

  // Reject events with bad jets
  if (countObjects(baseJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  auto radiusCalcLep = [] (const AnalysisObject& lep,const AnalysisObject&) {
    return (0.04 + 10/lep.Pt()) > 0.4 ? 0.4 : 0.04 + 10/lep.Pt();
  };
  // hard lepton overlap removal and signal lepton/jet definitions
  baseElectrons = overlapRemoval(baseElectrons, baseElectrons, 0.05);
  baseElectrons = overlapRemoval(baseElectrons, baseMuons, 0.01);
  baseJets = overlapRemoval(baseJets, baseElectrons, 0.2);
  baseElectrons = overlapRemoval(baseElectrons, baseJets, radiusCalcLep);
  baseJets = overlapRemoval(baseJets, baseElectrons, 0.2);
  baseElectrons = overlapRemoval(baseElectrons, baseJets, radiusCalcLep);
  baseJets = overlapRemoval(baseJets, baseMuons, 0.2, LessThan3Tracks);
  baseMuons = overlapRemoval(baseMuons, baseJets, radiusCalcLep);

  auto signalJets      = filterObjects( baseJets,      30,  2.8,  JVT59Jet);
  // auto signalBJets     = filterObjects( signalJets,    30,  2.5,  BTag85MV2c10);
  auto signalElectrons = filterObjects( baseElectrons, 4.5, 2.47, EMediumLH|ED0Sigma5|EZ05mm|EIsoGradientLoose);
  auto signalMuons     = filterObjects( baseMuons,     3,   2.5,  MuD0Sigma3|MuZ05mm|MuIsoPflowLoose_VarRad);


  AnalysisObjects baseLeptons   = baseElectrons+baseMuons;
  AnalysisObjects signalLeptons = signalElectrons+signalMuons;


  //---------------------------------------------------------
  //
  //                Calculate variables
  //
  //---------------------------------------------------------

  float lep1Pt    = signalLeptons.size() >= 1 ? signalLeptons[0].Pt() : -1.;
  float jet1Pt    = signalJets.size() >= 1 ? signalJets[0].Pt() : -1.;
  float dPhiJ1Met = signalJets.size() >= 1 ? fabs( signalJets[0].DeltaPhi( metVec ) ) : -1.;

  // Minimum of the delta phi between all jets with MET
  float minDPhiAllJetsMet = minDphi(metVec,signalJets, 10000, 30.);
  float minDPhiAllJetsMet_muons_invis = minDphi(met_muons_invis_vec,signalJets, 10000, 30. );

  //---------------------------------------------------------
  //
  //                Perform selection
  //
  //---------------------------------------------------------

  accept("noselection");

  // Common cuts
  bool pass_common_cuts = false;

  if ( ( signalJets.size()   <= 4 )
       && ( jet1Pt >  250. )
      ) pass_common_cuts = true;

  if ( !pass_common_cuts ){
   return;
  }

  accept("preselection_jets");

  // 0L region
  if ( baseLeptons.size() == 0 ){

    accept("preselection_leptonveto");
    fill("hist_minDPhiAllJetsMet", minDPhiAllJetsMet);

    if( minDPhiAllJetsMet > 0.4 ){

      accept("preselection_minDPhiAllJetsMet");
      fill("hist_met", met);
      fill("hist_nJets", signalJets.size());
      fill("hist_jet1Pt", jet1Pt );

      if( met > 500 ) accept("CR_0L_HighMET_500");
      if( met > 600 ) accept("CR_0L_HighMET");
      else if( 300 < met && met < 400) accept("CR_0L_LowMET");
    }
  }

  auto HSTruth = event->getHSTruth(); // for accessing BSM particles
  auto HSDecayTruth = event->getHSTruth(); // for accessing BSM particles
  auto Charginos = event->getHSTruth(20, 5, Chargino1); // for accessing specific BSM particle with Pt>20 and |eta|<5

  // Access to BSM particles
  float leadingSd0 = -1.;
  for (auto bsmParticle : HSDecayTruth) {

    if( !(bsmParticle.status() == 1)) continue; // stable particles (pion, leptons & neutralino)
    if( fabs(bsmParticle.pdgId()) == 1000022 ) continue; // skip neutralino
    if( bsmParticle.charge() == 0 ) continue; // skip neutral particles

    // std::cout << "=== BSM decay particle with ============" << std::endl;
    // std::cout << "pdgid : " << bsmParticle.pdgId() << std::endl;
    // std::cout << "mother Id : " << bsmParticle.motherID() << std::endl;
    // std::cout << "charge : " << bsmParticle.charge() << std::endl;
    // std::cout << "pt : " << bsmParticle.Pt() << std::endl;
    // if (bsmParticle.prodVtx().Mag2() != 0) {
    //   std::cout << "ProdVtx (x,y,z)=(" << bsmParticle.prodVtx().X() << "," << bsmParticle.prodVtx().Y() << "," << bsmParticle.prodVtx().Z() << ")" << std::endl;
    // }

    if( fabs(bsmParticle.motherID()) == 1000024 ) fill("hist_R_C1", bsmParticle.prodVtx().Perp() );
    if( fabs(bsmParticle.motherID()) == 1000023 ) fill("hist_R_N2", bsmParticle.prodVtx().Perp() );

    // float d0 = bsmParticle.prodVtx().Perp();
    float d0 = fabs( bsmParticle.Px() * bsmParticle.prodVtx().Y() - bsmParticle.Py() * bsmParticle.prodVtx().X() ) / bsmParticle.Pt();
    float d0sigma = 0.0680862 / bsmParticle.Pt() + 0.0123892; // Obtained by Fit to tracks in W->taunu MC
    float Sd0 = d0/d0sigma;
    float dot = bsmParticle.Px() * bsmParticle.prodVtx().X() + bsmParticle.Py() * bsmParticle.prodVtx().Y();
    float det = bsmParticle.Px() * bsmParticle.prodVtx().Y() - bsmParticle.Py() * bsmParticle.prodVtx().X();
    float angle = atan2(det, dot);
    // std::cout << "=======================================================" << std::endl;
    // std::cout << "ProdVtx R : " << bsmParticle.prodVtx().Perp() << std::endl;
    // std::cout << "Angle     : " << angle << std::endl;
    // std::cout << "d0        : " << d0 << std::endl;
    // std::cout << "d0sigma   : " << d0sigma << std::endl;
    // std::cout << "Sd0       : " << d0/d0sigma << std::endl;
    // std::cout << "=======================================================" << std::endl;

    // 0L region
    if( !( met >  600. ) ) continue;
    if( !( minDPhiAllJetsMet > 0.4 ) ) continue;
    if( !( baseLeptons.size() == 0 ) ) continue;
    if( !( 2.0 <= bsmParticle.Pt() && bsmParticle.Pt() <= 5.0 ) ) continue;
    if( !( fabs(TVector2::Phi_mpi_pi( bsmParticle.Phi() - met_Phi)) < 0.4 )  ) continue;

    if( Sd0 > leadingSd0 ){
      leadingSd0 = Sd0;
      if (leadingSd0 > 0){
        std::cout << "Found second track candidate " << std::endl;
      }
    }

  }
  if (leadingSd0 > 0){
    accept("SR_0L_AD");
    fill("hist_Sd0", leadingSd0);

    if     ( leadingSd0 > 20 ) accept("SR_0L_A_High");
    else if( leadingSd0 > 8 ) accept("SR_0L_A_Low");
    if ( leadingSd0 > 8 ) accept("SR_0L_A_All");
  }

  ntupVar("mcChannel"   , event->getMCNumber());
  ntupVar("mcWeights"   , event->getMCWeights());
  ntupVar("nLep_base"   , int(baseLeptons.size()));
  ntupVar("nLep_signal" , int(signalLeptons.size()));
  ntupVar("nMu_base"    , int(baseMuons.size()));
  ntupVar("nEle_base"   , int(baseElectrons.size()));
  ntupVar("nMu_signal"  , int(signalMuons.size()));
  ntupVar("nEle_signal" , int(signalElectrons.size()));
  ntupVar("lep1Pt", lep1Pt );
  ntupVar("met_Et", metVec.Pt() );
  ntupVar("minDPhi_met_AllJets", minDPhiAllJetsMet );
  ntupVar("met_muons_invis_Et", met_muons_invis_vec.Pt() );
  ntupVar("minDPhi_met_muons_invis_AllJets", minDPhiAllJetsMet_muons_invis );

  // Tau decay tracks
  auto TruthTau = event->getTaus( 0., 1000.);
  for (auto muon : baseMuons) {

    if( fabs(muon.motherID()) != 15 ) continue; // Only tau muons

    if( ( baseLeptons.size() == 0 )
        && ( minDPhiAllJetsMet > 0.4 ) ){

      // std::cout << "=== Tau decay particle in 0 lepton region with ============" << std::endl;
      // std::cout << "pt               : " << muon.Pt() << std::endl;
      // std::cout << "alignment (met)  : " << fabs(TVector2::Phi_mpi_pi( muon.Phi() - met_Phi )) << std::endl;

      if( fabs(TVector2::Phi_mpi_pi( muon.Phi() - met_Phi )) < 0.4 ){
        if( 2.0 <= muon.Pt() && muon.Pt() <= 5.0 ){
          if ( met > 600 ) accept("SR_0L_A_TauLep");
          else if ( 300 < met && met < 400 ) accept("VR_0L_B_TauLep");
        }
        else if ( 8.0 <= muon.Pt() && muon.Pt() <= 20.0 ){
          if ( met > 600 ) accept("CR_TauHad_A_TauLep");
          else if ( 300 < met && met < 400 ) accept("CR_TauHad_B_TauLep");
        }
      }
    }
    // // For leptonic tau region, a displaced muon should be required
    // // However, the production vertex is not stored -> No d0 requirement emulated for muons ?
    // if( ( baseMuons.size() == 1 )
    //    && ( baseLeptons.size() == 1 )
    //    && ( signalLeptons.size() == 0 )
    else if( ( signalMuons.size() == 1 )
        && ( baseLeptons.size() == 1 )
        && ( minDPhiAllJetsMet_muons_invis > 0.4 ) ){
      if( ( fabs(muon.DeltaPhi(met_muons_invis_vec)) < 0.4 )
        && ( 8.0 <= muon.Pt() && muon.Pt() <= 20.0 ) ){
        if ( met_muons_invis_vec.Pt() > 600 ) accept("CR_TauLep_A_TauLep");
        else if ( 300 < met_muons_invis_vec.Pt() && met_muons_invis_vec.Pt() < 400 ) accept("CR_TauLep_B_TauLep");

      }
    }
  }

  for (auto TauParticle : TruthTau) {

    if( ( baseLeptons.size() == 0 )
        && ( minDPhiAllJetsMet > 0.4 ) ){

      std::cout << "=== Tau decay particle in 0 lepton region with ============" << std::endl;
      std::cout << "pt               : " << TauParticle.Pt() << std::endl;
      std::cout << "charge           : " << TauParticle.charge() << std::endl;
      std::cout << "alignment (met)  : " << fabs(TauParticle.DeltaPhi(metVec)) << std::endl;

      if( fabs(TauParticle.DeltaPhi(metVec)) < 0.4 ){
        if( 2.0 <= TauParticle.Pt() && TauParticle.Pt() <= 5.0 ){
          if ( met > 600 ) accept("SR_0L_A_TauHad");
          else if ( 300 < met && met < 400 ) accept("VR_0L_B_TauHad");
        }
        else if ( 8.0 <= TauParticle.Pt() && TauParticle.Pt() <= 20.0 ){
          if ( met > 600 ) accept("CR_TauHad_A_TauHad");
          else if ( 300 < met && met < 400 ) accept("CR_TauHad_B_TauHad");
        }
      }
    }
    // // For leptonic tau region, a displaced TauParticle should be required
    // // However, the production vertex is not stored -> No d0 requirement emulated for TauParticles ?
    // if( ( baseTauParticles.size() == 1 )
    //    && ( baseLeptons.size() == 1 )
    //    && ( signalLeptons.size() == 0 )
    else if( ( signalMuons.size() == 1 )
        && ( baseLeptons.size() == 1 )
        && ( minDPhiAllJetsMet_muons_invis > 0.4 ) ){
      if( ( fabs(TauParticle.DeltaPhi(met_muons_invis_vec)) < 0.4 )
        && ( 8.0 <= TauParticle.Pt() && TauParticle.Pt() <= 20.0 ) ){
        if ( met_muons_invis_vec.Pt() > 600 ) accept("CR_TauLep_A_TauHad");
        else if ( 300 < met_muons_invis_vec.Pt() && met_muons_invis_vec.Pt() < 400 ) accept("CR_TauLep_B_TauHad");

      }
    }


  }

}
